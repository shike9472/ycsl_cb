package com.ztjo.middle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 陈彬
 * @version 2021/8/17
 * description：无锡中间程序启动类
 */
@SpringBootApplication
public class YcslMiddleApplication {
    public static void main(String[] args) {
        SpringApplication.run(YcslMiddleApplication.class, args);
    }
}
