package com.ztjo.core.mapper.biz;

import com.ztjo.data.entity.biz.SjSjsqMappingQtxtjsr;
import com.ztjo.common.frame.MyBaseMapper;

/**
 * <p>
 * 收件 - 第三方系统办件人员关联表 Mapper 接口
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjSjsqMappingQtxtjsrMapper extends MyBaseMapper<SjSjsqMappingQtxtjsr> {

}
