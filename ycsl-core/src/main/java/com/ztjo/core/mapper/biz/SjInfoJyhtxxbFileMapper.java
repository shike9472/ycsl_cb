package com.ztjo.core.mapper.biz;

import com.ztjo.data.entity.biz.SjInfoJyhtxxbFile;
import com.ztjo.common.frame.MyBaseMapper;

/**
 * <p>
 * 交易合同信息扩展附件数据表 Mapper 接口
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjInfoJyhtxxbFileMapper extends MyBaseMapper<SjInfoJyhtxxbFile> {

}
