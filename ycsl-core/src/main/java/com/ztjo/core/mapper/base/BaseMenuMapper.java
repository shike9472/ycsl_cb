package com.ztjo.core.mapper.base;

import com.ztjo.common.frame.MyBaseMapper;
import com.ztjo.data.entity.base.BaseMenu;

import java.util.List;

/**
 * <p>
 * 菜单资源表 - 保存系统菜单 Mapper 接口
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
public interface BaseMenuMapper extends MyBaseMapper<BaseMenu> {

    /**
     * 根据岗位组集合（base_group.code）查询菜单集合
     * @param groups
     * @return
     */
    List<BaseMenu> selectMenusByGroups(List<String> groups);
}
