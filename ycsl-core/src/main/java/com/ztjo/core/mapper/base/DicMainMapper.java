package com.ztjo.core.mapper.base;

import com.ztjo.common.frame.MyBaseMapper;
import com.ztjo.data.entity.base.DicMain;

/**
 * <p>
 * 字典-字典主项表（目录和字典实例） Mapper 接口
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
public interface DicMainMapper extends MyBaseMapper<DicMain> {

}
