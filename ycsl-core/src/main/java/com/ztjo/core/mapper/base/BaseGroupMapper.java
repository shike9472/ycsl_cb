package com.ztjo.core.mapper.base;

import com.ztjo.common.frame.MyBaseMapper;
import com.ztjo.data.entity.base.BaseGroup;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 角色组表 - 权限集合表征 Mapper 接口
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
public interface BaseGroupMapper extends MyBaseMapper<BaseGroup> {

    /**
     * 获取成员的角色组标识
     * @param uid
     * @return
     */
    Set<String> selectMemberGroups(Long uid);

    /**
     * 获取领导的角色组标识
     * @param uid
     * @return
     */
    Set<String> selectLeaderGroups(Long uid);

    /**
     * 获取用户挂接岗位所具备的角色组标识
     * @param uid
     * @return
     */
    Set<String> selectPositionGroupsByUserid(Long uid);

    /**
     * 按租户权限查询租户授权可以给组织机构授权的角色组信息
     * @param tenantId
     * @param groupType
     * @return
     */
    List<BaseGroup> selectGroupsByTenant(@Param("tenantId") Long tenantId, @Param("groupType") String groupType);
}
