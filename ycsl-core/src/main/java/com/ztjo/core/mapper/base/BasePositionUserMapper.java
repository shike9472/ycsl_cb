package com.ztjo.core.mapper.base;

import com.ztjo.common.frame.MyBaseMapper;
import com.ztjo.data.entity.base.BasePositionUser;

/**
 * <p>
 * 岗位 - 用户设置表 Mapper 接口
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
public interface BasePositionUserMapper extends MyBaseMapper<BasePositionUser> {

}
