package com.ztjo.core.mapper.base;

import com.ztjo.common.frame.MyBaseMapper;
import com.ztjo.data.entity.base.BaseGroupMember;

/**
 * <p>
 * 用户组 - 成员表 Mapper 接口
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
public interface BaseGroupMemberMapper extends MyBaseMapper<BaseGroupMember> {

}
