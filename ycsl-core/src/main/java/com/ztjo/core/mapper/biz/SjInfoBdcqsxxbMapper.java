package com.ztjo.core.mapper.biz;

import com.ztjo.data.entity.biz.SjInfoBdcqsxxb;
import com.ztjo.common.frame.MyBaseMapper;

/**
 * <p>
 * info表 - 不动产权属信息表 Mapper 接口
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjInfoBdcqsxxbMapper extends MyBaseMapper<SjInfoBdcqsxxb> {

}
