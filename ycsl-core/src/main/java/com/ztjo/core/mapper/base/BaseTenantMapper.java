package com.ztjo.core.mapper.base;

import com.ztjo.common.frame.MyBaseMapper;
import com.ztjo.data.entity.base.BaseTenant;

/**
 * <p>
 * 租户表 Mapper 接口
 * </p>
 *
 * @author 陈彬
 * @since 2021-10-26
 */
public interface BaseTenantMapper extends MyBaseMapper<BaseTenant> {

}
