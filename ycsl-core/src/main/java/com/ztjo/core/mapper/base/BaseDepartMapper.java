package com.ztjo.core.mapper.base;

import com.ztjo.common.frame.MyBaseMapper;
import com.ztjo.data.entity.base.BaseArea;
import com.ztjo.data.entity.base.BaseDepart;

import java.util.List;

/**
 * <p>
 * 部门表 - 用于管理系统内部门 Mapper 接口
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
public interface BaseDepartMapper extends MyBaseMapper<BaseDepart> {
    /**
     * 查询部门接入区域列表
     * @param departId
     * @return
     */
    List<BaseArea> selectDepartAreas(Long departId);
}
