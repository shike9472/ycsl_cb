package com.ztjo.core.mapper.biz;

import com.ztjo.data.entity.biz.SjInfoSwsjlrxxb;
import com.ztjo.common.frame.MyBaseMapper;

/**
 * <p>
 * info表 - 税务收件录入信息表 Mapper 接口
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjInfoSwsjlrxxbMapper extends MyBaseMapper<SjInfoSwsjlrxxb> {

}
