package com.ztjo.core.mapper.biz;

import com.ztjo.data.entity.biz.SjInfoSwhwsxxb;
import com.ztjo.common.frame.MyBaseMapper;

/**
 * <p>
 * info表 - 税务核/完税信息表 Mapper 接口
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjInfoSwhwsxxbMapper extends MyBaseMapper<SjInfoSwhwsxxb> {

}
