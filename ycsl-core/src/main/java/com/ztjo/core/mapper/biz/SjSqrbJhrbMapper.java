package com.ztjo.core.mapper.biz;

import com.ztjo.common.frame.MyBaseMapper;
import com.ztjo.data.entity.biz.SjSqrbJhrb;

/**
 * <p>
 * 申请人-监护人表 Mapper 接口
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-12
 */
public interface SjSqrbJhrbMapper extends MyBaseMapper<SjSqrbJhrb> {

}
