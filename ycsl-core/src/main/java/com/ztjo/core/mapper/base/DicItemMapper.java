package com.ztjo.core.mapper.base;

import com.ztjo.common.frame.MyBaseMapper;
import com.ztjo.data.entity.base.DicItem;

/**
 * <p>
 * 字典-字典字项表 Mapper 接口
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
public interface DicItemMapper extends MyBaseMapper<DicItem> {

}
