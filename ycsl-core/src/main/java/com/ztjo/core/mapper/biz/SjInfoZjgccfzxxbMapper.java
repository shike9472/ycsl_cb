package com.ztjo.core.mapper.biz;

import com.ztjo.data.entity.biz.SjInfoZjgccfzxxb;
import com.ztjo.common.frame.MyBaseMapper;

/**
 * <p>
 * info表 - 在建工程查封幢信息表（查封/解封均入该表） Mapper 接口
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjInfoZjgccfzxxbMapper extends MyBaseMapper<SjInfoZjgccfzxxb> {

}
