package com.ztjo.core.mapper.biz;

import com.ztjo.data.entity.biz.SjInfoCflrxxb;
import com.ztjo.common.frame.MyBaseMapper;

/**
 * <p>
 * info - 查封信息录入表 Mapper 接口
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjInfoCflrxxbMapper extends MyBaseMapper<SjInfoCflrxxb> {

}
