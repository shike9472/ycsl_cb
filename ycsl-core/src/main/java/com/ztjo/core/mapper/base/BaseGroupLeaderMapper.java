package com.ztjo.core.mapper.base;

import com.ztjo.common.frame.MyBaseMapper;
import com.ztjo.data.entity.base.BaseGroupLeader;

/**
 * <p>
 * 用户组 - 领导表 Mapper 接口
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
public interface BaseGroupLeaderMapper extends MyBaseMapper<BaseGroupLeader> {

}
