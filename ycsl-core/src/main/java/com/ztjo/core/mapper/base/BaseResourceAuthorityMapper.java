package com.ztjo.core.mapper.base;

import com.ztjo.common.frame.MyBaseMapper;
import com.ztjo.data.entity.base.BaseResourceAuthority;

/**
 * <p>
 * 菜单，uri，按钮等资源 - group授权表 Mapper 接口
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
public interface BaseResourceAuthorityMapper extends MyBaseMapper<BaseResourceAuthority> {

}
