package com.ztjo.core.mapper.biz;

import com.ztjo.data.entity.biz.SjInfoJyhtxxbBcywy;
import com.ztjo.common.frame.MyBaseMapper;

/**
 * <p>
 * 交易合同的补充与违约类数据表 Mapper 接口
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjInfoJyhtxxbBcywyMapper extends MyBaseMapper<SjInfoJyhtxxbBcywy> {

}
