package com.ztjo.core.mapper.biz;

import com.ztjo.data.entity.biz.SjSjsqMappingBdcbiz;
import com.ztjo.common.frame.MyBaseMapper;

/**
 * <p>
 * 收件 - 不动产业务关联表 Mapper 接口
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjSjsqMappingBdcbizMapper extends MyBaseMapper<SjSjsqMappingBdcbiz> {

}
