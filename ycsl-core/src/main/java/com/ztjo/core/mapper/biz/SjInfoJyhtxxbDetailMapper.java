package com.ztjo.core.mapper.biz;

import com.ztjo.data.entity.biz.SjInfoJyhtxxbDetail;
import com.ztjo.common.frame.MyBaseMapper;

/**
 * <p>
 * 交易合同信息表 - 补充细节数据表 Mapper 接口
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjInfoJyhtxxbDetailMapper extends MyBaseMapper<SjInfoJyhtxxbDetail> {

}
