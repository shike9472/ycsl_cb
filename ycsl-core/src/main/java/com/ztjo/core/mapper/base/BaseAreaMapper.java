package com.ztjo.core.mapper.base;

import com.ztjo.common.frame.MyBaseMapper;
import com.ztjo.data.entity.base.BaseArea;

/**
 * <p>
 * 区域表 - 用区域隔离时使用 Mapper 接口
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
public interface BaseAreaMapper extends MyBaseMapper<BaseArea> {

}
