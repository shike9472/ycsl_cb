package com.ztjo.core.mapper.biz;

import com.ztjo.data.entity.biz.SjBdcglb;
import com.ztjo.common.frame.MyBaseMapper;

/**
 * <p>
 * 业务 - 不动产关联表（关联info表和bdc表的记录） Mapper 接口
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjBdcglbMapper extends MyBaseMapper<SjBdcglb> {

}
