package com.ztjo.core.mapper.base;

import com.ztjo.common.frame.MyBaseMapper;
import com.ztjo.data.entity.base.BasePositionGroup;

/**
 * <p>
 * 岗位 - 角色组挂接表 Mapper 接口
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
public interface BasePositionGroupMapper extends MyBaseMapper<BasePositionGroup> {

}
