package com.ztjo.core.mapper.base;

import com.ztjo.common.frame.MyBaseMapper;
import com.ztjo.data.entity.base.BaseTenantGroup;

/**
 * <p>
 * 租户拥有的<角色处置权>授权表 Mapper 接口
 * </p>
 *
 * @author 陈彬
 * @since 2021-10-26
 */
public interface BaseTenantGroupMapper extends MyBaseMapper<BaseTenantGroup> {

}
