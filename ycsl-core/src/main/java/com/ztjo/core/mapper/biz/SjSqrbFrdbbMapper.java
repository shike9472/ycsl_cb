package com.ztjo.core.mapper.biz;

import com.ztjo.common.frame.MyBaseMapper;
import com.ztjo.data.entity.biz.SjSqrbFrdbb;

/**
 * <p>
 * 机构申请人法人代表表 Mapper 接口
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-12
 */
public interface SjSqrbFrdbbMapper extends MyBaseMapper<SjSqrbFrdbb> {

}
