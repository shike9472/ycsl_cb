package com.ztjo.core.mapper.biz;

import com.ztjo.data.entity.biz.SjSqrbWcnznb;
import com.ztjo.common.frame.MyBaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjSqrbWcnznbMapper extends MyBaseMapper<SjSqrbWcnznb> {

}
