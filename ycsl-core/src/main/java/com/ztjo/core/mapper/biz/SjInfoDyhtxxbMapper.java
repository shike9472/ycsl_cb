package com.ztjo.core.mapper.biz;

import com.ztjo.data.entity.biz.SjInfoDyhtxxb;
import com.ztjo.common.frame.MyBaseMapper;

/**
 * <p>
 * info - 抵押合同信息表 Mapper 接口
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjInfoDyhtxxbMapper extends MyBaseMapper<SjInfoDyhtxxb> {

}
