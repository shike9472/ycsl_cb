package com.ztjo.core.mapper.biz;

import com.ztjo.data.entity.biz.SjInfoBdcqsxxbTxqb;
import com.ztjo.common.frame.MyBaseMapper;

/**
 * <p>
 * 不动产权属信息的  他项权记录表 Mapper 接口
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjInfoBdcqsxxbTxqbMapper extends MyBaseMapper<SjInfoBdcqsxxbTxqb> {

}
