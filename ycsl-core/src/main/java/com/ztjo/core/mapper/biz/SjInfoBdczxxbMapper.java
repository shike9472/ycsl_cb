package com.ztjo.core.mapper.biz;

import com.ztjo.data.entity.biz.SjInfoBdczxxb;
import com.ztjo.common.frame.MyBaseMapper;

/**
 * <p>
 * info表 - 不动产幢信息表（不动产首次登记时可以使用该表收录首次登记的相关幢信息） Mapper 接口
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjInfoBdczxxbMapper extends MyBaseMapper<SjInfoBdczxxb> {

}
