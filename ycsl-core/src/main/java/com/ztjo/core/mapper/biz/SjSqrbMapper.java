package com.ztjo.core.mapper.biz;

import com.ztjo.common.frame.MyBaseMapper;
import com.ztjo.data.entity.biz.SjSqrb;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-12
 */
public interface SjSqrbMapper extends MyBaseMapper<SjSqrb> {

}
