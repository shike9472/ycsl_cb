package com.ztjo.core.mapper.biz;

import com.ztjo.data.entity.biz.SjBdczdb;
import com.ztjo.common.frame.MyBaseMapper;

/**
 * <p>
 * 不动产 - 宗地信息表 Mapper 接口
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjBdczdbMapper extends MyBaseMapper<SjBdczdb> {

}
