package com.ztjo.core.mapper.biz;

import com.ztjo.data.entity.biz.SjSjsqBdcsfb;
import com.ztjo.common.frame.MyBaseMapper;

/**
 * <p>
 * 不动产业务收费表 Mapper 接口
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjSjsqBdcsfbMapper extends MyBaseMapper<SjSjsqBdcsfb> {

}
