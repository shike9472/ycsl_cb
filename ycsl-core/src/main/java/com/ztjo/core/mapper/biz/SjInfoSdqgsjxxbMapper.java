package com.ztjo.core.mapper.biz;

import com.ztjo.data.entity.biz.SjInfoSdqgsjxxb;
import com.ztjo.common.frame.MyBaseMapper;

/**
 * <p>
 * info - 水电气广收件信息表 Mapper 接口
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjInfoSdqgsjxxbMapper extends MyBaseMapper<SjInfoSdqgsjxxb> {

}
