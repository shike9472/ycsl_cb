package com.ztjo.core.mapper.biz;

import com.ztjo.data.entity.biz.SjInfoZjgcdyzxxb;
import com.ztjo.common.frame.MyBaseMapper;

/**
 * <p>
 * info表 - 在建工程抵押幢信息表（设立/变更/转移/注销均可入该表） Mapper 接口
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjInfoZjgcdyzxxbMapper extends MyBaseMapper<SjInfoZjgcdyzxxb> {

}
