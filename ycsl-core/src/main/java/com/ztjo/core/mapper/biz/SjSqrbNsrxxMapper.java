package com.ztjo.core.mapper.biz;

import com.ztjo.data.entity.biz.SjSqrbNsrxx;
import com.ztjo.common.frame.MyBaseMapper;

/**
 * <p>
 * 纳税人表 Mapper 接口
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjSqrbNsrxxMapper extends MyBaseMapper<SjSqrbNsrxx> {

}
