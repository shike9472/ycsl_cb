package com.ztjo.core.mapper.base;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ztjo.common.frame.MyBaseMapper;
import com.ztjo.data.entity.base.BaseElement;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 组件资源表 - 保存uri或button资源的表 Mapper 接口
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
public interface BaseElementMapper extends MyBaseMapper<BaseElement> {

    /**
     * 根据岗位组集合（base_group.code）查询请求资源集合
     * @param groups
     * @return
     */
    List<BaseElement> selectElementsByGroups(List<String> groups);

    /**
     * 分页查询资源信息
     * @param page
     * @param entity
     * @return
     */
    IPage<BaseElement> selectElementsPage(Page page, @Param("condition") BaseElement entity);
}
