package com.ztjo.core.mapper.base;

import com.ztjo.common.frame.MyBaseMapper;
import com.ztjo.data.entity.base.DictValue;

/**
 * <p>
 * 字典值表 - 用于规范固定字典项 Mapper 接口
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
public interface DictValueMapper extends MyBaseMapper<DictValue> {

}
