package com.ztjo.core.mapper.biz;

import com.ztjo.data.entity.biz.SjInfoXtncljgb;
import com.ztjo.common.frame.MyBaseMapper;

/**
 * <p>
 * info表 - 系统内处理结果表 Mapper 接口
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjInfoXtncljgbMapper extends MyBaseMapper<SjInfoXtncljgb> {

}
