package com.ztjo.core.mapper.biz;

import com.ztjo.data.entity.biz.SjInfoBdccfxxb;
import com.ztjo.common.frame.MyBaseMapper;

/**
 * <p>
 * info - 不动产查封信息表（解封时查询使用） Mapper 接口
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjInfoBdccfxxbMapper extends MyBaseMapper<SjInfoBdccfxxb> {

}
