package com.ztjo.core.mapper.base;

import com.ztjo.common.frame.MyBaseMapper;
import com.ztjo.data.entity.base.BasePosition;

/**
 * <p>
 * 物理岗位表 - 用户人员岗位分类使用 Mapper 接口
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
public interface BasePositionMapper extends MyBaseMapper<BasePosition> {

}
