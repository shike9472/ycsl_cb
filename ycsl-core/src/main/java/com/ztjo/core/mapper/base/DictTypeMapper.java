package com.ztjo.core.mapper.base;

import com.ztjo.common.frame.MyBaseMapper;
import com.ztjo.data.entity.base.DictType;

/**
 * <p>
 * 字典类型 - 用于规范固定字典项类型 Mapper 接口
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
public interface DictTypeMapper extends MyBaseMapper<DictType> {

}
