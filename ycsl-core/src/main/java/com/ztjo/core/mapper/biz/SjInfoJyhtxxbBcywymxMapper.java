package com.ztjo.core.mapper.biz;

import com.ztjo.data.entity.biz.SjInfoJyhtxxbBcywymx;
import com.ztjo.common.frame.MyBaseMapper;

/**
 * <p>
 * 交易合同的补充与违约明细类数据表 Mapper 接口
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjInfoJyhtxxbBcywymxMapper extends MyBaseMapper<SjInfoJyhtxxbBcywymx> {

}
