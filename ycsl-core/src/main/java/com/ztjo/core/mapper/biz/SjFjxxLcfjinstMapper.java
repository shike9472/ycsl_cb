package com.ztjo.core.mapper.biz;

import com.ztjo.data.entity.biz.SjFjxxLcfjinst;
import com.ztjo.common.frame.MyBaseMapper;

/**
 * <p>
 * 附件信息 - 流程附件实例信息表（树形） Mapper 接口
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjFjxxLcfjinstMapper extends MyBaseMapper<SjFjxxLcfjinst> {

}
