package com.ztjo.core.mapper.base;

import com.ztjo.common.frame.MyBaseMapper;
import com.ztjo.data.entity.base.BaseUser;

/**
 * <p>
 * 系统用户表 Mapper 接口
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
public interface BaseUserMapper extends MyBaseMapper<BaseUser> {

}
