package com.ztjo.core.mapper.sys;

import com.ztjo.data.pojo.bo.MysqlTablesBo;

import java.util.List;

/**
 * @author 陈彬
 * @version 2022/1/12
 * description：
 */
public interface GeneratorMapper {
    /**
     * 查询mysql数据库表
     * @param name
     * @return
     */
    List<MysqlTablesBo> selectTables(String name);
}
