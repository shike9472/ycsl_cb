package com.ztjo.core.mapper.biz;

import com.ztjo.data.entity.biz.SjFjxxFile;
import com.ztjo.common.frame.MyBaseMapper;

/**
 * <p>
 * 系统附件表 Mapper 接口
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjFjxxFileMapper extends MyBaseMapper<SjFjxxFile> {

}
