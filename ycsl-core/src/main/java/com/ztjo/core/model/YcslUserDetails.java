package com.ztjo.core.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ztjo.data.entity.base.BaseUser;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @author 陈彬
 * @version 2021/9/18
 * description：一窗受理用户细节模型 - spring-security框架下必须实现的登录用户实体详情
 */
@Data
public class YcslUserDetails implements UserDetails {

    /**
     * 登录用户唯一标识 - 单用户登录时可以使用的到
     */
    private String token;

    /**
     * 请求资源路径
     */
    private String tarUrl;

    /**
     * 请求来源ip
     */
    private String comeIp;

    /**
     * 请求的业务区域
     */
    private String reqBizArea;

    /**
     * 本次请求可能会操作的中间程序地址前缀(http://ip:port/)
     */
    private String reqMiddleHost;

    /**
     * 登录时间
     */
    private Date loginTime;

    /**
     * 过期时间
     */
    private Date expireTime;

    /**
     * 系统用户信息
     */
    private BaseUser user;

    /**
     * 权限角色列表
     */
    private Set<String> permissions;

    /**
     * spring-security权限列表(本系统中将该变量设置为用户所具备的全部角色的标识（group code）)
     */
    private List<SimpleGrantedAuthority> authorities;

    public YcslUserDetails() {}

    public YcslUserDetails(BaseUser user, Set<String> permissions, List<SimpleGrantedAuthority> authorities) {
        this.user = user;
        this.permissions = permissions;
        this.authorities = authorities;
    }

    @JsonIgnore
    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getUsername();
    }

    /**
     * 账户是否未过期,过期无法验证
     */
    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * 指定用户是否解锁,锁定的用户无法进行身份验证
     *
     * @return
     */
    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    /**
     * 指示是否已过期的用户的凭据(密码),过期的凭据防止认证
     *
     * @return
     */
    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * 是否可用 ,禁用的用户不能身份验证
     *
     * @return
     */
    @JsonIgnore
    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }
}
