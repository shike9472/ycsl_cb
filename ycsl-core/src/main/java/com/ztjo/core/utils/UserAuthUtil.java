package com.ztjo.core.utils;

import com.github.ag.core.constants.CommonConstants;
import com.github.ag.core.context.BaseContextHandler;
import com.github.ag.core.util.jwt.IJWTInfo;
import com.github.ag.core.util.jwt.JWTHelper;
import com.github.wxiaoqi.security.common.exception.auth.NonLoginException;
import com.ztjo.common.properties.AuthCustomProperties;
import com.ztjo.core.component.AuthTokenComponent;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.SignatureException;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author 陈彬
 * @version 2021/7/22
 * description：用户权限token解析工具
 */
@Component
public class UserAuthUtil {
    @Autowired
    private AuthCustomProperties authProperties;

    @Autowired
    private AuthTokenComponent authComponent;

    @Autowired
    private JWTHelper jwtHelper;

    /**
     * 描述：传入token的解析方法, 解析的结果放入 访问线程相互隔离 的一个全局静态变量中
     * 作者：陈彬
     * 日期：2021/7/22
     * 参数：[token]
     * 返回：com.github.ag.core.util.jwt.IJWTInfo
     * 更新记录：更新人：{}，更新日期：{}
     */
    public IJWTInfo getInfoFromToken(String token) throws Exception {
        try {
            IJWTInfo infoFromToken = jwtHelper.getInfoFromToken(token, authProperties.getUserPubKey());
            // 设置BaseContextHandler
            BaseContextHandler.setUsername(infoFromToken.getUniqueName());
            BaseContextHandler.setName(infoFromToken.getName());
            BaseContextHandler.setUserID(infoFromToken.getId());
            BaseContextHandler.setTenantID(infoFromToken.getOtherInfo().get(CommonConstants.JWT_KEY_TENANT_ID));
            BaseContextHandler.setDepartID(infoFromToken.getOtherInfo().get(CommonConstants.JWT_KEY_DEPART_ID));
            // token是否有效
//            if(authComponent.invalidCheck(infoFromToken)){
//                throw new NonLoginException("token验证失败!");
//            }
            // token是否过期
            if(new DateTime(infoFromToken.getExpireTime())
                    .plusMinutes(authProperties.getToken().getTokenExpire()).isBeforeNow()){
                throw new NonLoginException("token已过期!");
            }
            return infoFromToken;
        } catch (ExpiredJwtException ex) {
            throw new NonLoginException("token已过期!");
        } catch (SignatureException ex) {
            throw new NonLoginException("token签名异常!");
        } catch (IllegalArgumentException ex) {
            throw new NonLoginException("User token is null or empty!");
        }
    }
}
