package com.ztjo.core.utils;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ztjo.data.pojo.api.TreeElement;
import com.ztjo.data.pojo.vo.model.TreeEntityVo;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author 陈彬
 * @version 2021/12/15
 * description：树形实体操作类
 */
@Slf4j
public class TreeUtils {

    /**
     * 根节点上级id（系统约定俗成）
     */
    private static final Long rootParentId = -1L;

    /**
     * 满足树形结构的变量做树形构造(枝桠向上获取)(取全部)
     * @param pool
     * @param <T>
     * @return
     */
    public static <T extends TreeElement> List<TreeEntityVo<T>> getTreeUpFromPool(List<T> pool) {
        Set<Long> allIds = pool.stream().map(T::getId).collect(Collectors.toSet());
        List<T> ts = getRootEntitysFromPool(pool, allIds);
        return getTreeUpFromPool(pool, ts);
    }

    /**
     * 满足树形结构的变量做树形构造（指定父节点id）(枝桠向上获取)(取指定节点向下-不包含指定节点本身)
     * 其实是取自目标节点开始向下的所有节点，并以树形组装
     * @param pool
     * @param pid
     * @param <T>
     * @return
     */
    public static <T extends TreeElement> List<TreeEntityVo<T>> getTreeUpFromPool(List<T> pool, Long pid) {
        List<T> ts = getTarNodeEntitysFromPool(pool, pid);
        return getTreeUpFromPool(pool, ts);
    }

    /**
     *
     * @param pool
     * @param ts
     * @param <T>
     * @return
     */
    public static <T extends TreeElement> List<TreeEntityVo<T>> getTreeUpFromPool(List<T> pool, List<T> ts) {
        List<Long> parentIds = pool.stream().map(T::getParentId).collect(Collectors.toList());
        if(CollUtil.isNotEmpty(ts)) {
            return ts.stream().map(t -> {
                TreeEntityVo<T> root = new TreeEntityVo<>();
                root.setEntity(t);
                root.setNodes(getNodeChildren(t, pool, parentIds));
                return root;
            }).collect(Collectors.toList());
        }
        return CollUtil.newArrayList();
    }

    /**
     * 满足树形结构的变量做树形构造（指定父节点id）(枝桠向上)(不含pid自身元素)
     * 其实是取自目标节点开始向下的所有节点，并以树形组装
     * @param pool
     * @param pid
     * @param <T>
     * @return
     */
    public static <T extends TreeElement> List<T> getEntityUpFromPool(List<T> pool, Long pid) {
        List<T> list = new ArrayList<>();
        List<Long> parentIds = pool.stream().map(T::getParentId).collect(Collectors.toList());
        addElements2List(list, pool, pid, parentIds);
        return list;
    }

    /**
     * 将子级元素添加至集合中
     * @param list
     * @param pool
     * @param pid
     * @param parentIds
     * @param <T>
     */
    public static <T extends TreeElement> void addElements2List(List<T> list, List<T> pool, Long pid, List<Long> parentIds) {
        if(!parentIds.contains(pid)) {
            return;
        }
        List<T> ts = getTarNodeEntitysFromPool(pool, pid);
        if(CollUtil.isNotEmpty(ts)) {
            list.addAll(ts);
            for(T t: ts) {
                addElements2List(list, pool, t.getId(), parentIds);
            }
        }
    }

    /**
     * 满足树形结构的变量做树形构造(枝桠向下)
     * @param pool
     * @param <T>
     * @return
     */
    public static <T extends TreeElement> TreeEntityVo<T> getTreeDownFromPool(T element, List<T> pool) {
        TreeEntityVo<T> node = new TreeEntityVo<>();
        node.setEntity(element);
        return getTreeDownFromPool(node, pool);
    }

    /**
     * 满足树形结构的变量做树形构造(枝桠向下)
     * @param pool
     * @param <T>
     * @return
     */
    public static <T extends TreeElement> TreeEntityVo<T> getTreeDownFromPool(TreeEntityVo<T> node, List<T> pool) {
        Long tarPid = node.getEntity().getParentId();
        if(ObjectUtil.isNull(tarPid) || rootParentId.equals(tarPid)) {
            return node;
        }
        List<TreeEntityVo<T>> nodes = CollUtil.newArrayList(node);
        for(T t: pool) {
            if(t.getId().equals(tarPid)) {
                TreeEntityVo<T> parent = new TreeEntityVo<>();
                parent.setEntity(t);
                parent.setNodes(nodes);
                return getTreeDownFromPool(parent, pool);
            }
        }
        return node;
    }

    /**
     * 满足树形结构的变量做集合构造(枝桠向下)1
     * @param pool
     * @param <T>
     * @return
     */
    public static <T extends TreeElement> List<T> getEntityDownFromPool(T element, List<T> pool) {
        TreeEntityVo<T> root = getTreeDownFromPool(element, pool);
        List<T> elements = new ArrayList<>();
        addElement2list(root, elements);
        return elements;
    }

    /**
     * 满足树形结构的变量做集合构造(枝桠向下)2
     * @param pool
     * @param <T>
     * @return
     */
    public static <T extends TreeElement> List<T> getEntityDownFromPool(TreeEntityVo<T> node, List<T> pool) {
        TreeEntityVo<T> root = getTreeDownFromPool(node, pool);
        List<T> elements = new ArrayList<>();
        addElement2list(root, elements);
        return elements;
    }

    /**
     * 获取自身向上根节点一级
     * @param element
     * @param pool
     * @param <T>
     * @return
     */
    public static <T extends TreeElement> T getFirstElement(T element, List<T> pool) {
        TreeEntityVo<T> root = getTreeDownFromPool(element, pool);
        if(ObjectUtil.isNotNull(root)) {
            return root.getEntity();
        }
        return null;
    }

    /**
     * 获取自身向上根节点向下第二级
     * @param element
     * @param pool
     * @param <T>
     * @return
     */
    public static <T extends TreeElement> T getSecondElement(T element, List<T> pool) {
        TreeEntityVo<T> root = getTreeDownFromPool(element, pool);
        if(ObjectUtil.isNotNull(root) && CollUtil.isNotEmpty(root.getNodes())) {
            return root.getNodes().get(0).getEntity();
        }
        return null;
    }

    /**
     * 获取输入池(pool)中全部根节点
     * @param pool
     * @param allIds
     * @param <T>
     * @return
     */
    public static <T extends TreeElement> List<T> getRootEntitysFromPool(List<T> pool, Set<Long> allIds) {
        List<T> ts = new ArrayList<>();
        for(T t: pool) {
            if(
                    // 未设置父级
                    ObjectUtil.isNull(t.getParentId()) ||
                    // 父级为系统默认根
                    rootParentId.equals(t.getParentId()) ||
                    // 无法在池中找到父级
                    !allIds.contains(t.getParentId())
            ) {
                ts.add(t);
            }
        }
        return ts;
    }

    /**
     * 获取实体样例中全部根节点实体
     * @param pool
     * @param <T>
     * @return
     */
    public static <T extends TreeElement> List<T> getTarNodeEntitysFromPool(List<T> pool, Long pid) {
        List<T> ts = new ArrayList<>();
        for(T t: pool) {
            if(pid.equals(t.getParentId())) {
                ts.add(t);
            }
        }
        return ts;
    }

    /**
     * 目标节点是否包含子级节点
     * @param pool
     * @param tarId
     * @param <T>
     * @return
     */
    public static <T extends TreeElement> boolean containsNodes(List<T> pool, Long tarId) {
        if(CollUtil.isNotEmpty(getTarNodeEntitysFromPool(pool, tarId))) {
            return true;
        }
        return false;
    }

    /**
     * 取节点子节点信息
     * @param element
     * @param pool
     * @param parentIds
     * @param <T>
     * @return
     */
    public static <T extends TreeElement> List<TreeEntityVo<T>> getNodeChildren(T element, List<T> pool, List<Long> parentIds) {
        List<TreeEntityVo<T>> nodes = new ArrayList<>();
        if(parentIds.contains(element.getId())) {
            for(T t: pool) {
                if(element.getId().equals(t.getParentId())) {
                    TreeEntityVo<T> node = new TreeEntityVo<>();
                    node.setEntity(t);
                    node.setNodes(getNodeChildren(t, pool, parentIds));
                    nodes.add(node);
                }
            }
        }
        return nodes;
    }

    /**
     * 集合中添加满足条件元素
     * @param node
     * @param elements
     * @param <T>
     */
    public static <T extends TreeElement> void addElement2list(TreeEntityVo<T> node, List<T> elements) {
        if(ObjectUtil.isNotNull(node)) {
            elements.add(node.getEntity());
            List<TreeEntityVo<T>> nodes = node.getNodes();
            if(CollUtil.isNotEmpty(nodes)) {
                addElement2list(nodes.get(0), elements);
            }
        }
    }
}
