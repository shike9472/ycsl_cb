package com.ztjo.core.utils;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ztjo.data.entity.model.BaseRecordModel1;
import com.ztjo.data.entity.model.BaseRecordModel2;
import com.ztjo.data.entity.model.BaseTenantModel;

import java.util.Date;

/**
 * @author 陈彬
 * @version 2021/12/16
 * description：
 */
public class ModelDefaultColumnSetUtils {

    /**
     * TenantModel模型实体新增时实体类赋值
     * @param entity
     */
    public static void setWhenTenantModelAdd(BaseTenantModel entity) {
        setWhenBaseRecordModel2Add(entity);
        if(ObjectUtil.isNull(entity.getTenantId())) {
            entity.setTenantId(SecurityUtils.curTenantId());
        }
    }

    /**
     * TenantModel模型实体更新时实体类赋值
     * @param entity
     */
    public static void setWhenTenantModelUpdate(BaseTenantModel entity) {
        setWhenBaseRecordModel2Update(entity, null);
    }

    /**
     * TenantModel模型实体新增或更新时实体类赋值
     * @param entity
     */
    public static void setWhenTenantModelSaveOrUpdate(BaseTenantModel entity) {
        if(ObjectUtil.isNull(entity.getCrtUserId())) {
            setWhenTenantModelAdd(entity);
        } else {
            setWhenTenantModelUpdate(entity);
        }
    }

    /**
     * BaseRecordModel1模型实体新增时实体类赋值
     * @param entity
     */
    public static void setWhenBaseRecordModel1Add(BaseRecordModel1 entity) {
        // 统一使用应用服务器时间
        Date date = DateUtil.date();

        // 设置创建用户变量
        entity.setCrtHost(SecurityUtils.curComeIp());
        entity.setCrtName(SecurityUtils.curName());
        entity.setCrtUser(SecurityUtils.curUid());
        entity.setCrtTime(date);

        // 设置更新用户变量
        setWhenBaseRecordModel1Update(entity, date);
    }

    /**
     * BaseRecordModel1模型实体更新时实体类赋值
     * @param entity
     * @param date
     */
    public static void setWhenBaseRecordModel1Update(BaseRecordModel1 entity, Date date) {
        if(ObjectUtil.isNull(date)) {
            date = DateUtil.date();
        }
        entity.setUpdHost(SecurityUtils.curComeIp());
        entity.setUpdName(SecurityUtils.curName());
        entity.setUpdUser(SecurityUtils.curUid());
        entity.setUpdTime(date);
    }

    /**
     * BaseRecordModel1模型实体新增或更新时实体类赋值
     * @param entity
     */
    public static void setWhenBaseRecordModel1SaveOrUpdate(BaseRecordModel1 entity) {
        if(ObjectUtil.isNull(entity.getCrtUser())) {
            setWhenBaseRecordModel1Add(entity);
        } else {
            setWhenBaseRecordModel1Update(entity, null);
        }
    }

    /**
     * BaseRecordModel2模型实体新增时实体类赋值
     * @param entity
     */
    public static void setWhenBaseRecordModel2Add(BaseRecordModel2 entity) {
        // 统一使用应用服务器时间
        Date date = DateUtil.date();

        // 设置创建用户变量
        entity.setCrtUserName(SecurityUtils.curUname());
        entity.setCrtUserId(SecurityUtils.curUid());
        entity.setCrtTime(date);

        // 设置更新用户变量
        setWhenBaseRecordModel2Update(entity, date);
    }

    /**
     * BaseRecordModel2模型实体更新时实体类赋值
     * @param entity
     * @param date
     */
    public static void setWhenBaseRecordModel2Update(BaseRecordModel2 entity, Date date) {
        if(ObjectUtil.isNull(date)) {
            date = DateUtil.date();
        }
        entity.setUpdUserName(SecurityUtils.curUname());
        entity.setUpdUserId(SecurityUtils.curUid());
        entity.setUpdTime(date);
    }

    /**
     * BaseRecordModel2模型实体新增或更新时实体类赋值
     * @param entity
     */
    public static void setWhenBaseRecordModel2SaveOrUpdate(BaseRecordModel2 entity) {
        if(ObjectUtil.isNull(entity.getCrtUserId())) {
            setWhenBaseRecordModel2Add(entity);
        } else {
            setWhenBaseRecordModel2Update(entity, null);
        }
    }
}
