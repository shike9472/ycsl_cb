package com.ztjo.core.utils;

import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import com.ztjo.data.entity.base.BaseElement;
import com.ztjo.data.entity.base.BaseMenu;
import com.ztjo.data.exception.YcslBizException;

import static com.ztjo.data.constants.ResourceTypes.*;

/**
 * @author 陈彬
 * @version 2021/12/16
 * description：不同场景下资源类型检查方法
 */
public class ResourceTypeCheckUtils {
    /**
     * 菜单类型检查
     * @param menu
     * @param isAdd
     */
    public static void checkMenuTypeRight(BaseMenu menu, boolean isAdd) {
        if(StrUtil.isBlank(menu.getType())) {
            if(isAdd) {
                throw new YcslBizException("设置菜单类型不可为空");
            }
        } else {
            if(!ArrayUtil.contains(menuTypes, menu.getType())) {
                throw new YcslBizException("请正确设置菜单类型选项！");
            }
        }
    }

    /**
     * 资源通用检查
     * @param element
     * @param isAdd
     */
    public static void checkElementTypeRight(BaseElement element, boolean isAdd) {
        if(StrUtil.isBlank(element.getType())) {
            if(isAdd) {
                throw new YcslBizException("设置资源类型不可为空");
            }
        } else {
            if(!ArrayUtil.contains(elementTypes, element.getType())) {
                throw new YcslBizException("请正确设置资源类型选项！");
            }
        }
    }

    /**
     * 菜单资源检查
     * @param element
     * @param isAdd
     */
    public static void checkMenuResourceTypeRight(BaseElement element, boolean isAdd) {
        if(StrUtil.isBlank(element.getType())) {
            if(isAdd) {
                throw new YcslBizException("设置菜单内资源类型不可为空");
            }
        } else {
            if(!ArrayUtil.contains(elementInMenuTypes, element.getType())) {
                throw new YcslBizException("请正确设置菜单内资源类型选项！");
            }
        }
    }
}
