package com.ztjo.core.utils;

import com.github.ag.core.util.d;
import com.ztjo.core.model.YcslUserDetails;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 陈彬
 * @version 2021/9/25
 * description：一窗受理线程安全的全局变量操作类
 */
public class YcslContextHandler {
    public static ThreadLocal<Map<String, Object>> threadLocal = new ThreadLocal();

    public YcslContextHandler() {
    }

    public static void set(String key, Object value) {
        Map<String, Object> map;
        if ((map = threadLocal.get()) == null) {
            map = new HashMap();
            threadLocal.set(map);
        }

        map.put(key, value);
    }

    public static Object get(String key) {
        Map<String, Object> map;
        if ((map = threadLocal.get()) == null) {
            map = new HashMap();
            threadLocal.set(map);
        }

        return map.get(key);
    }

    public static Long getDepartID() {
        return returnIdValue(get("currentDepartId"));
    }

    public static void setDepartID(Long departID) {
        set("currentDepartId", departID);
    }

    public static Long getUserID() {
        return returnIdValue(get("currentUserId"));
    }

    public static void setUserID(Long userID) {
        set("currentUserId", userID);
    }

    public static String getUsername() {
        return returnObjectValue(get("currentUserName"));
    }

    public static void setUsername(String username) {
        set("currentUserName", username);
    }

    public static String getName() {
        return d.a(get("currentUser"));
    }

    public static void setName(String name) {
        set("currentUser", name);
    }

    public static String getToken() {
        return d.a(get("currentUserToken"));
    }

    public static void setToken(String token) {
        set("currentUserToken", token);
    }

    public static YcslUserDetails getUserInfos() {
        return (YcslUserDetails) get("currentUserInfos");
    }

    public static void setUserDetails(YcslUserDetails currentUserInfos) {
        set("currentUserInfos", currentUserInfos);
    }

    private static String returnObjectValue(Object value) {
        return value == null ? null : value.toString();
    }

    private static Long returnIdValue(Object value) {
        return value==null ? null : Long.parseLong(value.toString());
    }

    public static void remove() {
        threadLocal.remove();
    }
}
