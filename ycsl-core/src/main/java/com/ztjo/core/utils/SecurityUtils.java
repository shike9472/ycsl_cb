package com.ztjo.core.utils;

import com.ztjo.core.model.YcslUserDetails;
import com.ztjo.data.exception.YcslAuthenticationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Set;

import static com.ztjo.data.constants.HttpStatus.UNAUTHORIZED;
import static com.ztjo.data.enums.SFEnums.SF_S;

/**
 * @author 陈彬
 * @version 2021/9/19
 * description：处置线程过程中，用户相关信息工具类
 */
@Slf4j
public class SecurityUtils {

    /**
     * 获取用户账户
     **/
    public static String curUname()
    {
        try
        {
            return curLoginUser().getUsername();
        }
        catch (Exception e)
        {
            log.error("取全局账户信息异常！", e);
            throw new YcslAuthenticationException("取全局账户信息异常！", UNAUTHORIZED);
        }
    }

    /**
     * 获取用户账户
     **/
    public static String curName()
    {
        try
        {
            return curLoginUser().getUser().getName();
        }
        catch (Exception e)
        {
            log.error("取全局账户信息异常！", e);
            throw new YcslAuthenticationException("取全局账户信息异常！", UNAUTHORIZED);
        }
    }

    /**
     *  取登录用户id
     * @return
     */
    public static Long curUid()
    {
        try
        {
            return curLoginUser().getUser().getId();
        }
        catch (Exception e)
        {
            log.error("取全局账户信息异常！", e);
            throw new YcslAuthenticationException("取全局账户信息异常！", UNAUTHORIZED);
        }
    }

    /**
     *  取登录用户所在部门id
     * @return
     */
    public static Long curDepartId()
    {
        try
        {
            return curLoginUser().getUser().getDepartId();
        }
        catch (Exception e)
        {
            log.error("取全局账户信息异常！", e);
            throw new YcslAuthenticationException("取全局账户信息异常！", UNAUTHORIZED);
        }
    }

    /**
     *  取登录用户的租户id
     * @return
     */
    public static Long curTenantId()
    {
        try
        {
            return curLoginUser().getUser().getTenantId();
        }
        catch (Exception e)
        {
            log.error("取全局账户信息异常！", e);
            throw new YcslAuthenticationException("取全局账户信息异常！", UNAUTHORIZED);
        }
    }

    /**
     * 获取用户
     **/
    public static YcslUserDetails curLoginUser()
    {
        try
        {
            return (YcslUserDetails) getAuthentication().getPrincipal();
        }
        catch (Exception e)
        {
            log.error("取全局账户信息异常！", e);
            throw new YcslAuthenticationException("取全局账户信息异常！", UNAUTHORIZED);
        }
    }

    /**
     * 获取Authentication
     */
    public static Authentication getAuthentication()
    {
        try
        {
            return SecurityContextHolder.getContext().getAuthentication();
        } catch (Exception e) {
            log.error("取全局账户信息异常！", e);
            throw new YcslAuthenticationException("取全局账户信息异常！", UNAUTHORIZED);
        }
    }

    /**
     * 获取用户角色集合
     * @return
     */
    public static Set<String> getGroups(){
        try
        {
            return curLoginUser().getPermissions();
        } catch (Exception e) {
            log.error("取全局账户信息异常！", e);
            throw new YcslAuthenticationException("取全局账户信息异常！", UNAUTHORIZED);
        }
    }

    /**
     * 是否为管理员
     *
     * @return 结果
     */
    public static boolean isAdmin()
    {
        try {
            return SF_S.getCode().equals(curLoginUser().getUser().getIsSuperAdmin());
        } catch (Exception e) {
            log.error("取全局账户信息异常！", e);
            throw new YcslAuthenticationException("取全局账户信息异常！", UNAUTHORIZED);
        }
    }

    /**
     * 取本次请求的目标资源地址
     * @return
     */
    public static String curTarUri()
    {
        try {
            return curLoginUser().getTarUrl();
        } catch (Exception e) {
            log.error("取全局账户信息异常！", e);
            throw new YcslAuthenticationException("取全局账户信息异常！", UNAUTHORIZED);
        }
    }

    /**
     * 本次请求的发起客户端ip
     * @return
     */
    public static String curComeIp()
    {
        try {
            return curLoginUser().getComeIp();
        } catch (Exception e) {
            log.error("取全局账户信息异常！", e);
            throw new YcslAuthenticationException("取全局账户信息异常！", UNAUTHORIZED);
        }
    }

    /**
     * 本次请求业务区域（请求线程中暂存） - 异步线程使用时，有需要入参请携带该项
     * @return
     */
    public static String reqBizArea()
    {
        try {
            return curLoginUser().getReqBizArea();
        } catch (Exception e) {
            log.error("取全局账户信息异常！", e);
            throw new YcslAuthenticationException("取全局账户信息异常！", UNAUTHORIZED);
        }
    }

    /**
     * 请求若需要使用中间程序，提供正确的中间程序地址前缀 - 异步线程使用时，有需要入参请携带该项
     * @return
     */
    public static String reqMiddleHost()
    {
        try {
            return curLoginUser().getReqMiddleHost();
        } catch (Exception e) {
            log.error("取全局账户信息异常！", e);
            throw new YcslAuthenticationException("取全局账户信息异常！", UNAUTHORIZED);
        }
    }
}
