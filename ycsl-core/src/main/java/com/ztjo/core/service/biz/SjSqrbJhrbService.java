package com.ztjo.core.service.biz;

import com.ztjo.data.entity.biz.SjSqrbJhrb;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 申请人-监护人表 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-12
 */
public interface SjSqrbJhrbService extends IService<SjSqrbJhrb> {

}
