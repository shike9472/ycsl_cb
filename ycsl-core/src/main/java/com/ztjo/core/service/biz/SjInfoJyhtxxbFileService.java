package com.ztjo.core.service.biz;

import com.ztjo.data.entity.biz.SjInfoJyhtxxbFile;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 交易合同信息扩展附件数据表 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjInfoJyhtxxbFileService extends IService<SjInfoJyhtxxbFile> {

}
