package com.ztjo.core.service.biz.impl;

import com.ztjo.data.entity.biz.SjInfoZjgccfzxxb;
import com.ztjo.core.mapper.biz.SjInfoZjgccfzxxbMapper;
import com.ztjo.core.service.biz.SjInfoZjgccfzxxbService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * info表 - 在建工程查封幢信息表（查封/解封均入该表） 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Service
public class SjInfoZjgccfzxxbServiceImpl extends ServiceImpl<SjInfoZjgccfzxxbMapper, SjInfoZjgccfzxxb> implements SjInfoZjgccfzxxbService {

}
