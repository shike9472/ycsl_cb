package com.ztjo.core.service.biz.impl;

import com.ztjo.data.entity.biz.SjSjsqBdcsfmxb;
import com.ztjo.core.mapper.biz.SjSjsqBdcsfmxbMapper;
import com.ztjo.core.service.biz.SjSjsqBdcsfmxbService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 不动产业务收费明细表 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Service
public class SjSjsqBdcsfmxbServiceImpl extends ServiceImpl<SjSjsqBdcsfmxbMapper, SjSjsqBdcsfmxb> implements SjSjsqBdcsfmxbService {

}
