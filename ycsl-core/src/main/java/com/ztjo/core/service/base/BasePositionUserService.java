package com.ztjo.core.service.base;

import com.ztjo.data.entity.base.BasePositionUser;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ztjo.data.pojo.dto.RelationModifyDto;

/**
 * <p>
 * 岗位 - 用户设置表 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
public interface BasePositionUserService extends IService<BasePositionUser> {

    /**
     * 岗位用户授权
     * @param dto
     * @return
     */
    boolean modifyPositionUsers(RelationModifyDto<BasePositionUser> dto);
}
