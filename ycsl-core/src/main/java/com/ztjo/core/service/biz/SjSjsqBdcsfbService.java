package com.ztjo.core.service.biz;

import com.ztjo.data.entity.biz.SjSjsqBdcsfb;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 不动产业务收费表 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjSjsqBdcsfbService extends IService<SjSjsqBdcsfb> {

}
