package com.ztjo.core.service.base;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ztjo.data.entity.base.BaseDepartArea;
import com.ztjo.data.pojo.dto.RelationModifyDto;

/**
 * <p>
 * 部门区域权限表 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2021-10-26
 */
public interface BaseDepartAreaService extends IService<BaseDepartArea> {

    /**
     * 更新部门区域接入权限（以部门为主更新）
     * @param dto
     * @return
     */
    boolean modifyDepartAreas(RelationModifyDto<BaseDepartArea> dto);

    /**
     * 更新区域部门关系（以区域为主更新）
     * @param dto
     * @return
     */
    boolean modifyAreaDeparts(RelationModifyDto<BaseDepartArea> dto);
}
