package com.ztjo.core.service.biz;

import com.ztjo.data.entity.biz.SjBdczdb;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 不动产 - 宗地信息表 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjBdczdbService extends IService<SjBdczdb> {

}
