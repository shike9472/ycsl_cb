package com.ztjo.core.service.biz;

import com.ztjo.data.entity.biz.SjInfoBdcdyxxb;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * info表 - 不动产抵押信息表 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjInfoBdcdyxxbService extends IService<SjInfoBdcdyxxb> {

}
