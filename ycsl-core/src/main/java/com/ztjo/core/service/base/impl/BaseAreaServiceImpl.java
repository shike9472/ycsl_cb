package com.ztjo.core.service.base.impl;

import cn.hutool.core.text.StrFormatter;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ztjo.core.mapper.base.BaseAreaMapper;
import com.ztjo.core.service.base.BaseAreaService;
import com.ztjo.core.service.base.BaseDepartAreaService;
import com.ztjo.core.service.model.BaseRecordModel2ServiceImpl;
import com.ztjo.data.entity.base.BaseArea;
import com.ztjo.data.entity.base.BaseDepartArea;
import com.ztjo.data.exception.YcslBizException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 * <p>
 * 区域表 - 用区域隔离时使用 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
@Service
public class BaseAreaServiceImpl extends BaseRecordModel2ServiceImpl<BaseAreaMapper, BaseArea> implements BaseAreaService {
    @Autowired
    private BaseDepartAreaService departAreaService;

    /**
     * 删除区域信息
     * @param id
     * @return
     */
    @Override
    public boolean removeById(Serializable id) {
        int dasCount = departAreaService.count(Wrappers.<BaseDepartArea>query().lambda().eq(BaseDepartArea::getAreaId, id));
        if(dasCount>0) {
            throw new YcslBizException(StrFormatter.format("区域在{}个部门仍存在授权！请先取消这些部门对区域的使用权限！", dasCount));
        }
        return super.removeById(id);
    }

}
