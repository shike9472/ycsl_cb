package com.ztjo.core.service.base;

import com.ztjo.data.entity.base.DictValue;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 字典值表 - 用于规范固定字典项 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
public interface DictValueService extends IService<DictValue> {

}
