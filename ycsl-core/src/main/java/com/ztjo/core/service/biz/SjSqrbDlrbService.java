package com.ztjo.core.service.biz;

import com.ztjo.data.entity.biz.SjSqrbDlrb;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 申请人-代理人表 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-12
 */
public interface SjSqrbDlrbService extends IService<SjSqrbDlrb> {

}
