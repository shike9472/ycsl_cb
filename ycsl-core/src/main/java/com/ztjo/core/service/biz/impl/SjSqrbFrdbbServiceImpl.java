package com.ztjo.core.service.biz.impl;

import com.ztjo.data.entity.biz.SjSqrbFrdbb;
import com.ztjo.core.mapper.biz.SjSqrbFrdbbMapper;
import com.ztjo.core.service.biz.SjSqrbFrdbbService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 机构申请人法人代表表 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-12
 */
@Service
public class SjSqrbFrdbbServiceImpl extends ServiceImpl<SjSqrbFrdbbMapper, SjSqrbFrdbb> implements SjSqrbFrdbbService {

}
