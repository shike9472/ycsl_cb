package com.ztjo.core.service.biz.impl;

import com.ztjo.data.entity.biz.SjSjsqBdcsfb;
import com.ztjo.core.mapper.biz.SjSjsqBdcsfbMapper;
import com.ztjo.core.service.biz.SjSjsqBdcsfbService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 不动产业务收费表 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Service
public class SjSjsqBdcsfbServiceImpl extends ServiceImpl<SjSjsqBdcsfbMapper, SjSjsqBdcsfb> implements SjSjsqBdcsfbService {

}
