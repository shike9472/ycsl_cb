package com.ztjo.core.service.biz;

import com.ztjo.data.entity.biz.SjInfoJyhtxxbBcywymx;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 交易合同的补充与违约明细类数据表 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjInfoJyhtxxbBcywymxService extends IService<SjInfoJyhtxxbBcywymx> {

}
