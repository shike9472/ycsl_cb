package com.ztjo.core.service.biz.impl;

import com.ztjo.data.entity.biz.SjInfoJyhtxxbFile;
import com.ztjo.core.mapper.biz.SjInfoJyhtxxbFileMapper;
import com.ztjo.core.service.biz.SjInfoJyhtxxbFileService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 交易合同信息扩展附件数据表 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Service
public class SjInfoJyhtxxbFileServiceImpl extends ServiceImpl<SjInfoJyhtxxbFileMapper, SjInfoJyhtxxbFile> implements SjInfoJyhtxxbFileService {

}
