package com.ztjo.core.service.biz.impl;

import com.ztjo.data.entity.biz.SjInfoBdczxxb;
import com.ztjo.core.mapper.biz.SjInfoBdczxxbMapper;
import com.ztjo.core.service.biz.SjInfoBdczxxbService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * info表 - 不动产幢信息表（不动产首次登记时可以使用该表收录首次登记的相关幢信息） 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Service
public class SjInfoBdczxxbServiceImpl extends ServiceImpl<SjInfoBdczxxbMapper, SjInfoBdczxxb> implements SjInfoBdczxxbService {

}
