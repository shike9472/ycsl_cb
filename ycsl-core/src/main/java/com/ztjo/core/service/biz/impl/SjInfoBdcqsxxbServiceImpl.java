package com.ztjo.core.service.biz.impl;

import com.ztjo.data.entity.biz.SjInfoBdcqsxxb;
import com.ztjo.core.mapper.biz.SjInfoBdcqsxxbMapper;
import com.ztjo.core.service.biz.SjInfoBdcqsxxbService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * info表 - 不动产权属信息表 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Service
public class SjInfoBdcqsxxbServiceImpl extends ServiceImpl<SjInfoBdcqsxxbMapper, SjInfoBdcqsxxb> implements SjInfoBdcqsxxbService {

}
