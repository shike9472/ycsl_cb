package com.ztjo.core.service.biz.impl;

import com.ztjo.data.entity.biz.SjSqrbJhrb;
import com.ztjo.core.mapper.biz.SjSqrbJhrbMapper;
import com.ztjo.core.service.biz.SjSqrbJhrbService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 申请人-监护人表 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-12
 */
@Service
public class SjSqrbJhrbServiceImpl extends ServiceImpl<SjSqrbJhrbMapper, SjSqrbJhrb> implements SjSqrbJhrbService {

}
