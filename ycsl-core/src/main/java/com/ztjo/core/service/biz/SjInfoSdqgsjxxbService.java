package com.ztjo.core.service.biz;

import com.ztjo.data.entity.biz.SjInfoSdqgsjxxb;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * info - 水电气广收件信息表 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjInfoSdqgsjxxbService extends IService<SjInfoSdqgsjxxb> {

}
