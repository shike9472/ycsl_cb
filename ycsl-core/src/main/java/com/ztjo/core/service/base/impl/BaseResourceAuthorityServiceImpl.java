package com.ztjo.core.service.base.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.google.common.collect.Lists;
import com.ztjo.core.mapper.base.BaseResourceAuthorityMapper;
import com.ztjo.core.service.base.BaseElementService;
import com.ztjo.core.service.base.BaseMenuService;
import com.ztjo.core.service.base.BaseResourceAuthorityService;
import com.ztjo.core.service.model.BaseRecordModel1ServiceImpl;
import com.ztjo.core.utils.ModelDefaultColumnSetUtils;
import com.ztjo.core.utils.SecurityUtils;
import com.ztjo.data.entity.base.BaseElement;
import com.ztjo.data.entity.base.BaseMenu;
import com.ztjo.data.entity.base.BaseResourceAuthority;
import com.ztjo.data.pojo.bo.MenuBo;
import com.ztjo.data.pojo.dto.RelationModifyDto;
import com.ztjo.data.pojo.vo.model.RelationModifyConditionVo;
import com.ztjo.data.pojo.vo.model.TreeEntityVo;
import com.ztjo.data.pojo.vo.settings.AllGroupResourceVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import static com.ztjo.data.constants.BatchThresholdConstants.BATCH_START_THRESHOLD;
import static com.ztjo.data.constants.BatchThresholdConstants.defaultPresetLength;
import static com.ztjo.data.constants.RedisKeyHeaders.CACHE_VALUE_ELEMENT_USER;
import static com.ztjo.data.constants.RedisKeyHeaders.CACHE_VALUE_MENU_USER;
import static com.ztjo.data.constants.ResourceTypes.*;

/**
 * <p>
 * 菜单，uri，按钮等资源 - group授权表 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
@Service
public class BaseResourceAuthorityServiceImpl extends BaseRecordModel1ServiceImpl<BaseResourceAuthorityMapper, BaseResourceAuthority> implements BaseResourceAuthorityService {

    @Autowired
    private BaseElementService elementService;
    @Autowired
    private BaseMenuService menuService;

    /**
     * 角色资源权限授权
     * @param dto
     * @return
     */
    @Override
    @CacheEvict(value = {CACHE_VALUE_MENU_USER, CACHE_VALUE_ELEMENT_USER}, allEntries = true)
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT,
            rollbackFor = Exception.class)
    public boolean modifyGroupResources(RelationModifyDto<BaseResourceAuthority> dto) {
        // 清理原有设置
        baseMapper.delete(Wrappers.<BaseResourceAuthority>query().lambda().eq(BaseResourceAuthority::getAuthorityId, dto.getTarId()));
        // 取当前需要存储的目标设置
        List<BaseResourceAuthority> groupResources = dto.getCurEntitys();
        if(CollUtil.isNotEmpty(groupResources)) {
            // 遍历目标设置并赋值特定值
            for (BaseResourceAuthority groupResource : groupResources) {
                // 关联项赋值检查
                if(ObjectUtil.isNull(groupResource.getAuthorityId())) {
                    groupResource.setAuthorityId(dto.getTarId());
                }
                // 基础项赋值
                ModelDefaultColumnSetUtils.setWhenBaseRecordModel1Add(groupResource);
            }
            // 批量新增目标设置
            return super.saveBatch(groupResources);
        }
        return true;
    }

    /**
     * 按菜单id集合移除资源角色权限关系
     * @param menuIds
     */
    @Override
    @CacheEvict(value = {CACHE_VALUE_MENU_USER}, allEntries = true)
    public void removeByMenuIds(List<Long> menuIds) {
        removeByResourceIds(menuIds, menuTypes);
    }

    /**
     * 按菜单内element id集合移除资源角色权限关系
     * @param elementIds
     */
    @Override
    @CacheEvict(value = {CACHE_VALUE_ELEMENT_USER}, allEntries = true)
    public void removeByElementIdsInMenu(List<Long> elementIds) {
        removeByResourceIds(elementIds, elementInMenuTypes);
    }

    /**
     * 按element id集合移除资源角色权限关系
     * @param elementIds
     */
    @Override
    @CacheEvict(value = {CACHE_VALUE_ELEMENT_USER}, allEntries = true)
    public void removeByElementIds(List<Long> elementIds) {
        removeByResourceIds(elementIds, elementTypes);
    }

    /**
     * 加载角色资源授权全部依赖的数据
     * @param tarId
     * @return
     */
    @Override
    public RelationModifyConditionVo<AllGroupResourceVo> loadAlready(Long tarId) {
        RelationModifyConditionVo<AllGroupResourceVo> vo = new RelationModifyConditionVo<>();
        // 设置目标id
        vo.setTarId(tarId);
        // 设置已经授权
        vo.setAlreadys(
                this.list(
                        Wrappers.<BaseResourceAuthority>lambdaQuery()
                                .eq(BaseResourceAuthority::getAuthorityId, tarId)
                ).stream()
                .map(BaseResourceAuthority::getResourceId)
                .collect(Collectors.toList())
        );
        // 将全部资源进行加载
        vo.setPool(loadAllResources());
        return vo;
    }

    /**
     * 加载全部资源信息
     * @return
     */
    @Override
    public AllGroupResourceVo loadAllResources() {
        // 定义返回值
        AllGroupResourceVo vo = new AllGroupResourceVo();
        List<BaseElement> eles = elementService.list();
        vo.setElementResources(eles);
        // 加载菜单资源
        List<BaseMenu> pool = menuService.list();
        List<TreeEntityVo<MenuBo>> menus = menuService.loadTree(pool);
        vo.setMenuResourceTree(menus);
        return vo;
    }

    /**
     * 按资源id和资源类型清理资源角色关系
     * @param resourceIds
     * @param resourceTypes
     */
    private void removeByResourceIds(List<Long> resourceIds, String[] resourceTypes) {
        if(CollUtil.isNotEmpty(resourceIds)) {
            if(resourceIds.size() < BATCH_START_THRESHOLD) {
                for(Long resourceId: resourceIds) {
                    baseMapper.delete(
                            Wrappers.<BaseResourceAuthority>lambdaQuery()
                                    .eq(BaseResourceAuthority::getResourceId, resourceId)
                                    .in(BaseResourceAuthority::getResourceType, resourceTypes)
                    );
                }
            } else {
                // 并行流集合长度截取设置
                int presetLength = defaultPresetLength(resourceIds.size());
                // 批量删除（截并行流）
                Lists.partition(resourceIds, presetLength)
                        .parallelStream()
                        .forEach(list -> baseMapper.delete(
                                Wrappers.<BaseResourceAuthority>lambdaQuery()
                                        .in(BaseResourceAuthority::getResourceId, list)
                                        .in(BaseResourceAuthority::getResourceType, resourceTypes)
                                )
                        );
            }
        }
    }
}
