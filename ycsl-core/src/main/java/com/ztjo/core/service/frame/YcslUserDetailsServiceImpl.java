package com.ztjo.core.service.frame;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ztjo.core.service.base.BaseGroupService;
import com.ztjo.core.service.base.BaseUserService;
import com.ztjo.core.model.YcslUserDetails;
import com.ztjo.data.entity.base.BaseUser;
import com.ztjo.data.exception.YcslBizException;
import com.ztjo.data.exception.YcslTokenErrorException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.ztjo.data.constants.SpecialGroupConstants.GROUP_ALL_HOLD;
import static com.ztjo.data.enums.LoginErrorEnums.TOKEN_USER_NOT_EXIST;
import static com.ztjo.data.enums.SFEnums.SF_S;

/**
 * @author 陈彬
 * @version 2021/9/18
 * description：用户细节处置服务 - spring-security框架下必须设置该实现，使用默认的必须托管相关类
 */
@Slf4j
@Service
public class YcslUserDetailsServiceImpl implements UserDetailsService {
    /**
     * 用户服务
     */
    @Autowired
    private BaseUserService baseUserService;
    /**
     * 角色组服务
     */
    @Autowired
    private BaseGroupService baseGroupService;

    /**
     * 重写根据用户名获取用户细节信息方法
     * @param username
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        BaseUser user = baseUserService.getOne(Wrappers.<BaseUser>query().lambda().eq(BaseUser::getUsername, username));
        if(ObjectUtil.isNull(user)) {
            log.error("登录用户：{} 不存在！", username);
            throw new YcslTokenErrorException(TOKEN_USER_NOT_EXIST);
        }
        if(SF_S.equals(user.getIsDeleted())) {
            log.warn("登录用户：{} 已被删除！", username);
            throw new YcslBizException("对不起，您的账号：" + username + " 已被删除");
        }
        if(SF_S.equals(user.getIsDisabled())) {
            log.warn("登录用户：{} 已被停用.", username);
            throw new YcslBizException("对不起，您的账号：" + username + " 已停用");
        }
        return createLoginUser(user, false);
    }

    /**
     * 封装根据本系统用户设置生成用户细节方法
     * @param user
     * @param loadGroup
     * @return
     */
    public UserDetails createLoginUser(BaseUser user, boolean loadGroup) {
        YcslUserDetails loginUser = new YcslUserDetails(user, null, null);
        if(loadGroup) {
            addGroups2LoginUser(loginUser);
        }
        return loginUser;
    }

    /**
     * 向登录用户细节数据中添加角色组数据
     * @param loginUser
     */
    public void addGroups2LoginUser(YcslUserDetails loginUser) {
        // 取用户的角色信息
        Set<String> groupCode = baseGroupService.gainUserGroups(loginUser.getUser().getId());
        // 设置角色信息
        groupCode = groupCode.parallelStream().map(s -> "GROUP_" + s).collect(Collectors.toSet());
        groupCode.add("ROLE_ACTIVITI_USER");
        // 管理员授all-hold角色
        if(SF_S.getCode().equals(loginUser.getUser().getIsSuperAdmin())) {
            groupCode.add(GROUP_ALL_HOLD);
        }
        loginUser.setPermissions(groupCode);
        // 角色抽象为授权 - 设置授权信息
        List<SimpleGrantedAuthority> collect = groupCode.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());
        loginUser.setAuthorities(collect);
    }
}
