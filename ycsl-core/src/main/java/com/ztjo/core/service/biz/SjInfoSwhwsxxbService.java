package com.ztjo.core.service.biz;

import com.ztjo.data.entity.biz.SjInfoSwhwsxxb;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * info表 - 税务核/完税信息表 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjInfoSwhwsxxbService extends IService<SjInfoSwhwsxxb> {

}
