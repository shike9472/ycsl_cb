package com.ztjo.core.service.base.impl;

import com.ztjo.core.mapper.base.DictTypeMapper;
import com.ztjo.core.service.base.DictTypeService;
import com.ztjo.core.service.model.BaseRecordModel2ServiceImpl;
import com.ztjo.data.entity.base.DictType;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 字典类型 - 用于规范固定字典项类型 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
@Service
public class DictTypeServiceImpl extends BaseRecordModel2ServiceImpl<DictTypeMapper, DictType> implements DictTypeService {

}
