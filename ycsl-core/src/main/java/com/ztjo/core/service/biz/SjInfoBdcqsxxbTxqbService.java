package com.ztjo.core.service.biz;

import com.ztjo.data.entity.biz.SjInfoBdcqsxxbTxqb;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 不动产权属信息的  他项权记录表 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjInfoBdcqsxxbTxqbService extends IService<SjInfoBdcqsxxbTxqb> {

}
