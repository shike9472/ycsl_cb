package com.ztjo.core.service.base;

import com.ztjo.data.entity.base.BasePosition;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 物理岗位表 - 用户人员岗位分类使用 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
public interface BasePositionService extends IService<BasePosition> {

}
