package com.ztjo.core.service.biz.impl;

import com.ztjo.data.entity.biz.SjSjsqDyclb;
import com.ztjo.core.mapper.biz.SjSjsqDyclbMapper;
import com.ztjo.core.service.biz.SjSjsqDyclbService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 收件打印材料记录表 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Service
public class SjSjsqDyclbServiceImpl extends ServiceImpl<SjSjsqDyclbMapper, SjSjsqDyclb> implements SjSjsqDyclbService {

}
