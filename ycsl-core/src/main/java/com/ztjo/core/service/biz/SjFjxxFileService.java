package com.ztjo.core.service.biz;

import com.ztjo.data.entity.biz.SjFjxxFile;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统附件表 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjFjxxFileService extends IService<SjFjxxFile> {

}
