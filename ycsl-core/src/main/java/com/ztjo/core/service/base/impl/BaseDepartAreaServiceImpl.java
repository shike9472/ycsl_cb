package com.ztjo.core.service.base.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ztjo.core.mapper.base.BaseDepartAreaMapper;
import com.ztjo.core.service.base.BaseDepartAreaService;
import com.ztjo.data.entity.base.BaseDepartArea;
import com.ztjo.data.pojo.dto.RelationModifyDto;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.ztjo.data.constants.RedisKeyHeaders.CACHE_VALUE_DEPART_AREA_GRANT;

/**
 * <p>
 * 部门区域权限表 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2021-10-26
 */
@Service
public class BaseDepartAreaServiceImpl extends ServiceImpl<BaseDepartAreaMapper, BaseDepartArea> implements BaseDepartAreaService {

    /**
     * 更新部门区域接入权限
     * @param dto
     * @return
     */
    @Override
    @CacheEvict(value = {CACHE_VALUE_DEPART_AREA_GRANT}, key = "#p0.tarId")
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT,
            rollbackFor = Exception.class)
    public boolean modifyDepartAreas(RelationModifyDto<BaseDepartArea> dto) {
        // 清理原有设置
        baseMapper.delete(Wrappers.<BaseDepartArea>query().lambda().eq(BaseDepartArea::getDepartId, dto.getTarId()));
        // 取当前需要存储的目标设置
        List<BaseDepartArea> departAreas = dto.getCurEntitys();
        if(CollUtil.isNotEmpty(departAreas)) {
            // 遍历目标设置并赋值特定值
            for (BaseDepartArea departArea : departAreas) {
                // 关联项赋值检查
                if(ObjectUtil.isNull(departArea.getDepartId())) {
                    departArea.setDepartId(dto.getTarId());
                }
            }
            // 批量新增目标设置
            return super.saveBatch(departAreas);
        }
        return true;
    }

    /**
     * 更新区域部门关系（以区域为主更新）
     * @param dto
     * @return
     */
    @Override
    @CacheEvict(value = {CACHE_VALUE_DEPART_AREA_GRANT}, allEntries = true)
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT,
            rollbackFor = Exception.class)
    public boolean modifyAreaDeparts(RelationModifyDto<BaseDepartArea> dto) {
        // 清理原有设置
        baseMapper.delete(Wrappers.<BaseDepartArea>query().lambda().eq(BaseDepartArea::getAreaId, dto.getTarId()));
        // 取当前需要存储的目标设置
        List<BaseDepartArea> departAreas = dto.getCurEntitys();
        if(CollUtil.isNotEmpty(departAreas)) {
            // 遍历目标设置并赋值特定值
            for (BaseDepartArea departArea : departAreas) {
                // 关联项赋值检查
                if(ObjectUtil.isNull(departArea.getAreaId())) {
                    departArea.setAreaId(dto.getTarId());
                }
            }
            // 批量新增目标设置
            return super.saveBatch(departAreas);
        }
        return true;
    }
}
