package com.ztjo.core.service.base;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ztjo.data.entity.base.BaseTenant;

/**
 * <p>
 * 租户表 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2021-10-26
 */
public interface BaseTenantService extends IService<BaseTenant> {

    /**
     * 判定租户是否可以被删除
     * @param id
     */
    void tenantCanDelete(Long id);
}
