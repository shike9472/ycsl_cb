package com.ztjo.core.service.biz;

import com.ztjo.data.entity.biz.SjInfoJyhtxxb;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * info - 交易合同信息表 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjInfoJyhtxxbService extends IService<SjInfoJyhtxxb> {

}
