package com.ztjo.core.service.biz.impl;

import com.ztjo.data.entity.biz.SjInfoBdcqsxxbTxqb;
import com.ztjo.core.mapper.biz.SjInfoBdcqsxxbTxqbMapper;
import com.ztjo.core.service.biz.SjInfoBdcqsxxbTxqbService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 不动产权属信息的  他项权记录表 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Service
public class SjInfoBdcqsxxbTxqbServiceImpl extends ServiceImpl<SjInfoBdcqsxxbTxqbMapper, SjInfoBdcqsxxbTxqb> implements SjInfoBdcqsxxbTxqbService {

}
