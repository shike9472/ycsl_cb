package com.ztjo.core.service.biz.impl;

import com.ztjo.data.entity.biz.SjInfoZjgcdyzxxb;
import com.ztjo.core.mapper.biz.SjInfoZjgcdyzxxbMapper;
import com.ztjo.core.service.biz.SjInfoZjgcdyzxxbService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * info表 - 在建工程抵押幢信息表（设立/变更/转移/注销均可入该表） 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Service
public class SjInfoZjgcdyzxxbServiceImpl extends ServiceImpl<SjInfoZjgcdyzxxbMapper, SjInfoZjgcdyzxxb> implements SjInfoZjgcdyzxxbService {

}
