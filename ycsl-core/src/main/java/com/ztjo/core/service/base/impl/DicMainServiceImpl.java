package com.ztjo.core.service.base.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ztjo.core.mapper.base.DicMainMapper;
import com.ztjo.core.service.base.DicMainService;
import com.ztjo.data.entity.base.DicMain;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;

import static com.ztjo.data.constants.RedisKeyHeaders.CACHE_VALUE_DIC_MAIN_ALL;

/**
 * <p>
 * 字典-字典主项表（目录和字典实例） 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
@Service
public class DicMainServiceImpl extends ServiceImpl<DicMainMapper, DicMain> implements DicMainService {

    @Override
    @Cacheable(value = CACHE_VALUE_DIC_MAIN_ALL, unless = "#result.isEmpty()")
    public List<DicMain> list() {
        return super.list();
    }

    @Override
    @CacheEvict(value = {CACHE_VALUE_DIC_MAIN_ALL}, allEntries = true)
    public boolean save(DicMain entity) {
        return super.save(entity);
    }

    @Override
    @CacheEvict(value = {CACHE_VALUE_DIC_MAIN_ALL}, allEntries = true)
    public boolean updateById(DicMain entity) {
        return super.updateById(entity);
    }

    @Override
    @CacheEvict(value = {CACHE_VALUE_DIC_MAIN_ALL}, allEntries = true)
    public boolean removeById(Serializable id) {
        return super.removeById(id);
    }

}
