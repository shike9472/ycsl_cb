package com.ztjo.core.service.biz;

import com.ztjo.data.entity.biz.SjInfoBdccfxxb;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * info - 不动产查封信息表（解封时查询使用） 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjInfoBdccfxxbService extends IService<SjInfoBdccfxxb> {

}
