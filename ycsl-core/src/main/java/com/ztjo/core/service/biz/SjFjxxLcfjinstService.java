package com.ztjo.core.service.biz;

import com.ztjo.data.entity.biz.SjFjxxLcfjinst;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 附件信息 - 流程附件实例信息表（树形） 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjFjxxLcfjinstService extends IService<SjFjxxLcfjinst> {

}
