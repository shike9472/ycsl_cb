package com.ztjo.core.service.biz.impl;

import com.ztjo.data.entity.biz.SjInfoBdccfxxb;
import com.ztjo.core.mapper.biz.SjInfoBdccfxxbMapper;
import com.ztjo.core.service.biz.SjInfoBdccfxxbService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * info - 不动产查封信息表（解封时查询使用） 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Service
public class SjInfoBdccfxxbServiceImpl extends ServiceImpl<SjInfoBdccfxxbMapper, SjInfoBdccfxxb> implements SjInfoBdccfxxbService {

}
