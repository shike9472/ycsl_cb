package com.ztjo.core.service.biz.impl;

import com.ztjo.data.entity.biz.SjBdcglb;
import com.ztjo.core.mapper.biz.SjBdcglbMapper;
import com.ztjo.core.service.biz.SjBdcglbService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 业务 - 不动产关联表（关联info表和bdc表的记录） 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Service
public class SjBdcglbServiceImpl extends ServiceImpl<SjBdcglbMapper, SjBdcglb> implements SjBdcglbService {

}
