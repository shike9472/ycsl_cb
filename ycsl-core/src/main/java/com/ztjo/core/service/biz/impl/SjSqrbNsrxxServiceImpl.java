package com.ztjo.core.service.biz.impl;

import com.ztjo.data.entity.biz.SjSqrbNsrxx;
import com.ztjo.core.mapper.biz.SjSqrbNsrxxMapper;
import com.ztjo.core.service.biz.SjSqrbNsrxxService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 纳税人表 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Service
public class SjSqrbNsrxxServiceImpl extends ServiceImpl<SjSqrbNsrxxMapper, SjSqrbNsrxx> implements SjSqrbNsrxxService {

}
