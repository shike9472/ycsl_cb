package com.ztjo.core.service.base.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ztjo.core.mapper.base.BaseDepartMapper;
import com.ztjo.core.service.base.BaseDepartAreaService;
import com.ztjo.core.service.base.BaseDepartService;
import com.ztjo.core.service.base.BasePositionService;
import com.ztjo.core.service.base.BaseUserService;
import com.ztjo.core.service.model.TenantModelServiceImpl;
import com.ztjo.data.entity.base.*;
import com.ztjo.data.exception.YcslBizException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;

import static com.ztjo.data.constants.RedisKeyHeaders.CACHE_VALUE_DEPART_AREA_GRANT;

/**
 * <p>
 * 部门表 - 用于管理系统内部门 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
@Service
public class BaseDepartServiceImpl extends TenantModelServiceImpl<BaseDepartMapper, BaseDepart> implements BaseDepartService {

    @Autowired
    private BaseDepartAreaService departAreaService;
    @Autowired
    private BaseUserService userService;
    @Autowired
    private BasePositionService positionService;

    /**
     * 获取部门接入区域列表
     * @param departId
     * @return
     */
    @Override
    @Cacheable(value = CACHE_VALUE_DEPART_AREA_GRANT, key = "#p0", unless = "#result.isEmpty()")
    public List<BaseArea> gainDepartAreas(long departId) {
        return baseMapper.selectDepartAreas(departId);
    }

    /**
     * 删除部门信息
     * @param id
     * @return
     */
    @Override
    public boolean removeById(Serializable id) {
        int dasCount = departAreaService.count(Wrappers.<BaseDepartArea>query().lambda().eq(BaseDepartArea::getDepartId, id));
        if(dasCount>0) {
            throw new YcslBizException("部门下存在区域授权！请先取消区域授权");
        }
        int dusCount = userService.count(Wrappers.<BaseUser>query().lambda().eq(BaseUser::getDepartId, id));
        if(dusCount>0) {
            throw new YcslBizException("部门下存在用户！请先清空用户信息");
        }
        int dpsCount = positionService.count(Wrappers.<BasePosition>query().lambda().eq(BasePosition::getDepartId, id));
        if(dpsCount>0) {
            throw new YcslBizException("部门下存在岗位设置！请先清空岗位信息");
        }
        return super.removeById(id);
    }
}
