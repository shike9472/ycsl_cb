package com.ztjo.core.service.biz;

import com.ztjo.data.entity.biz.SjInfoXtncljgb;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * info表 - 系统内处理结果表 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjInfoXtncljgbService extends IService<SjInfoXtncljgb> {

}
