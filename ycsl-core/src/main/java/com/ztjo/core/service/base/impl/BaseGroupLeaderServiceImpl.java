package com.ztjo.core.service.base.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ztjo.core.mapper.base.BaseGroupLeaderMapper;
import com.ztjo.core.service.base.BaseGroupLeaderService;
import com.ztjo.core.service.base.BaseGroupService;
import com.ztjo.core.service.model.BaseRecordModel1ServiceImpl;
import com.ztjo.core.utils.ModelDefaultColumnSetUtils;
import com.ztjo.core.utils.SecurityUtils;
import com.ztjo.data.entity.base.BaseGroup;
import com.ztjo.data.entity.base.BaseGroupLeader;
import com.ztjo.data.entity.base.BaseUser;
import com.ztjo.data.pojo.dto.RelationModifyDto;
import com.ztjo.data.pojo.vo.model.RelationModifyConditionVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static com.ztjo.data.constants.GroupTypes.GROUP_ORG;
import static com.ztjo.data.constants.GroupTypes.GROUP_ROLE;
import static com.ztjo.data.constants.RedisKeyHeaders.*;

/**
 * <p>
 * 用户组 - 领导表 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
@Slf4j
@Service
public class BaseGroupLeaderServiceImpl extends BaseRecordModel1ServiceImpl<BaseGroupLeaderMapper, BaseGroupLeader> implements BaseGroupLeaderService {

    @Autowired
    private BaseGroupService groupService;

    /**
     * 加载领导角色授权
     * @param user
     * @return
     */
    @Override
    public RelationModifyConditionVo<List<BaseGroup>> loadLeaderGreoupsAlready(BaseUser user) {
        RelationModifyConditionVo<List<BaseGroup>> vo = new RelationModifyConditionVo<>();
        vo.setTarId(user.getId());
        List<BaseGroup> pool;
        log.info("加载领导可配置角色信息");
        if(SecurityUtils.isAdmin()) {
            pool = groupService.list(Wrappers.<BaseGroup>lambdaQuery().eq(BaseGroup::getGroupType, GROUP_ROLE));
        } else {
            pool = groupService.loadByTenant(user.getTenantId(), GROUP_ROLE);
        }
        vo.setPool(pool);
        log.info("加载已经授权的领导角色信息");
        vo.setAlreadys(this.list(
                Wrappers.<BaseGroupLeader>lambdaQuery()
                        .eq(BaseGroupLeader::getUserId, user.getId()))
                .stream()
                .map(BaseGroupLeader::getGroupId)
                .collect(Collectors.toList())
        );
        return vo;
    }

    /**
     * 岗位用户授权方法(领导)
     * @param dto
     * @return
     */
    @Override
    @CacheEvict(value = {CACHE_VALUE_USER_GROUP_PERMISSION,
            CACHE_VALUE_MENU_USER, CACHE_VALUE_ELEMENT_USER}, allEntries = true)
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT,
            rollbackFor = Exception.class)
    public boolean modifyGroupLeaders(RelationModifyDto<BaseGroupLeader> dto) {
        // 清理原有设置
        baseMapper.delete(Wrappers.<BaseGroupLeader>query().lambda().eq(BaseGroupLeader::getUserId, dto.getTarId()));
        // 取当前需要存储的目标设置
        List<BaseGroupLeader> groupLeaders = dto.getCurEntitys();
        if(CollUtil.isNotEmpty(groupLeaders)) {
            // 遍历目标设置并赋值特定值
            for (BaseGroupLeader groupLeader : groupLeaders) {
                // 关联项赋值检查
                if(ObjectUtil.isNull(groupLeader.getUserId())) {
                    groupLeader.setUserId(dto.getTarId());
                }
                groupLeader.setDescription("角色组领导配置");
                // 基础项赋值
                ModelDefaultColumnSetUtils.setWhenBaseRecordModel1Add(groupLeader);
            }
            // 批量新增目标设置
            return super.saveBatch(groupLeaders);
        }
        return true;
    }
}
