package com.ztjo.core.service.base;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ztjo.data.entity.base.BaseTenantGroup;
import com.ztjo.data.pojo.dto.RelationModifyDto;
import com.ztjo.data.pojo.vo.model.RelationModifyConditionVo;
import com.ztjo.data.pojo.vo.settings.AllGroupVo;

/**
 * <p>
 * 租户拥有的<角色处置权>授权表 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2021-10-26
 */
public interface BaseTenantGroupService extends IService<BaseTenantGroup> {
    /**
     * 租户角色操作权限授权
     * @param dto
     * @return
     */
    boolean modifyTenantGroups(RelationModifyDto<BaseTenantGroup> dto);

    /**
     * 加载租户-角色关系
     * @param tarId
     * @return
     */
    RelationModifyConditionVo<AllGroupVo> loadAlready(Long tarId);
}
