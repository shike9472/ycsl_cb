package com.ztjo.core.service.biz.impl;

import com.ztjo.data.entity.biz.SjInfoDyhtxxb;
import com.ztjo.core.mapper.biz.SjInfoDyhtxxbMapper;
import com.ztjo.core.service.biz.SjInfoDyhtxxbService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * info - 抵押合同信息表 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Service
public class SjInfoDyhtxxbServiceImpl extends ServiceImpl<SjInfoDyhtxxbMapper, SjInfoDyhtxxb> implements SjInfoDyhtxxbService {

}
