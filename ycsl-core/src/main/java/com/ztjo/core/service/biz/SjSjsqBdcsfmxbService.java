package com.ztjo.core.service.biz;

import com.ztjo.data.entity.biz.SjSjsqBdcsfmxb;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 不动产业务收费明细表 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjSjsqBdcsfmxbService extends IService<SjSjsqBdcsfmxb> {

}
