package com.ztjo.core.service.biz.impl;

import com.ztjo.data.entity.biz.SjInfoZjgccfzxxbHxxb;
import com.ztjo.core.mapper.biz.SjInfoZjgccfzxxbHxxbMapper;
import com.ztjo.core.service.biz.SjInfoZjgccfzxxbHxxbService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Service
public class SjInfoZjgccfzxxbHxxbServiceImpl extends ServiceImpl<SjInfoZjgccfzxxbHxxbMapper, SjInfoZjgccfzxxbHxxb> implements SjInfoZjgccfzxxbHxxbService {

}
