package com.ztjo.core.service.base;

import com.ztjo.data.entity.base.BaseGroup;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ztjo.data.entity.base.BaseResourceAuthority;
import com.ztjo.data.pojo.bo.GroupBo;
import com.ztjo.data.pojo.vo.model.TreeEntityVo;
import com.ztjo.data.pojo.vo.settings.AllGroupVo;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * <p>
 * 角色组表 - 权限集合表征 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
public interface BaseGroupService extends IService<BaseGroup> {

    /**
     * 根据登录用户id获取角色组信息
     * @param uid
     * @return
     */
    Set<String> gainUserGroups(Long uid);

    /**
     * 删除检查
     * @param id
     */
    void groupCanRemove(Serializable id);

    /**
     * 加载全部角色bo集合
     * @return
     */
    List<GroupBo> loadGroupBos();

    /**
     * 加载目标角色树（全部）
     * @return
     */
    AllGroupVo loadGroupsTree();

    /**
     * 加载目标角色树
     * @param groups
     * @return
     */
    AllGroupVo loadTarGroupsTree(List<BaseGroup> groups);

    /**
     * 资源权限下发
     * @param resourceAuthorities
     * @param tarTree
     */
    void grant2Kids(List<BaseResourceAuthority> resourceAuthorities, List<TreeEntityVo<GroupBo>> tarTree);

    /**
     * 按租户权限加载租户授权可以给组织机构授权的角色组信息
     * @param tenantId
     * @param groupType
     * @return
     */
    List<BaseGroup> loadByTenant(Long tenantId, String groupType);
}
