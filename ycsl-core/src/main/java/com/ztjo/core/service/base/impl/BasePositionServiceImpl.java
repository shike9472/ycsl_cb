package com.ztjo.core.service.base.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ztjo.core.mapper.base.BasePositionMapper;
import com.ztjo.core.service.base.BasePositionGroupService;
import com.ztjo.core.service.base.BasePositionService;
import com.ztjo.core.service.base.BasePositionUserService;
import com.ztjo.core.service.model.TenantModelServiceImpl;
import com.ztjo.data.constants.ZtjoRespCodes;
import com.ztjo.data.entity.base.BasePosition;
import com.ztjo.data.entity.base.BasePositionGroup;
import com.ztjo.data.entity.base.BasePositionUser;
import com.ztjo.data.exception.YcslBizException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 * <p>
 * 物理岗位表 - 用户人员岗位分类使用 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
@Service
public class BasePositionServiceImpl extends TenantModelServiceImpl<BasePositionMapper, BasePosition> implements BasePositionService {

    @Autowired
    private BasePositionUserService positionUserService;
    @Autowired
    private BasePositionGroupService positionGroupService;

    @Override
    public boolean removeById(Serializable id) {
        int puCount = positionUserService.count(Wrappers.<BasePositionUser>lambdaQuery().eq(BasePositionUser::getPositionId, id));
        if(puCount>0) {
            throw new YcslBizException("岗位下存在用户，请先清空岗位用户设置");
        }
        int pgCount = positionGroupService.count(Wrappers.<BasePositionGroup>lambdaQuery().eq(BasePositionGroup::getPositionId, id));
        if(pgCount>0) {
            throw new YcslBizException("岗位仍挂接存在岗位角色组信息，请先清空岗位的角色组权限设置");
        }
        return super.removeById(id);
    }
}
