package com.ztjo.core.service.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.ztjo.common.properties.AuthCustomProperties;
import com.ztjo.core.component.AuthTokenComponent;
import com.ztjo.core.model.YcslUserDetails;
import com.ztjo.core.service.JwtHandleService;
import com.ztjo.core.utils.YcslContextHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



/**
 * @author 陈彬
 * @version 2021/9/18
 * description：jwt操作服务，中层jwt逻辑组织服务，将系统用户设置与jwt结合的service服务
 *  *                  另一个将以上两者结合的服务是系统登录/登出服务，但该服务属于系统交互级服务，因此设置在main中
 */
@Slf4j
@Service
public class JwtHandleServiceImpl implements JwtHandleService {

    /**
     * 用户处置组件
     */
    @Autowired
    private AuthTokenComponent authComponent;

    /**
     * 用户security级细节信息获取
     *      YcslContextHandler对单次请求线程的登录用户信息的保存，灵感来自老A - mr MG
     *      不过鉴于鉴权本身使用spring-security，此处的设定放弃使用
     * @param token
     * @return
     */
    @Override
    public YcslUserDetails getLoginUser(String token) {
        if(StrUtil.isNotBlank(token)) {
            YcslContextHandler.setToken(token);
            // 取登录用户细节信息
            YcslUserDetails userDetails = authComponent.getUserDetailsFromToken(token);
            if(ObjectUtil.isNotNull(userDetails)) {
                // 设置BaseContextHandler
                YcslContextHandler.setUserDetails(userDetails);
                YcslContextHandler.setUsername(userDetails.getUsername());
                YcslContextHandler.setName(userDetails.getUser().getName());
                YcslContextHandler.setUserID(userDetails.getUser().getId());
                YcslContextHandler.setDepartID(userDetails.getUser().getDepartId());
            }
            return userDetails;
        }
        return null;
    }

    /**
     * 验证登录用户是否正确
     * @param loginUser
     */
    @Override
    public void verifyUdetailAndAddGroups(String token, YcslUserDetails loginUser) {
        // 做最后的验证
        authComponent.verifyUserDetail(token, loginUser);
        // 刷新redis中token保存节点
        authComponent.refreshToken(loginUser.getUser(), token);
        // 添加group组数据
        authComponent.addGroups2UserDetails(loginUser);
    }
}
