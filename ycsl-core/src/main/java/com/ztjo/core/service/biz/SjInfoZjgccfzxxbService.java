package com.ztjo.core.service.biz;

import com.ztjo.data.entity.biz.SjInfoZjgccfzxxb;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * info表 - 在建工程查封幢信息表（查封/解封均入该表） 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjInfoZjgccfzxxbService extends IService<SjInfoZjgccfzxxb> {

}
