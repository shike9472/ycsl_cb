package com.ztjo.core.service.biz.impl;

import com.ztjo.data.entity.biz.SjInfoJyhtxxbBcywy;
import com.ztjo.core.mapper.biz.SjInfoJyhtxxbBcywyMapper;
import com.ztjo.core.service.biz.SjInfoJyhtxxbBcywyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 交易合同的补充与违约类数据表 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Service
public class SjInfoJyhtxxbBcywyServiceImpl extends ServiceImpl<SjInfoJyhtxxbBcywyMapper, SjInfoJyhtxxbBcywy> implements SjInfoJyhtxxbBcywyService {

}
