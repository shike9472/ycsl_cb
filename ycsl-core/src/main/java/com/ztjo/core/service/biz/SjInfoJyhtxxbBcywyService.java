package com.ztjo.core.service.biz;

import com.ztjo.data.entity.biz.SjInfoJyhtxxbBcywy;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 交易合同的补充与违约类数据表 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjInfoJyhtxxbBcywyService extends IService<SjInfoJyhtxxbBcywy> {

}
