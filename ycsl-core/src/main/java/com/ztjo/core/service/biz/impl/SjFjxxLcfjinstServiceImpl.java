package com.ztjo.core.service.biz.impl;

import com.ztjo.data.entity.biz.SjFjxxLcfjinst;
import com.ztjo.core.mapper.biz.SjFjxxLcfjinstMapper;
import com.ztjo.core.service.biz.SjFjxxLcfjinstService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 附件信息 - 流程附件实例信息表（树形） 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Service
public class SjFjxxLcfjinstServiceImpl extends ServiceImpl<SjFjxxLcfjinstMapper, SjFjxxLcfjinst> implements SjFjxxLcfjinstService {

}
