package com.ztjo.core.service.base.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ztjo.core.mapper.base.DicItemMapper;
import com.ztjo.core.service.base.DicItemService;
import com.ztjo.data.entity.base.DicItem;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import static com.ztjo.data.constants.RedisKeyHeaders.CACHE_VALUE_DIC_ITEM_DICCODE;

/**
 * <p>
 * 字典-字典字项表 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
@Service
public class DicItemServiceImpl extends ServiceImpl<DicItemMapper, DicItem> implements DicItemService {

    /**
     * 根据dic_main的dicCode加载字典项
     * @param dicCode
     * @return
     */
    @Override
    @Cacheable(value = CACHE_VALUE_DIC_ITEM_DICCODE, key = "#p0", unless = "#result.isEmpty()")
    public List<DicItem> loadByMainCode(String dicCode) {
        return this.list(Wrappers.<DicItem>lambdaQuery().eq(DicItem::getDicCode, dicCode).orderByAsc(DicItem::getItemOrder));
    }

    /**
     * 新增字典项
     * @param entity
     * @return
     */
    @Override
    @CacheEvict(value = {CACHE_VALUE_DIC_ITEM_DICCODE}, key = "#p0.dicCode")
    public boolean save(DicItem entity) {
        return super.save(entity);
    }

    /**
     * 更新字典项
     * @param entity
     * @return
     */
    @Override
    @CacheEvict(value = {CACHE_VALUE_DIC_ITEM_DICCODE}, key = "#p0.dicCode")
    public boolean updateById(DicItem entity) {
        return super.updateById(entity);
    }

    /**
     * 按id删除字典项，入参为字典项实体
     * @param entity
     * @return
     */
    @Override
    @CacheEvict(value = {CACHE_VALUE_DIC_ITEM_DICCODE}, key = "#p0.dicCode")
    public boolean removeById(DicItem entity) {
        return super.removeById(entity.getId());
    }

    /**
     * 批量删除，入参要求：
     *      传入的id集合中字典项的diccode必须需条件一的一致
     * @param dicCode
     * @param idList
     * @return
     */
    @Override
    @CacheEvict(value = {CACHE_VALUE_DIC_ITEM_DICCODE}, key = "#p0")
    public boolean removeBatch(String dicCode, List<Long> idList) {
        return this.removeByIds(idList);
    }
}
