package com.ztjo.core.service.model;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ztjo.core.utils.ModelDefaultColumnSetUtils;
import com.ztjo.data.entity.model.BaseRecordModel1;

/**
 * @author 陈彬
 * @version 2021/12/16
 * description：操作留痕模型1
 *      仅重写单笔实体新增和更新的，其它复杂场景在具体service实现类中补充
 */
public class BaseRecordModel1ServiceImpl <M extends BaseMapper<T>, T extends BaseRecordModel1> extends ServiceImpl<M, T> {
    /**
     * 单新增默认值赋值
     * @param entity
     * @return
     */
    @Override
    public boolean save(T entity) {
        ModelDefaultColumnSetUtils.setWhenBaseRecordModel1Add(entity);
        return super.save(entity);
    }

    /**
     * 单更新默认值赋值
     * @param entity
     * @return
     */
    @Override
    public boolean updateById(T entity) {
        ModelDefaultColumnSetUtils.setWhenBaseRecordModel1Update(entity, null);
        return super.updateById(entity);
    }
}
