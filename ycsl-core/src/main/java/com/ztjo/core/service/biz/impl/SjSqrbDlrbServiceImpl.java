package com.ztjo.core.service.biz.impl;

import com.ztjo.data.entity.biz.SjSqrbDlrb;
import com.ztjo.core.mapper.biz.SjSqrbDlrbMapper;
import com.ztjo.core.service.biz.SjSqrbDlrbService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 申请人-代理人表 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-12
 */
@Service
public class SjSqrbDlrbServiceImpl extends ServiceImpl<SjSqrbDlrbMapper, SjSqrbDlrb> implements SjSqrbDlrbService {

}
