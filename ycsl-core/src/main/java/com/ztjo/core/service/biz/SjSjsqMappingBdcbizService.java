package com.ztjo.core.service.biz;

import com.ztjo.data.entity.biz.SjSjsqMappingBdcbiz;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 收件 - 不动产业务关联表 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjSjsqMappingBdcbizService extends IService<SjSjsqMappingBdcbiz> {

}
