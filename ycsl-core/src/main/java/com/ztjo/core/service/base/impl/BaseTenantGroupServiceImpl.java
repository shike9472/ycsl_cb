package com.ztjo.core.service.base.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ztjo.core.mapper.base.BaseTenantGroupMapper;
import com.ztjo.core.service.base.BaseGroupService;
import com.ztjo.core.service.base.BaseTenantGroupService;
import com.ztjo.core.service.model.BaseRecordModel2ServiceImpl;
import com.ztjo.core.utils.ModelDefaultColumnSetUtils;
import com.ztjo.data.entity.base.BaseTenantGroup;
import com.ztjo.data.pojo.dto.RelationModifyDto;
import com.ztjo.data.pojo.vo.model.RelationModifyConditionVo;
import com.ztjo.data.pojo.vo.settings.AllGroupVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import static com.ztjo.core.utils.TreeUtils.getTreeUpFromPool;

/**
 * <p>
 * 租户拥有的<角色处置权>授权表 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2021-10-26
 */
@Service
public class BaseTenantGroupServiceImpl extends BaseRecordModel2ServiceImpl<BaseTenantGroupMapper, BaseTenantGroup> implements BaseTenantGroupService {

    @Autowired
    private BaseGroupService groupService;

    /**
     * 租户角色操作权限授权
     * @param dto
     * @return
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT,
            rollbackFor = Exception.class)
    public boolean modifyTenantGroups(RelationModifyDto<BaseTenantGroup> dto) {
        // 清理原有设置
        baseMapper.delete(Wrappers.<BaseTenantGroup>query().lambda().eq(BaseTenantGroup::getTenantId, dto.getTarId()));
        // 取当前需要存储的目标设置
        List<BaseTenantGroup> tenantGroups = dto.getCurEntitys();
        if(CollUtil.isNotEmpty(tenantGroups)) {
            // 遍历目标设置并赋值特定值
            for (BaseTenantGroup tenantGroup : tenantGroups) {
                // 关联项赋值检查
                if(ObjectUtil.isNull(tenantGroup.getTenantId())) {
                    tenantGroup.setTenantId(dto.getTarId());
                }
                // 基础项赋值
                ModelDefaultColumnSetUtils.setWhenBaseRecordModel2Add(tenantGroup);
            }
            // 批量新增目标设置
            return super.saveBatch(tenantGroups);
        }
        return true;
    }

    /**
     * 加载租户-角色关系
     * @param tarId
     * @return
     */
    @Override
    public RelationModifyConditionVo<AllGroupVo> loadAlready(Long tarId) {
        RelationModifyConditionVo<AllGroupVo> vo = new RelationModifyConditionVo<>();
        // 目标id
        vo.setTarId(tarId);
        // 池子
        vo.setPool(groupService.loadGroupsTree());
        // 已经配置过的
        vo.setAlreadys(this.list(
                Wrappers.<BaseTenantGroup>lambdaQuery()
                        .eq(BaseTenantGroup::getTenantId, tarId)
        ).stream().map(BaseTenantGroup::getGroupId).collect(Collectors.toList()));
        return vo;
    }
}
