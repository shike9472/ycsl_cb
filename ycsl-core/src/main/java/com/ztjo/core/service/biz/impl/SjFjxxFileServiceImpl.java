package com.ztjo.core.service.biz.impl;

import com.ztjo.data.entity.biz.SjFjxxFile;
import com.ztjo.core.mapper.biz.SjFjxxFileMapper;
import com.ztjo.core.service.biz.SjFjxxFileService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统附件表 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Service
public class SjFjxxFileServiceImpl extends ServiceImpl<SjFjxxFileMapper, SjFjxxFile> implements SjFjxxFileService {

}
