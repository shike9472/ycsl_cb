package com.ztjo.core.service.biz.impl;

import com.ztjo.data.entity.biz.SjInfoJyhtxxbDetail;
import com.ztjo.core.mapper.biz.SjInfoJyhtxxbDetailMapper;
import com.ztjo.core.service.biz.SjInfoJyhtxxbDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 交易合同信息表 - 补充细节数据表 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Service
public class SjInfoJyhtxxbDetailServiceImpl extends ServiceImpl<SjInfoJyhtxxbDetailMapper, SjInfoJyhtxxbDetail> implements SjInfoJyhtxxbDetailService {

}
