package com.ztjo.core.service;

import com.ztjo.core.model.YcslUserDetails;

import javax.servlet.http.HttpServletRequest;

/**
 * @author 陈彬
 * @version 2021/9/18
 * description：jwt操作服务，中层jwt逻辑组织服务
 */
public interface JwtHandleService {
    /**
     * 取系统运行时用户详情模型数据方法
     * @param token
     * @return
     */
    YcslUserDetails getLoginUser(String token);

    /**
     * 验证登录用户是否正确
     * @param loginUser
     */
    void verifyUdetailAndAddGroups(String token, YcslUserDetails loginUser);
}
