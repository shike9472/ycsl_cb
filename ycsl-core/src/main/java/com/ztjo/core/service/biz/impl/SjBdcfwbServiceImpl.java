package com.ztjo.core.service.biz.impl;

import com.ztjo.data.entity.biz.SjBdcfwb;
import com.ztjo.core.mapper.biz.SjBdcfwbMapper;
import com.ztjo.core.service.biz.SjBdcfwbService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Service
public class SjBdcfwbServiceImpl extends ServiceImpl<SjBdcfwbMapper, SjBdcfwb> implements SjBdcfwbService {

}
