package com.ztjo.core.service.base;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ztjo.data.entity.base.BaseGroup;
import com.ztjo.data.entity.base.BaseGroupMember;
import com.ztjo.data.entity.base.BaseUser;
import com.ztjo.data.pojo.dto.RelationModifyDto;
import com.ztjo.data.pojo.vo.model.RelationModifyConditionVo;

import java.util.List;

/**
 * <p>
 * 用户组 - 成员表 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
public interface BaseGroupMemberService extends IService<BaseGroupMember> {
    /**
     * 加载成员角色授权
     * @param user
     * @return
     */
    RelationModifyConditionVo<List<BaseGroup>> loadMumberGreoupsAlready(BaseUser user);
    /**
     * 岗位用户授权方法(成员)
     * @param dto
     * @return
     */
    boolean modifyGroupMembers(RelationModifyDto<BaseGroupMember> dto);
}
