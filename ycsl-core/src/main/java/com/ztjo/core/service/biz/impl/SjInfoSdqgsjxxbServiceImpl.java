package com.ztjo.core.service.biz.impl;

import com.ztjo.data.entity.biz.SjInfoSdqgsjxxb;
import com.ztjo.core.mapper.biz.SjInfoSdqgsjxxbMapper;
import com.ztjo.core.service.biz.SjInfoSdqgsjxxbService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * info - 水电气广收件信息表 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Service
public class SjInfoSdqgsjxxbServiceImpl extends ServiceImpl<SjInfoSdqgsjxxbMapper, SjInfoSdqgsjxxb> implements SjInfoSdqgsjxxbService {

}
