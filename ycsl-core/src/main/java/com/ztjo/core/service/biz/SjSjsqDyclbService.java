package com.ztjo.core.service.biz;

import com.ztjo.data.entity.biz.SjSjsqDyclb;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 收件打印材料记录表 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjSjsqDyclbService extends IService<SjSjsqDyclb> {

}
