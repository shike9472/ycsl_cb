package com.ztjo.core.service.base;

import com.ztjo.data.entity.base.DicMain;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ztjo.data.pojo.bo.DicMainBo;
import com.ztjo.data.pojo.vo.model.TreeEntityVo;

import java.util.List;

/**
 * <p>
 * 字典-字典主项表（目录和字典实例） 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
public interface DicMainService extends IService<DicMain> {

}
