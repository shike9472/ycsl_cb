package com.ztjo.core.service.base;

import com.ztjo.data.entity.base.DictType;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 字典类型 - 用于规范固定字典项类型 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
public interface DictTypeService extends IService<DictType> {

}
