package com.ztjo.core.service.biz;

import com.ztjo.data.entity.biz.SjInfoDyhtxxb;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * info - 抵押合同信息表 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjInfoDyhtxxbService extends IService<SjInfoDyhtxxb> {

}
