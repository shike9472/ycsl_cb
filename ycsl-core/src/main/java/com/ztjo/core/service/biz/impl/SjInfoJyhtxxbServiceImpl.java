package com.ztjo.core.service.biz.impl;

import com.ztjo.data.entity.biz.SjInfoJyhtxxb;
import com.ztjo.core.mapper.biz.SjInfoJyhtxxbMapper;
import com.ztjo.core.service.biz.SjInfoJyhtxxbService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * info - 交易合同信息表 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Service
public class SjInfoJyhtxxbServiceImpl extends ServiceImpl<SjInfoJyhtxxbMapper, SjInfoJyhtxxb> implements SjInfoJyhtxxbService {

}
