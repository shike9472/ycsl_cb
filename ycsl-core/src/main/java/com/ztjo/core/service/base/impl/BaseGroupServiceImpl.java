package com.ztjo.core.service.base.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ztjo.core.mapper.base.BaseGroupMapper;
import com.ztjo.core.service.base.*;
import com.ztjo.core.service.model.BaseRecordModel2ServiceImpl;
import com.ztjo.data.entity.base.*;
import com.ztjo.data.exception.YcslBizException;
import com.ztjo.data.pojo.bo.GroupBo;
import com.ztjo.data.pojo.dto.RelationModifyDto;
import com.ztjo.data.pojo.vo.model.TreeEntityVo;
import com.ztjo.data.pojo.vo.settings.AllGroupVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.ztjo.core.utils.TreeUtils.getTreeUpFromPool;
import static com.ztjo.data.constants.GroupTypes.GROUP_ORG;
import static com.ztjo.data.constants.GroupTypes.GROUP_ROLE;
import static com.ztjo.data.constants.RedisKeyHeaders.CACHE_VALUE_USER_GROUP_PERMISSION;

/**
 * <p>
 * 角色组表 - 权限集合表征 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
@Service
public class BaseGroupServiceImpl extends BaseRecordModel2ServiceImpl<BaseGroupMapper, BaseGroup> implements BaseGroupService {
    @Autowired
    private BaseGroupLeaderService groupLeaderService;
    @Autowired
    private BaseGroupMemberService groupMemberService;
    @Autowired
    private BasePositionGroupService positionGroupService;
    @Autowired
    private BaseTenantGroupService tenantGroupService;
    @Autowired
    private BaseResourceAuthorityService resourceAuthorityService;

    /**
     * 根据用户id获取用户各种途径取得的角色组信息
     * @param uid
     * @return
     */
    @Override
    @Cacheable(value = CACHE_VALUE_USER_GROUP_PERMISSION, key = "#p0", unless = "#result.isEmpty()")
    public Set<String> gainUserGroups(Long uid) {
        Set<String> userGroups = new HashSet<>();
        Set<String> memberGroups = baseMapper.selectMemberGroups(uid);
        if(CollUtil.isNotEmpty(memberGroups)) {
            userGroups.addAll(memberGroups);
        }
        Set<String> leaderGroups = baseMapper.selectLeaderGroups(uid);
        if(CollUtil.isNotEmpty(leaderGroups)) {
            userGroups.addAll(leaderGroups);
        }
        Set<String> positionGroups = baseMapper.selectPositionGroupsByUserid(uid);
        if(CollUtil.isNotEmpty(positionGroups)) {
            userGroups.addAll(positionGroups);
        }
        return userGroups;
    }

    /**
     * 删除检查
     * @param id
     */
    @Override
    public void groupCanRemove(Serializable id) {
        // 删除group之前会检查group各挂接关系是否存在
        int groupCount = groupLeaderService.count(Wrappers.<BaseGroupLeader>query().lambda().eq(BaseGroupLeader::getGroupId, id));
        if(groupCount>0) {
            throw new YcslBizException("角色内仍配置有用户，请先解绑用户与角色关系");
        }

        groupCount = groupMemberService.count(Wrappers.<BaseGroupMember>query().lambda().eq(BaseGroupMember::getGroupId, id));
        if(groupCount>0) {
            throw new YcslBizException("角色内仍配置有用户，请先解绑用户与角色关系");
        }

        groupCount = positionGroupService.count(Wrappers.<BasePositionGroup>query().lambda().eq(BasePositionGroup::getGroupId, id));
        if(groupCount>0) {
            throw new YcslBizException("角色内仍配置有岗位，请先解绑岗位与角色关系");
        }

        groupCount = resourceAuthorityService.count(Wrappers.<BaseResourceAuthority>query().lambda().eq(BaseResourceAuthority::getAuthorityId, id));
        if(groupCount>0) {
            throw new YcslBizException("角色仍被设置有一部分资源权限，请先解绑角色资源权限关系");
        }
    }

    /**
     * 删除group组方法（重写方法）
     * @param id
     * @return
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT,
            rollbackFor = Exception.class)
    public boolean removeById(Serializable id) {
        // 删除租户-角色关系
        tenantGroupService.remove(Wrappers.<BaseTenantGroup>lambdaQuery().eq(BaseTenantGroup::getGroupId, id));
        // 执行删除
        baseMapper.deleteById(id);
        return true;
    }

    /**
     * 加载全部角色bo集合
     * @return
     */
    @Override
    public List<GroupBo> loadGroupBos() {
        List<BaseGroup> groups = this.list();
        // 返回数据
        return groups.stream()
                .map(g -> Convert.convert(GroupBo.class, g))
                .collect(Collectors.toList());
    }

    /**
     * 加载目标角色树（全部）
     * @return
     */
    @Override
    public AllGroupVo loadGroupsTree() {
        List<BaseGroup> groups = this.list();
        // 返回数据
        return loadTarGroupsTree(groups);
    }

    /**
     * 加载目标角色树
     * @param groups
     * @return
     */
    @Override
    public AllGroupVo loadTarGroupsTree(List<BaseGroup> groups) {
        // 声明
        AllGroupVo pool = new AllGroupVo();
        // 用户角色处理
        List<GroupBo> userGroups = groups.stream()
                .filter(g -> GROUP_ROLE.equals(g.getGroupType()))
                .map(g -> Convert.convert(GroupBo.class, g))
                .collect(Collectors.toList());
        pool.setUserGroups(getTreeUpFromPool(userGroups));
        // 机构角色处理
        List<GroupBo> orgGroups = groups.stream()
                .filter(g -> GROUP_ORG.equals(g.getGroupType()))
                .map(g -> Convert.convert(GroupBo.class, g))
                .collect(Collectors.toList());
        pool.setPositionGroups(getTreeUpFromPool(orgGroups));
        // 返回数据
        return pool;
    }

    /**
     * 资源权限下发
     * @param resourceAuthorities
     * @param tarTree
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT,
            rollbackFor = Exception.class)
    public void grant2Kids(List<BaseResourceAuthority> resourceAuthorities, List<TreeEntityVo<GroupBo>> tarTree) {
        for(TreeEntityVo<GroupBo> tar: tarTree) {
            resourceAuthorities
                    .parallelStream()
                    .forEach(ra -> {
                        ra.setId(null);
                        ra.setAuthorityId(null);
                    });
            RelationModifyDto<BaseResourceAuthority> dto = new RelationModifyDto<>();
            dto.setTarId(tar.getEntity().getId());
            dto.setCurEntitys(resourceAuthorities);
            resourceAuthorityService.modifyGroupResources(dto);
            grant2Kids(resourceAuthorities, tar.getNodes());
        }
    }

    /**
     * 按租户权限加载租户授权可以给组织机构授权的角色组信息
     * @param tenantId
     * @param groupType
     * @return
     */
    @Override
    public List<BaseGroup> loadByTenant(Long tenantId, String groupType) {
        if(ObjectUtil.isNull(tenantId)) {
            throw new YcslBizException("按租户角色授权关系查询角色时租户id入参不可为空");
        }
        return baseMapper.selectGroupsByTenant(tenantId, groupType);
    }
}
