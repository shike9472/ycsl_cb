package com.ztjo.core.service.biz;

import com.ztjo.data.entity.biz.SjBdcglb;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 业务 - 不动产关联表（关联info表和bdc表的记录） 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjBdcglbService extends IService<SjBdcglb> {

}
