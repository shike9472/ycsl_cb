package com.ztjo.core.service.biz.impl;

import com.ztjo.data.entity.biz.SjBdczdb;
import com.ztjo.core.mapper.biz.SjBdczdbMapper;
import com.ztjo.core.service.biz.SjBdczdbService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 不动产 - 宗地信息表 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Service
public class SjBdczdbServiceImpl extends ServiceImpl<SjBdczdbMapper, SjBdczdb> implements SjBdczdbService {

}
