package com.ztjo.core.service.base;

import com.ztjo.data.entity.base.BaseGroup;
import com.ztjo.data.entity.base.BasePosition;
import com.ztjo.data.entity.base.BasePositionGroup;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ztjo.data.pojo.dto.RelationModifyDto;
import com.ztjo.data.pojo.vo.model.RelationModifyConditionVo;

import java.util.List;

/**
 * <p>
 * 岗位 - 角色组挂接表 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
public interface BasePositionGroupService extends IService<BasePositionGroup> {

    /**
     * 加载岗位角色设置
     * @param position
     * @return
     */
    RelationModifyConditionVo<List<BaseGroup>> loadPositionGroupsAlready(BasePosition position);

    /**
     * 岗位角色权限授权
     * @param dto
     * @return
     */
    boolean modifyPositionGroups(RelationModifyDto<BasePositionGroup> dto);
}
