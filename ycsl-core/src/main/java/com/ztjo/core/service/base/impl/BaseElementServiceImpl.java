package com.ztjo.core.service.base.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ztjo.core.mapper.base.BaseElementMapper;
import com.ztjo.core.service.base.BaseElementService;
import com.ztjo.core.service.model.BaseRecordModel1ServiceImpl;
import com.ztjo.data.entity.base.BaseElement;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static com.ztjo.data.constants.RedisKeyHeaders.CACHE_VALUE_ELEMENT_ALL;

/**
 * <p>
 * 组件资源表 - 保存uri或button资源的表 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
@Service
public class BaseElementServiceImpl extends BaseRecordModel1ServiceImpl<BaseElementMapper, BaseElement> implements BaseElementService {

    /**
     * 根据角色集合加载请求与按钮资源
     * @param groups
     * @return
     */
    @Override
    public List<BaseElement> getElementsByGroups(Set<String> groups) {
        return baseMapper.selectElementsByGroups(new ArrayList<>(groups));
    }

    /**
     * 分页查询资源信息
     * @param page
     * @param entity
     * @return
     */
    @Override
    public IPage<BaseElement> getElementsPage(Page page, BaseElement entity) {
        return baseMapper.selectElementsPage(page, entity);
    }

    @Override
    @Cacheable(value = CACHE_VALUE_ELEMENT_ALL, unless = "#result.isEmpty()")
    public List<BaseElement> list() {
        return super.list();
    }

    @Override
    @CacheEvict(value = {CACHE_VALUE_ELEMENT_ALL}, allEntries = true)
    public boolean save(BaseElement entity) {
        return super.save(entity);
    }

    @Override
    @CacheEvict(value = {CACHE_VALUE_ELEMENT_ALL}, allEntries = true)
    public boolean removeById(Serializable id) {
        return super.removeById(id);
    }

    @Override
    @CacheEvict(value = {CACHE_VALUE_ELEMENT_ALL}, allEntries = true)
    public boolean updateById(BaseElement entity) {
        return super.updateById(entity);
    }

    /**
     * 根据menuid删除资源
     * @param menuId
     */
    @Override
    @CacheEvict(value = {CACHE_VALUE_ELEMENT_ALL}, allEntries = true)
    public void removeByMenuId(Long menuId) {
        this.remove(Wrappers.<BaseElement>lambdaQuery().eq(BaseElement::getMenuId, menuId));
    }

    /**
     * 根据id集合删除资源
     * @param ids
     */
    @Override
    @CacheEvict(value = {CACHE_VALUE_ELEMENT_ALL}, allEntries = true)
    public void removeByIds(List<Long> ids) {
        super.removeByIds(ids);
    }
}
