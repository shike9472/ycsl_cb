package com.ztjo.core.service.base.impl;

import com.ztjo.core.mapper.base.DictValueMapper;
import com.ztjo.core.service.base.DictValueService;
import com.ztjo.core.service.model.BaseRecordModel2ServiceImpl;
import com.ztjo.data.entity.base.DictValue;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 字典值表 - 用于规范固定字典项 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
@Service
public class DictValueServiceImpl extends BaseRecordModel2ServiceImpl<DictValueMapper, DictValue> implements DictValueService {

}
