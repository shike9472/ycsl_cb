package com.ztjo.core.service.base;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ztjo.data.entity.base.BaseMenu;
import com.ztjo.data.pojo.bo.MenuBo;
import com.ztjo.data.pojo.vo.model.TreeEntityVo;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 菜单资源表 - 保存系统菜单 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
public interface BaseMenuService extends IService<BaseMenu> {

    /**
     * 根据角色集合加载菜单资源
     * @param groups
     * @return
     */
    List<BaseMenu> getMenusByGroups(Set<String> groups);

    /**
     * 菜单项（作为资源进行加载）(树形)
     * @param pool
     * @return
     */
    List<TreeEntityVo<MenuBo>> loadTree(List<BaseMenu> pool);

}
