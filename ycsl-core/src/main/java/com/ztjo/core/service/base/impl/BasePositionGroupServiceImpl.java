package com.ztjo.core.service.base.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ztjo.core.mapper.base.BasePositionGroupMapper;
import com.ztjo.core.service.base.BaseGroupService;
import com.ztjo.core.service.base.BasePositionGroupService;
import com.ztjo.core.utils.SecurityUtils;
import com.ztjo.data.entity.base.BaseGroup;
import com.ztjo.data.entity.base.BasePosition;
import com.ztjo.data.entity.base.BasePositionGroup;
import com.ztjo.data.pojo.dto.RelationModifyDto;
import com.ztjo.data.pojo.vo.model.RelationModifyConditionVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import static com.ztjo.data.constants.GroupTypes.GROUP_ORG;
import static com.ztjo.data.constants.RedisKeyHeaders.*;

/**
 * <p>
 * 岗位 - 角色组挂接表 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
@Service
public class BasePositionGroupServiceImpl extends ServiceImpl<BasePositionGroupMapper, BasePositionGroup> implements BasePositionGroupService {

    @Autowired
    private BaseGroupService groupService;

    /**
     * 加载岗位角色设置
     * @param position
     * @return
     */
    @Override
    public RelationModifyConditionVo<List<BaseGroup>> loadPositionGroupsAlready(BasePosition position) {
        RelationModifyConditionVo<List<BaseGroup>> vo = new RelationModifyConditionVo<>();
        vo.setTarId(position.getId());
        List<BaseGroup> pool;
        if(SecurityUtils.isAdmin()) {
            pool = groupService.list(Wrappers.<BaseGroup>lambdaQuery().eq(BaseGroup::getGroupType, GROUP_ORG));
        } else {
            pool = groupService.loadByTenant(position.getTenantId(), GROUP_ORG);
        }
        vo.setPool(pool);
        vo.setAlreadys(this.list(
                Wrappers.<BasePositionGroup>lambdaQuery()
                        .eq(BasePositionGroup::getPositionId, position.getId()))
                .stream()
                .map(BasePositionGroup::getGroupId)
                .collect(Collectors.toList())
        );
        return vo;
    }

    /**
     * 岗位角色授权
     * @param dto
     * @return
     */
    @Override
    @CacheEvict(value = {CACHE_VALUE_USER_GROUP_PERMISSION,
            CACHE_VALUE_MENU_USER, CACHE_VALUE_ELEMENT_USER}, allEntries = true)
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT,
            rollbackFor = Exception.class)
    public boolean modifyPositionGroups(RelationModifyDto<BasePositionGroup> dto) {
        // 清理原有设置
        baseMapper.delete(Wrappers.<BasePositionGroup>query().lambda().eq(BasePositionGroup::getPositionId, dto.getTarId()));
        // 取当前需要存储的目标设置
        List<BasePositionGroup> positionGroups = dto.getCurEntitys();
        if(CollUtil.isNotEmpty(positionGroups)) {
            // 遍历目标设置并赋值特定值
            for (BasePositionGroup positionGroup : positionGroups) {
                // 关联项赋值检查
                if(ObjectUtil.isNull(positionGroup.getPositionId())) {
                    positionGroup.setPositionId(dto.getTarId());
                }
            }
            // 批量新增目标设置
            return super.saveBatch(positionGroups);
        }
        return true;
    }
}
