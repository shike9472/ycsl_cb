package com.ztjo.core.service.biz;

import com.ztjo.data.entity.biz.SjSqrbFrdbb;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 机构申请人法人代表表 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-12
 */
public interface SjSqrbFrdbbService extends IService<SjSqrbFrdbb> {

}
