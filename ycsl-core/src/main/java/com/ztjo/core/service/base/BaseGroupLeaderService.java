package com.ztjo.core.service.base;

import com.ztjo.data.entity.base.BaseGroup;
import com.ztjo.data.entity.base.BaseGroupLeader;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ztjo.data.entity.base.BaseUser;
import com.ztjo.data.pojo.dto.RelationModifyDto;
import com.ztjo.data.pojo.vo.model.RelationModifyConditionVo;

import java.util.List;

/**
 * <p>
 * 用户组 - 领导表 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
public interface BaseGroupLeaderService extends IService<BaseGroupLeader> {

    /**
     * 加载领导角色授权
     * @param user
     * @return
     */
    RelationModifyConditionVo<List<BaseGroup>> loadLeaderGreoupsAlready(BaseUser user);

    /**
     * 岗位用户授权方法(领导)
     * @param dto
     * @return
     */
    boolean modifyGroupLeaders(RelationModifyDto<BaseGroupLeader> dto);
}
