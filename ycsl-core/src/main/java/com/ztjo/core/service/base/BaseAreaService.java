package com.ztjo.core.service.base;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ztjo.data.entity.base.BaseArea;

/**
 * <p>
 * 区域表 - 用区域隔离时使用 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
public interface BaseAreaService extends IService<BaseArea> {

}
