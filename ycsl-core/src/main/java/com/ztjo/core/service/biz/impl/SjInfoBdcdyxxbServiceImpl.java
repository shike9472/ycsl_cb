package com.ztjo.core.service.biz.impl;

import com.ztjo.data.entity.biz.SjInfoBdcdyxxb;
import com.ztjo.core.mapper.biz.SjInfoBdcdyxxbMapper;
import com.ztjo.core.service.biz.SjInfoBdcdyxxbService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * info表 - 不动产抵押信息表 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Service
public class SjInfoBdcdyxxbServiceImpl extends ServiceImpl<SjInfoBdcdyxxbMapper, SjInfoBdcdyxxb> implements SjInfoBdcdyxxbService {

}
