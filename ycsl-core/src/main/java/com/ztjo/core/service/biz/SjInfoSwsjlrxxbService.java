package com.ztjo.core.service.biz;

import com.ztjo.data.entity.biz.SjInfoSwsjlrxxb;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * info表 - 税务收件录入信息表 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjInfoSwsjlrxxbService extends IService<SjInfoSwsjlrxxb> {

}
