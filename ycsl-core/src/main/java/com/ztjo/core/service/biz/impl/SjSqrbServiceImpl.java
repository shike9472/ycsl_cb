package com.ztjo.core.service.biz.impl;

import com.ztjo.data.entity.biz.SjSqrb;
import com.ztjo.core.mapper.biz.SjSqrbMapper;
import com.ztjo.core.service.biz.SjSqrbService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-12
 */
@Service
public class SjSqrbServiceImpl extends ServiceImpl<SjSqrbMapper, SjSqrb> implements SjSqrbService {

}
