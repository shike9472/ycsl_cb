package com.ztjo.core.service.base.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ztjo.core.mapper.base.BaseMenuMapper;
import com.ztjo.core.service.base.BaseMenuService;
import com.ztjo.core.service.model.BaseRecordModel1ServiceImpl;
import com.ztjo.core.utils.TreeUtils;
import com.ztjo.data.entity.base.BaseMenu;
import com.ztjo.data.pojo.bo.MenuBo;
import com.ztjo.data.pojo.vo.model.TreeEntityVo;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static com.ztjo.data.constants.RedisKeyHeaders.CACHE_VALUE_MENU_ALL;

/**
 * <p>
 * 菜单资源表 - 保存系统菜单 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
@Service
public class BaseMenuServiceImpl extends BaseRecordModel1ServiceImpl<BaseMenuMapper, BaseMenu> implements BaseMenuService {

    /**
     * 根据角色集合加载菜单资源
     * @param groups
     * @return
     */
    @Override
    public List<BaseMenu> getMenusByGroups(Set<String> groups) {
        return baseMapper.selectMenusByGroups(new ArrayList<>(groups));
    }

    /**
     * 菜单项（作为资源进行加载）(树形)
     * @return
     */
    @Override
    public List<TreeEntityVo<MenuBo>> loadTree(List<BaseMenu> pool) {
        if(CollUtil.isNotEmpty(pool)) {
            List<MenuBo> bos = Convert.toList(MenuBo.class, pool);
            return TreeUtils.getTreeUpFromPool(bos);
        }
        return CollUtil.newArrayList();
    }

    @Override
    @Cacheable(value = CACHE_VALUE_MENU_ALL, unless = "#result.isEmpty()")
    public List<BaseMenu> list() {
        return super.list();
    }

    @Override
    @CacheEvict(value = {CACHE_VALUE_MENU_ALL}, allEntries = true)
    public boolean save(BaseMenu entity) {
        return super.save(entity);
    }

    @Override
    @CacheEvict(value = {CACHE_VALUE_MENU_ALL}, allEntries = true)
    public boolean removeById(Serializable id) {
        return super.removeById(id);
    }

    @Override
    @CacheEvict(value = {CACHE_VALUE_MENU_ALL}, allEntries = true)
    public boolean updateById(BaseMenu entity) {
        return super.updateById(entity);
    }
}
