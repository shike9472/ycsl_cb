package com.ztjo.core.service.biz.impl;

import com.ztjo.data.entity.biz.SjInfoJyhtxxbBcywymx;
import com.ztjo.core.mapper.biz.SjInfoJyhtxxbBcywymxMapper;
import com.ztjo.core.service.biz.SjInfoJyhtxxbBcywymxService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 交易合同的补充与违约明细类数据表 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Service
public class SjInfoJyhtxxbBcywymxServiceImpl extends ServiceImpl<SjInfoJyhtxxbBcywymxMapper, SjInfoJyhtxxbBcywymx> implements SjInfoJyhtxxbBcywymxService {

}
