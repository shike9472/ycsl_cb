package com.ztjo.core.service.biz;

import com.ztjo.data.entity.biz.SjInfoBdczxxb;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * info表 - 不动产幢信息表（不动产首次登记时可以使用该表收录首次登记的相关幢信息） 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjInfoBdczxxbService extends IService<SjInfoBdczxxb> {

}
