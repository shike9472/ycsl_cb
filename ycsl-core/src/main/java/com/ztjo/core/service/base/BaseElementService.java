package com.ztjo.core.service.base;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ztjo.data.entity.base.BaseElement;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 组件资源表 - 保存uri或button资源的表 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
public interface BaseElementService extends IService<BaseElement> {

    /**
     * 根据角色集合加载请求与按钮资源
     * @param groups
     * @return
     */
    List<BaseElement> getElementsByGroups(Set<String> groups);

    /**
     * 分页查询资源信息
     * @param page
     * @param entity
     * @return
     */
    IPage<BaseElement> getElementsPage(Page page, BaseElement entity);

    /**
     * 根据menuid删除资源
     * @param menuId
     */
    void removeByMenuId(Long menuId);

    /**
     * 根据id集合删除资源
     * @param ids
     */
    void removeByIds(List<Long> ids);
}
