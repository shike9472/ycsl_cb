package com.ztjo.core.service.biz.impl;

import com.ztjo.data.entity.biz.SjInfoCflrxxb;
import com.ztjo.core.mapper.biz.SjInfoCflrxxbMapper;
import com.ztjo.core.service.biz.SjInfoCflrxxbService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * info - 查封信息录入表 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Service
public class SjInfoCflrxxbServiceImpl extends ServiceImpl<SjInfoCflrxxbMapper, SjInfoCflrxxb> implements SjInfoCflrxxbService {

}
