package com.ztjo.core.service.biz.impl;

import com.ztjo.data.entity.biz.SjSjsqMappingBdcbiz;
import com.ztjo.core.mapper.biz.SjSjsqMappingBdcbizMapper;
import com.ztjo.core.service.biz.SjSjsqMappingBdcbizService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 收件 - 不动产业务关联表 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Service
public class SjSjsqMappingBdcbizServiceImpl extends ServiceImpl<SjSjsqMappingBdcbizMapper, SjSjsqMappingBdcbiz> implements SjSjsqMappingBdcbizService {

}
