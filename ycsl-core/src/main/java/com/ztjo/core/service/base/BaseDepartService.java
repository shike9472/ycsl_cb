package com.ztjo.core.service.base;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ztjo.data.entity.base.BaseArea;
import com.ztjo.data.entity.base.BaseDepart;

import java.util.List;

/**
 * <p>
 * 部门表 - 用于管理系统内部门 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
public interface BaseDepartService extends IService<BaseDepart> {

    /**
     * 获取部门接入区域列表
     * @param departId
     * @return
     */
    List<BaseArea> gainDepartAreas(long departId);
}
