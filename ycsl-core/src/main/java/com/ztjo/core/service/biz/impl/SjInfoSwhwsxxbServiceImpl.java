package com.ztjo.core.service.biz.impl;

import com.ztjo.data.entity.biz.SjInfoSwhwsxxb;
import com.ztjo.core.mapper.biz.SjInfoSwhwsxxbMapper;
import com.ztjo.core.service.biz.SjInfoSwhwsxxbService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * info表 - 税务核/完税信息表 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Service
public class SjInfoSwhwsxxbServiceImpl extends ServiceImpl<SjInfoSwhwsxxbMapper, SjInfoSwhwsxxb> implements SjInfoSwhwsxxbService {

}
