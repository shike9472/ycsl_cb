package com.ztjo.core.service.base.impl;

import cn.hutool.core.text.StrFormatter;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ztjo.core.mapper.base.BaseTenantMapper;
import com.ztjo.core.service.base.*;
import com.ztjo.core.service.model.BaseRecordModel2ServiceImpl;
import com.ztjo.data.entity.base.*;
import com.ztjo.data.exception.YcslBizException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

/**
 * <p>
 * 租户表 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2021-10-26
 */
@Service
public class BaseTenantServiceImpl extends BaseRecordModel2ServiceImpl<BaseTenantMapper, BaseTenant> implements BaseTenantService {

    @Autowired
    private BaseDepartService departService;
    @Autowired
    private BasePositionService positionService;
    @Autowired
    private BaseUserService userService;
    @Autowired
    private BaseTenantGroupService tenantGroupService;

    /**
     * 判定租户是否可以被删除
     * @param id
     */
    @Override
    public void tenantCanDelete(Long id) {
        if(departService.count(Wrappers.<BaseDepart>lambdaQuery().eq(BaseDepart::getTenantId, id))>0) {
            throw new YcslBizException(StrFormatter.format("租户 - [{}]下存在部门，无法删除", id));
        }
        if(positionService.count(Wrappers.<BasePosition>lambdaQuery().eq(BasePosition::getTenantId, id))>0) {
            throw new YcslBizException(StrFormatter.format("租户 - [{}]下存在岗位，无法删除", id));
        }
        if(userService.count(Wrappers.<BaseUser>lambdaQuery().eq(BaseUser::getTenantId, id))>0) {
            throw new YcslBizException(StrFormatter.format("租户 - [{}]下存在用户，无法删除", id));
        }
    }

    /**
     * 重新租户删除方法
     * @param id
     * @return
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT,
            rollbackFor = Exception.class)
    public boolean removeById(Serializable id) {
        // 删除租户-角色授权
        tenantGroupService.remove(Wrappers.<BaseTenantGroup>lambdaQuery().eq(BaseTenantGroup::getTenantId, id));
        // 删除租户信息
        return super.removeById(id);
    }
}
