package com.ztjo.core.service.base;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ztjo.data.entity.base.BaseResourceAuthority;
import com.ztjo.data.pojo.dto.RelationModifyDto;
import com.ztjo.data.pojo.vo.model.RelationModifyConditionVo;
import com.ztjo.data.pojo.vo.settings.AllGroupResourceVo;

import java.util.List;

/**
 * <p>
 * 菜单，uri，按钮等资源 - group授权表 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
public interface BaseResourceAuthorityService extends IService<BaseResourceAuthority> {
    /**
     * 角色资源权限授权
     * @param dto
     * @return
     */
    boolean modifyGroupResources(RelationModifyDto<BaseResourceAuthority> dto);

    /**
     * 按菜单id集合移除资源角色权限关系
     * @param resourceIds
     */
    void removeByMenuIds(List<Long> resourceIds);

    /**
     * 按菜单内element id集合移除资源角色权限关系
     * @param elementIds
     */
    void removeByElementIdsInMenu(List<Long> elementIds);

    /**
     * 按element id集合移除资源角色权限关系
     * @param elementIds
     */
    void removeByElementIds(List<Long> elementIds);

    /**
     * 加载角色资源授权全部依赖的数据
     * @param tarId
     * @return
     */
    RelationModifyConditionVo<AllGroupResourceVo> loadAlready(Long tarId);

    /**
     * 加载全部资源信息
     * @return
     */
    AllGroupResourceVo loadAllResources();
}
