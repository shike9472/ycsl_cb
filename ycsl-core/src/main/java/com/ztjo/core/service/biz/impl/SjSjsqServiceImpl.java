package com.ztjo.core.service.biz.impl;

import com.ztjo.data.entity.biz.SjSjsq;
import com.ztjo.core.mapper.biz.SjSjsqMapper;
import com.ztjo.core.service.biz.SjSjsqService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Service
public class SjSjsqServiceImpl extends ServiceImpl<SjSjsqMapper, SjSjsq> implements SjSjsqService {

}
