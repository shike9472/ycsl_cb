package com.ztjo.core.service.biz;

import com.ztjo.data.entity.biz.SjSqrbNsrxx;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 纳税人表 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjSqrbNsrxxService extends IService<SjSqrbNsrxx> {

}
