package com.ztjo.core.service.base.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ztjo.core.mapper.base.BaseUserMapper;
import com.ztjo.core.service.base.BaseGroupLeaderService;
import com.ztjo.core.service.base.BaseGroupMemberService;
import com.ztjo.core.service.base.BasePositionUserService;
import com.ztjo.core.service.base.BaseUserService;
import com.ztjo.core.service.model.TenantModelServiceImpl;
import com.ztjo.data.entity.base.*;
import com.ztjo.data.exception.YcslBizException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 * <p>
 * 系统用户表 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
@Service
public class BaseUserServiceImpl extends TenantModelServiceImpl<BaseUserMapper, BaseUser> implements BaseUserService {

    @Autowired
    private BaseGroupLeaderService groupLeaderService;
    @Autowired
    private BaseGroupMemberService groupMemberService;
    @Autowired
    private BasePositionUserService positionUserService;

    /**
     * 系统加密bean
     */
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public boolean save(BaseUser entity) {
        entity.setPassword(bCryptPasswordEncoder.encode(entity.getPassword()));
        return super.save(entity);
    }

    /**
     * 删除检查
     * @param id
     */
    @Override
    public void userCanRemove(Serializable id) {
        // 检查用户角色领导关系
        int userCount = groupLeaderService.count(Wrappers.<BaseGroupLeader>query().lambda().eq(BaseGroupLeader::getUserId, id));
        if(userCount>0) {
            throw new YcslBizException("用户仍授权有角色权限，请先解绑用户与角色关系");
        }
        // 检查用户角色成员关系
        userCount = groupMemberService.count(Wrappers.<BaseGroupMember>query().lambda().eq(BaseGroupMember::getUserId, id));
        if(userCount>0) {
            throw new YcslBizException("用户仍授权有角色权限，请先解绑用户与角色关系");
        }
        // 检查用户岗位关系
        userCount = positionUserService.count(Wrappers.<BasePositionUser>query().lambda().eq(BasePositionUser::getUserId, id));
        if(userCount>0) {
            throw new YcslBizException("用户仍存在岗位挂接关系，请先解绑用户与岗位关系");
        }
    }
}
