package com.ztjo.core.service.biz.impl;

import com.ztjo.data.entity.biz.SjInfoXtncljgb;
import com.ztjo.core.mapper.biz.SjInfoXtncljgbMapper;
import com.ztjo.core.service.biz.SjInfoXtncljgbService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * info表 - 系统内处理结果表 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Service
public class SjInfoXtncljgbServiceImpl extends ServiceImpl<SjInfoXtncljgbMapper, SjInfoXtncljgb> implements SjInfoXtncljgbService {

}
