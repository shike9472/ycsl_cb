package com.ztjo.core.service.base;

import com.ztjo.data.entity.base.DicItem;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 字典-字典字项表 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
public interface DicItemService extends IService<DicItem> {

    /**
     * 根据dic_main的dicCode加载字典项
     * @param dicCode
     * @return
     */
    List<DicItem> loadByMainCode(String dicCode);

    /**
     * 按id删除字典项，入参为字典项实体
     * @param entity
     * @return
     */
    boolean removeById(DicItem entity);

    /**
     * 批量删除，入参要求：
     *      传入的id集合中字典项的diccode必须需条件一的一致
     * @param dicCode
     * @param idList
     * @return
     */
    boolean removeBatch(String dicCode, List<Long> idList);
}
