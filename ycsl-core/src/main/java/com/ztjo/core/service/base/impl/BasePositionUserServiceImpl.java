package com.ztjo.core.service.base.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ztjo.core.mapper.base.BasePositionUserMapper;
import com.ztjo.core.service.base.BasePositionUserService;
import com.ztjo.core.utils.SecurityUtils;
import com.ztjo.data.entity.base.BasePositionGroup;
import com.ztjo.data.entity.base.BasePositionUser;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ztjo.data.pojo.dto.RelationModifyDto;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.ztjo.data.constants.RedisKeyHeaders.*;

/**
 * <p>
 * 岗位 - 用户设置表 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
@Service
public class BasePositionUserServiceImpl extends ServiceImpl<BasePositionUserMapper, BasePositionUser> implements BasePositionUserService {

    /**
     * 岗位用户授权
     * @param dto
     * @return
     */
    @Override
    @CacheEvict(value = {CACHE_VALUE_USER_GROUP_PERMISSION,
            CACHE_VALUE_MENU_USER, CACHE_VALUE_ELEMENT_USER}, allEntries = true)
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT,
            rollbackFor = Exception.class)
    public boolean modifyPositionUsers(RelationModifyDto<BasePositionUser> dto) {
        // 清理原有设置
        baseMapper.delete(Wrappers.<BasePositionUser>query().lambda().eq(BasePositionUser::getPositionId, dto.getTarId()));
        // 取当前需要存储的目标设置
        List<BasePositionUser> positionUsers = dto.getCurEntitys();
        if(CollUtil.isNotEmpty(positionUsers)) {
            // 遍历目标设置并赋值特定值
            for (BasePositionUser positionUser : positionUsers) {
                // 关联项赋值检查
                if(ObjectUtil.isNull(positionUser.getPositionId())) {
                    positionUser.setPositionId(dto.getTarId());
                }
            }
            // 批量新增目标设置
            return super.saveBatch(positionUsers);
        }
        return true;
    }
}
