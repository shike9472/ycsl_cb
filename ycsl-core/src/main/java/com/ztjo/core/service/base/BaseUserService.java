package com.ztjo.core.service.base;

import com.ztjo.data.entity.base.BaseUser;
import com.baomidou.mybatisplus.extension.service.IService;

import java.io.Serializable;

/**
 * <p>
 * 系统用户表 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
public interface BaseUserService extends IService<BaseUser> {

    /**
     * 删除检查
     * @param id
     */
    void userCanRemove(Serializable id);
}
