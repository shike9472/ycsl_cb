package com.ztjo.core.service.biz.impl;

import com.ztjo.data.entity.biz.SjSjsqMappingQtxtjsr;
import com.ztjo.core.mapper.biz.SjSjsqMappingQtxtjsrMapper;
import com.ztjo.core.service.biz.SjSjsqMappingQtxtjsrService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 收件 - 第三方系统办件人员关联表 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Service
public class SjSjsqMappingQtxtjsrServiceImpl extends ServiceImpl<SjSjsqMappingQtxtjsrMapper, SjSjsqMappingQtxtjsr> implements SjSjsqMappingQtxtjsrService {

}
