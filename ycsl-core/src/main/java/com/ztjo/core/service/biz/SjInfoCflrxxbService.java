package com.ztjo.core.service.biz;

import com.ztjo.data.entity.biz.SjInfoCflrxxb;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * info - 查封信息录入表 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjInfoCflrxxbService extends IService<SjInfoCflrxxb> {

}
