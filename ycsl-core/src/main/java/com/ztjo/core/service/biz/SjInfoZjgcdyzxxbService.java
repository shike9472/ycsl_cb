package com.ztjo.core.service.biz;

import com.ztjo.data.entity.biz.SjInfoZjgcdyzxxb;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * info表 - 在建工程抵押幢信息表（设立/变更/转移/注销均可入该表） 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjInfoZjgcdyzxxbService extends IService<SjInfoZjgcdyzxxb> {

}
