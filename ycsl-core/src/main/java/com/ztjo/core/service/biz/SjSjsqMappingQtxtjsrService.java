package com.ztjo.core.service.biz;

import com.ztjo.data.entity.biz.SjSjsqMappingQtxtjsr;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 收件 - 第三方系统办件人员关联表 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjSjsqMappingQtxtjsrService extends IService<SjSjsqMappingQtxtjsr> {

}
