package com.ztjo.core.service.biz.impl;

import com.ztjo.data.entity.biz.SjInfoSwsjlrxxb;
import com.ztjo.core.mapper.biz.SjInfoSwsjlrxxbMapper;
import com.ztjo.core.service.biz.SjInfoSwsjlrxxbService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * info表 - 税务收件录入信息表 服务实现类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Service
public class SjInfoSwsjlrxxbServiceImpl extends ServiceImpl<SjInfoSwsjlrxxbMapper, SjInfoSwsjlrxxb> implements SjInfoSwsjlrxxbService {

}
