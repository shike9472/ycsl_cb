package com.ztjo.core.service.biz;

import com.ztjo.data.entity.biz.SjInfoJyhtxxbDetail;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 交易合同信息表 - 补充细节数据表 服务类
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
public interface SjInfoJyhtxxbDetailService extends IService<SjInfoJyhtxxbDetail> {

}
