package com.ztjo.core.filter;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.ztjo.common.properties.AuthCustomProperties;
import com.ztjo.core.model.YcslUserDetails;
import com.ztjo.core.service.JwtHandleService;
import com.ztjo.core.utils.SecurityUtils;
import com.ztjo.data.exception.YcslTokenErrorException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.ztjo.data.enums.LoginErrorEnums.TOKEN_CANNOT_ANALYSIS;

/**
 * @author 陈彬
 * @version 2021/9/18
 * description：token过滤器 验证token有效性
 */
@Slf4j
@Component
public class JwtAuthenticationTokenFilter extends OncePerRequestFilter {

    /**
     * jwt服务装载
     */
    @Autowired
    private JwtHandleService jwtService;

    /**
     * auth配置信息
     */
    @Autowired
    private AuthCustomProperties authProperties;

    /**
     * 未知来源ip
     */
    private static final String COME_IP_UNKNOWN = "unknown";

    /**
     * 本地IP
     */
    private static final String COME_IP_PROXY_LOCAL = "127.0.0.1";

    /**
     * 过滤器执行方法
     * @param request
     * @param response
     * @param chain
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws ServletException, IOException
    {
        log.info("请求地址：{} - {} - start", request.getRequestURI(), request.getRequestURL());
        // 取token
        String token = request.getHeader(authProperties.getToken().getTokenHeader());
        if(StrUtil.isNotBlank(token)) {
            log.info("进入token解析");
            // token未解析出用户细节数据 -> 判定为token不合法 todo 此处应加单元测试
            YcslUserDetails loginUser = jwtService.getLoginUser(token);
            if (ObjectUtil.isNull(loginUser)) {
                throw new YcslTokenErrorException(TOKEN_CANNOT_ANALYSIS);
            }
            // 安全性数据封装
            loginUser.setTarUrl(request.getRequestURI());
            loginUser.setComeIp(getIpAddress(request));

            log.info("jwt token信息解析成功！");
            if (ObjectUtil.isNull(SecurityUtils.getAuthentication())) {
                // token解析出的用户信息进一步验证及角色组信息加载
                jwtService.verifyUdetailAndAddGroups(token, loginUser);
                // 用户权限信息设置进入全局，方便后续 @PreAuthorize 注解解析权限
                UsernamePasswordAuthenticationToken authenticationToken =
                        new UsernamePasswordAuthenticationToken(loginUser, null, loginUser.getAuthorities());
                authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
            }
        }
        chain.doFilter(request, response);
    }

    /**
     * 获取请求主机IP地址,如果通过代理进来，则透过防火墙获取真实IP地址
     * 使用过程中约定俗成的 http client ip 请求头
     * @param request
     * @return
     * @throws IOException
     */
    public final static String getIpAddress(HttpServletRequest request) throws IOException {
        // 记录全部可能的IP
        List<String> comeIps = new ArrayList<>();

        // RFC 7239（Forwarded HTTP Extension）标准下真实client ip - 事实标准设定
        String xffIp = request.getHeader("X-Forwarded-For");
        log.info("getIpAddress(HttpServletRequest) - [X-Forwarded-For] - String ip = {}", xffIp);
        comeIps.add(xffIp);
        if (ipCanUse(xffIp)) {
            if (xffIp.length() > 15) {
                String[] ips = xffIp.split(",");
                for (int index = 0; index < ips.length; index++) {
                    String strIp = (String) ips[index];
                    if (ipCanUse(strIp)) {
                        return strIp;
                    }
                }
            }
        } else {
            // Proxy-Client-IP
            String pcIp = request.getHeader("Proxy-Client-IP");
            log.info("getIpAddress(HttpServletRequest) - [Proxy-Client-IP] - String ip = {}", pcIp);
            if(ipCanUse(pcIp)) {
                return pcIp;
            }
            comeIps.add(pcIp);
            // WL-Proxy-Client-IP
            String wpcIp = request.getHeader("WL-Proxy-Client-IP");
            log.info("getIpAddress(HttpServletRequest) - [WL-Proxy-Client-IP] - String ip = {}", wpcIp);
            if(ipCanUse(wpcIp)) {
                return wpcIp;
            }
            comeIps.add(wpcIp);
            // HTTP_CLIENT_IP
            String hcIp = request.getHeader("HTTP_CLIENT_IP");
            log.info("getIpAddress(HttpServletRequest) - [HTTP_CLIENT_IP] - String ip = {}", hcIp);
            if (ipCanUse(hcIp)) {
                return hcIp;
            }
            comeIps.add(hcIp);
            // HTTP_X_FORWARDED_FOR
            String hxffIp = request.getHeader("HTTP_X_FORWARDED_FOR");
            log.info("getIpAddress(HttpServletRequest) - [HTTP_X_FORWARDED_FOR] - String ip = {}", hxffIp);
            if (ipCanUse(hxffIp)) {
                return hxffIp;
            }
            comeIps.add(hxffIp);
            // RemoteAddr
            String remoteAddr = request.getRemoteAddr();
            log.info("getIpAddress(HttpServletRequest) - [getRemoteAddr] - String ip = {}", remoteAddr);
            if (ipCanUse(remoteAddr)) {
                return remoteAddr;
            }
            comeIps.add(remoteAddr);
        }
        if(comeIps.stream().anyMatch(ip -> StrUtil.isNotBlank(ip) && ip.contains(COME_IP_PROXY_LOCAL))) {
            return COME_IP_PROXY_LOCAL;
        }
        return COME_IP_UNKNOWN;
    }

    /**
     * ip可用标准
     * @param ip
     * @return
     */
    public static boolean ipCanUse(String ip) {
        return StrUtil.isNotBlank(ip) && !COME_IP_UNKNOWN.equalsIgnoreCase(ip) && !COME_IP_PROXY_LOCAL.equalsIgnoreCase(ip);
    }
}
