package com.ztjo.core.filter;

import com.alibaba.fastjson.JSON;
import com.ztjo.common.utils.ServletUtils;
import com.ztjo.data.exception.YcslAuthenticationException;
import com.ztjo.data.pojo.api.ZtjoResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author 陈彬
 * @version 2021/9/25
 * description：全局异常拦截过滤器，所有进入过滤器的最外层，防止异常直接抛给前端
 */
@Slf4j
@Component
public class GlobalErrorInterceptFilter extends OncePerRequestFilter {

    /**
     * 全局安全异常拦截过滤器
     * @param request
     * @param response
     * @param chain
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws ServletException, IOException
    {
        try {
            chain.doFilter(request, response);
        } catch (Exception e) {
            log.error("请求地址：{} - {}，抛出异常信息！", request.getRequestURI(), request.getRequestURL(), e);
            handleSecurityError(response, e);
        } finally {
            log.info("请求地址：{} - {} - end", request.getRequestURI(), request.getRequestURL());
        }
    }

    /**
     * 安全过滤器异常处理逻辑
     * @param response
     * @param e
     */
    private void handleSecurityError(HttpServletResponse response, Exception e) {
        if(e instanceof YcslAuthenticationException) {
            YcslAuthenticationException ee = (YcslAuthenticationException) e;
            ServletUtils.renderString(response, JSON.toJSONString(ZtjoResponse.failed(ee.getStatus(), ee.getMessage())));
        } else {
            ServletUtils.renderString(response, JSON.toJSONString(ZtjoResponse.failed("请求失败，请联系管理员！")));
        }
    }
}
