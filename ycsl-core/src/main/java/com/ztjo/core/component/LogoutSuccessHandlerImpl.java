package com.ztjo.core.component;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.ztjo.common.properties.AuthCustomProperties;
import com.ztjo.common.utils.ServletUtils;
import com.ztjo.core.model.YcslUserDetails;
import com.ztjo.core.service.JwtHandleService;
import com.ztjo.core.utils.SecurityUtils;
import com.ztjo.data.pojo.api.ZtjoResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static com.ztjo.data.constants.RedisKeyHeaders.LOGIN_TOKEN_HEADER;
import static com.ztjo.data.constants.SysPublicConstants.LOGOUT_MARK_VALUE;

/**
 * @author 陈彬
 * @version 2021/9/18
 * description：登出时执行方法，主要用于数据释放，用户登录信息删除等
 */
@Slf4j
@Configuration
public class LogoutSuccessHandlerImpl implements LogoutSuccessHandler {
    /**
     * token解析组件
     */
    @Autowired
    private JwtHandleService jwtService;

    /**
     * redis操作组件
     */
    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    /**
     * 自定义jwt配置
     */
    @Autowired
    private AuthCustomProperties authProperties;

    /**
     * 描述：重写登出执行方法
     * 作者：陈彬
     * 日期：2021/9/18
     * 参数：[httpServletRequest, httpServletResponse, authentication]
     * 返回：void
     * 更新记录：更新人：{}，更新日期：{}
     */
    @Override
    public void onLogoutSuccess(
            HttpServletRequest request,
            HttpServletResponse response,
            Authentication authentication) throws IOException, ServletException
    {
        YcslUserDetails loginUser = jwtService.getLoginUser(request.getHeader(authProperties.getToken().getTokenHeader()));
        if(ObjectUtil.isNotNull(loginUser)) {
            log.info("用户 - {}登出", loginUser.getUser().getName());
            // token redis中保存的key
            final String tokenRedisKey = LOGIN_TOKEN_HEADER + ":" + loginUser.getUser().getId();
            // 登出标记留存20分钟
            redisTemplate.opsForValue().set(tokenRedisKey, LOGOUT_MARK_VALUE, authProperties.getJwt().getExpire(), TimeUnit.SECONDS);
        }

        ServletUtils.renderString(response, JSON.toJSONString(ZtjoResponse.ok(true, "退出登录成功!")));
    }
}
