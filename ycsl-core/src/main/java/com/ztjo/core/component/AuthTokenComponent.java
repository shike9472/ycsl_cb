package com.ztjo.core.component;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.github.ag.core.constants.CommonConstants;
import com.github.ag.core.util.jwt.IJWTInfo;
import com.github.ag.core.util.jwt.JWTInfo;
import com.ztjo.common.properties.AuthCustomProperties;
import com.ztjo.common.utils.jwt.JwtTokenUtil;
import com.ztjo.core.model.YcslUserDetails;
import com.ztjo.core.service.frame.YcslUserDetailsServiceImpl;
import com.ztjo.data.entity.base.BaseUser;
import com.ztjo.data.exception.YcslBizException;
import com.ztjo.data.exception.YcslTokenErrorException;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.ztjo.data.constants.RedisKeyHeaders.LOGIN_TOKEN_HEADER;
import static com.ztjo.data.constants.SysPublicConstants.LOGOUT_MARK_VALUE;
import static com.ztjo.data.enums.LoginErrorEnums.*;

/**
 * @author 陈彬
 * @version 2021/9/4
 * description：用户token相关组件
 */
@Slf4j
@Component
public class AuthTokenComponent {

    /**
     * 自定义用户认证逻辑
     */
    @Autowired
    private UserDetailsService userDetailsService;

    /**
     * 自定义jwt配置
     */
    @Autowired
    private AuthCustomProperties authProperties;

    /**
     * jwt操作工具类
     */
    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    /**
     * redis操作组件
     */
    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    /**
     * 根据token初步取得用户细节数据（未添加岗位授权信息）
     * @param token
     * @return
     */
    public YcslUserDetails getUserDetailsFromToken(String token) {
        IJWTInfo infoFromToken = getInfoFromToken(token);
        if(ObjectUtil.isNotNull(infoFromToken)) {
            YcslUserDetails userDetails = (YcslUserDetails)userDetailsService.loadUserByUsername(infoFromToken.getUniqueName());
            if(ObjectUtil.isNotNull(userDetails)) {
                userDetails.setExpireTime(infoFromToken.getExpireTime());
            }
            return userDetails;
        }
        return null;
    }

    /**
     * 根据系统用户初始化token
     * @param user
     * @return
     */
    public String initToken(BaseUser user) {
        // 声明返回token对象
        String token = "";
        // 准备token可解析数据集合
        Map<String, String> map = new HashMap<>();
        // 部门id需要放入
        map.put(CommonConstants.JWT_KEY_DEPART_ID, String.valueOf(user.getDepartId()));
        // 定义token超时时间
        Date expireTime = DateTime.now().plusMinutes(authProperties.getToken().getTokenExpire()).toDate();
        // 初始化JWTInfo对象 - 用于token生成
        JWTInfo jwtInfo = new JWTInfo(user.getUsername(), user.getId()+"", user.getName(), expireTime);
        try {
            // 通过jwtinfo生成token
            token = jwtTokenUtil.generateToken(jwtInfo, map, expireTime);
        } catch (Exception e) {
            log.error("token初始化失败 - ", e);
            throw new YcslBizException("token初始化失败！");
        }
        // 返回生成好的token
        return token;
    }

    /**
     * token解析方法
     * @param token
     * @return
     */
    public IJWTInfo getInfoFromToken(String token) {
        try {
            return jwtTokenUtil.getInfoFromToken(token);
        } catch (Exception e) {
            log.warn("请求携带的token解析失败！", e);
        }
        return null;
    }

    /**
     * 描述：token刷新方法
     * 作者：陈彬
     * 日期：2021/9/4
     * 参数：[user, tokenIn]
     * 返回：java.lang.String
     * 更新记录：更新人：{}，更新日期：{}
     */
    public String refreshToken(BaseUser user, String tokenIn) {
        // token redis中保存的key
        final String tokenRedisKey = LOGIN_TOKEN_HEADER+":"+user.getId();
        // 取得redis中存储的token
        String token = redisTemplate.opsForValue().get(tokenRedisKey);
        // 是否二次生成标识
        boolean initAgain = false;
        if(StrUtil.isBlank(token)) {
            initAgain = true;
        } else {
            IJWTInfo infoFromToken = getInfoFromToken(token);
            if(ObjectUtil.isNull(infoFromToken)) {
                initAgain = true;
            } else {
                // token超期
                if(new DateTime(infoFromToken.getExpireTime()).isBeforeNow()){
                    initAgain = true;
                }
            }
        }
        if(initAgain) {
            if(StrUtil.isBlank(tokenIn)) {
                token = initToken(user);
            } else {
                token = tokenIn;
            }
        }
        // 重置token redis存储超时时间
        redisTemplate.opsForValue().set(tokenRedisKey, token, authProperties.getJwt().getExpire(), TimeUnit.SECONDS);
        return token;
    }

    /**
     * 清理登录用户token
     * @param uid
     */
    public void clearLoginToken(Long uid) {
        // token redis中保存的key
        final String tokenRedisKey = LOGIN_TOKEN_HEADER + ":" + uid;
        // 清空token
        redisTemplate.delete(tokenRedisKey);
    }

    /**
     * 通过userDetail信息获取token
     * @param userDetails
     * @return
     */
    public String gainToken(YcslUserDetails userDetails) {
        return gainToken(userDetails.getUser());
    }

    /**
     * 描述：token获取方法
     * 作者：陈彬
     * 日期：2021/9/4
     * 参数：[user]
     * 返回：java.lang.String
     * 更新记录：更新人：{}，更新日期：{}
     */
    public String gainToken(BaseUser user) {
        // token redis中保存的key
        final String tokenRedisKey = LOGIN_TOKEN_HEADER+":"+user.getId();
        /**
         * 注意如果做单客户端登录，
         * 此处不检查原有token，
         * 新token直接保存至redis完成覆盖即可，
         * 先前登录用户下次请求时会被检查token已不合法
         */
        // 之前存过
        if(redisTemplate.hasKey(tokenRedisKey)) {
            // 取用户redis存储token
            String tokenRedis = redisTemplate.opsForValue().get(tokenRedisKey);
            // 不为已登出用户
            if(!LOGOUT_MARK_VALUE.equals(tokenRedis)) {
                // 刷新token
                return refreshToken(user, null);
            }
        }
        // 未存过时
        String token = initToken(user);
        // 重置token redis存储超时时间
        redisTemplate.opsForValue().set(tokenRedisKey, token, authProperties.getJwt().getExpire(), TimeUnit.SECONDS);
        return token;
    }

    /**
     * 检查token是否超时，页面是否超时
     * @param loginUser
     */
    public void verifyUserDetail(String token, YcslUserDetails loginUser) {
        // token本身是否过期
        if(new DateTime(loginUser.getExpireTime()).isBeforeNow()) {
            throw new YcslTokenErrorException(TOKEN_EXPIRE);
        }
        // token在redis存储是否过期（页面超时）
        final String tokenRedisKey = LOGIN_TOKEN_HEADER+":"+loginUser.getUser().getId();
        String tokenRedis = redisTemplate.opsForValue().get(tokenRedisKey);
        if(StrUtil.isBlank(tokenRedis)) {
            throw new YcslTokenErrorException(TOKEN_NOT_FINDED);
        }
        if(LOGOUT_MARK_VALUE.equals(tokenRedis)) {
            throw new YcslTokenErrorException(TOKEN_USER_ALREADY_LOGOUT);
        }
        // 比较token是否为当前有效的（页面超时的第二种情况）
        if(!tokenRedis.equals(token)) {
            throw new YcslTokenErrorException(TOKEN_NOT_FINDED);
        }
    }

    /**
     * 加载用户授权信息（角色）
     * @param loginUser
     */
    public void addGroups2UserDetails(YcslUserDetails loginUser) {
        ((YcslUserDetailsServiceImpl)userDetailsService).addGroups2LoginUser(loginUser);
    }
}
