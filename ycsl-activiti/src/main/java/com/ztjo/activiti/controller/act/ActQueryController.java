package com.ztjo.activiti.controller.act;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ztjo.activiti.service.ActQueryService;
import com.ztjo.data.pojo.api.ZtjoResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 陈彬
 * @version 2022/1/10
 * description：工作流相关查询API
 */
@Slf4j
@RestController
@Api(value = "act-query", tags = {"[act-query]工作流相关查询API"})
public class ActQueryController {
    @Autowired
    private ActQueryService actQueryService;

    @ApiOperation("查询候选任务列表")
    @GetMapping("/act/query/task/candidate/page")
    public ZtjoResponse queryCommonTaskPage(Page page) {
        return ZtjoResponse.ok(actQueryService.queryCommonTaskPage(page));
    }

    @ApiOperation("查询待办任务列表")
    @GetMapping("/act/query/task/needDeal/page")
    public ZtjoResponse queryPrivateTaskPage(Page page) {
        return ZtjoResponse.ok(actQueryService.queryPrivateTaskPage(page));
    }
}
