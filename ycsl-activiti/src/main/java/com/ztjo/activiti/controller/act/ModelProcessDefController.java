package com.ztjo.activiti.controller.act;

import cn.hutool.core.text.StrFormatter;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ztjo.activiti.service.ActModelProcessService;
import com.ztjo.data.annotation.authorize.PreElement;
import com.ztjo.data.annotation.authorize.PreMenu;
import com.ztjo.data.exception.YcslBizException;
import com.ztjo.data.pojo.api.ZtjoResponse;
import com.ztjo.data.pojo.dto.act.ActModelCreateDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.repository.Model;
import org.activiti.engine.repository.ProcessDefinition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.IOException;

import static com.ztjo.data.enums.SFEnums.SF_S;

/**
 * @author 陈彬
 * @version 2022/1/4
 * description：activiti工作流模板与流程定义-部署controller
 */
@Slf4j
@RestController
@PreMenu("menu_lcsz")
@Api(value = "act-modeler", tags = {"[act-model]工作流模板与流程定义-部署组件API"})
public class ModelProcessDefController {

    @Autowired
    private ActModelProcessService modelProcessService;

    @Resource
    private RepositoryService repositoryService;

    @Resource
    private RuntimeService runtimeService;

    /**
     * 模板列表的分页查询
     *      需要携带是否完成部署的状态信息
     */
    @ApiOperation("查询模板列表")
    @GetMapping("/act/model/page")
    public ZtjoResponse queryModelPage(Page page, String name, String isDeployed)
    {
        return ZtjoResponse.ok(modelProcessService.actModelsPage(page, name, isDeployed));
    }

    /**
     * 创建模板
     * @param dto
     * @return
     */
    @ApiOperation("创建模板")
    @PostMapping("/act/model/create")
    @PreElement("button_act_model_create")
    public ZtjoResponse<Model> create(@RequestBody ActModelCreateDto dto)
    {
        Model modelData = modelProcessService.create(dto);
        return ZtjoResponse.ok(modelData);
    }

    /**
     * 模板部署
     */
    @ApiOperation("流程部署")
    @GetMapping("/act/model/deploy")
    @PreElement("api_act_model_deploy")
    public ZtjoResponse<String> deploy(@RequestParam("modelId") String modelId) {
        Model model = repositoryService.getModel(modelId);
        if(ObjectUtil.isNull(model)) {
            throw new YcslBizException("流程不存在");
        }
        try {
            modelProcessService.deploy(model);
        } catch (IOException e) {
            log.error("流程部署IO异常 -> ", e);
            throw new YcslBizException(StrFormatter.format("部署失败！失败原因 -> {}", e.getMessage()));
        }
        return ZtjoResponse.ok("流程部署成功！");
    }

    /**
     * 取消部署
     */
    @ApiOperation("取消部署")
    @GetMapping("/act/model/deploy/cancer")
    @PreElement("api_act_model_deploy")
    public ZtjoResponse<String> cancerDeploy(@RequestParam("deploymentId") String deploymentId,
                                             @RequestParam(value = "force", required = false) String force)
    {
        // 流程是否有启动实例检查
        if(!SF_S.getCode().equals(force)) {
            ProcessDefinition pd = repositoryService.createProcessDefinitionQuery().deploymentId(deploymentId).singleResult();
            if(ObjectUtil.isNotNull(pd)) {
                long count = runtimeService.createExecutionQuery().processDefinitionId(pd.getId()).count();
                if(count > 0) {
                    throw new YcslBizException("流程仍存在在办业务，请在业务完结后取消部署！" +
                            "如果需要隐藏该流程，可以使用流程禁用功能来保持流程业务不会在之后被创建！");
                }
            }
        }
        /**
         * true代表级联删除,
         * 无论有没有启动实例都删除,
         * 删除将会
         */
        repositoryService.deleteDeployment(deploymentId,true);

        return ZtjoResponse.ok("流程取消部署成功！");
    }

    /**
     * 删除模板
     */
    @ApiOperation("删除模板")
    @GetMapping("/act/model/delete")
    @PreElement("button_act_model_delete")
    public ZtjoResponse<String> delete(@RequestParam("modelId") String modelId)
    {
        // 检查流程是否部署状态
        Model model = repositoryService.createModelQuery().modelId(modelId).singleResult();
        if(StrUtil.isNotEmpty(model.getDeploymentId())) {
            throw new YcslBizException("模板已被部署，删除失败！");
        }
        // 执行删除
        modelProcessService.delete(modelId);
        return ZtjoResponse.ok("删除成功");
    }

    /**
     * 模板可修改验证
     */
    @ApiOperation("模板可修改验证")
    @GetMapping("/act/model/modify/verify")
    @PreElement("button_act_model_modify")
    public ZtjoResponse<String> modifyCheck(@RequestParam("modelId") String modelId) {
        // 检查流程是否部署状态
        Model model = repositoryService.createModelQuery().modelId(modelId).singleResult();
        if(StrUtil.isNotEmpty(model.getDeploymentId())) {
            throw new YcslBizException("模板已被部署，禁止修改！");
        }
        return ZtjoResponse.ok(null);
    }
}
