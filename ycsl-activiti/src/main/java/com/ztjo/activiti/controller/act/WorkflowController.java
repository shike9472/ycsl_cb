package com.ztjo.activiti.controller.act;

import com.ztjo.activiti.service.WorkflowService;
import com.ztjo.data.pojo.api.ZtjoResponse;
import com.ztjo.data.pojo.dto.act.TaskBackOffDto;
import com.ztjo.data.pojo.dto.act.TaskSubmitDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * @author 陈彬
 * @version 2022/1/6
 * description：工作流执行及操作controller
 */
@Slf4j
@RestController
@Api(value = "act-workflow", tags = {"[act-workflow]工作流流程执行组件API"})
public class WorkflowController {
    @Autowired
    private WorkflowService workflowService;
    @Resource
    private TaskService taskService;

    /**
     * 流程启动
     */
    @PreAuthorize("@ps.canStartActModel(#modelId)")
    @GetMapping("/act/workflow/start/{modelId}")
    @ApiOperation(value = "[act-workflow]流程启动接口", notes = "[act-workflow]流程启动接口")
    public ZtjoResponse<String> start(@PathVariable String modelId)
    {
        workflowService.start(modelId);
        return ZtjoResponse.ok("启动成功！");
    }

    /**
     * 任务签收
     */
    @PreAuthorize("@ps.canSignActTask(#taskId)")
    @GetMapping("/act/workflow/sign/{taskId}")
    @ApiOperation(value = "[act-workflow]任务签收接口", notes = "[act-workflow]任务签收接口")
    public ZtjoResponse<String> sign(@PathVariable String taskId)
    {
        workflowService.sign(taskId);
        return ZtjoResponse.ok("签收成功！");
    }

    /**
     * 任务转办
     */
    @PreAuthorize("@ps.canHandleActTask(#taskId, true)")
    @GetMapping("/act/workflow/transfer/{taskId}")
    @ApiOperation(value = "[act-workflow]任务转办接口", notes = "[act-workflow]任务转办接口")
    public ZtjoResponse<String> transfer(@PathVariable String taskId, @RequestParam("tarUser") String tarUser)
    {
        workflowService.transfer(taskId, tarUser);
        return ZtjoResponse.ok("转办成功！");
    }

    /**
     * 任务提交
     * todo 注意 -> 提交动作一定发生在数据保存之后，
     *      原因： 1. 数据保存成功而提交失败了没事，2. 但如果反过来问题就大了
     */
    @PreAuthorize("@ps.canHandleActTask(#dto.taskId, false)")
    @PostMapping("/act/workflow/submit")
    @ApiOperation(value = "[act-workflow]任务提交接口", notes = "[act-workflow]任务提交接口")
    public ZtjoResponse<String> submit(@Valid @RequestBody TaskSubmitDto dto)
    {
        Task task = taskService.createTaskQuery().taskId(dto.getTaskId()).singleResult();
        workflowService.submit(dto, task);
        return ZtjoResponse.ok("提交成功！");
    }

    /**
     * 任务回退
     * todo 注意 -> 1. 回退动作不用伴随业务数据还原，即允许脏数据存在，
     *              2. 这样再次提交时，回退之前过程步骤产生的业务数据会同步加载出来
     */
    @PreAuthorize("@ps.canHandleActTask(#dto.taskId, true)")
    @PostMapping("/act/workflow/backOff")
    @ApiOperation(value = "[act-workflow]任务回退接口", notes = "[act-workflow]任务回退接口")
    public ZtjoResponse<String> backOff(@Valid @RequestBody TaskBackOffDto dto)
    {
        workflowService.backOff(dto);
        return ZtjoResponse.ok("回退成功！");
    }

    /**
     * 流程挂起
     */
    @PreAuthorize("@ps.canHandleActTask(#taskId, true)")
    @GetMapping("/act/workflow/suspend")
    @ApiOperation(value = "[act-workflow]流程挂起接口", notes = "[act-workflow]流程挂起接口")
    public ZtjoResponse<String> suspend(@RequestParam("taskId") String taskId)
    {
        Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
        workflowService.suspend(task);
        return ZtjoResponse.ok("挂起成功！");
    }

    /**
     * 挂起流程激活
     */
    @GetMapping("/act/workflow/activate")
    @ApiOperation(value = "[act-workflow]挂起流程激活接口", notes = "[act-workflow]挂起流程激活接口")
    public ZtjoResponse<String> activate(@RequestParam("prcInstId") String prcInstId)
    {
        workflowService.activate(prcInstId);
        return ZtjoResponse.ok("流程激活成功！");
    }

    /**
     * 流程实例删除
     */
    @PreAuthorize("@ps.actProcParticipant(#prcInstId, true)")
    @GetMapping("/act/workflow/delete")
    @ApiOperation(value = "[act-workflow]流程实例删除接口", notes = "[act-workflow]流程实例删除接口")
    public ZtjoResponse<String> delete(@RequestParam("prcInstId") String prcInstId)
    {
        workflowService.delete(prcInstId);
        return ZtjoResponse.ok("流程删除成功！");
    }
}
