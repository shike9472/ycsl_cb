package com.ztjo.activiti.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.text.StrFormatter;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.ztjo.activiti.component.WorkflowComponent;
import com.ztjo.activiti.service.WorkflowService;
import com.ztjo.common.properties.ActBackLockProperties;
import com.ztjo.core.utils.SecurityUtils;
import com.ztjo.data.exception.YcslActClashException;
import com.ztjo.data.exception.YcslBizException;
import com.ztjo.data.pojo.dto.act.TaskBackOffDto;
import com.ztjo.data.pojo.dto.act.TaskSubmitDto;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.impl.identity.Authentication;
import org.activiti.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.repository.Model;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

import static com.ztjo.data.enums.SFEnums.SF_S;

/**
 * @author 陈彬
 * @version 2022/1/6
 * description：工作流执行及操作service impl
 */
@Slf4j
@Service
public class WorkflowServiceImpl implements WorkflowService {

    /** 工作流精细化工作组件 */
    @Autowired
    private WorkflowComponent workflowComponent;
    @Autowired
    private ActBackLockProperties actBackLockProperties;
    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    /** 工作流资源服务 */
    @Resource
    private RepositoryService repositoryService;
    @Resource
    private TaskService taskService;
    @Resource
    private RuntimeService runtimeService;

    /**
     * 流程启动
     * @param modelId
     */
    @Override
    public void start(String modelId) {
        Model model = repositoryService.getModel(modelId);
        if(ObjectUtil.isNull(model)) {
            throw new YcslBizException("模板不存在");
        }
        if(StrUtil.isBlank(model.getDeploymentId())) {
            throw new YcslBizException("流程还未成功部署");
        }
        this.start(model);
    }

    /**
     * 流程启动
     * @param model
     */
    @Override
    public void start(Model model) {
        ProcessDefinition definition = repositoryService
                .createProcessDefinitionQuery()
                .deploymentId(model.getDeploymentId()).singleResult();
        if(ObjectUtil.isNull(definition)) {
            throw new YcslBizException("请先定义流程（重新编辑流程并保存）");
        }
        this.start(definition);
    }

    /**
     * 流程启动
     * @param definition
     */
    @Override
    public void start(ProcessDefinition definition) {
        workflowComponent.startFlowByProcDefKey(definition.getKey());
    }

    /**
     * 任务签收
     * @param taskId
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public void sign(String taskId) {
        sign(taskId, SecurityUtils.curUname());
    }

    /**
     * 任务签收
     * @param taskId
     * @param username
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public void sign(String taskId, String username) {
        taskService.claim(taskId, username);
    }

    /**
     * 任务转办
     * @param taskId
     * @param tarUser
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public void transfer(String taskId, String tarUser) {
        taskService.addComment(taskId, null,
                StrFormatter.format("[{}] - 流程转办", SecurityUtils.curName()));
        taskService.setAssignee(taskId, tarUser);
    }

    /**
     * 任务提交
     * @param dto
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public void submit(TaskSubmitDto dto, Task task) {
        // 数据准备和检查
        String username = SecurityUtils.curUname();
        if(StrUtil.isBlank(dto.getComment())) {
            dto.setComment("agree");
        }
        // 批注人的名称  一定要写，不然查看的时候不知道人物信息
        Authentication.setAuthenticatedUserId(username);
        // 添加审批意见
        taskService.addComment(dto.getTaskId(), null, dto.getComment());
        // 提交时流程节点回退锁检查，防止同类型流程在该节点回退流程产生全局的提交异常
        if(!workflowComponent.checkAndWait4BackOff(task)) {
            throw new YcslActClashException(
                    StrFormatter.format(
                            "[{}]-任务提交失败！当前流程在[{}]节点存在任务回退锁，受其影响暂时无法完成提交！请稍后重试！",
                            dto.getTaskId(), task.getTaskDefinitionKey()
                    )
            );
        }
        // 提交任务
        taskService.complete(dto.getTaskId(), dto.getVariables());

        // 下一节点参与人员设置
        workflowComponent.nextTaskAssignOrCandidateSetting(task.getProcessInstanceId(), dto.getNextTaskUsers());
    }

    /**
     * 任务回退
     * @param dto
     */
    @Override
    public void backOff(TaskBackOffDto dto) {
        backOff(dto.getTaskId(), dto.getTarActivityDefKey(), dto.getComment());
    }

    /**
     * 流程回退转向设置，加流程步骤提交redis锁，加同步调用锁
     *      1. 防止流程转向期间，其它业务提交产生工作流提交方向异常
     *      2. 防止并发调用redis锁提前释放
     * @param taskId
     * @param activityId
     * @param comment
     */
    @Override
    public void backOff(String taskId, String activityId, String comment){
        log.info("转向准备！");
        // 1. 取流程定义信息
        ProcessDefinitionEntity processDefinition = workflowComponent.findProcessDefinitionEntityByTaskId(taskId);
        Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
        String sourceActivityId = task.getTaskDefinitionKey();

        // 2. 获取节点模板
        //      2.1 获取节点模板【当前】
        ActivityImpl currActivity = workflowComponent.findActivitiImpl(sourceActivityId, processDefinition);
        //      2.2 获取节点模板【目标】
        ActivityImpl pointActivity = workflowComponent.findActivitiImpl(activityId, processDefinition);

        // 3. 回退限制redis锁 - key定义
        final String lockKeyBack = actBackLockProperties.getLockKeyPre() + ":"+ currActivity.getProcessDefinition().getKey();
        final String lockKeySubmit = actBackLockProperties.getLockKeyPre()+ ":"+ currActivity.getId();
        // 4. 检查回退限制锁是否存在 - 尝试等待锁解除
        if(!workflowComponent.checkAndWait4BackOffAgain(lockKeyBack)) {
            throw new YcslActClashException(StrFormatter.format("[{}]流程存在其它正在执行的回退，请稍后重试回退！", processDefinition.getName()));
        }
        // 5. 生成redis回退双锁
        //      5.1 生成【流程回退锁】
        log.info("设置回退流程其它回退动作限制锁 - {}", lockKeyBack);
        redisTemplate.opsForValue().set(lockKeyBack,
                SF_S.getCode(), actBackLockProperties.getBackoffLockTimeout(), TimeUnit.MILLISECONDS);
        //      5.2 生成【节点提交锁】
        log.info("设置回退流程相同步骤其它提交限制锁 - {}", lockKeySubmit);
        redisTemplate.opsForValue().set(lockKeySubmit,
                SF_S.getCode(), actBackLockProperties.getSubmitLockTimeout(), TimeUnit.MILLISECONDS);

        // 6. 转向执行，执行后恢复转向
        workflowComponent.backOffTask(currActivity, pointActivity, taskId, activityId, comment);

        // 7. 释放redis回退双锁
        redisTemplate.delete(lockKeyBack);
        redisTemplate.delete(lockKeySubmit);
        log.info("释放回退流程提交限制锁成功 - {}", currActivity.getId());
        log.info("转向结束！");
    }

    /**
     * 流程挂起
     * @param task
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public void suspend(Task task) {
        taskService.addComment(task.getId(), null,
                StrFormatter.format("用户[{}] - 挂起任务[{}]", SecurityUtils.curName(), task.getId()));
        suspend(task.getProcessInstanceId());
    }

    /**
     * 流程挂起
     * @param prcInstId
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public void suspend(String prcInstId) {
        runtimeService.suspendProcessInstanceById(prcInstId);
    }

    /**
     * 流程激活
     * @param prcInstId
     */
    @Override
    public void activate(String prcInstId) {
        runtimeService.activateProcessInstanceById(prcInstId);
    }

    /**
     * 流程实例删除
     * @param prcInstId
     */
    @Override
    public void delete(String prcInstId) {
        runtimeService.deleteProcessInstance(prcInstId,
                StrFormatter.format("[{}]-[{}]删除流程-[{}]",
                        SecurityUtils.curName(), DateUtil.now(), prcInstId));
    }

}
