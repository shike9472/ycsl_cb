package com.ztjo.activiti.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.activiti.engine.impl.persistence.entity.TaskEntity;
import org.activiti.engine.task.Task;

/**
 * @author 陈彬
 * @version 2022/1/10
 * description：工作流相关查询service
 */
public interface ActQueryService {

    /**
     * 待签收任务查询
     * @param page
     * @return
     */
    IPage<TaskEntity> queryCommonTaskPage(Page page);

    /**
     * 待办任务查询
     * @param page
     * @return
     */
    IPage<TaskEntity> queryPrivateTaskPage(Page page);

}
