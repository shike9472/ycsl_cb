package com.ztjo.activiti.service;

import com.ztjo.data.pojo.dto.act.TaskBackOffDto;
import com.ztjo.data.pojo.dto.act.TaskSubmitDto;
import org.activiti.engine.repository.Model;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.task.Task;

/**
 * @author 陈彬
 * @version 2022/1/6
 * description：工作流执行及操作service
 */
public interface WorkflowService {

    /**
     * 流程启动
     * @param modelId
     */
    void start(String modelId);

    /**
     * 流程启动
     * @param model
     */
    void start(Model model);

    /**
     * 流程启动
     * @param definition
     */
    void start(ProcessDefinition definition);

    /**
     * 任务签收
     * @param taskId
     */
    void sign(String taskId);

    /**
     * 任务签收
     * @param taskId
     * @param username
     */
    void sign(String taskId, String username);

    /**
     * 任务转办
     * @param taskId
     * @param tarUser
     */
    void transfer(String taskId, String tarUser);

    /**
     * 任务提交
     * @param dto
     */
    void submit(TaskSubmitDto dto, Task task);

    /**
     * 任务回退
     * @param dto
     */
    void backOff(TaskBackOffDto dto);

    /**
     * 任务回退
     * @param sourceTaskId
     * @param tarActivityDefKey
     * @param comment
     */
    void backOff(String sourceTaskId, String tarActivityDefKey, String comment);

    /**
     * 流程挂起（任务入口）
     * @param task
     */
    void suspend(Task task);

    /**
     * 流程挂起
     * @param prcInstId
     */
    void suspend(String prcInstId);

    /**
     * 流程激活
     * @param prcInstId
     */
    void activate(String prcInstId);

    /**
     * 流程实例删除
     * @param prcInstId
     */
    void delete(String prcInstId);
}
