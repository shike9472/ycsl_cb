package com.ztjo.activiti.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.ztjo.activiti.service.ActModelProcessService;
import com.ztjo.data.exception.YcslBizException;
import com.ztjo.data.pojo.dto.act.ActModelCreateDto;
import com.ztjo.data.pojo.vo.act.ModelEntityVo;
import lombok.extern.slf4j.Slf4j;
import org.activiti.bpmn.converter.BpmnXMLConverter;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.editor.constants.ModelDataJsonConstants;
import org.activiti.editor.language.json.converter.BpmnJsonConverter;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.Model;
import org.activiti.engine.repository.ModelQuery;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.stream.Collectors;

import static com.ztjo.data.enums.SFEnums.SF_S;

/**
 * @author 陈彬
 * @version 2022/1/4
 * description：工作流模板与流程定义-部署组件service impl
 */
@Slf4j
@Service
public class ActModelProcessServiceImpl implements ActModelProcessService {

    @Autowired
    protected ObjectMapper objectMapper;

    @Resource
    private RepositoryService repositoryService;

    private static String MODEL_EDITOR_ENCODING = "utf-8";

    /**
     * 工作流模型分页加载
     * @param page
     * @param name
     * @param isDeployed
     * @return
     */
    @Override
    public IPage<ModelEntityVo> actModelsPage(Page page, String name, String isDeployed) {
        ModelQuery modelQuery = repositoryService.createModelQuery();
        if(StrUtil.isNotBlank(name)) {
            modelQuery = modelQuery.modelNameLike("%"+ name +"%");
        }
        if(SF_S.getCode().equals(isDeployed)) {
            modelQuery = modelQuery.deployed();
        }
        modelQuery = modelQuery.latestVersion().orderByLastUpdateTime().desc();
        // 总数设置
        page.setTotal(modelQuery.count());
        // 设置具体记录数量
        List<Model> rows = modelQuery.listPage((int)((page.getCurrent()-1) * page.getSize()), (int)page.getSize());
        List<ModelEntityVo> vos = rows.stream()
                .map(row -> Convert.convert(ModelEntityVo.class, row))
                .collect(Collectors.toList());
        page.setRecords(vos);
        return page;
    }

    /**
     * 创建model模型
     * @param dto
     * @return
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public Model create(ActModelCreateDto dto) {
        try {
            ObjectNode editorNode = objectMapper.createObjectNode();
            editorNode.put("id", "canvas");
            editorNode.put("resourceId", "canvas");
            ObjectNode properties = objectMapper.createObjectNode();
            properties.put("process_author", "jeesite");
            editorNode.put("properties", properties);
            ObjectNode stencilset = objectMapper.createObjectNode();
            stencilset.put("namespace", "http://b3mn.org/stencilset/bpmn2.0#");
            editorNode.put("stencilset", stencilset);

            Model modelData = repositoryService.newModel();
            modelData.setKey(StringUtils.defaultString(dto.getKey()));
            modelData.setName(dto.getName());
            modelData.setVersion(Integer.parseInt(String.valueOf(
                    repositoryService.createModelQuery().modelKey(modelData.getKey()).count() + 1)));

            ObjectNode modelObjectNode = objectMapper.createObjectNode();
            modelObjectNode.put(ModelDataJsonConstants.MODEL_NAME, dto.getName());
            modelObjectNode.put(ModelDataJsonConstants.MODEL_REVISION, modelData.getVersion());
            modelObjectNode.put(ModelDataJsonConstants.MODEL_DESCRIPTION,
                    StringUtils.defaultString(dto.getDescription()));
            modelData.setMetaInfo(modelObjectNode.toString());

            repositoryService.saveModel(modelData);
            repositoryService.addModelEditorSource(modelData.getId(),
                    editorNode.toString().getBytes(MODEL_EDITOR_ENCODING));
            return modelData;
        } catch (UnsupportedEncodingException e) {
            log.error("[流程模板创建] - 系统不支持的编码异常 -> ", e);
            throw new YcslBizException(StrUtil.format("系统不支持的编码异常 -> {}", e.getMessage()));
        } catch (Exception e) {
            log.error("[流程模板创建] - 系统其它运行异常 -> ", e);
            throw new YcslBizException(StrUtil.format("系统运行异常 -> {}", e.getMessage()));
        }
    }

    /**
     * 模板删除
     * @param modelId
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public void delete(String modelId) {
        /**
         * 下面的几项删除将被放在基础模块中，做工作流业务分离
         */
        // todo 删除流程本身的扩展设置(扩展开发完成后补全)

        // todo 删除流程步骤的扩展配置(扩展开发完成后补全)

        // todo 删除流程附件条目设置(扩展开发完成后补全)

        // 删除工作流模板
        repositoryService.deleteModel(modelId);
    }

    /**
     * 流程部署
     * @param model
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public void deploy(Model model) throws IOException {
        byte[] bytes = repositoryService.getModelEditorSource(model.getId());
        if (bytes == null) {
            throw new YcslBizException("bpmn流程模型数据为空，请先点击[编辑]设计流程并保存");
        }
        JsonNode modelNode = new ObjectMapper().readTree(bytes);
        BpmnModel bpmnModel = new BpmnJsonConverter().convertToBpmnModel(modelNode);
        if (CollectionUtil.isEmpty(bpmnModel.getProcesses())) {
            throw new YcslBizException("bpmn模型不符合要求，请至少设计一条主线流程");
        }
        byte[] bpmnBytes = new BpmnXMLConverter().convertToXML(bpmnModel);
        //发布流程
        String processName = model.getName() + ".bpmn20.xml";
        Deployment deployment = repositoryService.createDeployment()
                .name(model.getName())
                .addString(processName, new String(bpmnBytes, "UTF-8"))
                .deploy();
        model.setDeploymentId(deployment.getId());
        repositoryService.saveModel(model);

        /**
         * todo 设置将被写在基础配置中开独立接口，做工作流+配置分离
         * todo 流程步骤的自定义扩展配置项 merge，
         *      按要求请封装为一个独立的 step setting init 方法
         */
    }
}
