package com.ztjo.activiti.service.impl;

import cn.hutool.core.convert.Convert;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ztjo.activiti.service.ActQueryService;
import com.ztjo.core.utils.SecurityUtils;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.TaskService;
import org.activiti.engine.impl.persistence.entity.TaskEntity;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author 陈彬
 * @version 2022/1/10
 * description：工作流相关查询service impl
 */
@Slf4j
@Service
public class ActQueryServiceImpl implements ActQueryService {

    @Autowired
    private TaskService taskService;

    /**
     * 待签收任务查询
     *      todo lazy load bug 等待业务实例扩展表创建后重写查询逻辑解决
     * @param page
     * @return
     */
    @Override
    public IPage<TaskEntity> queryCommonTaskPage(Page page) {
        List<Task> tasks = taskService.createTaskQuery()
                .taskCandidateUser(SecurityUtils.curUname())
                .orderByTaskCreateTime().desc()
                .listPage((int)((page.getCurrent()-1) * page.getSize()), (int)page.getSize());
        return page.setRecords(tasks.stream().map(task -> Convert.convert(TaskEntity.class, task)).collect(Collectors.toList()));
    }

    /**
     * 待办任务查询
     *      todo lazy load bug 等待业务实例扩展表创建后重写查询逻辑解决
     * @param page
     * @return
     */
    @Override
    public IPage<TaskEntity> queryPrivateTaskPage(Page page) {
        List<Task> tasks = taskService.createTaskQuery()
                .taskAssignee(SecurityUtils.curUname())
                .orderByTaskCreateTime().desc()
                .listPage((int)((page.getCurrent()-1) * page.getSize()), (int)page.getSize());
        return page.setRecords(tasks.stream().map(task -> Convert.convert(TaskEntity.class, task)).collect(Collectors.toList()));
    }
}
