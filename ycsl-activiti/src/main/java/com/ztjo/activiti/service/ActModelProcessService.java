package com.ztjo.activiti.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ztjo.data.pojo.dto.act.ActModelCreateDto;
import com.ztjo.data.pojo.vo.act.ModelEntityVo;
import org.activiti.engine.repository.Model;

import java.io.IOException;

/**
 * @author 陈彬
 * @version 2022/1/4
 * description：工作流模板与流程定义-部署组件service
 */
public interface ActModelProcessService {

    /**
     * 工作流模型分页加载
     * @param page
     * @param name
     * @param isDeployed
     * @return
     */
    IPage<ModelEntityVo> actModelsPage(Page page, String name, String isDeployed);

    /**
     * 创建model模型
     * @param dto
     * @return
     */
    Model create(ActModelCreateDto dto);

    /**
     * 模板删除
     * @param modelId
     */
    void delete(String modelId);

    /**
     * 流程部署
     * @param model
     */
    void deploy(Model model) throws IOException;
}
