package com.ztjo.activiti.component;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.ztjo.common.properties.ActBackLockProperties;
import com.ztjo.core.utils.SecurityUtils;
import com.ztjo.data.pojo.dto.act.TaskSubmitDto;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.HistoryService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.impl.RepositoryServiceImpl;
import org.activiti.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.activiti.engine.impl.persistence.entity.TaskEntity;
import org.activiti.engine.impl.pvm.PvmTransition;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.impl.pvm.process.ProcessDefinitionImpl;
import org.activiti.engine.impl.pvm.process.TransitionImpl;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.IdentityLink;
import org.activiti.engine.task.IdentityLinkType;
import org.activiti.engine.task.Task;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.ztjo.data.enums.SFEnums.SF_S;

/**
 * @author 陈彬
 * @version 2022/1/7
 * description：工作流执行及操作服务组件
 */
@Slf4j
@Component
public class WorkflowComponent {

    /** 工作流回退流程使用锁定参数 */
    @Autowired
    private ActBackLockProperties actBackLockProperties;
    @Autowired
    private RedisTemplate<String, String> redisTemplate;
    @Resource
    private RuntimeService runtimeService;
    @Resource
    private TaskService taskService;
    @Resource
    private RepositoryService repositoryService;
    @Resource
    private HistoryService historyService;

    public static final String ACT_MODEL_START = "start";
    public static final String ACT_MODEL_END = "end";

    /**
     * 流程的实际启动方法
     * @param procDefKey
     * @return
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public void startFlowByProcDefKey(String procDefKey) {
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(procDefKey, CollUtil.newHashMap());
        List<Task> tasks = taskService.createTaskQuery().processInstanceId(processInstance.getId()).list();
        if(CollUtil.isNotEmpty(tasks)) {
            tasks.forEach(task -> {
                taskService.claim(task.getId(), SecurityUtils.curUname());
            });
        }
    }

    /**
     * 下一节点参与人员设置
     * todo 注意：如果步骤配置中使用选择下一候选人的候选人策略，
     *          即便流程任务配置有监听器，监听器中应做好策略判断，主动选择模式的时候 -> 不再默认生成候选人集合
     * @param processInstId
     * @param nextTaskUsers
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public void nextTaskAssignOrCandidateSetting(String processInstId, List<TaskSubmitDto.nextTaskUser> nextTaskUsers) {
        if(CollUtil.isNotEmpty(nextTaskUsers)) {
            // 参数做分类
            Map<String, TaskSubmitDto.nextTaskUser> nextTaskUserMap = nextTaskUsers.stream()
                    .collect(Collectors.groupingBy(
                            TaskSubmitDto.nextTaskUser::getNextTaskDefKey,
                            Collectors.collectingAndThen(Collectors.toList(), l -> l.get(0))
                    ));
            // 查询现有的任务
            List<Task> tasks = taskService.createTaskQuery().processInstanceId(processInstId).list();
            if (CollUtil.isNotEmpty(tasks)) {
                tasks.forEach(task -> {
                    TaskSubmitDto.nextTaskUser nextTaskUser = nextTaskUserMap.get(task.getTaskDefinitionKey());
                    if (ObjectUtil.isNotNull(nextTaskUser)) {
                        if (StrUtil.isNotBlank(nextTaskUser.getNextTaskAssign())) {
                            // 指定执行人
                            taskService.claim(task.getId(), nextTaskUser.getNextTaskAssign());
                        } else if (CollUtil.isNotEmpty(nextTaskUser.getNextTaskCandidates())) {
                            for (String candidate : nextTaskUser.getNextTaskCandidates()) {
                                // 指定候选人
                                taskService.addCandidateUser(task.getId(), candidate);
                            }
                        }
                    }
                });
            }
        }
    }

    /**
     * 执行回退
     * @param currActivity      -   当前节点
     * @param pointActivity     -   目标节点
     * @param taskId            -   当前任务
     * @param activityId        -   目标节点id
     * @param comment           -   回退意见
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, rollbackFor = Exception.class)
    public void backOffTask(
            ActivityImpl currActivity,
            ActivityImpl pointActivity,
            String taskId,
            String activityId,
            String comment)
    {
        log.info("转向开始！");
        // 清空当前流向
        List<PvmTransition> oriPvmTransitionList = clearTransition(currActivity);
        // 初始化创建新流向 - 此时未指定新流向的目标节点
        TransitionImpl newTransition = currActivity.createOutgoingTransition();
        // 设置新流向的目标节点
        newTransition.setDestination(pointActivity);
        log.info("模板转向设置结束！");

        //暂时不需要打回修改//添加审批意见
        taskService.addComment(taskId, null, comment);
        // 执行提交
        if (ACT_MODEL_START.equals(activityId.toLowerCase())){
            taskService.complete(taskId, null);
        }else {
            taskService.complete(taskId);
        }
        log.info("提交动作执行结束！");
        // 删除目标节点新流入
        pointActivity.getIncomingTransitions().remove(newTransition);
        // 还原以前流向
        restoreTransition(currActivity, oriPvmTransitionList);
        log.info("模板转向设置恢复！");
    }

    /**
     * 根据任务ID和节点ID获取活动节点 <br>
     * @param activityId 流程节点id
     * @return
     */
    public ActivityImpl findActivitiImpl(String activityId, ProcessDefinitionEntity processDefinition) {
        // 根据流程定义，获取该流程实例的结束节点
        if (ACT_MODEL_END.equals(activityId.toLowerCase())) {
            for (ActivityImpl activityImpl : processDefinition.getActivities()) {
                List<PvmTransition> pvmTransitionList = activityImpl
                        .getOutgoingTransitions();
                if (pvmTransitionList.isEmpty()) {
                    return activityImpl;
                }
            }
        }
        // 根据节点ID，获取对应的活动节点
        ActivityImpl activityImpl = ((ProcessDefinitionImpl) processDefinition)
                .findActivity(activityId);
        return activityImpl;
    }

    /**
     * 检查以及等待回退执行
     * @param task
     */
    public boolean checkAndWait4BackOff(Task task) {
        log.info("检查回退设置的提交限制锁是否存在 - {}", task.getTaskDefinitionKey());
        final String lockKey = actBackLockProperties.getLockKeyPre()+ ":"+ task.getTaskDefinitionKey();
        for(int i=0; i<50; i++) {
            String lock = redisTemplate.opsForValue().get(lockKey);
            if (SF_S.getCode().equals(lock)) {
                log.info("[{}]提交锁<存在>！", lockKey);
                try {
                    Thread.sleep(actBackLockProperties.getLockSubmitTryTime());
                } catch (InterruptedException e) {
                    log.error("[提交锁]执行线程等待异常！", e);
                }
            } else {
                log.info("[{}]提交锁<不存在>！", lockKey);
                return true;
            }
        }
        // 最后一次检查
        String lock = redisTemplate.opsForValue().get(lockKey);
        if(SF_S.getCode().equals(lock)) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * 检查回退设置的其它回退限制锁是否存在
     * @param lockKey
     */
    public boolean checkAndWait4BackOffAgain(String lockKey) {
        log.info("检查回退设置的其它回退限制锁是否存在 - {}", lockKey);
        // 是否存在其它回退
        if (!redisTemplate.opsForValue().getOperations().hasKey(lockKey)) {
            return true;
        }
        for(int i=0; i<100; i++) {
            if (redisTemplate.opsForValue().getOperations().hasKey(lockKey)) {
                log.info("[{}]回退锁<存在>！", lockKey);
                try {
                    Thread.sleep(actBackLockProperties.getLockBackoffDoTime());
                } catch (InterruptedException e) {
                    log.error("[回退锁检查]执行线程等待异常！", e);
                }
            } else {
                log.info("[{}]回退锁<不存在>！", lockKey);
                return true;
            }
        }
        return false;
    }

    /**
     * 根据任务ID获取流程定义
     *
     * @param taskId 任务ID
     * @return
     */
    public ProcessDefinitionEntity findProcessDefinitionEntityByTaskId(String taskId) {
        // 取得流程定义
        ProcessDefinitionEntity processDefinition = (ProcessDefinitionEntity) ((RepositoryServiceImpl) repositoryService)
                .getDeployedProcessDefinition(findTaskDefinitionId(taskId));
        if (processDefinition == null) {
            throw new RuntimeException("流程定义未找到!");
        }
        return processDefinition;
    }

    /**
     * 取任务定义id
     * @param taskId
     * @return
     */
    public String findTaskDefinitionId(String taskId){
        TaskEntity task = (TaskEntity) taskService.createTaskQuery().taskId(
                taskId).singleResult();
        if (task == null){
            HistoricTaskInstance taskInstance = historyService.createHistoricTaskInstanceQuery().taskId(taskId).singleResult();
            return taskInstance.getProcessDefinitionId();
        }
        return  task.getProcessDefinitionId();
    }

    /**
     * 清空任务候选人方法
     * @param taskId
     */
    public void clearTaskCandidates(String taskId) {
        List<IdentityLink> identityLinks = taskService.getIdentityLinksForTask(taskId);
        clearTaskCandidates(taskId, identityLinks);
    }

    /**
     * 清空任务候选人方法
     * @param taskId
     * @param identityLinks
     */
    public void clearTaskCandidates(String taskId, List<IdentityLink> identityLinks) {
        if(CollUtil.isNotEmpty(identityLinks)) {
            identityLinks.forEach(i -> {
                if (IdentityLinkType.CANDIDATE.equals(i.getType())) {
                    taskService.deleteCandidateUser(taskId, i.getUserId());
                }
            });
        }
    }

    /**
     * 确认流程有无完结
     * @param procInstId
     * @return
     */
    public Boolean procIsEnd(String procInstId) {
        // 取运行时流程实例
        ProcessInstance singleResult = runtimeService
                .createProcessInstanceQuery()
                .processInstanceId(procInstId).singleResult();

        if (ObjectUtil.isNull(singleResult)) {
            //执行完毕
            return true;
        } else {
            //正在执行
            return false;
        }
    }

    /**
     * 清空指定活动节点流向
     *
     * @param activityImpl 活动节点
     * @return 节点流向集合
     */
    private List<PvmTransition> clearTransition(ActivityImpl activityImpl) {
        // 存储当前节点所有流向临时变量
        List<PvmTransition> oriPvmTransitionList = new ArrayList<PvmTransition>();
        // 获取当前节点所有流向，存储到临时变量，然后清空
        List<PvmTransition> pvmTransitionList = activityImpl
                .getOutgoingTransitions();
        for (PvmTransition pvmTransition : pvmTransitionList) {
            oriPvmTransitionList.add(pvmTransition);
        }
        pvmTransitionList.clear();
        return oriPvmTransitionList;
    }

    /**
     * 还原指定活动节点流向
     *
     * @param activityImpl         活动节点
     * @param oriPvmTransitionList 原有节点流向集合
     */
    private void restoreTransition(ActivityImpl activityImpl,
                                   List<PvmTransition> oriPvmTransitionList) {
        // 清空现有流向
        List<PvmTransition> pvmTransitionList = activityImpl
                .getOutgoingTransitions();
        pvmTransitionList.clear();
        // 还原以前流向
        for (PvmTransition pvmTransition : oriPvmTransitionList) {
            pvmTransitionList.add(pvmTransition);
        }
    }
}
