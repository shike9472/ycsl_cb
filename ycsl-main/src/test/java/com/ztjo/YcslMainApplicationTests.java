package com.ztjo;

import com.ztjo.core.service.biz.SjInfoJyhtxxbDetailService;
import com.ztjo.data.entity.biz.SjInfoJyhtxxbDetail;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class YcslMainApplicationTests {
    @Autowired
    private SjInfoJyhtxxbDetailService sjInfoJyhtxxbDetailService;

    @Test
    public void contextLoads() {
        SjInfoJyhtxxbDetail detail = new SjInfoJyhtxxbDetail();
        detail.setInfoId(12345L);
        detail.setSfbhfcfsss("123");
        sjInfoJyhtxxbDetailService.save(detail);
    }

}

