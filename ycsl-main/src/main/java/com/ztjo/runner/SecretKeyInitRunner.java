package com.ztjo.runner;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSONArray;
import com.github.ag.core.util.RsaKeyHelper;
import com.ztjo.common.properties.AuthCustomProperties;
import com.ztjo.common.properties.RedisCustomProperties;
import com.ztjo.common.utils.jwt.AESUtil;
import com.ztjo.file.settings.FtpSettings;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.Map;
import java.util.Set;

import static com.ztjo.data.constants.RedisKeyHeaders.*;
import static com.ztjo.file.settings.GlobalControlSettings.ftpEnable;
import static com.ztjo.file.settings.GlobalControlSettings.ftpPoolEnable;

/**
 * @author 陈彬
 * @version 2021/7/22
 * description：密钥初始化工具，spring工程启动后初始化用户 公钥/私钥
 */
@Slf4j
@Configuration
public class SecretKeyInitRunner implements CommandLineRunner {
    /**
     * 基础保存常量
     */
    private static final String REDIS_USER_PRI_KEY = "YCSL:AUTH:JWT:PRI";
    private static final String REDIS_USER_PUB_KEY = "YCSL:AUTH:JWT:PUB";

    /**
     * redis操作组件
     */
    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    /**
     * 用户鉴权配置项
     */
    @Autowired
    private AuthCustomProperties authProperties;

    /**
     * redis自定义配置项
     */
    @Autowired
    private RedisCustomProperties redisProperties;

    /**
     * AES加解密工具
     */
    @Autowired
    private AESUtil aecUtil;

    /**
     *
     */
    @Autowired
    private RsaKeyHelper rsaKeyHelper;

    /**
     * ftp设置
     */
    @Autowired
    private FtpSettings ftpSettings;

    /**
     * 系统初始化清理缓存时需要被清理的key前缀
     */
    private final String[] clearKeysPreWhenInit = {
            LOGIN_TOKEN_HEADER + "*",
            CACHE_VALUE_USER_GROUP_PERMISSION + "*",
            CACHE_VALUE_DEPART_AREA_GRANT + "*",
            CACHE_VALUE_MENU_ALL + "*",
            CACHE_VALUE_MENU_USER + "*",
            CACHE_VALUE_ELEMENT_ALL + "*",
            CACHE_VALUE_ELEMENT_USER + "*",
            CACHE_VALUE_DIC_MAIN_ALL + "*",
            CACHE_VALUE_DIC_ITEM_DICCODE + "*"
    };

    /**
     * 描述：系统用户公私钥初始化方法
     * 作者：陈彬
     * 日期：2021/7/22
     * 参数：[args]
     * 返回：void
     * 更新记录：更新人：{}，更新日期：{}
     */
    @Override
    public void run(String... args) throws Exception {
        /**
         * 1. 缓存清理
         */
        if(redisProperties.isClearWhenInit()) {
            log.info("正在清空现有缓存记录（系统初始化要求）");
            Set<String> keys = CollUtil.newHashSet();
            for(String key: clearKeysPreWhenInit) {
                keys.addAll(redisTemplate.keys(key));
            }
            log.info("clear redis keys -> {}", JSONArray.toJSONString(keys));
            long count = redisTemplate.delete(keys);
            log.info("清空现有缓存记录（系统初始化要求）结束，成功清理缓存条数 -> {}", count);
        }

        /**
         * 2. 用户密码加密密钥初始化
         */
        log.info("开始公钥/私钥初始化...");
        boolean flag = false;
        if (redisTemplate.hasKey(REDIS_USER_PRI_KEY)&&redisTemplate.hasKey(REDIS_USER_PUB_KEY)) {
            try {
                authProperties.setUserPriKey(rsaKeyHelper.toBytes(aecUtil.decrypt(redisTemplate.opsForValue().get(REDIS_USER_PRI_KEY))));
                authProperties.setUserPubKey(rsaKeyHelper.toBytes(redisTemplate.opsForValue().get(REDIS_USER_PUB_KEY)));
            }catch (Exception e){
                log.error("[异常] - 初始化公钥/密钥异常！正在尝试重新初始化公钥/密钥！",e);
                flag = true;
            }
        } else {
            flag = true;
        }
        if(flag){
            Map<String, byte[]> keyMap = rsaKeyHelper.generateKey(authProperties.getJwt().getRsaSecret());
            authProperties.setUserPriKey(keyMap.get("pri"));
            authProperties.setUserPubKey(keyMap.get("pub"));
            redisTemplate.opsForValue().set(REDIS_USER_PRI_KEY, aecUtil.encrypt(rsaKeyHelper.toHexString(keyMap.get("pri"))));
            redisTemplate.opsForValue().set(REDIS_USER_PUB_KEY, rsaKeyHelper.toHexString(keyMap.get("pub")));
        }
        log.info("完成公钥/密钥的初始化...pri-{}，pub-{}",
                authProperties.getUserPriKey().hashCode(), authProperties.getUserPubKey().hashCode());

        /**
         * 3. 全局的ftp池化关键组件配置
         */
        log.info("全局FTP使用环境配置！");
        if(ObjectUtil.isNotNull(ftpSettings)) {
            ftpEnable = ftpSettings.isEnable();
            log.info("ftp使用开闭状态设置成功 - {}", ftpEnable);
            ftpPoolEnable = ftpSettings.isUsePool();
            log.info("ftp使用池化管理开闭状态设置成功 - {}", ftpPoolEnable);
        }
        log.info("全局FTP使用环境配置结束");
    }
}
