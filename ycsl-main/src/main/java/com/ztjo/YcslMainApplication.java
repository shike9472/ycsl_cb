package com.ztjo;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
import com.spring4all.swagger.EnableSwagger2Doc;
import org.activiti.spring.boot.SecurityAutoConfiguration;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author 陈彬
 * @version 2021/8/18
 * description：一窗受理主程序启动类
 *      全局获取用户信息，此系统提供了两种方式：
 *          1. 针对性较强，扩展性较低的：YcslContextHandler获取，方法脱胎于老A框架
 *          2. 通用性和扩展性非常强的spring-security方法，通过自定义SecurityUtils获取
 *                  可以做到，你的系统用户定义为什么就取得什么
 */
@EnableSwagger2Doc
@MapperScan("com.ztjo.*.mapper")
@EnableFeignClients({"com.ztjo.common.feign.middle"})
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class, DruidDataSourceAutoConfigure.class})
public class YcslMainApplication {
    public static void main(String[] args) {
        SpringApplication.run(YcslMainApplication.class, args);
    }
}
