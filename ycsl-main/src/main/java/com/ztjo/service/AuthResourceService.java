package com.ztjo.service;

import com.ztjo.data.pojo.vo.resource.ResourceElementVo;
import com.ztjo.data.pojo.vo.resource.ResourceMenuVo;

import java.util.List;

/**
 * @author 陈彬
 * @version 2021/10/28
 * description：用户登录资源加载service
 */
public interface AuthResourceService {

    /**
     * 加在用户菜单资源
     * @return
     */
    List<ResourceMenuVo> getUserMenus(Long uid);

    /**
     * 加载用户请求与按钮资源
     * @return
     */
    List<ResourceElementVo> getUserElements(Long uid);
}
