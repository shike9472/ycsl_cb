package com.ztjo.service;

import com.ztjo.data.entity.base.BaseElement;
import com.ztjo.data.entity.base.BaseMenu;
import com.ztjo.data.pojo.bo.MenuBo;
import com.ztjo.data.pojo.vo.model.TreeEntityVo;

import java.io.Serializable;
import java.util.List;

/**
 * @author 陈彬
 * @version 2021/12/15
 * description：菜单管理服务
 */
public interface MenuManageService {

    /**
     * 加载树形菜单列表
     * @return
     */
    List<TreeEntityVo<MenuBo>> loadTree();

    /**
     * 自定义菜单资源删除
     * @param id
     * @param elements
     * @return
     */
    Boolean removeById(Serializable id, List<BaseElement> elements);

    /**
     * 加载对应用户的菜单
     * @param uid
     * @return
     */
    List<BaseMenu> loadTarUserMenus(Long uid);
}
