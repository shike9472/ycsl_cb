package com.ztjo.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import com.ztjo.core.service.base.BaseElementService;
import com.ztjo.core.service.base.BaseMenuService;
import com.ztjo.core.service.base.BaseResourceAuthorityService;
import com.ztjo.core.utils.SecurityUtils;
import com.ztjo.core.utils.TreeUtils;
import com.ztjo.data.entity.base.BaseElement;
import com.ztjo.data.entity.base.BaseMenu;
import com.ztjo.data.pojo.bo.MenuBo;
import com.ztjo.data.pojo.vo.model.TreeEntityVo;
import com.ztjo.service.MenuManageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.ztjo.data.constants.RedisKeyHeaders.CACHE_VALUE_MENU_USER;

/**
 * @author 陈彬
 * @version 2021/12/15
 * description：菜单管理逻辑控制服务
 */
@Slf4j
@Service
public class MenuManageServiceImpl implements MenuManageService {

    @Autowired
    private BaseMenuService menuService;
    @Autowired
    private BaseElementService elementService;
    @Autowired
    private BaseResourceAuthorityService resourceAuthorityService;

    /**
     * 加载树形菜单列表
     * @return
     */
    @Override
    public List<TreeEntityVo<MenuBo>> loadTree() {
        // 加载菜单资源
        List<BaseMenu> pool = menuService.list();
        return menuService.loadTree(pool);
    }

    /**
     * 自定义菜单资源删除
     * @param id
     * @param elements
     * @return
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT,
            rollbackFor = Exception.class)
    public Boolean removeById(Serializable id, List<BaseElement> elements) {
        if(CollUtil.isNotEmpty(elements)) {
            // 删除菜单下element
            elementService.removeByMenuId(Long.parseLong(id.toString()));
            // 删除组件下element权限
            resourceAuthorityService.removeByElementIdsInMenu(elements.stream().map(BaseElement::getId).collect(Collectors.toList()));
        }
        // 删除涉及的资源（包括）与角色的关系
        resourceAuthorityService.removeByMenuIds(CollUtil.newArrayList(Long.parseLong(id.toString())));
        // 删除指定菜单
        return menuService.removeById(id);
    }

    /**
     * 加载对应用户的菜单
     * @param uid
     * @return
     */
    @Override
    @Cacheable(value = CACHE_VALUE_MENU_USER, key = "#p0", unless = "#result.isEmpty()")
    public List<BaseMenu> loadTarUserMenus(Long uid) {
        Set<String> groups = SecurityUtils.getGroups();
        return menuService.getMenusByGroups(groups);
    }

}
