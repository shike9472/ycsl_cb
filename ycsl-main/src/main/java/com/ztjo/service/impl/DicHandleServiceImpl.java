package com.ztjo.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ztjo.core.service.base.DicItemService;
import com.ztjo.core.service.base.DicMainService;
import com.ztjo.core.utils.TreeUtils;
import com.ztjo.data.entity.base.DicItem;
import com.ztjo.data.entity.base.DicMain;
import com.ztjo.data.exception.YcslBizException;
import com.ztjo.data.pojo.bo.DicMainBo;
import com.ztjo.data.pojo.vo.model.TreeEntityVo;
import com.ztjo.service.DicHandleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author 陈彬
 * @version 2021/12/24
 * description：字典处理service impl
 */
@Slf4j
@Service
public class DicHandleServiceImpl implements DicHandleService {
    @Autowired
    private DicMainService dicMainService;
    @Autowired
    private DicItemService dicItemService;

    /**
     * 按树形加载字典目录
     * @return
     */
    @Override
    public List<TreeEntityVo<DicMainBo>> loadDicMainTree() {
        List<DicMain> pool = dicMainService.list();
        List<DicMainBo> bos = pool.stream().map(p -> Convert.convert(DicMainBo.class, p)).collect(Collectors.toList());
        return TreeUtils.getTreeUpFromPool(bos);
    }

    /**
     * 根据dic_main的dicCode加载字典项
     * @param dicCode
     * @return
     */
    @Override
    public List<DicItem> loadByMainCode(String dicCode) {
        return dicItemService.loadByMainCode(dicCode);
    }

    /**
     * 根据id删除字典配置项
     * @param id
     * @return
     */
    @Override
    public boolean deleteById(Serializable id) {
        DicItem item = dicItemService.getById(id);
        return dicItemService.removeById(item);
    }

    /**
     * item数据批量删除
     * @param ids
     */
    @Override
    public void removeItemsByIds(List<Long> ids) {
        if(CollUtil.isNotEmpty(ids)) {
            List<DicItem> items = dicItemService.list(Wrappers.<DicItem>lambdaQuery().in(DicItem::getId, ids));
            if (CollUtil.isNotEmpty(items)) {
                Set<String> dicSet = items.stream().map(DicItem::getDicCode).collect(Collectors.toSet());
                if(dicSet.size()>1) {
                    throw new YcslBizException("批量删除必须删除相同的字典目录下的设定值！传入的id集合包含不同的字典目录项");
                }
                dicItemService.removeBatch(items.get(0).getDicCode(), ids);
            }
        }
    }

}
