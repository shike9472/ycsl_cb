package com.ztjo.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ztjo.core.service.base.*;
import com.ztjo.core.utils.SecurityUtils;
import com.ztjo.data.entity.base.*;
import com.ztjo.data.exception.YcslBizException;
import com.ztjo.data.pojo.bo.DepartBo;
import com.ztjo.data.pojo.dto.RelationModifyDto;
import com.ztjo.data.pojo.vo.model.RelationModifyConditionVo;
import com.ztjo.data.pojo.vo.model.TreeEntityVo;
import com.ztjo.service.OrgAndUserManageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.ztjo.data.enums.SFEnums.SF_F;
import static com.ztjo.data.enums.SFEnums.SF_S;

/**
 * @author 陈彬
 * @version 2021/12/27
 * description：组织机构相关service impl
 */
@Slf4j
@Service
public class OrgAndUserManageServiceImpl implements OrgAndUserManageService {

    @Autowired
    private BaseUserService userService;
    @Autowired
    private BasePositionService positionService;
    @Autowired
    private BaseDepartService departService;
    @Autowired
    private BaseAreaService areaService;
    @Autowired
    private BaseDepartAreaService departAreaService;
    @Autowired
    private BasePositionUserService positionUserService;

    /**
     * 获取用户租户部门信息
     * @return
     */
    @Override
    public List<BaseDepart> getTenantDeparts() {
        if(SecurityUtils.isAdmin()) {
            return departService.list();
        } else {
            Long tenantId = SecurityUtils.curTenantId();
            if(ObjectUtil.isNotNull(tenantId)) {
                return departService.list(
                        Wrappers.<BaseDepart>lambdaQuery()
                                .eq(BaseDepart::getTenantId, tenantId)
                );
            }
        }
        return CollUtil.newArrayList();
    }

    /**
     * 获取用户租户部门信息
     * @return
     */
    @Override
    public List<DepartBo> getTenantDepartBos() {
        List<BaseDepart> departs = getTenantDeparts();
        if(ObjectUtil.isNotNull(departs)) {
            return Convert.toList(DepartBo.class, departs);
        }
        return CollUtil.newArrayList();
    }

    /**
     * 根据id删除部门信息
     * @param id
     * @return
     */
    @Override
    public boolean removeDepartById(Long id) {
        if(ObjectUtil.isNull(id)) {
            throw new YcslBizException("部门删除时，请传入部门id!");
        }
        List<BaseDepart> departs = departService.list(
                Wrappers.<BaseDepart>lambdaQuery()
                        .eq(BaseDepart::getParentId, id)
        );
        if(CollUtil.isNotEmpty(departs)) {
            throw new YcslBizException("被删除部门下存在下级部门，请先清理下级部门！");
        }
        return departService.removeById(id);
    }

    /**
     * 加载部门下用户信息（分页）（区分租户）
     * @param page
     * @param user
     * @param notLoadAlreadyDelete - 不加载已被删除的用户信息(默认给true即可)
     * @return
     */
    @Override
    public IPage<BaseUser> loadTenantUserPage(Page page, BaseUser user, boolean notLoadAlreadyDelete) {
        if(!SecurityUtils.isAdmin()) {
            user.setTenantId(SecurityUtils.curTenantId());
        }
        IPage<BaseUser> users = userService.page(
                page,
                Wrappers.<BaseUser>lambdaQuery()
                        .eq(StrUtil.isNotBlank(user.getUsername()), BaseUser::getUsername, user.getUsername())
                        .likeLeft(StrUtil.isNotBlank(user.getName()), BaseUser::getName, user.getName())
                        .eq(ObjectUtil.isNotNull(user.getDepartId()), BaseUser::getDepartId, user.getDepartId())
                        .eq(ObjectUtil.isNotNull(user.getTenantId()), BaseUser::getTenantId, user.getTenantId())
                        // 管理员查询全部被删除用户时可以添加该条件 - user.getIsDeleted()=1
                        .eq(StrUtil.isNotBlank(user.getIsDeleted()), BaseUser::getIsDeleted, user.getIsDeleted())
                        // 普通查询默认给 notLoadAlreadyDelete = true，不再加载已被删除的用户信息
                        .and(
                                notLoadAlreadyDelete,
                                qw -> qw.isNull(BaseUser::getIsDeleted).or().eq(BaseUser::getIsDeleted, SF_F.getCode())
                        )
        );
        addDepartNames2Users(users);
        return users;
    }

    /**
     * 根据id作废用户信息
     * @param id
     * @return
     */
    @Override
    public boolean disableUserById(Long id) {
        BaseUser user = new BaseUser();
        user.setId(id);
        user.setIsDisabled(SF_S.getCode());
        return userService.updateById(user);
    }

    /**
     * 根据id删除用户信息(逻辑删)
     * @param id
     * @return
     */
    @Override
    public boolean deleteUserById(Long id) {
        // 检查是否可以做删除
        userService.userCanRemove(id);
        // 做逻辑删除
        BaseUser user = new BaseUser();
        user.setId(id);
        user.setIsDeleted(SF_S.getCode());
        return userService.updateById(user);
    }

    /**
     * 通过id删除用户信息(物理删)
     * @param id
     * @return
     */
    @Override
    public boolean removeUserById(Long id) {
        // 检查是否可以做删除
        userService.userCanRemove(id);
        // 执行删除
        return userService.removeById(id);
    }

    /**
     * 加载已经授权的部门区域权限关系
     * @param departId
     * @return
     */
    @Override
    public RelationModifyConditionVo<List<BaseArea>> loadDepartAreasAlready(Long departId) {
        RelationModifyConditionVo<List<BaseArea>> vo = new RelationModifyConditionVo<>();
        vo.setTarId(departId);
        vo.setAlreadys(
                departService.gainDepartAreas(departId)
                        .stream()
                        .map(BaseArea::getId)
                        .collect(Collectors.toList())
        );
        vo.setPool(areaService.list());
        return vo;
    }

    /**
     * 部门的区域授权关系下发至子级部门中
     * @param areas
     * @param tarTree
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT,
            rollbackFor = Exception.class)
    public void grantAreas2Kids(List<BaseDepartArea> areas, List<TreeEntityVo<DepartBo>> tarTree)
    {
        for(TreeEntityVo<DepartBo> tar: tarTree) {
            areas.parallelStream()
                    .forEach(a -> {
                        a.setId(null);
                        a.setDepartId(null);
                    });
            RelationModifyDto<BaseDepartArea> dto = new RelationModifyDto<>();
            dto.setTarId(tar.getEntity().getId());
            dto.setCurEntitys(areas);
            departAreaService.modifyDepartAreas(dto);
            if(CollUtil.isNotEmpty(tar.getNodes())) {
                grantAreas2Kids(areas, tar.getNodes());
            }
        }
    }

    /**
     * 加载部门下岗位信息（分页）（区分租户）
     * @param page
     * @param position
     * @return
     */
    @Override
    public IPage<BasePosition> loadTenantPositionPage(Page page, BasePosition position) {
        // 租户设置
        if(!SecurityUtils.isAdmin()) {
            position.setTenantId(SecurityUtils.curTenantId());
        }
        // 分页查询部门下设置的岗位
        return positionService.page(
                page,
                Wrappers.<BasePosition>lambdaQuery()
                        .eq(StrUtil.isNotBlank(position.getCode()), BasePosition::getCode, position.getCode())
                        .eq(ObjectUtil.isNotNull(position.getDepartId()), BasePosition::getDepartId, position.getDepartId())
                        .eq(ObjectUtil.isNotNull(position.getTenantId()), BasePosition::getTenantId, position.getTenantId())
                        .like(StrUtil.isNotBlank(position.getName()), BasePosition::getName, position.getName()));
    }

    /**
     * 加载岗位用户设置
     * @param tenantId
     * @param positionId
     * @return
     */
    @Override
    public RelationModifyConditionVo<List<BaseUser>> loadPositionUsersAlready(Long tenantId, Long positionId) {
        RelationModifyConditionVo<List<BaseUser>> vo = new RelationModifyConditionVo<>();
        vo.setTarId(positionId);
        vo.setPool(userService.list(Wrappers.<BaseUser>lambdaQuery().eq(BaseUser::getTenantId, tenantId)));
        vo.setAlreadys(
                positionUserService.list(
                        Wrappers.<BasePositionUser>lambdaQuery()
                                .eq(BasePositionUser::getPositionId, positionId))
                        .stream()
                        .map(BasePositionUser::getUserId)
                        .collect(Collectors.toList())
        );
        return vo;
    }

    /**
     * 加载相同租户的部门
     * @param tenantId
     * @return
     */
    @Override
    public List<BaseDepart> loadDepartsByTenant(Long tenantId) {
        return departService.list(Wrappers.<BaseDepart>lambdaQuery().eq(BaseDepart::getTenantId, tenantId));
    }

    /**
     * 给传入用户添加部门(分页结果)
     * @param userIPage
     */
    public void addDepartNames2Users(IPage<BaseUser> userIPage) {
        List<BaseUser> users = userIPage.getRecords();
        addDepartNames2Users(users);
    }

    /**
     * 给传入用户添加部门
     * @param users
     */
    public void addDepartNames2Users(List<BaseUser> users) {
        if(users.size()<4) {
            for(BaseUser user: users) {
                if(ObjectUtil.isNotNull(user.getDepartId())) {
                    BaseDepart depart = departService.getById(user.getDepartId());
                    if(ObjectUtil.isNotNull(depart)) {
                        user.setDepartName(depart.getName());
                    }
                }
            }
        } else {
            List<Long> ids = users.stream().map(BaseUser::getDepartId).distinct().collect(Collectors.toList());
            if(CollUtil.isNotEmpty(ids)) {
                List<BaseDepart> departs = departService.listByIds(ids);
                if(CollUtil.isNotEmpty(departs)) {
                    Map<Long, List<BaseUser>> userMap = users.stream()
                            .filter(u -> ObjectUtil.isNotNull(u.getDepartId()))
                            .collect(Collectors.groupingBy(BaseUser::getDepartId));
                    for(BaseDepart depart: departs) {
                        List<BaseUser> userList = userMap.get(depart.getId());
                        if(CollUtil.isNotEmpty(userList)) {
                            userList.forEach(u -> u.setDepartName(depart.getName()));
                        }
                    }
                }
            }
        }
    }
}
