package com.ztjo.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ObjectUtil;
import com.ztjo.core.service.base.BaseElementService;
import com.ztjo.core.service.base.BaseMenuService;
import com.ztjo.core.utils.SecurityUtils;
import com.ztjo.data.entity.base.BaseElement;
import com.ztjo.data.entity.base.BaseMenu;
import com.ztjo.data.pojo.vo.resource.ResourceElementVo;
import com.ztjo.data.pojo.vo.resource.ResourceMenuVo;
import com.ztjo.service.AuthResourceService;
import com.ztjo.service.ElementManageService;
import com.ztjo.service.MenuManageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.ztjo.data.constants.RedisKeyHeaders.CACHE_VALUE_MENU_USER;
import static com.ztjo.data.constants.SpecialGroupConstants.GROUP_ALL_HOLD;

/**
 * @author 陈彬
 * @version 2021/10/28
 * description：用户登录资源加载service impl
 */
@Slf4j
@Service
public class AuthResourceServiceImpl implements AuthResourceService {

    @Autowired
    private BaseMenuService baseMenuService;

    @Autowired
    private BaseElementService baseElementService;

    @Autowired
    private MenuManageService menuManageService;

    @Autowired
    private ElementManageService elementManageService;

    /**
     * 加在用户菜单资源
     * @return
     */
    @Override
    public List<ResourceMenuVo> getUserMenus(Long uid) {
        List<BaseMenu> menus;
        if(SecurityUtils.isAdmin()) {
            // 管理员加载
            menus = baseMenuService.list();
        } else {
            // 普通用户加载
            menus = menuManageService.loadTarUserMenus(uid);
        }
        if(CollUtil.isEmpty(menus)) {
            return CollUtil.newArrayList();
        }
        List<ResourceMenuVo> resourceMenuVos = menus.stream()
                .filter(ObjectUtil::isNotNull).map(m -> {
                    ResourceMenuVo v = Convert.convert(ResourceMenuVo.class, m);
                    v.setMenu(m);
                    return v;
                }).collect(Collectors.toList());
        return resourceMenuVos;
    }

    /**
     * 加载用户请求与按钮资源
     * @return
     */
    @Override
    public List<ResourceElementVo> getUserElements(Long uid) {
        List<BaseElement> elements;
        if(SecurityUtils.isAdmin()) {
            // 管理员加载
            elements = baseElementService.list();
        } else {
            // 普通用户加载
            elements = elementManageService.loadTarUserElements(uid);
        }
        if(CollUtil.isEmpty(elements)) {
            return CollUtil.newArrayList();
        }
        List<ResourceElementVo> resourceElementVos = elements.stream()
                .filter(ObjectUtil::isNotNull).map(e -> {
                    ResourceElementVo v = Convert.convert(ResourceElementVo.class, e);
                    v.setElement(e);
                    return v;
                }).collect(Collectors.toList());
        return resourceElementVos;
    }
}
