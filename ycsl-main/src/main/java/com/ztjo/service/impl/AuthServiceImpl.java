package com.ztjo.service.impl;

import com.ztjo.core.component.AuthTokenComponent;
import com.ztjo.core.model.YcslUserDetails;
import com.ztjo.core.service.base.BaseUserService;
import com.ztjo.core.utils.SecurityUtils;
import com.ztjo.data.entity.base.BaseUser;
import com.ztjo.data.exception.YcslBizException;
import com.ztjo.data.pojo.dto.UserChangePasswordDto;
import com.ztjo.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author 陈彬
 * @version 2021/9/4
 * description：
 */
@Service
public class AuthServiceImpl implements AuthService {
    /**
     * 用户组件
     */
    @Autowired
    private BaseUserService userService;
    /**
     * 用户操作服务
     */
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    /**
     * token操作组件
     */
    @Autowired
    private AuthTokenComponent authTokenComponent;
    /**
     * 用户细节服务组件
     */
    @Autowired
    private UserDetailsService userDetailsService;
    /**
     * spring-security自定义登录实现组件
     */
    @Resource
    private AuthenticationManager authenticationManager;

    /**
     * 描述：常规登录方法
     * 作者：陈彬
     * 日期：2021/9/4
     * 参数：[username, password]
     * 返回：java.lang.String
     * 更新记录：更新人：{}，更新日期：{}
     */
    @Override
    public String login(String username, String password) {
        // 用户验证，基于spring-security
        Authentication authentication = null;
        try{
            /**
             * 该方法会执行两个操作：
             *      1. 按照securityConfig定义的bCryptPasswordEncoder方式去对传入的password做碰撞验证，
             *              -- 验证有可能会抛出BadCredentialsException异常，密码不匹配
             *      2. 调用UserDetailsServiceImpl.loadUserByUsername方法（已被我们重写了实现）加载用户细节数据
             *              -- 可能抛出用户不存在，已被删除，已被作废等异常
             */
            authentication = authenticationManager
                    .authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (Exception e) {
            //  异步记录异常
            if (e instanceof BadCredentialsException){
                throw new YcslBizException("输入用户名或密码错误！");
            } else {
                throw new YcslBizException(e.getMessage());
            }
        }
        // 取得用户细节信息
        YcslUserDetails loginUser = (YcslUserDetails) authentication.getPrincipal();
        // 生成token
        return authTokenComponent.gainToken(loginUser);
    }

    /**
     * 描述：免密登录方法
     * 作者：陈彬
     * 日期：2021/9/4
     * 参数：[username]
     * 返回：java.lang.String
     * 更新记录：更新人：{}，更新日期：{}
     */
    @Override
    public String loginIgnorePwd(String username) {
        // 查询目标用户并生成细节数据
        YcslUserDetails loginUser = (YcslUserDetails) userDetailsService.loadUserByUsername(username);
        // 生成token
        return authTokenComponent.gainToken(loginUser);
    }

    /**
     * 检查输入密码是否正确
     * @param password
     * @return
     */
    @Override
    public Boolean checkIptPassword(String password) {
        return bCryptPasswordEncoder.matches(password, SecurityUtils.curLoginUser().getPassword());
    }

    /**
     * 修改用户密码
     * @param userLoginDto
     */
    @Override
    public void changeSelfPassword(UserChangePasswordDto userLoginDto) {
        // 检查两次输入密码是否一致
        if(!userLoginDto.getNewPassword1().equals(userLoginDto.getNewPassword2())) {
            throw new YcslBizException("两次输入密码不一致");
        }
        // 处理用户更新数据
        BaseUser user = new BaseUser();
        user.setId(SecurityUtils.curUid());
        String newPassword = bCryptPasswordEncoder.encode(userLoginDto.getNewPassword1());
        user.setPassword(newPassword);

        // 清理redis中对应登录用户token
        authTokenComponent.clearLoginToken(SecurityUtils.curUid());
        // 更新全新的密码
        userService.updateById(user);
    }

    /**
     * 描述：登出方法
     * 作者：陈彬
     * 日期：2021/9/4
     * 参数：[token]
     * 返回：java.lang.Boolean
     * 更新记录：更新人：{}，更新日期：{}
     */
    @Override
    public Boolean logout(String token) {
        return null;
    }
}
