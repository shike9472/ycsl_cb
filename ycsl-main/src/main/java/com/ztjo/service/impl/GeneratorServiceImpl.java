package com.ztjo.service.impl;

import com.ztjo.data.pojo.bo.MysqlTablesBo;
import com.ztjo.core.mapper.sys.GeneratorMapper;
import com.ztjo.service.GeneratorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author 陈彬
 * @version 2022/1/12
 * description：
 */
@Slf4j
@Service
public class GeneratorServiceImpl implements GeneratorService {
    @Autowired
    private GeneratorMapper generatorMapper;

    /**
     * 加载mysql数据库表名的方法
     * @param name
     * @return
     */
    @Override
    public List<String> getMysqlTableNames(String name) {
        return generatorMapper.selectTables(name).stream().map(MysqlTablesBo::getName).collect(Collectors.toList());
    }
}
