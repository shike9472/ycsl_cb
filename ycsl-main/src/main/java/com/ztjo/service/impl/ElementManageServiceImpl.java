package com.ztjo.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.ztjo.core.service.base.BaseElementService;
import com.ztjo.core.service.base.BaseResourceAuthorityService;
import com.ztjo.core.utils.SecurityUtils;
import com.ztjo.data.entity.base.BaseElement;
import com.ztjo.service.ElementManageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import static com.ztjo.data.constants.RedisKeyHeaders.CACHE_VALUE_ELEMENT_USER;

/**
 * @author 陈彬
 * @version 2021/12/17
 * description：资源管理逻辑控制服务
 */
@Slf4j
@Service
public class ElementManageServiceImpl implements ElementManageService {
    @Autowired
    private BaseElementService elementService;
    @Autowired
    private BaseResourceAuthorityService resourceAuthorityService;

    /**
     * 根据id集合(menu菜单内的)删除对应资源
     * @param ids
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT,
            rollbackFor = Exception.class)
    public void removeInMenuByIds(List<Long> ids) {
        // 删除资源角色关系
        resourceAuthorityService.removeByElementIdsInMenu(ids);
        // 删除资源
        elementService.removeByIds(ids);
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT,
            rollbackFor = Exception.class)
    public Boolean removeById(Serializable id) {
        resourceAuthorityService.removeByElementIds(CollUtil.newArrayList(Long.parseLong(id.toString())));
        return elementService.removeById(id);
    }

    /**
     * 加载对应用户的资源
     * @param uid
     * @return
     */
    @Override
    @Cacheable(value = CACHE_VALUE_ELEMENT_USER, key = "#p0", unless = "#result.isEmpty()")
    public List<BaseElement> loadTarUserElements(Long uid) {
        Set<String> groups = SecurityUtils.getGroups();
        return elementService.getElementsByGroups(groups);
    }
}
