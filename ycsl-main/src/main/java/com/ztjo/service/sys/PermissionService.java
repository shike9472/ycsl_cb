package com.ztjo.service.sys;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.ztjo.core.model.YcslUserDetails;
import com.ztjo.core.service.base.BaseDepartService;
import com.ztjo.core.utils.SecurityUtils;
import com.ztjo.data.entity.base.BaseArea;
import com.ztjo.data.pojo.vo.resource.ResourceElementVo;
import com.ztjo.data.pojo.vo.resource.ResourceMenuVo;
import com.ztjo.service.AuthResourceService;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.ztjo.data.constants.SpecialGroupConstants.GROUP_ALL_HOLD;

/**
 * @author 陈彬
 * @version 2021/10/27
 * description：系统准入检查service
 */
@Slf4j
@Service("ps")
public class PermissionService {
    @Autowired
    private AuthResourceService authResourceService;
    @Autowired
    private BaseDepartService departService;

    @Resource
    private TaskService taskService;

    /**
     * 角色是否在登录用户角色列表
     * @param group
     * @return
     */
    public boolean hasTarGroup(String group) {
        if(StrUtil.isBlank(group)) {
            return false;
        }
        YcslUserDetails userDetails = SecurityUtils.curLoginUser();
        if(ObjectUtil.isNull(userDetails)) {
            return false;
        }
        Set<String> groups = userDetails.getPermissions();
        if(CollUtil.isEmpty(groups)) {
            return false;
        }
        return groups.contains(GROUP_ALL_HOLD) || groups.contains(group);
    }

    /**
     * 资源权限是否在用户权限范围
     * 此验证管理员可以无视
     * @param resource
     * @return
     */
    public boolean hasTarResource(String resource) {
        log.info("正在检查用户资源访问权限");
        if(StrUtil.isBlank(resource)) {
            return false;
        }
        YcslUserDetails userDetails = SecurityUtils.curLoginUser();
        if(ObjectUtil.isNull(userDetails)) {
            return false;
        }
        Set<String> groups = userDetails.getPermissions();
        if(CollUtil.isEmpty(groups)) {
            return false;
        }
        if(userIsAdmin()) {
            return true;
        }
        return hasTarMenu(resource) || hasTarElement(resource);
    }

    /**
     * 检查用户是否具备菜单权限
     * @param menu
     * @return
     */
    public boolean hasTarMenu(String menu) {
        log.info("正在检查用户菜单资源访问权限");
        // 取用户权限菜单
        List<ResourceMenuVo> menuPermissions = authResourceService.getUserMenus(SecurityUtils.curUid());
        // 检查权限菜单是否包含目标菜单
        if(CollUtil.isNotEmpty(menuPermissions)) {
            return menuPermissions.stream()
                    .filter(m -> StrUtil.isNotBlank(m.getCode()))
                    .anyMatch(m -> m.getCode().equals(menu));
        }
        return false;
    }

    /**
     * 检查用户是否具备资源请求的权限
     * @param element
     * @return
     */
    public boolean hasTarElement(String element) {
        log.info("正在检查用户访问接口资源权限");
        List<ResourceElementVo> elementPermissions = authResourceService.getUserElements(SecurityUtils.curUid());
        if(CollUtil.isNotEmpty(elementPermissions)) {
            return elementPermissions.stream()
                    .filter(e -> StrUtil.isNotBlank(e.getCode()))
                    .anyMatch(e -> e.getCode().equals(element));
        }
        return false;
    }

    /**
     * 是否具备目标区域权限
     * @param tarArea
     * @param curDepart
     * @return
     */
    public boolean hasTarArea(String tarArea, Long curDepart)
    {
        log.info("正在检查用户区域操作权限");
        List<BaseArea> areas = departService.gainDepartAreas(curDepart);
        if(CollUtil.isNotEmpty(areas)) {
            return areas.stream().anyMatch(a -> tarArea.equals(a.getCode()));
        }
        return false;
    }

    /**
     * 查询目标区域（部门权限内）
     * @param tarArea
     * @param curDepart
     * @return
     */
    public BaseArea findTarAreaInDepartLevel(String tarArea, Long curDepart)
    {
        log.info("正在加载用户权限内选中区域细节信息");
        List<BaseArea> areas = departService.gainDepartAreas(curDepart);
        if(CollUtil.isNotEmpty(areas)) {
            List<BaseArea> tarAreas = areas.stream().filter(a -> tarArea.equals(a.getCode())).collect(Collectors.toList());
            if(CollUtil.isNotEmpty(tarAreas)) {
                return tarAreas.get(0);
            }
        }
        return null;
    }

    /**
     * 检查用户是否是超管管理员
     * @return
     */
    public boolean userIsAdmin() {
        return SecurityUtils.isAdmin();
    }

    /**
     * 是否可以启动流程
     * @param modelId
     * @return
     */
    public boolean canStartActModel(String modelId) {
        log.info("用户[{}]是否可以启动流程[{}]的检查", SecurityUtils.curName(), modelId);
        Set<String> groups = SecurityUtils.getGroups();
        return true;
    }

    /**
     * 是否可以签收流程任务
     * @param taskId
     * @return
     */
    public boolean canSignActTask(String taskId) {

        // 检查待签收用户列表是否有用户

        // 如果没有待签收用户列表，检查任务步骤-用户权限关系（读写）

        return true;
    }

    /**
     * 是否可以操作任务
     * @param taskId
     * @return
     */
    public boolean canHandleActTask(String taskId, boolean adminCanDo) {
        // 查询任务现状
        Task task  = taskService.createTaskQuery().taskId(taskId).singleResult();
        if(ObjectUtil.isNull(task)) {
            return false;
        }
        return adminCanDo && SecurityUtils.isAdmin()
                || SecurityUtils.curUname().equals(task.getAssignee());
    }

    /**
     * 是否流程参与者判定
     * @param prcInstId
     * @param adminCanDo
     * @return
     */
    public boolean actProcParticipant(String prcInstId, boolean adminCanDo) {
        return true;
    }
}