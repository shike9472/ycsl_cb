package com.ztjo.service;

import com.ztjo.data.entity.base.BaseElement;

import java.io.Serializable;
import java.util.List;

/**
 * @author 陈彬
 * @version 2021/12/17
 * description：
 */
public interface ElementManageService {

    /**
     * 根据id集合(menu菜单内的)删除对应资源
     * @param ids
     * @return
     */
    void removeInMenuByIds(List<Long> ids);

    /**
     * 根据id删除
     * @param id
     * @return
     */
    Boolean removeById(Serializable id);

    /**
     * 加载对应用户的资源
     * @param uid
     * @return
     */
    List<BaseElement> loadTarUserElements(Long uid);
}
