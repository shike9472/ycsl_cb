package com.ztjo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ztjo.data.entity.base.*;
import com.ztjo.data.pojo.bo.DepartBo;
import com.ztjo.data.pojo.vo.model.RelationModifyConditionVo;
import com.ztjo.data.pojo.vo.model.TreeEntityVo;

import java.util.List;

/**
 * @author 陈彬
 * @version 2021/12/27
 * description：组织机构相关service
 */
public interface OrgAndUserManageService {

    /**
     * 获取用户租户部门信息
     * @return
     */
    List<BaseDepart> getTenantDeparts();

    /**
     * 获取用户租户部门信息
     * @return
     */
    List<DepartBo> getTenantDepartBos();

    /**
     * 根据id删除部门信息
     * @param id
     * @return
     */
    boolean removeDepartById(Long id);

    /**
     * 加载部门下用户信息（分页）（区分租户）
     * @param page
     * @param user
     * @param notLoadAlreadyDelete - 不加载已被删除的用户信息(默认给true即可)
     * @return
     */
    IPage<BaseUser> loadTenantUserPage(Page page, BaseUser user, boolean notLoadAlreadyDelete);

    /**
     * 根据id作废用户信息
     * @param id
     * @return
     */
    boolean disableUserById(Long id);

    /**
     * 根据id删除用户信息(逻辑删)
     * @param id
     * @return
     */
    boolean deleteUserById(Long id);

    /**
     * 通过id删除用户信息(物理删)
     * @param id
     * @return
     */
    boolean removeUserById(Long id);

    /**
     * 加载已经授权的部门区域权限关系
      * @param departId
     * @return
     */
    RelationModifyConditionVo<List<BaseArea>> loadDepartAreasAlready(Long departId);

    /**
     * 部门的区域授权关系下发至子级部门中
     * @param areas
     * @param tarTree
     */
    void grantAreas2Kids(List<BaseDepartArea> areas, List<TreeEntityVo<DepartBo>> tarTree);

    /**
     * 加载部门下岗位信息（分页）（区分租户）
     * @param page
     * @param position
     * @return
     */
    IPage<BasePosition> loadTenantPositionPage(Page page, BasePosition position);

    /**
     * 加载岗位用户设置(同租户用户不区分部门，均可进行管理)
     * @param tenantId
     * @param positionId
     * @return
     */
    RelationModifyConditionVo<List<BaseUser>> loadPositionUsersAlready(Long tenantId, Long positionId);

    /**
     * 根据租户加载部门
     * @param tenantId
     * @return
     */
    List<BaseDepart> loadDepartsByTenant(Long tenantId);

}
