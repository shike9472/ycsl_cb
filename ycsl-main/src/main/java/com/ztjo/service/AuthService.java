package com.ztjo.service;

import com.ztjo.data.pojo.dto.UserChangePasswordDto;

/**
 * @author 陈彬
 * @version 2021/9/4
 * description：用户登入/登出服务
 */
public interface AuthService {
    /**
     * 登录方法
     * @param username
     * @param password
     * @return
     * @throws Exception
     */
    String login(String username, String password);

    /**
     * 免密登录方法
     * @param username
     * @return
     * @throws Exception
     */
    String loginIgnorePwd(String username);

    /**
     * 检查输入密码是否正确
     * @param password
     * @return
     */
    Boolean checkIptPassword(String password);

    /**
     * 修改用户密码
     * @param userLoginDto
     */
    void changeSelfPassword(UserChangePasswordDto userLoginDto);

    /**
     * 登出方法
     * @param token
     * @return
     */
    Boolean logout(String token);
}
