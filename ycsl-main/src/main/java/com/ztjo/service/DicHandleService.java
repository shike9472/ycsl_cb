package com.ztjo.service;

import com.ztjo.data.entity.base.DicItem;
import com.ztjo.data.pojo.bo.DicMainBo;
import com.ztjo.data.pojo.vo.model.TreeEntityVo;

import java.io.Serializable;
import java.util.List;

/**
 * @author 陈彬
 * @version 2021/12/24
 * description：字典处理service
 */
public interface DicHandleService {
    /**
     * 按树形加载字典目录
     * @return
     */
    List<TreeEntityVo<DicMainBo>> loadDicMainTree();

    /**
     * 根据dic_main的dicCode加载字典项
     * @param dicCode
     * @return
     */
    List<DicItem> loadByMainCode(String dicCode);

    /**
     * 根据id删除字典配置项
     * @param id
     * @return
     */
    boolean deleteById(Serializable id);

    /**
     * item数据批量删除
     * @param ids
     */
    void removeItemsByIds(List<Long> ids);
}
