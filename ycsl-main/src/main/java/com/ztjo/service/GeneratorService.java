package com.ztjo.service;

import java.util.List;

/**
 * @author 陈彬
 * @version 2022/1/12
 * description：
 */
public interface GeneratorService {

    /**
     * 加载mysql数据库表名的方法
     * @param name
     * @return
     */
    List<String> getMysqlTableNames(String name);
}
