package com.ztjo.controller.auth;

import cn.hutool.core.util.ObjectUtil;
import com.ztjo.core.service.base.BaseDepartService;
import com.ztjo.core.service.base.BaseUserService;
import com.ztjo.core.utils.SecurityUtils;
import com.ztjo.data.annotation.authorize.PreElement;
import com.ztjo.data.annotation.authorize.PreMenuIgnore;
import com.ztjo.data.entity.base.BaseArea;
import com.ztjo.data.entity.base.BaseDepart;
import com.ztjo.data.entity.base.BaseUser;
import com.ztjo.data.exception.YcslBizException;
import com.ztjo.data.pojo.api.ZtjoResponse;
import com.ztjo.data.pojo.vo.resource.ResourceElementVo;
import com.ztjo.data.pojo.vo.resource.ResourceMenuVo;
import com.ztjo.service.AuthResourceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author 陈彬
 * @version 2021/10/26
 * description：用户登录资源加载controller
 */
@RestController
@RequestMapping(value = "/auth")
@Api(tags = {"[auth]用户登录资源加载API"})
public class AuthResourceController {

    @Autowired
    private AuthResourceService authResourceService;
    @Autowired
    private BaseUserService userService;
    @Autowired
    private BaseDepartService departService;

    /**
     * 加在用户菜单资源
     * @return
     */
    @PreElement("function_login")
    @GetMapping("/resource/menus")
    @ApiOperation(value = "加载用户菜单资源", notes = "加载用户菜单资源")
    public ZtjoResponse<List<ResourceMenuVo>> getUserMenus() {
        return ZtjoResponse.ok(authResourceService.getUserMenus(SecurityUtils.curUid()), "查询成功");
    }

    /**
     * 加载用户请求与按钮资源
     * @return
     */
    @PreElement("function_login")
    @GetMapping("/resource/elements")
    @ApiOperation(value = "加载用户请求与按钮资源", notes = "加载用户请求与按钮资源")
    public ZtjoResponse<List<ResourceElementVo>> getUserElements() {
        return ZtjoResponse.ok(authResourceService.getUserElements(SecurityUtils.curUid()), "查询成功");
    }

    /**
     * 登录用户信息数据加载
     * @return
     */
    @PreElement("function_login")
    @ApiOperation(value = "加载用户信息", notes = "加载用户信息")
    @GetMapping(value = "/load/self")
    public ZtjoResponse<BaseUser> loadSelf() {
        return ZtjoResponse.ok(userService.getById(SecurityUtils.curUid()));
    }

    /**
     * 登录用户部门信息数据加载（机构端用户登录后调用）
     * @return
     */
    @PreElement("function_login")
    @ApiOperation(value = "加载用户所属部门信息", notes = "加载用户所属部门信息")
    @GetMapping(value = "/load/belong/depart")
    public ZtjoResponse<BaseDepart> loadBelongDepart() {
        if(ObjectUtil.isNull(SecurityUtils.curDepartId())) {
            throw new YcslBizException("机构端用户登录请正确挂接所属部门！");
        }
        return ZtjoResponse.ok(departService.getById(SecurityUtils.curDepartId()));
    }

    /**
     * 获取部门区域配置
     * @param departId
     * @return
     */
    @PreElement("function_login")
    @GetMapping("/gainDepartAreas")
    @ApiOperation(value = "获取部门区域配置", notes = "获取部门区域配置")
    public ZtjoResponse<List<BaseArea>> gainDepartAreas(@RequestParam("departId") Long departId)
    {
        return ZtjoResponse.ok(departService.gainDepartAreas(departId));
    }
}
