package com.ztjo.controller.auth;

import com.ztjo.data.annotation.authorize.PreElement;
import com.ztjo.data.pojo.api.ZtjoResponse;
import com.ztjo.data.pojo.dto.UserChangePasswordDto;
import com.ztjo.data.pojo.dto.UserLoginDto;
import com.ztjo.data.pojo.group.LoginWithPwd;
import com.ztjo.service.AuthService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.groups.Default;

/**
 * @author 陈彬
 * @version 2021/9/4
 * description: 用户登入/登出接口api
 */
@RestController
@Api(tags = {"[auth]用户登录登出API"})
public class AuthController {
    @Autowired
    private AuthService authService;

    @PostMapping("login")
    @ApiOperation(value = "用户登录接口", notes = "用户登录接口")
    public ZtjoResponse<String> login(@Validated(value = {LoginWithPwd.class, Default.class}) @RequestBody UserLoginDto userLoginDto) {
        return ZtjoResponse.ok(authService.login(
                userLoginDto.getUsername(), userLoginDto.getPassword()));
    }

    @PostMapping("loginIgnPwg")
    @ApiOperation(value = "用户登录接口（免密）", notes = "用户登录接口（免密）")
    public ZtjoResponse<String> loginIgnPwg(@Validated(value = {Default.class}) @RequestBody UserLoginDto userLoginDto) {
        return ZtjoResponse.ok(authService.loginIgnorePwd(userLoginDto.getUsername()));
    }

    @PostMapping("confirmPassword")
    @ApiOperation(value = "确认密码是否正确（密码修改时需调用一遍）",
            notes = "确认密码是否正确（密码修改时需调用一遍）")
    public ZtjoResponse<Boolean> checkIptPassword(
            @Validated(value = {LoginWithPwd.class}) @RequestBody UserLoginDto userLoginDto)
    {
        return ZtjoResponse.ok(authService.checkIptPassword(userLoginDto.getPassword()));
    }

    @PreElement("function_change_password")
    @PostMapping("changeSelfPassword")
    @ApiOperation(value = "修改密码", notes = "修改密码")
    public ZtjoResponse<String> changeSelfPassword(
            @Valid @RequestBody UserChangePasswordDto userLoginDto)
    {
        authService.changeSelfPassword(userLoginDto);
        return ZtjoResponse.ok("密码修改成功！");
    }

    @PostMapping("logout")
    @ApiOperation(value = "用户登出接口", notes = "用户登出接口")
    public ZtjoResponse<Boolean> logout() {
        return ZtjoResponse.ok(true);
    }
}
