package com.ztjo.controller.generator;

import cn.hutool.core.util.StrUtil;
import com.ztjo.common.component.GeneratorComponent;
import com.ztjo.common.utils.PackageUtil;
import com.ztjo.data.annotation.authorize.PreMenu;
import com.ztjo.data.pojo.api.ZtjoResponse;
import com.ztjo.data.pojo.dto.GeneratorDto;
import com.ztjo.data.pojo.vo.SupportParamModelVo;
import com.ztjo.service.GeneratorService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.ztjo.data.constants.DataSourceKeysConstants.DATA_SOURCE_KEY_ZTK;
import static com.ztjo.data.constants.PackageConstants.SYS_ENTITY_MODEL_PACKAGE;

/**
 * @author 陈彬
 * @version 2021/8/19
 * description：代码动态生成restful接口
 */
@Slf4j
@RestController
@RequestMapping("/generator")
@PreMenu("menu_dmkssc")
@Api(tags = {"[generator]代码快速生成API"})
public class GeneratorController {

    @Autowired
    private GeneratorService generatorService;

    /**
     * 加载目前支持的实体类父级模型集合
     * @param searchSign
     * @return
     */
    @GetMapping("/load/entityModel")
    @ApiOperation(value = "加载目前支持的实体类父级模型集合",
            notes = "加载目前支持的实体类父级模型集合")
    public ZtjoResponse<List<SupportParamModelVo>> loadEntityModel(
            @RequestParam(value = "searchSign",required = false) String searchSign) throws IOException, ClassNotFoundException
    {
        List<SupportParamModelVo> vos = new ArrayList<>();
        vos.add(SupportParamModelVo.builder()
                .simpleName("Model")
                .name("com.baomidou.mybatisplus.extension.activerecord.Model")
                .title("一窗受理基础数据实体模型")
                .node("默认状态下，或父级实体模型未指定情况下使用该模型供给数据库映射实体继承")
                .build()
        );
        PackageUtil.getClasses(SYS_ENTITY_MODEL_PACKAGE, searchSign, vos);
        return ZtjoResponse.ok(vos, "success");
    }

    /**
     * 加载数据库表名
     * @param searchSign
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     */
    @GetMapping("/load/tablenames")
    @ApiOperation(value = "加载数据库表名", notes = "加载数据库表名")
    public ZtjoResponse<List<String>> loadTablenames(
            @RequestParam(value = "searchSign",required = false) String searchSign) throws IOException, ClassNotFoundException
    {
        return ZtjoResponse.ok(generatorService.getMysqlTableNames(searchSign), "success");
    }

    /**
     * 根据入参动态生成需要的mvc基础curd代码
     * 此接口仅管理员可用
     * @param dto
     * @return
     */
    @PostMapping("/initTables")
    @PreAuthorize("@ps.hasTarGroup('GROUP_ALL_HOLD')")
    @ApiOperation(value = "根据入参动态生成需要的mvc基础curd代码",
            notes = "根据入参动态生成需要的mvc基础curd代码")
    public ZtjoResponse<String> initTables(@RequestBody GeneratorDto dto) {
        if(StrUtil.isBlank(dto.getKey())) {
            dto.setKey(DATA_SOURCE_KEY_ZTK);
        }
        GeneratorComponent.tableLogicCreate(dto);
        return ZtjoResponse.ok("初始化成功");
    }
}
