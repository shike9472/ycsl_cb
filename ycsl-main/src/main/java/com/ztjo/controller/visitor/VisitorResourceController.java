package com.ztjo.controller.visitor;

import com.ztjo.core.service.base.BaseAreaService;
import com.ztjo.data.entity.base.BaseArea;
import com.ztjo.data.pojo.api.ZtjoResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author 陈彬
 * @version 2021/12/27
 * description：游客使用资源controller
 */
@RestController
@RequestMapping(value = "/visitor")
@Api(tags = {"[visitor]游客使用资源加载API"})
public class VisitorResourceController {

    @Autowired
    private BaseAreaService areaService;

    /**
     * 查询系统支持的全部区域 - 用于游客查看
     * @return
     */
    @GetMapping("/gainAllAreas")
    @ApiOperation(value = "查询全部区域", notes = "查询全部区域")
    public ZtjoResponse<List<BaseArea>> gainAllAreas() {
        return ZtjoResponse.ok(areaService.list());
    }
}
