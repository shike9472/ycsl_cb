package com.ztjo.controller.base;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ztjo.core.service.base.BasePositionService;
import com.ztjo.data.annotation.authorize.PreElement;
import com.ztjo.data.annotation.authorize.PreMenu;
import com.ztjo.data.annotation.authorize.PreTenant;
import com.ztjo.data.entity.base.BasePosition;
import com.ztjo.data.exception.YcslBizException;
import com.ztjo.data.pojo.api.ZtjoResponse;
import com.ztjo.service.OrgAndUserManageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 物理岗位表 - 用户人员岗位分类使用 前端控制器
 * </p>
 *
 * @author 陈彬
 * @since 2021-10-30
 */
@RestController
@RequestMapping("/basePosition")
@PreMenu("menu_bmgl")
@Api(tags = {"[Base]岗位管理API"})
public class BasePositionController {
    /**
    * 基础service装载
    */
    @Autowired
    private BasePositionService baseService;

    /**
     * 组织结构及用户操作service装载
     */
    @Autowired
    private OrgAndUserManageService orgAndUserManageService;

    /**
     * TODO 部门岗位管理 - start
     */
    @GetMapping("/load/page")
    @ApiOperation(value = "加载部门下岗位(分页)", notes = "加载部门下岗位(分页)")
    public ZtjoResponse loadDepartUsers(
            Page page,
            BasePosition position
    ) {
        if(ObjectUtil.isNull(position.getDepartId())) {
            throw new YcslBizException("部门岗位加载时，部门id必传！");
        }
        return ZtjoResponse.ok(orgAndUserManageService.loadTenantPositionPage(page, position));
    }

    @ApiOperation(value = "根据id查询", notes = "根据id查询")
    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    public ZtjoResponse<BasePosition> get(@PathVariable Serializable id) {
        Object o = baseService.getById(id);
        return ZtjoResponse.ok((BasePosition) o);
    }

    @PreTenant
    @PreElement("button_position_add")
    @ApiOperation(value = "记录新增", notes = "记录新增")
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    public ZtjoResponse<BasePosition> add(@RequestBody BasePosition entity) {
        baseService.save(entity);
        return ZtjoResponse.ok(entity);
    }

    @PreTenant
    @PreElement("button_position_update")
    @ApiOperation(value = "根据id更新", notes = "根据id更新")
    @RequestMapping(value = "/updateById",method = RequestMethod.POST)
    public ZtjoResponse<BasePosition> updateById(@RequestBody BasePosition entity) {
        baseService.updateById(entity);
        return ZtjoResponse.ok(entity);
    }

    @PreTenant
    @PreElement("button_position_delete")
    @ApiOperation(value = "根据id删除", notes = "根据id删除")
    @RequestMapping(value = "/remove",method = RequestMethod.POST)
    public ZtjoResponse<Boolean> remove(@RequestBody BasePosition entity) {
        return ZtjoResponse.ok(baseService.removeById(entity.getId()));
    }
    /**
     * TODO 部门岗位管理 - end
     */
}

