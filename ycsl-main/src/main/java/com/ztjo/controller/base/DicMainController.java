package com.ztjo.controller.base;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ztjo.core.service.base.DicItemService;
import com.ztjo.core.service.base.DicMainService;
import com.ztjo.data.annotation.authorize.PreElement;
import com.ztjo.data.annotation.authorize.PreMenu;
import com.ztjo.data.entity.base.DicItem;
import com.ztjo.data.entity.base.DicMain;
import com.ztjo.data.exception.YcslBizException;
import com.ztjo.data.pojo.api.ZtjoResponse;
import com.ztjo.data.pojo.bo.DicMainBo;
import com.ztjo.data.pojo.vo.model.TreeEntityVo;
import com.ztjo.service.DicHandleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 字典-字典主项表（目录和字典实例） 前端控制器
 * </p>
 *
 * @author 陈彬
 * @since 2021-10-30
 */
@RestController
@RequestMapping("/dicMain")
@PreMenu("menu_zdgl")
@Api(tags = {"[Base]字典目录管理API"})
public class DicMainController {
    /**
    * 基础service装载
    */
    @Autowired
    private DicMainService baseService;
    @Autowired
    private DicItemService dicItemService;
    @Autowired
    private DicHandleService dicHandleService;

    @GetMapping("/load/tree")
    @ApiOperation(value = "按树形加载字典目录", notes = "按树形加载字典目录")
    public ZtjoResponse<List<TreeEntityVo<DicMainBo>>> loadTree() {
        return ZtjoResponse.ok(dicHandleService.loadDicMainTree());
    }

    @PreElement("api_dic_modify")
    @ApiOperation(value = "记录新增", notes = "记录新增")
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    public ZtjoResponse<DicMain> add(@RequestBody DicMain entity) {
        baseService.save(entity);
        return ZtjoResponse.ok(entity);
    }

    @PreElement("api_dic_modify")
    @ApiOperation(value = "根据id更新", notes = "根据id更新")
    @RequestMapping(value = "/updateById",method = RequestMethod.POST)
    public ZtjoResponse<DicMain> updateById(@RequestBody DicMain entity) {
        baseService.updateById(entity);
        return ZtjoResponse.ok(entity);
    }

    @PreElement("api_dic_modify")
    @ApiOperation(value = "根据id删除", notes = "根据id删除")
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    public ZtjoResponse<Boolean> remove(@PathVariable Serializable id) {
        // 存在子级
        List<DicMain> children = baseService.list(Wrappers.<DicMain>lambdaQuery().eq(DicMain::getParentId, id));
        if(CollUtil.isNotEmpty(children)) {
            throw new YcslBizException("目录内存在字典目录配置，请先清理子级目录配置");
        }
        // 检索待删除的项
        DicMain del = baseService.getById(id);
        if(ObjectUtil.isNotNull(del)) {
            List<DicItem> items = dicItemService.list(Wrappers.<DicItem>lambdaQuery().eq(DicItem::getDicCode, del.getDicCode()));
            if (CollUtil.isNotEmpty(items)) {
                throw new YcslBizException("目录内存在字典项配置，请先清理字典项");
            }
        }
        // 执行删除
        return ZtjoResponse.ok(baseService.removeById(id));
    }

    @ApiOperation(value = "根据id查询", notes = "根据id查询")
    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    public ZtjoResponse<DicMain> get(@PathVariable Serializable id) {
        Object o = baseService.getById(id);
        return ZtjoResponse.ok((DicMain) o);
    }

    @ApiOperation(value = "查询全部", notes = "查询全部")
    @RequestMapping(value = "/all",method = RequestMethod.GET)
    public ZtjoResponse<List<DicMain>> all() {
        return ZtjoResponse.ok(baseService.list());
    }

    @ApiOperation(value = "按条件查询", notes = "按传入条件查询")
    @RequestMapping(value = "/query",method = RequestMethod.POST)
    public ZtjoResponse<List<DicMain>> query(@RequestBody DicMain entity) {
        return ZtjoResponse.ok(baseService.list(Wrappers.lambdaQuery(entity)));
    }

    @ApiOperation(value = "根据条件分页查询", notes = "根据条件分页查询")
    @RequestMapping(value = "/page",method = RequestMethod.GET)
    public ZtjoResponse list(Page page, DicMain entity) {
        return ZtjoResponse.ok(baseService.page(page, Wrappers.query(entity)));
    }
}

