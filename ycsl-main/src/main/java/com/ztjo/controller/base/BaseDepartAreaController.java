package com.ztjo.controller.base;

import cn.hutool.core.convert.Convert;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ztjo.core.service.base.BaseDepartAreaService;
import com.ztjo.core.service.base.BaseDepartService;
import com.ztjo.data.annotation.authorize.PreElement;
import com.ztjo.data.annotation.authorize.PreMenu;
import com.ztjo.data.entity.base.BaseArea;
import com.ztjo.data.entity.base.BaseDepartArea;
import com.ztjo.data.entity.base.BaseResourceAuthority;
import com.ztjo.data.pojo.api.ZtjoResponse;
import com.ztjo.data.pojo.bo.DepartBo;
import com.ztjo.data.pojo.bo.GroupBo;
import com.ztjo.data.pojo.dto.RelationModifyDto;
import com.ztjo.data.pojo.vo.model.RelationModifyConditionVo;
import com.ztjo.data.pojo.vo.model.TreeEntityVo;
import com.ztjo.service.OrgAndUserManageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

import static com.ztjo.core.utils.TreeUtils.getTreeUpFromPool;

/**
 * <p>
 * 部门区域权限表 前端控制器
 * </p>
 *
 * @author 陈彬
 * @since 2021-10-30
 */
@RestController
@RequestMapping("/baseDepartArea")
@PreMenu("menu_bmgl")
@Api(tags = {"[Base]部门 - 区域管理API"})
public class BaseDepartAreaController {
    /**
    * 基础service装载
    */
    @Autowired
    private BaseDepartAreaService baseService;

    /**
     * 部门service装载
     */
    @Autowired
    private BaseDepartService departService;

    /**
     * 组织结构及用户操作service装载
     */
    @Autowired
    private OrgAndUserManageService orgAndUserManageService;

    /**
     * TODO 部门区域管理 - start
     */
    /**
     * 部门 - 区域权限加载
     * @param departId
     * @return
     */
    @GetMapping("/load")
    @ApiOperation(value = "加载部门区域授权情况", notes = "加载部门区域授权情况")
    public ZtjoResponse<RelationModifyConditionVo<List<BaseArea>>> load(@RequestParam("departId") Long departId)
    {
        return ZtjoResponse.ok(orgAndUserManageService.loadDepartAreasAlready(departId));
    }

    /**
     * 部门 - 区域权限授权
     * @param dto
     * @return
     */
    @PreElement("button_depart_area_modify")
    @ApiOperation(value = "更新部门区域授权 - 根据传入部门id更新",
            notes = "更新部门区域授权 - 根据传入部门id更新")
    @RequestMapping(value = "/modify",method = RequestMethod.POST)
    public ZtjoResponse<String> modify(@Valid @RequestBody RelationModifyDto<BaseDepartArea> dto)
    {
        baseService.modifyDepartAreas(dto);
        return ZtjoResponse.ok("更新部门区域授权成功！");
    }

    /**
     * 部门 - 区域权限下发
     * @param id
     * @return
     */
    @PreElement("button_area_grant_down")
    @GetMapping("/grant/copyParent")
    @ApiOperation(value = "部门 - 区域权限下发", notes = "部门 - 区域权限下发")
    public ZtjoResponse<String> grant(@RequestParam("id") Long id) {
        // 查询部门区域授权集合
        List<BaseDepartArea> departAreas = baseService.list(
                Wrappers.<BaseDepartArea>query().lambda().eq(BaseDepartArea::getDepartId, id));
        // 加载全部部门信息
        List<DepartBo> pool = departService.list().stream().map(d -> Convert.convert(DepartBo.class, d)).collect(Collectors.toList());
        // 获取目标节点的下级部门（树形结构）
        List<TreeEntityVo<DepartBo>> tarTree = getTreeUpFromPool(pool, id);
        orgAndUserManageService.grantAreas2Kids(departAreas, tarTree);
        return ZtjoResponse.ok("权限下发成功！");
    }

    /**
     * TODO 部门区域管理 - end
     */

}

