package com.ztjo.controller.base;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ztjo.core.service.base.BaseUserService;
import com.ztjo.data.annotation.authorize.PreElement;
import com.ztjo.data.annotation.authorize.PreMenu;
import com.ztjo.data.annotation.authorize.PreTenant;
import com.ztjo.data.entity.base.BaseUser;
import com.ztjo.data.pojo.api.ZtjoResponse;
import com.ztjo.service.OrgAndUserManageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;

import static com.ztjo.data.enums.SFEnums.SF_S;

/**
 * <p>
 * 系统用户表 前端控制器
 * </p>
 *
 * @author 陈彬
 * @since 2021-10-30
 */
@RestController
@RequestMapping("/baseUser")
@PreMenu("menu_yhgl")
@Api(tags = {"[Base]用户管理API"})
public class BaseUserController {

    /**
    * 基础service装载
    */
    @Autowired
    private BaseUserService baseService;

    /**
     * 组织机构管理service装载
     */
    @Autowired
    private OrgAndUserManageService orgAndUserManageService;

    /**
     * 分页查询用户信息（分租户）
     * @param page
     * @param user
     * @return
     */
    @GetMapping("/page")
    @ApiOperation(value = "分页加载用户信息（分租户）",
            notes = "分页加载用户信息，检索条件支持：" +
                    "username的精确查找，" +
                    "name的模糊查找（仅可以左精确，右模糊）" +
                    "departId精确查找" +
                    "tenantId精确查找等")
    public ZtjoResponse loadUsers(
            Page page,
            BaseUser user
    ) {
        return ZtjoResponse.ok(orgAndUserManageService.loadTenantUserPage(page, user, true));
    }

    /**
     * 分页查询用户信息（分租户）(已逻辑删除的)
     * @param page
     * @param user
     * @return
     */
    @GetMapping("/page/delete")
    @ApiOperation(value = "分页加载用户信息（分租户）(已逻辑删除的)",
            notes = "分页加载用户信息(已逻辑删除的)，检索条件支持：" +
                    "username的精确查找，" +
                    "name的模糊查找（仅可以左精确，右模糊）" +
                    "departId精确查找" +
                    "tenantId精确查找等")
    public ZtjoResponse loadUsersDelete(
            Page page,
            BaseUser user
    ) {
        // 培育逻辑删除专用检索条件
        user.setIsDeleted(SF_S.getCode());
        // 执行查询
        return ZtjoResponse.ok(orgAndUserManageService.loadTenantUserPage(page, user, false));
    }

    /**
     * 查询用户详情
     * @param id
     * @return
     */
    @ApiOperation(value = "根据id查询", notes = "根据id查询")
    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    public ZtjoResponse<BaseUser> get(@PathVariable Serializable id) {
        Object o = baseService.getById(id);
        return ZtjoResponse.ok((BaseUser) o);
    }

    /**
     * 新增用户
     * @param entity
     * @return
     */
    @PreTenant
    @PreElement("button_user_add")
    @ApiOperation(value = "记录新增", notes = "记录新增")
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    public ZtjoResponse<BaseUser> add(@RequestBody BaseUser entity) {
        baseService.save(entity);
        return ZtjoResponse.ok(entity);
    }

    /**
     * 更新用户
     * @param entity
     * @return
     */
    @PreTenant
    @PreElement("button_user_update")
    @ApiOperation(value = "根据id更新", notes = "根据id更新")
    @RequestMapping(value = "/updateById",method = RequestMethod.POST)
    public ZtjoResponse<BaseUser> updateById(@RequestBody BaseUser entity) {
        baseService.updateById(entity);
        return ZtjoResponse.ok(entity);
    }

    /**
     * 部门用户作废
     * @param user
     * @return
     */
    @PreTenant
    @PreElement("button_user_disable")
    @PostMapping("/user/disabled")
    @ApiOperation(value = "部门用户删除", notes = "部门用户删除")
    public ZtjoResponse<Boolean> disableUser(@RequestBody BaseUser user) {
        return ZtjoResponse.ok(orgAndUserManageService.disableUserById(user.getId()));
    }

    /**
     * 用户删除(逻辑删 - 超管权限)
     * @param id
     * @return
     */
    @PreAuthorize("@ps.hasTarGroup('GROUP_ALL_HOLD')")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @ApiOperation(value = "用户删除(逻辑删 - 超管权限)", notes = "用户删除(逻辑删 - 超管权限)")
    public ZtjoResponse<Boolean> delete(@PathVariable Serializable id) {
        return ZtjoResponse.ok(orgAndUserManageService.deleteUserById(Long.parseLong(id.toString())));
    }

    /**
     * 用户删除(物理删 - 超管权限)
     * @param id
     * @return
     */
    @PreAuthorize("@ps.hasTarGroup('GROUP_ALL_HOLD')")
    @RequestMapping(value = "/remove/{id}", method = RequestMethod.DELETE)
    @ApiOperation(value = "用户删除(物理删 - 超管权限)", notes = "用户删除(物理删 - 超管权限)")
    public ZtjoResponse<Boolean> remove(@PathVariable Serializable id) {
        return ZtjoResponse.ok(orgAndUserManageService.removeUserById(Long.parseLong(id.toString())));
    }

}

