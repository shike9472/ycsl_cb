package com.ztjo.controller.base;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ztjo.core.service.base.BaseDepartService;
import com.ztjo.core.service.base.BasePositionService;
import com.ztjo.core.service.base.BaseUserService;
import com.ztjo.core.utils.TreeUtils;
import com.ztjo.data.annotation.authorize.PreElement;
import com.ztjo.data.annotation.authorize.PreMenu;
import com.ztjo.data.annotation.authorize.PreTenant;
import com.ztjo.data.entity.base.BaseDepart;
import com.ztjo.data.entity.base.BaseUser;
import com.ztjo.data.exception.YcslBizException;
import com.ztjo.data.pojo.api.ZtjoResponse;
import com.ztjo.data.pojo.bo.DepartBo;
import com.ztjo.data.pojo.vo.model.TreeEntityVo;
import com.ztjo.service.OrgAndUserManageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 部门表 - 用于管理系统内部门 前端控制器
 * </p>
 *
 * @author 陈彬
 * @since 2021-10-30
 */
@RestController
@RequestMapping("/baseDepart")
@PreMenu("menu_bmgl")
@Api(tags = {"[Base]部门管理API"})
public class BaseDepartController {
    /**
    * 基础service装载
    */
    @Autowired
    private BaseDepartService baseService;

    /**
     * 用户service装载
     */
    @Autowired
    private BaseUserService userService;

    /**
     * 组织结构及用户操作service装载
     */
    @Autowired
    private OrgAndUserManageService orgAndUserManageService;

    /**
     * TODO 部门基础管理 - start
     */
    /**
     * 查询部门树
     * @return
     */
    @GetMapping("/load/tree")
    @ApiOperation(value = "按树形加载部门信息", notes = "按树形加载部门信息")
    public ZtjoResponse<List<TreeEntityVo<DepartBo>>> loadTree() {
        List<DepartBo> departs = orgAndUserManageService.getTenantDepartBos();
        if(CollUtil.isEmpty(departs)) {
            return ZtjoResponse.ok(CollUtil.newArrayList());
        }
        return ZtjoResponse.ok(TreeUtils.getTreeUpFromPool(departs));
    }

    /**
     * 按id加载部门详情数据
     * @param id
     * @return
     */
    @ApiOperation(value = "根据id查询", notes = "根据id查询")
    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    public ZtjoResponse<BaseDepart> get(@PathVariable Serializable id) {
        Object o = baseService.getById(id);
        return ZtjoResponse.ok((BaseDepart) o);
    }

    /**
     * 新增部门记录
     * @param entity
     * @return
     */
    @PreTenant
    @PreElement("button_depart_add")
    @ApiOperation(value = "记录新增", notes = "记录新增")
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    public ZtjoResponse<BaseDepart> add(@RequestBody BaseDepart entity) {
        baseService.save(entity);
        return ZtjoResponse.ok(entity);
    }

    /**
     * 更新部门记录
     * @param entity
     * @return
     */
    @PreTenant
    @PreElement("button_depart_update")
    @ApiOperation(value = "根据id更新", notes = "根据id更新")
    @RequestMapping(value = "/updateById",method = RequestMethod.POST)
    public ZtjoResponse<BaseDepart> updateById(@RequestBody BaseDepart entity) {
        baseService.updateById(entity);
        return ZtjoResponse.ok(entity);
    }

    /**
     * 删除部门
     * @param entity
     * @return
     */
    @PreTenant
    @PreElement("button_depart_delete")
    @ApiOperation(value = "根据id删除", notes = "根据id删除")
    @PostMapping(value = "/removeByid")
    public ZtjoResponse<Boolean> remove(@RequestBody BaseDepart entity) {
        return ZtjoResponse.ok(orgAndUserManageService.removeDepartById(entity.getId()));
    }
    /**
     * TODO 部门基础管理 - end
     */

    /**
     * TODO 部门用户管理 - start
     */
    /**
     * 加载部门下用户信息
     * @param page
     * @param user
     * @return
     */
    @GetMapping("/user/load")
    @ApiOperation(value = "加载部门下用户(分页)",
            notes = "加载部门下用户(分页)，检索条件支持：username的精确查找，name的模糊查找（仅可以左精确，右模糊）")
    public ZtjoResponse loadDepartUsers(
            Page page,
            BaseUser user
    ) {
        if(ObjectUtil.isNull(user.getDepartId())) {
            throw new YcslBizException("部门用户加载时，部门id必传！");
        }
        return ZtjoResponse.ok(orgAndUserManageService.loadTenantUserPage(page, user, true));
    }

    /**
     * 部门用户创建
     * @param user
     * @return
     */
    @PreTenant
    @PreElement("button_user_add_in_depart")
    @PostMapping("/user/add")
    @ApiOperation(value = "部门用户创建", notes = "部门用户创建")
    public ZtjoResponse<BaseUser> addDepartUser(@RequestBody BaseUser user) {
        userService.save(user);
        return ZtjoResponse.ok(user);
    }

    /**
     * 部门用户更新
     * @param user
     * @return
     */
    @PreTenant
    @PreElement("button_user_update_in_depart")
    @PostMapping("/user/update")
    @ApiOperation(value = "部门用户更新", notes = "部门用户更新")
    public ZtjoResponse<BaseUser> updateDepartUser(@RequestBody BaseUser user) {
        userService.updateById(user);
        return ZtjoResponse.ok(user);
    }

    /**
     * 部门用户作废
     * @param user
     * @return
     */
    @PreTenant
    @PreElement("button_user_disable_in_depart")
    @PostMapping("/user/disabled")
    @ApiOperation(value = "部门用户作废", notes = "部门用户作废")
    public ZtjoResponse<Boolean> disableDepartUser(@RequestBody BaseUser user) {
        return ZtjoResponse.ok(orgAndUserManageService.disableUserById(user.getId()));
    }
    /**
     * TODO 部门用户管理 - end
     */
}

