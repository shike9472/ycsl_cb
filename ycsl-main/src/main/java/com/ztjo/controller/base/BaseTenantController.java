package com.ztjo.controller.base;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ztjo.core.service.base.BaseTenantService;
import com.ztjo.data.annotation.authorize.PreElement;
import com.ztjo.data.annotation.authorize.PreMenu;
import com.ztjo.data.entity.base.BaseTenant;
import com.ztjo.data.pojo.api.ZtjoResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 租户表 前端控制器
 * </p>
 *
 * @author 陈彬
 * @since 2021-10-30
 */
@RestController
@RequestMapping("/baseTenant")
@PreMenu("menu_zhgl")
@Api(tags = {"[Base]租户信息管理API"})
public class BaseTenantController {
    /**
     * 基础service装载
     */
    @Autowired
    private BaseTenantService baseService;

    /**
     * 分页查询
     * @param page
     * @param entity
     * @return
     */
    @ApiOperation(value = "根据name条件模糊分页查询", notes = "根据name条件模糊分页查询")
    @RequestMapping(value = "/page",method = RequestMethod.GET)
    public ZtjoResponse list(Page page, BaseTenant entity) {
        LambdaQueryWrapper<BaseTenant> queryWrapper = Wrappers.lambdaQuery();
        if(StrUtil.isNotBlank(entity.getName())) {
            queryWrapper = Wrappers.<BaseTenant>query().lambda().like(BaseTenant::getName, entity.getName());
        }
        return ZtjoResponse.ok(baseService.page(page, queryWrapper));
    }

    /**
     * 租户新增
     * @param entity
     * @return
     */
    @PreElement("button_tenant_add")
    @ApiOperation(value = "记录新增", notes = "记录新增")
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    public ZtjoResponse<BaseTenant> add(@RequestBody BaseTenant entity) {
        baseService.save(entity);
        return ZtjoResponse.ok(entity);
    }

    /**
     * 租户更新
     * @param entity
     * @return
     */
    @PreElement("button_tenant_update")
    @ApiOperation(value = "根据id更新", notes = "根据id更新")
    @RequestMapping(value = "/updateById",method = RequestMethod.POST)
    public ZtjoResponse<BaseTenant> updateById(@RequestBody BaseTenant entity) {
        baseService.updateById(entity);
        return ZtjoResponse.ok(entity);
    }

    /**
     * 租户删除
     * @param id
     * @return
     */
    @PreElement("button_tenant_delete")
    @ApiOperation(value = "根据id删除", notes = "根据id删除")
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    public ZtjoResponse<Boolean> remove(@PathVariable Serializable id) {
        baseService.tenantCanDelete(Long.parseLong(id.toString()));
        return ZtjoResponse.ok(baseService.removeById(id));
    }

    /**
     * 单个详情查询
     * @param id
     * @return
     */
    @ApiOperation(value = "根据id查询", notes = "根据id查询")
    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    public ZtjoResponse<BaseTenant> get(@PathVariable Serializable id) {
        return ZtjoResponse.ok(baseService.getById(id));
    }

    @ApiOperation(value = "查询全部", notes = "查询全部")
    @RequestMapping(value = "/all",method = RequestMethod.GET)
    public ZtjoResponse<List<BaseTenant>> all() {
        return ZtjoResponse.ok(baseService.list());
    }

    @ApiOperation(value = "按条件查询", notes = "按传入条件查询")
    @RequestMapping(value = "/query",method = RequestMethod.POST)
    public ZtjoResponse<List<BaseTenant>> query(@RequestBody BaseTenant entity) {
        return ZtjoResponse.ok(baseService.list(Wrappers.lambdaQuery(entity)));
    }
}

