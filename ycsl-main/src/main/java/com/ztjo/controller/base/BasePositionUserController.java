package com.ztjo.controller.base;

import cn.hutool.core.util.ObjectUtil;
import com.ztjo.core.service.base.BasePositionUserService;
import com.ztjo.data.annotation.authorize.PreElement;
import com.ztjo.data.annotation.authorize.PreMenu;
import com.ztjo.data.annotation.authorize.PreTenant;
import com.ztjo.data.entity.base.BaseDepart;
import com.ztjo.data.entity.base.BasePosition;
import com.ztjo.data.entity.base.BasePositionUser;
import com.ztjo.data.entity.base.BaseUser;
import com.ztjo.data.exception.YcslBizException;
import com.ztjo.data.pojo.api.ZtjoResponse;
import com.ztjo.data.pojo.dto.RelationModifyDto;
import com.ztjo.data.pojo.vo.model.RelationModifyConditionVo;
import com.ztjo.service.OrgAndUserManageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 岗位 - 用户设置表 前端控制器
 * </p>
 *
 * @author 陈彬
 * @since 2021-10-30
 */
@RestController
@RequestMapping("/basePositionUser")
@PreMenu("menu_bmgl")
@Api(tags = {"[Base]岗位 - 用户挂接管理API"})
public class BasePositionUserController {
    /**
    * 基础service装载
    */
    @Autowired
    private BasePositionUserService baseService;

    /**
     * 组织结构及用户操作service装载
     */
    @Autowired
    private OrgAndUserManageService orgAndUserManageService;

    /**
     * TODO 岗位用户管理 - start
     */
    /**
     * 加载岗位-用户管理情况
     * @param position
     * @return
     */
    @PreTenant
    @PostMapping("/load")
    @ApiOperation(value = "加载岗位-用户管理情况", notes = "加载岗位-用户管理情况")
    public ZtjoResponse<RelationModifyConditionVo<List<BaseUser>>> load(@RequestBody BasePosition position)
    {
        if(ObjectUtil.isNull(position.getId())) {
            throw new YcslBizException("加载岗位-用户关系必须传入岗位id入参！");
        }
        return ZtjoResponse.ok(orgAndUserManageService.loadPositionUsersAlready(position.getTenantId(), position.getId()));
    }

    /**
     * 加载与岗位同租户的部门集合
     * @param position
     * @return
     */
    @PreTenant
    @PostMapping("/load/departs/sameTenant")
    @ApiOperation(value = "加载与岗位同租户的部门集合",
            notes = "加载与岗位同租户的部门集合 - 用于岗位用户管理时下拉选择预期操作用户的部门，以达到快速定位用户的作用")
    public ZtjoResponse<List<BaseDepart>> loadDepartsSameTenant(@RequestBody BasePosition position)
    {
        return ZtjoResponse.ok(orgAndUserManageService.loadDepartsByTenant(position.getTenantId()));
    }

    /**
     * 更新岗位 - 用户关系
     * @param dto
     * @return
     */
    @PreElement("button_position_user_modify")
    @ApiOperation(value = "更新岗位用户关系 - 根据传入岗位id更新",
            notes = "更新岗位用户关系 - 根据传入岗位id更新")
    @RequestMapping(value = "/modify",method = RequestMethod.POST)
    public ZtjoResponse<String> modify(@Valid @RequestBody RelationModifyDto<BasePositionUser> dto)
    {
        baseService.modifyPositionUsers(dto);
        return ZtjoResponse.ok("更新岗位用户关系成功！");
    }
    /**
     * TODO 岗位用户管理 - end
     */
}

