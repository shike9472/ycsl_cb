package com.ztjo.controller.base;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ztjo.core.service.base.DictTypeService;
import com.ztjo.data.annotation.authorize.PreMenu;
import com.ztjo.data.entity.base.DictType;
import com.ztjo.data.pojo.api.ZtjoResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 字典类型 - 用于规范固定字典项类型 前端控制器
 * </p>
 *
 * @author 陈彬
 * @since 2021-10-30
 */
@RestController
@RequestMapping("/dictType")
@PreMenu("menu_zdgl")
@Api(tags = {"[Base]字典（系统级）类型管理API"})
public class DictTypeController {
    /**
    * 基础service装载
    */
    @Autowired
    private DictTypeService baseService;

    @ApiOperation(value = "记录新增", notes = "记录新增")
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    public ZtjoResponse<DictType> add(@RequestBody DictType entity) {
        baseService.save(entity);
        return ZtjoResponse.ok(entity);
    }

    @ApiOperation(value = "根据id查询", notes = "根据id查询")
    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    public ZtjoResponse<DictType> get(@PathVariable Serializable id) {
        Object o = baseService.getById(id);
        return ZtjoResponse.ok((DictType) o);
    }

    @ApiOperation(value = "根据id更新", notes = "根据id更新")
    @RequestMapping(value = "/updateById",method = RequestMethod.POST)
    public ZtjoResponse<DictType> updateById(@RequestBody DictType entity) {
        baseService.updateById(entity);
        return ZtjoResponse.ok(entity);
    }

    @ApiOperation(value = "根据id删除", notes = "根据id删除")
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    public ZtjoResponse<Boolean> remove(@PathVariable Serializable id) {
        return ZtjoResponse.ok(baseService.removeById(id));
    }

    @ApiOperation(value = "查询全部", notes = "查询全部")
    @RequestMapping(value = "/all",method = RequestMethod.GET)
    public ZtjoResponse<List<DictType>> all() {
        return ZtjoResponse.ok(baseService.list());
    }

    @ApiOperation(value = "按条件查询", notes = "按传入条件查询")
    @RequestMapping(value = "/query",method = RequestMethod.POST)
    public ZtjoResponse<List<DictType>> query(@RequestBody DictType entity) {
        return ZtjoResponse.ok(baseService.list(Wrappers.lambdaQuery(entity)));
    }

    @ApiOperation(value = "根据条件分页查询", notes = "根据条件分页查询")
    @RequestMapping(value = "/page",method = RequestMethod.GET)
    public ZtjoResponse list(Page page, DictType entity) {
        return ZtjoResponse.ok(baseService.page(page, Wrappers.query(entity)));
    }
}

