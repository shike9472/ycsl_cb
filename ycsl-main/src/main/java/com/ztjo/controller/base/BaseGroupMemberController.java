package com.ztjo.controller.base;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ztjo.core.service.base.BaseGroupMemberService;
import com.ztjo.data.annotation.authorize.PreElement;
import com.ztjo.data.annotation.authorize.PreMenu;
import com.ztjo.data.annotation.authorize.PreTenant;
import com.ztjo.data.entity.base.BaseGroup;
import com.ztjo.data.entity.base.BaseGroupMember;
import com.ztjo.data.entity.base.BaseUser;
import com.ztjo.data.exception.YcslBizException;
import com.ztjo.data.pojo.api.ZtjoResponse;
import com.ztjo.data.pojo.dto.RelationModifyDto;
import com.ztjo.data.pojo.vo.model.RelationModifyConditionVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 用户组 - 成员表 前端控制器
 * </p>
 *
 * @author 陈彬
 * @since 2021-10-30
 */
@RestController
@RequestMapping("/baseGroupMember")
@PreMenu("menu_yhgl")
@Api(tags = {"[Base]角色组 - 成员管理API"})
public class BaseGroupMemberController {
        /**
    * 基础service装载
    */
    @Autowired
    private BaseGroupMemberService baseService;

    /**
     * 加载岗位-用户管理情况
     * @param user
     * @return
     */
    @PreTenant
    @PostMapping("/load")
    @ApiOperation(value = "加载用户（成员）-角色关联情况", notes = "加载用户（成员）-角色关联情况")
    public ZtjoResponse<RelationModifyConditionVo<List<BaseGroup>>> load(@RequestBody BaseUser user)
    {
        if(ObjectUtil.isNull(user.getId())) {
            throw new YcslBizException("加载用户-角色关系必须传入对应用户id入参！");
        }
        return ZtjoResponse.ok(baseService.loadMumberGreoupsAlready(user));
    }

    /**
     * 更新角色组成员 - 根据传入角色id更新
     * @param dto
     * @return
     */
    @PreElement("api_user_group_modify")
    @ApiOperation(value = "更新角色组成员 - 根据传入角色id更新",
            notes = "更新角色组成员 - 根据传入角色id更新")
    @RequestMapping(value = "/modify",method = RequestMethod.POST)
    public ZtjoResponse<String> modify(@Valid @RequestBody RelationModifyDto<BaseGroupMember> dto)
    {
        baseService.modifyGroupMembers(dto);
        return ZtjoResponse.ok("更新角色组成员成功！");
    }
}

