package com.ztjo.controller.base;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ztjo.core.service.base.BaseDepartAreaService;
import com.ztjo.data.annotation.authorize.PreElement;
import com.ztjo.data.annotation.authorize.PreMenu;
import com.ztjo.data.entity.base.BaseDepartArea;
import com.ztjo.data.pojo.api.ZtjoResponse;
import com.ztjo.data.pojo.dto.RelationModifyDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

import com.ztjo.data.entity.base.BaseArea;
import com.ztjo.core.service.base.BaseAreaService;

import javax.validation.Valid;

/**
 * <p>
 * 区域表 - 用区域隔离时使用 前端控制器
 * </p>
 *
 * @author 陈彬
 * @since 2021-10-30
 */
@RestController
@RequestMapping("/baseArea")
@PreMenu("menu_qysz")
@Api(tags = {"[Base]区域管理API"})
public class BaseAreaController {
    /**
    * 基础service装载
    */
    @Autowired
    private BaseAreaService baseService;
    /**
     * 部门区域授权关系操作service装载
     */
    @Autowired
    private BaseDepartAreaService departAreaService;

    @PreElement("button_area_add")
    @ApiOperation(value = "记录新增", notes = "记录新增")
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    public ZtjoResponse<BaseArea> add(@RequestBody BaseArea entity) {
        baseService.save(entity);
        return ZtjoResponse.ok(entity);
    }

    @PreElement("button_area_update")
    @ApiOperation(value = "根据id更新", notes = "根据id更新")
    @RequestMapping(value = "/updateById",method = RequestMethod.POST)
    public ZtjoResponse<BaseArea> updateById(@RequestBody BaseArea entity) {
        baseService.updateById(entity);
        return ZtjoResponse.ok(entity);
    }

    @PreElement("button_area_delete")
    @ApiOperation(value = "根据id删除", notes = "根据id删除")
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    public ZtjoResponse<Boolean> remove(@PathVariable Serializable id) {
        return ZtjoResponse.ok(baseService.removeById(id));
    }

    @PreElement("button_area_departs_cancer")
    @ApiOperation(value = "取消指定区域对所有部门的授权", notes = "取消指定区域对所有部门的授权")
    @RequestMapping(value = "/cancer",method = RequestMethod.POST)
    public ZtjoResponse<Boolean> cancer(@Valid @RequestBody RelationModifyDto<BaseDepartArea> dto) {
        dto.setCurEntitys(null);
        return ZtjoResponse.ok(departAreaService.modifyAreaDeparts(dto));
    }

    @ApiOperation(value = "根据id查询", notes = "根据id查询")
    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    public ZtjoResponse<BaseArea> get(@PathVariable Serializable id) {
        Object o = baseService.getById(id);
        return ZtjoResponse.ok((BaseArea) o);
    }

    @ApiOperation(value = "查询全部", notes = "查询全部")
    @RequestMapping(value = "/all",method = RequestMethod.GET)
    public ZtjoResponse<List<BaseArea>> all() {
        return ZtjoResponse.ok(baseService.list());
    }

    @ApiOperation(value = "按条件查询", notes = "按传入条件查询")
    @RequestMapping(value = "/query",method = RequestMethod.POST)
    public ZtjoResponse<List<BaseArea>> query(@RequestBody BaseArea entity) {
        return ZtjoResponse.ok(baseService.list(Wrappers.lambdaQuery(entity)));
    }

    @ApiOperation(value = "根据条件分页查询", notes = "根据条件分页查询")
    @RequestMapping(value = "/page",method = RequestMethod.GET)
    public ZtjoResponse list(Page page, BaseArea entity) {
        return ZtjoResponse.ok(baseService.page(page, Wrappers.query(entity)));
    }
}

