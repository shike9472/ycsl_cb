package com.ztjo.controller.base;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ztjo.core.service.base.DicItemService;
import com.ztjo.data.annotation.authorize.PreElement;
import com.ztjo.data.annotation.authorize.PreMenu;
import com.ztjo.data.annotation.authorize.PreMenuIgnore;
import com.ztjo.data.entity.base.DicItem;
import com.ztjo.data.pojo.api.ZtjoResponse;
import com.ztjo.service.DicHandleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 字典-字典字项表 前端控制器
 * </p>
 *
 * @author 陈彬
 * @since 2021-10-30
 */
@RestController
@RequestMapping("/dicItem")
@PreMenu("menu_zdgl")
@Api(tags = {"[Base]字典项管理API"})
public class DicItemController {
    /**
    * 基础service装载
    */
    @Autowired
    private DicItemService baseService;
    @Autowired
    private DicHandleService dicHandleService;

    /**
     * 忽略菜单鉴权
     * @param dicCode
     * @return
     */
    @PreMenuIgnore
    @GetMapping("/load/maincode")
    @ApiOperation(value = "根据dicCode查询dicItem项", notes = "根据dicCode查询dicItem项")
    public ZtjoResponse<List<DicItem>> loadByMainCode(@RequestParam("dicCode") String dicCode) {
        return ZtjoResponse.ok(dicHandleService.loadByMainCode(dicCode));
    }

    @PreElement("api_dic_modify")
    @ApiOperation(value = "记录新增", notes = "记录新增")
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    public ZtjoResponse<DicItem> add(@RequestBody DicItem entity) {
        baseService.save(entity);
        return ZtjoResponse.ok(entity);
    }

    @PreElement("api_dic_modify")
    @ApiOperation(value = "根据id更新", notes = "根据id更新")
    @RequestMapping(value = "/updateById",method = RequestMethod.POST)
    public ZtjoResponse<DicItem> updateById(@RequestBody DicItem entity) {
        baseService.updateById(entity);
        return ZtjoResponse.ok(entity);
    }

    @PreElement("api_dic_modify")
    @ApiOperation(value = "根据id删除", notes = "根据id删除")
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    public ZtjoResponse<Boolean> remove(@PathVariable Serializable id) {
        return ZtjoResponse.ok(dicHandleService.deleteById(id));
    }

    /**
     * 批量删除 -
     *      1. 注意redis cache 内容清理
     *      2. 注意被删除项必须全部属于同一个 dicMain
     */
    @PreElement("api_dic_modify")
    @ApiOperation(value = "根据id集合批量删除", notes = "根据id集合批量删除，要求所有被删除项必须同属一个DicMain")
    @PostMapping("/delete/batch")
    public ZtjoResponse<String> removeBatch(@RequestBody List<Long> ids) {
        dicHandleService.removeItemsByIds(ids);
        return ZtjoResponse.ok("批量删除成功");
    }

    @ApiOperation(value = "根据id查询", notes = "根据id查询")
    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    public ZtjoResponse<DicItem> get(@PathVariable Serializable id) {
        Object o = baseService.getById(id);
        return ZtjoResponse.ok((DicItem) o);
    }

    @ApiOperation(value = "查询全部", notes = "查询全部")
    @RequestMapping(value = "/all",method = RequestMethod.GET)
    public ZtjoResponse<List<DicItem>> all() {
        return ZtjoResponse.ok(baseService.list());
    }

    @ApiOperation(value = "按条件查询", notes = "按传入条件查询")
    @RequestMapping(value = "/query",method = RequestMethod.POST)
    public ZtjoResponse<List<DicItem>> query(@RequestBody DicItem entity) {
        return ZtjoResponse.ok(baseService.list(Wrappers.lambdaQuery(entity)));
    }

    @ApiOperation(value = "根据条件分页查询", notes = "根据条件分页查询")
    @RequestMapping(value = "/page",method = RequestMethod.GET)
    public ZtjoResponse list(Page page, DicItem entity) {
        return ZtjoResponse.ok(baseService.page(page, Wrappers.query(entity)));
    }
}

