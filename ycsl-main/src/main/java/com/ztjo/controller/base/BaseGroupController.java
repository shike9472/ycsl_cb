package com.ztjo.controller.base;

import cn.hutool.core.convert.Convert;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ztjo.core.service.base.BaseGroupService;
import com.ztjo.core.service.base.BaseResourceAuthorityService;
import com.ztjo.core.utils.TreeUtils;
import com.ztjo.data.annotation.authorize.PreElement;
import com.ztjo.data.annotation.authorize.PreMenu;
import com.ztjo.data.entity.base.BaseGroup;
import com.ztjo.data.entity.base.BaseResourceAuthority;
import com.ztjo.data.exception.YcslBizException;
import com.ztjo.data.pojo.api.ZtjoResponse;
import com.ztjo.data.pojo.bo.GroupBo;
import com.ztjo.data.pojo.vo.model.TreeEntityVo;
import com.ztjo.data.pojo.vo.settings.AllGroupVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import static com.ztjo.core.utils.TreeUtils.getTreeUpFromPool;

/**
 * <p>
 * 角色组表 - 权限集合表征 前端控制器
 * </p>
 *
 * @author 陈彬
 * @since 2021-10-30
 */
@RestController
@RequestMapping("/baseGroup")
@PreMenu("menu_jsgl")
@Api(tags = {"[Base]角色组管理API"})
public class BaseGroupController {
    /**
     * 基础service装载
     */
    @Autowired
    private BaseGroupService baseService;
    @Autowired
    private BaseResourceAuthorityService resourceAuthorityService;

    @ApiOperation(value = "查询全部", notes = "查询全部")
    @RequestMapping(value = "/all",method = RequestMethod.GET)
    public ZtjoResponse<List<BaseGroup>> all() {
        return ZtjoResponse.ok(baseService.list());
    }

    @ApiOperation(value = "按条件查询", notes = "按传入条件查询")
    @RequestMapping(value = "/query",method = RequestMethod.POST)
    public ZtjoResponse<List<BaseGroup>> query(@RequestBody BaseGroup entity) {
        return ZtjoResponse.ok(baseService.list(Wrappers.lambdaQuery(entity)));
    }

    @ApiOperation(value = "根据条件分页查询", notes = "根据条件分页查询")
    @RequestMapping(value = "/page",method = RequestMethod.GET)
    public ZtjoResponse list(Page page, BaseGroup entity) {
        return ZtjoResponse.ok(baseService.page(page, Wrappers.query(entity)));
    }

    /**
     * 新增
     * @param entity
     * @return
     */
    @PreElement("button_group_add")
    @ApiOperation(value = "记录新增", notes = "记录新增")
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    public ZtjoResponse<BaseGroup> add(@RequestBody BaseGroup entity) {
        baseService.save(entity);
        return ZtjoResponse.ok(entity);
    }

    /**
     * 更新
     * @param entity
     * @return
     */
    @PreElement("button_group_update")
    @ApiOperation(value = "根据id更新", notes = "根据id更新")
    @RequestMapping(value = "/updateById",method = RequestMethod.POST)
    public ZtjoResponse<BaseGroup> updateById(@RequestBody BaseGroup entity) {
        baseService.updateById(entity);
        return ZtjoResponse.ok(entity);
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @PreElement("button_group_delete")
    @ApiOperation(value = "根据id删除", notes = "根据id删除")
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    public ZtjoResponse<Boolean> remove(@PathVariable Serializable id) {
        List<GroupBo> bos = baseService.loadGroupBos();
        if(TreeUtils.containsNodes(bos, Long.parseLong(id.toString()))) {
            throw new YcslBizException("["+id+"]角色项中仍存在子目录，请清理子级角色目录后重试");
        }
        baseService.groupCanRemove(id);
        return ZtjoResponse.ok(baseService.removeById(id));
    }

    /**
     * 根据id加载详情
     * @param id
     * @return
     */
    @ApiOperation(value = "根据id查询", notes = "根据id查询")
    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    public ZtjoResponse<BaseGroup> get(@PathVariable Serializable id) {
        Object o = baseService.getById(id);
        return ZtjoResponse.ok((BaseGroup) o);
    }

    /**
     * 加载角色列表（树形）
     * @return
     */
    @GetMapping("/load/tree")
    @ApiOperation(value = "加载角色树", notes = "加载角色树")
    public ZtjoResponse<AllGroupVo> loadTree() {
        return ZtjoResponse.ok(baseService.loadGroupsTree());
    }

    /**
     * 角色 - 资源权限下发
     * @param id
     * @return
     */
    @PreElement("button_group_grant_down")
    @GetMapping("/grant/copyParent")
    @ApiOperation(value = "角色 - 资源权限下发", notes = "角色 - 资源权限下发")
    public ZtjoResponse<String> grant(@RequestParam("id") Long id) {
        List<BaseResourceAuthority> resourceAuthorities = resourceAuthorityService
                .list(Wrappers.<BaseResourceAuthority>query().lambda().eq(BaseResourceAuthority::getAuthorityId, id));
        List<GroupBo> pool = baseService.list().stream().map(g -> Convert.convert(GroupBo.class, g)).collect(Collectors.toList());
        List<TreeEntityVo<GroupBo>> tarTree = getTreeUpFromPool(pool, id);
        baseService.grant2Kids(resourceAuthorities, tarTree);
        return ZtjoResponse.ok("权限下发成功！");
    }
}

