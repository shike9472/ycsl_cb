package com.ztjo.controller.base;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ztjo.core.service.base.BaseElementService;
import com.ztjo.core.service.base.BaseMenuService;
import com.ztjo.core.utils.TreeUtils;
import com.ztjo.data.annotation.authorize.PreElement;
import com.ztjo.data.annotation.authorize.PreMenu;
import com.ztjo.data.annotation.authorize.PreMenuIgnore;
import com.ztjo.data.entity.base.BaseElement;
import com.ztjo.data.entity.base.BaseMenu;
import com.ztjo.data.exception.YcslBizException;
import com.ztjo.data.pojo.api.ZtjoResponse;
import com.ztjo.data.pojo.bo.MenuBo;
import com.ztjo.data.pojo.vo.model.TreeEntityVo;
import com.ztjo.service.ElementManageService;
import com.ztjo.service.MenuManageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

import static com.ztjo.core.utils.ResourceTypeCheckUtils.checkMenuResourceTypeRight;
import static com.ztjo.core.utils.ResourceTypeCheckUtils.checkMenuTypeRight;

/**
 * <p>
 * 菜单资源表 - 保存系统菜单 前端控制器
 * </p>
 *
 * @author 陈彬
 * @since 2021-10-30
 */
@RestController
@RequestMapping("/baseMenu")
@PreMenu("menu_cdgl")
@Api(tags = {"[Base]菜单资源管理API"})
public class BaseMenuController {

    /**
    * 基础service装载
    */
    @Autowired
    private BaseMenuService baseService;

    /**
     * 资源服务装载
     */
    @Autowired
    private BaseElementService elementService;

    /**
     * 菜单管理服务装载
     */
    @Autowired
    private MenuManageService menuManageService;

    /**
     * 资源管理服务装载
     */
    @Autowired
    private ElementManageService elementManageService;

    @PreMenuIgnore
    @ApiOperation(value = "根据id查询", notes = "根据id查询")
    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    public ZtjoResponse<BaseMenu> get(@PathVariable Serializable id) {
        Object o = baseService.getById(id);
        return ZtjoResponse.ok((BaseMenu) o);
    }

    @ApiOperation(value = "查询全部", notes = "查询全部")
    @RequestMapping(value = "/all",method = RequestMethod.GET)
    public ZtjoResponse<List<BaseMenu>> all() {
        return ZtjoResponse.ok(baseService.list());
    }

    @ApiOperation(value = "按条件查询", notes = "按传入条件查询")
    @RequestMapping(value = "/query",method = RequestMethod.POST)
    public ZtjoResponse<List<BaseMenu>> query(@RequestBody BaseMenu entity) {
        return ZtjoResponse.ok(baseService.list(Wrappers.lambdaQuery(entity)));
    }

    @ApiOperation(value = "根据条件分页查询", notes = "根据条件分页查询")
    @RequestMapping(value = "/page",method = RequestMethod.GET)
    public ZtjoResponse list(Page page, BaseMenu entity) {
        return ZtjoResponse.ok(baseService.page(page, Wrappers.query(entity)));
    }

    /**
     * 菜单管理功能内，菜单自身增删改查模块
     */
    @GetMapping(value = "/load/tree")
    @ApiOperation(value = "菜单管理 - 加载全部菜单（树形结构返回）",
            notes = "菜单管理 - 加载全部菜单（树形结构返回）")
    public ZtjoResponse<List<TreeEntityVo<MenuBo>>> loadTree() {
        return ZtjoResponse.ok(menuManageService.loadTree());
    }

    @PreElement("button_menu_add")
    @ApiOperation(value = "记录新增", notes = "记录新增")
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    public ZtjoResponse<BaseMenu> add(@RequestBody BaseMenu entity) {
        checkMenuTypeRight(entity, true);
        baseService.save(entity);
        return ZtjoResponse.ok(entity);
    }

    @PreElement("button_menu_update")
    @ApiOperation(value = "根据id更新", notes = "根据id更新")
    @RequestMapping(value = "/updateById",method = RequestMethod.POST)
    public ZtjoResponse<BaseMenu> updateById(@RequestBody BaseMenu entity) {
        checkMenuTypeRight(entity, false);
        baseService.updateById(entity);
        return ZtjoResponse.ok(entity);
    }

    @PreElement("button_menu_delete")
    @ApiOperation(value = "根据id删除", notes = "根据id删除")
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    public ZtjoResponse<Boolean> remove(@PathVariable Serializable id) {
        // 判断菜单下是否已经不存在子级目录
        List<BaseMenu> menus = baseService.list();
        List<MenuBo> bos = Convert.toList(MenuBo.class, menus);
        if(TreeUtils.containsNodes(bos, Long.parseLong(id.toString()))) {
            throw new YcslBizException("["+id+"]菜单项中仍存在子目录，请清理子级菜单目录后重试");
        }
        // 查询隶属菜单的element资源，查询前置做读写事务分离
        List<BaseElement> menusElements = elementService.list(
                Wrappers.<BaseElement>query().lambda().eq(BaseElement::getMenuId, id));
        /**
         * 调用菜单管理菜单移除功能方法，
         *      1. 删除对应菜单；
         *      2. 删除菜单下对应element；
         *      3. 删除菜单及element对应的用户角色权限关系；
         *      4. 清理对应缓存。
         */
        return ZtjoResponse.ok(menuManageService.removeById(id, menusElements));
    }

    /**
     * 菜单管理功能内，菜单扩展资源相关
     */
    @GetMapping("/elements")
    @ApiOperation(value = "获取菜单内的element资源", notes = "获取菜单内的element资源")
    public ZtjoResponse<List<BaseElement>> gainBelongElement(
            @RequestParam("menuId") Long menuId, @RequestParam(value = "eName", required = false) String eName) {
        return ZtjoResponse.ok(
                elementService.list(Wrappers.<BaseElement>query()
                        .lambda()
                        .eq(BaseElement::getMenuId, menuId)
                        .eq(StrUtil.isNotBlank(eName), BaseElement::getName, eName)
                )
        );
    }

    @PreElement("button_element_add_in_menu")
    @PostMapping(value = "/add/element")
    @ApiOperation(value = "菜单内新增资源", notes = "菜单内新增资源")
    public ZtjoResponse<BaseElement> addElement(@RequestBody BaseElement entity) {
        if(ObjectUtil.isNull(entity.getMenuId())) {
            throw new YcslBizException("新增菜单资源时，关联的菜单id不可为空");
        }
        if(StrUtil.isBlank(entity.getCode())) {
            throw new YcslBizException("新增菜单资源时，资源标识不可为空");
        }
        checkMenuResourceTypeRight(entity, true);
        elementService.save(entity);
        return ZtjoResponse.ok(entity);
    }

    @PreElement("button_element_update_in_menu")
    @PostMapping(value = "/update/element")
    @ApiOperation(value = "菜单内更新资源", notes = "菜单内更新资源")
    public ZtjoResponse<BaseElement> updateElement(@RequestBody BaseElement entity) {
        checkMenuResourceTypeRight(entity, false);
        elementService.updateById(entity);
        return ZtjoResponse.ok(entity);
    }

    @PreElement("button_element_remove_in_menu")
    @ApiOperation(value = "根据菜单内资源id集合删除", notes = "根据菜单内资源id集合删除")
    @PostMapping("/remove/elements")
    public ZtjoResponse<String> removeElements(@RequestBody List<Long> ids) {
        elementManageService.removeInMenuByIds(ids);
        return ZtjoResponse.ok("菜单资源删除成功！");
    }
}

