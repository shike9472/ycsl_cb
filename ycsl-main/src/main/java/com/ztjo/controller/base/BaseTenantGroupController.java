package com.ztjo.controller.base;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ztjo.core.service.base.BaseTenantGroupService;
import com.ztjo.data.annotation.authorize.PreElement;
import com.ztjo.data.annotation.authorize.PreMenu;
import com.ztjo.data.entity.base.BaseTenantGroup;
import com.ztjo.data.pojo.api.ZtjoResponse;
import com.ztjo.data.pojo.dto.RelationModifyDto;
import com.ztjo.data.pojo.vo.model.RelationModifyConditionVo;
import com.ztjo.data.pojo.vo.settings.AllGroupVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 租户拥有的<角色处置权>授权表 前端控制器
 * 描述租户可以给
 * 		1. 相同租户的用户 或 2. 相同租户的岗位
 * 授予哪些角色组
 * </p>
 *
 * @author 陈彬
 * @since 2021-10-30
 */
@RestController
@RequestMapping("/baseTenantGroup")
@PreMenu("menu_zhgl")
@Api(tags = {"[Base]租户 - 角色使用授权管理API"})
public class BaseTenantGroupController {
    /**
    * 基础service装载
    */
    @Autowired
    private BaseTenantGroupService baseService;

    /**
     * 更新租户角色授权 - 根据传入租户id更新
     * @param dto
     * @return
     */
    @PostMapping("/modify")
    @PreElement("button_tenant_group_grant")
    @ApiOperation(value = "更新租户角色授权 - 根据传入租户id更新",
            notes = "更新租户角色授权 - 根据传入租户id更新")
    public ZtjoResponse<String> modify(@Valid @RequestBody RelationModifyDto<BaseTenantGroup> dto)
    {
        baseService.modifyTenantGroups(dto);
        return ZtjoResponse.ok("更新租户角色授权成功！");
    }

    /**
     * 租户角色授权 - 加载所有待授权目标以及已授权目标
     * @param tarId
     * @return
     */
    @GetMapping("/load")
    @ApiOperation(value = "租户角色授权 - 加载所有待授权目标以及已授权目标",
            notes = "租户角色授权 - 加载所有待授权目标以及已授权目标")
    public ZtjoResponse<RelationModifyConditionVo<AllGroupVo>> loadTenantGroups(@RequestParam("tarId") Long tarId)
    {
        return ZtjoResponse.ok(baseService.loadAlready(tarId), "success");
    }

}

