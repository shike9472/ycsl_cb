package com.ztjo.controller.base;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ztjo.core.service.base.BaseResourceAuthorityService;
import com.ztjo.data.annotation.authorize.PreElement;
import com.ztjo.data.annotation.authorize.PreMenu;
import com.ztjo.data.entity.base.BaseResourceAuthority;
import com.ztjo.data.entity.base.BaseTenantGroup;
import com.ztjo.data.pojo.api.ZtjoResponse;
import com.ztjo.data.pojo.dto.RelationModifyDto;
import com.ztjo.data.pojo.vo.model.RelationModifyConditionVo;
import com.ztjo.data.pojo.vo.settings.AllGroupResourceVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 菜单，uri，按钮等资源 - group授权表 前端控制器
 * </p>
 *
 * @author 陈彬
 * @since 2021-10-30
 */
@RestController
@RequestMapping("/baseResourceAuthority")
@PreMenu("menu_jsgl")
@Api(tags = {"[Base]角色组 - 资源授权管理API"})
public class BaseResourceAuthorityController {
        /**
    * 基础service装载
    */
    @Autowired
    private BaseResourceAuthorityService baseService;

    @ApiOperation(value = "记录新增", notes = "记录新增")
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    public ZtjoResponse<BaseResourceAuthority> add(@RequestBody BaseResourceAuthority entity) {
        baseService.save(entity);
        return ZtjoResponse.ok(entity);
    }

    @ApiOperation(value = "根据id查询", notes = "根据id查询")
    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    public ZtjoResponse<BaseResourceAuthority> get(@PathVariable Serializable id) {
        Object o = baseService.getById(id);
        return ZtjoResponse.ok((BaseResourceAuthority) o);
    }

    @ApiOperation(value = "根据id更新", notes = "根据id更新")
    @RequestMapping(value = "/updateById",method = RequestMethod.POST)
    public ZtjoResponse<BaseResourceAuthority> updateById(@RequestBody BaseResourceAuthority entity) {
        baseService.updateById(entity);
        return ZtjoResponse.ok(entity);
    }

    @ApiOperation(value = "根据id删除", notes = "根据id删除")
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    public ZtjoResponse<Boolean> remove(@PathVariable Serializable id) {
        return ZtjoResponse.ok(baseService.removeById(id));
    }

    @ApiOperation(value = "查询全部", notes = "查询全部")
    @RequestMapping(value = "/all",method = RequestMethod.GET)
    public ZtjoResponse<List<BaseResourceAuthority>> all() {
        return ZtjoResponse.ok(baseService.list());
    }

    @ApiOperation(value = "按条件查询", notes = "按传入条件查询")
    @RequestMapping(value = "/query",method = RequestMethod.POST)
    public ZtjoResponse<List<BaseResourceAuthority>> query(@RequestBody BaseResourceAuthority entity) {
        return ZtjoResponse.ok(baseService.list(Wrappers.lambdaQuery(entity)));
    }

    @ApiOperation(value = "根据条件分页查询", notes = "根据条件分页查询")
    @RequestMapping(value = "/page",method = RequestMethod.GET)
    public ZtjoResponse list(Page page, BaseResourceAuthority entity) {
        return ZtjoResponse.ok(baseService.page(page, Wrappers.query(entity)));
    }

    /**
     * 加载资源权限，分两类：1. 菜单资源；2. element资源
     */
    @GetMapping("/load")
    @ApiOperation(value = "租户角色授权 - 加载所有待授权目标以及已授权目标",
            notes = "租户角色授权 - 加载所有待授权目标以及已授权目标")
    public ZtjoResponse<RelationModifyConditionVo<AllGroupResourceVo>> loadGroupResources(@RequestParam("tarId") Long tarId)
    {
        return ZtjoResponse.ok(baseService.loadAlready(tarId), "success");
    }

    /**
     * 角色资源授权
     * @param dto
     * @return
     */
    @PreElement("button_group_resource_grant")
    @ApiOperation(value = "更新角色资源授权 - 根据传入角色id更新",
            notes = "更新角色资源授权 - 根据传入角色id更新")
    @RequestMapping(value = "/modify",method = RequestMethod.POST)
    public ZtjoResponse<String> modify(@Valid @RequestBody RelationModifyDto<BaseResourceAuthority> dto)
    {
        baseService.modifyGroupResources(dto);
        return ZtjoResponse.ok("更新角色资源授权成功！");
    }

}

