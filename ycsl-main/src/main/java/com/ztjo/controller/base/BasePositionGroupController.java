package com.ztjo.controller.base;

import cn.hutool.core.util.ObjectUtil;
import com.ztjo.core.service.base.BasePositionGroupService;
import com.ztjo.data.annotation.authorize.PreElement;
import com.ztjo.data.annotation.authorize.PreMenu;
import com.ztjo.data.annotation.authorize.PreTenant;
import com.ztjo.data.entity.base.BaseGroup;
import com.ztjo.data.entity.base.BasePosition;
import com.ztjo.data.entity.base.BasePositionGroup;
import com.ztjo.data.exception.YcslBizException;
import com.ztjo.data.pojo.api.ZtjoResponse;
import com.ztjo.data.pojo.dto.RelationModifyDto;
import com.ztjo.data.pojo.vo.model.RelationModifyConditionVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 岗位 - 角色组挂接表 前端控制器
 * </p>
 *
 * @author 陈彬
 * @since 2021-10-30
 */
@RestController
@RequestMapping("/basePositionGroup")
@PreMenu("menu_bmgl")
@Api(tags = {"[Base]角色组 - 岗位授权管理API"})
public class BasePositionGroupController {
    /**
    * 基础service装载
    */
    @Autowired
    private BasePositionGroupService baseService;

    /**
     * TODO 岗位角色管理 - start
     */
    /**
     * 加载岗位-用户管理情况
     * @param position
     * @return
     */
    @PreTenant
    @PostMapping("/load")
    @ApiOperation(value = "加载岗位-角色管理情况", notes = "加载岗位-角色管理情况")
    public ZtjoResponse<RelationModifyConditionVo<List<BaseGroup>>> load(@RequestBody BasePosition position)
    {
        if(ObjectUtil.isNull(position.getId())) {
            throw new YcslBizException("加载岗位-角色关系必须传入岗位id入参！");
        }
        return ZtjoResponse.ok(baseService.loadPositionGroupsAlready(position));
    }

    /**
     * 修改岗位角色配置
     * @param dto
     * @return
     */
    @PreElement("button_position_group_modify")
    @ApiOperation(value = "更新角色组挂接岗位 - 根据传入岗位id更新",
            notes = "更新角色组挂接岗位 - 根据传入岗位id更新")
    @RequestMapping(value = "/modify",method = RequestMethod.POST)
    public ZtjoResponse<String> modify(@Valid @RequestBody RelationModifyDto<BasePositionGroup> dto)
    {
        baseService.modifyPositionGroups(dto);
        return ZtjoResponse.ok("更新角色组挂接岗位成功！");
    }
    /**
     * TODO 岗位角色管理 - end
     */
}

