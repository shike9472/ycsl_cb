package com.ztjo.controller.base;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ztjo.core.service.base.BaseElementService;
import com.ztjo.data.annotation.authorize.PreElement;
import com.ztjo.data.annotation.authorize.PreMenu;
import com.ztjo.data.annotation.authorize.PreMenuIgnore;
import com.ztjo.data.entity.base.BaseElement;
import com.ztjo.data.exception.YcslBizException;
import com.ztjo.data.pojo.api.ZtjoResponse;
import com.ztjo.service.ElementManageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

import static com.ztjo.core.utils.ResourceTypeCheckUtils.checkElementTypeRight;

/**
 * <p>
 * 组件资源表 - 保存uri或button资源的表 前端控制器
 * </p>
 *
 * @author 陈彬
 * @since 2021-10-30
 */
@RestController
@RequestMapping("/baseElement")
@PreMenu("menu_zygl")
@Api(tags = {"[Base]元素资源管理API"})
public class BaseElementController {
    /**
    * 基础service装载
    */
    @Autowired
    private BaseElementService baseService;

    /**
     * 资源管理服务装载
     */
    @Autowired
    private ElementManageService elementManageService;

    @ApiOperation(value = "根据条件分页查询，查询可以使用四个条件：menuid，code，name，type模糊",
            notes = "根据条件分页查询，查询可以使用四个条件：menuid，code，name，type模糊")
    @RequestMapping(value = "/page",method = RequestMethod.GET)
    public ZtjoResponse list(Page page, BaseElement entity) {
        return ZtjoResponse.ok(baseService.getElementsPage(page, entity));
    }

    @PreElement("button_element_add")
    @ApiOperation(value = "记录新增", notes = "记录新增")
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    public ZtjoResponse<BaseElement> add(@RequestBody BaseElement entity) {
        if(StrUtil.isBlank(entity.getCode())) {
            throw new YcslBizException("新增资源标识不可为空");
        }
        checkElementTypeRight(entity, true);
        baseService.save(entity);
        return ZtjoResponse.ok(entity);
    }

    @PreElement("button_element_update")
    @ApiOperation(value = "根据id更新", notes = "根据id更新")
    @RequestMapping(value = "/updateById",method = RequestMethod.POST)
    public ZtjoResponse<BaseElement> updateById(@RequestBody BaseElement entity) {
        checkElementTypeRight(entity, false);
        baseService.updateById(entity);
        return ZtjoResponse.ok(entity);
    }

    @PreElement("button_element_remove")
    @ApiOperation(value = "根据id删除", notes = "根据id删除")
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    public ZtjoResponse<Boolean> remove(@PathVariable Serializable id) {
        return ZtjoResponse.ok(elementManageService.removeById(id));
    }

    @PreMenuIgnore
    @ApiOperation(value = "根据id查询", notes = "根据id查询")
    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    public ZtjoResponse<BaseElement> get(@PathVariable Serializable id) {
        Object o = baseService.getById(id);
        return ZtjoResponse.ok((BaseElement) o);
    }

    @ApiOperation(value = "查询全部", notes = "查询全部")
    @RequestMapping(value = "/all",method = RequestMethod.GET)
    public ZtjoResponse<List<BaseElement>> all() {
        return ZtjoResponse.ok(baseService.list());
    }

    @ApiOperation(value = "按条件查询", notes = "按传入条件查询")
    @RequestMapping(value = "/query",method = RequestMethod.POST)
    public ZtjoResponse<List<BaseElement>> query(@RequestBody BaseElement entity) {
        return ZtjoResponse.ok(baseService.list(Wrappers.lambdaQuery(entity)));
    }
}

