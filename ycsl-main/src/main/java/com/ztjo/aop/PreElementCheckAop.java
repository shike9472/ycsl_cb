package com.ztjo.aop;

import cn.hutool.core.text.StrFormatter;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ztjo.core.service.base.BaseElementService;
import com.ztjo.core.service.base.BaseMenuService;
import com.ztjo.data.annotation.authorize.PreElement;
import com.ztjo.data.annotation.authorize.PreMenu;
import com.ztjo.data.entity.base.BaseElement;
import com.ztjo.data.entity.base.BaseMenu;
import com.ztjo.data.exception.YcslAuthenticationException;
import com.ztjo.data.exception.YcslBizException;
import com.ztjo.service.sys.PermissionService;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author 陈彬
 * @version 2021/10/29
 * description：组件权限检查aop
 */
@Slf4j
@Aspect
@Component
@Order(3)
public class PreElementCheckAop {

    @Autowired
    private PermissionService permissionService;
    @Autowired
    private BaseElementService elementService;
    @Autowired
    private BaseMenuService menuService;

    /**
     * 资源权限检查
     * @param joinPoint
     */
    @Before("@annotation(com.ztjo.data.annotation.authorize.PreElement)")
    public void preElementCheck(JoinPoint joinPoint) {
        // 切面织入方法
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        // 声明方法主题
        String value = "";
        // 注解实例获取
        PreElement elementOperation = (PreElement) signature.getMethod().getAnnotation(PreElement.class);
        if (ObjectUtil.isNotNull(elementOperation)) {
            value = elementOperation.value();
        }
        log.info("pre element check -> {}", value);
        // 资源检查
        if (StrUtil.isNotBlank(value)) {
            if (!permissionService.hasTarElement(value)) {
                throw new YcslAuthenticationException("用户资源["+value+"]使用越权！", 500);
            }
            // 资源菜单配置一致性检查
            log.info("进行菜单与资源一致性检查！");
            BaseElement element = elementService.getOne(Wrappers.<BaseElement>lambdaQuery().eq(BaseElement::getCode, value));
            Long menuId = element.getMenuId();
            if(ObjectUtil.isNotNull(menuId)) {
                BaseMenu curMenu = menuService.getById(menuId);
                PreMenu menuOperation = (PreMenu) signature.getDeclaringType().getAnnotation(PreMenu.class);
                if(ObjectUtil.isNotNull(menuOperation)) {
                    String standard = menuOperation.value();
                    if(StrUtil.isNotBlank(standard)) {
                        if (!standard.equals(curMenu.getCode())) {
                            throw new YcslBizException(
                                    StrFormatter.format("资源[{}]使用菜单配置为[{}]，程序设置为[{}]，违反菜单-资源一致性检查!",
                                            value, curMenu.getCode(), standard)
                            );
                        }
                    }
                }
            }
        }
    }
}
