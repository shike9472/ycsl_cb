package com.ztjo.aop;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.ztjo.core.model.YcslUserDetails;
import com.ztjo.core.utils.SecurityUtils;
import com.ztjo.data.entity.base.BaseArea;
import com.ztjo.data.exception.YcslAuthenticationException;
import com.ztjo.data.exception.YcslBizException;
import com.ztjo.service.sys.PermissionService;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

import java.util.Optional;

import static com.ztjo.data.constants.PreCheckConstants.PRE_AREA_SAVE_IN_HEADER;

/**
 * @author 陈彬
 * @version 2021/12/13
 * description：区域检查aop
 */
@Slf4j
@Aspect
@Component
@Order(5)
public class PreAreaCheckAop {
    @Autowired
    private PermissionService permissionService;

    /**
     * 资源权限检查
     * @param joinPoint
     */
    @Before("@annotation(com.ztjo.data.annotation.authorize.PreArea)")
    public void preAreaCheck(JoinPoint joinPoint) {
        // 所属部门
        Long departId = SecurityUtils.curDepartId();
        // 取请求头
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        String areaCode = request.getHeader(PRE_AREA_SAVE_IN_HEADER);
        if(StrUtil.isBlank(areaCode)) {
            throw new YcslBizException("请选择业务生效区域！");
        }
        // 检查部门区域是否够权限
        BaseArea area = permissionService.findTarAreaInDepartLevel(areaCode, departId);
        if(ObjectUtil.isNull(area)) {
            throw new YcslAuthenticationException("您暂时无法操作该区域办件！", 500);
        }
        YcslUserDetails curLoginUser = SecurityUtils.curLoginUser();
        // 赋值本次请求线程区域相关变量
        curLoginUser.setReqBizArea(areaCode);
        curLoginUser.setReqMiddleHost(
                area.getMiddleReqType()
                        + "://" + area.getMiddleHost()
                        + ":" + area.getMiddlePort()
                        + "/" + Optional.ofNullable(area.getMiddleSuffix()).map(s -> s +"/").orElse("")
        );
    }
}
