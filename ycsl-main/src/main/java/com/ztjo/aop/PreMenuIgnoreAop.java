package com.ztjo.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

import static com.ztjo.data.constants.PreCheckConstants.PRE_CHECK_MENU_IGNORE;
import static com.ztjo.data.enums.SFEnums.SF_S;

/**
 * @author 陈彬
 * @version 2021/12/13
 * description：菜单使用权限检查忽略aop
 */
@Slf4j
@Aspect
@Component
@Order(1)
public class PreMenuIgnoreAop {

    @Before("@annotation(com.ztjo.data.annotation.authorize.PreMenuIgnore)")
    public void preMenuIgnoreMark(JoinPoint joinPoint) {
        log.info("pre menu check ignore setting...");
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        request.setAttribute(PRE_CHECK_MENU_IGNORE, SF_S.getCode());
    }
}
