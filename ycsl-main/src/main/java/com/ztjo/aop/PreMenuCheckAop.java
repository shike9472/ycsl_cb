package com.ztjo.aop;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.ztjo.data.annotation.authorize.PreMenu;
import com.ztjo.data.exception.YcslAuthenticationException;
import com.ztjo.service.sys.PermissionService;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

import static com.ztjo.data.constants.PreCheckConstants.PRE_CHECK_MENU_IGNORE;
import static com.ztjo.data.enums.SFEnums.SF_S;

/**
 * @author 陈彬
 * @version 2021/10/29
 * description：菜单权限检查aop
 *      切点函数：
 *          @annotation - 接受方法被注解修饰的方法
 *          @args       - 接受方法入参被注解修饰的方法
 *          @target     - 接受被注解修饰的类
 *          @within     - 接受被注解修饰的类以及子类
 *      后两者函数的使用依赖@Bean = AtTargetAspect类的装载使用
 */
@Slf4j
@Aspect
@Component
@Order(2)
public class PreMenuCheckAop {

    @Autowired
    private PermissionService permissionService;

    /**
     * controller前置增强方法
     * menu权限检查
     * @param joinPoint
     */
    @Before("@within(com.ztjo.data.annotation.authorize.PreMenu)")
    public void preMenuCheck(JoinPoint joinPoint) {
        // 取请求头
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        // 是否已设置菜单检查忽略
        if(!SF_S.getCode().equals(request.getAttribute(PRE_CHECK_MENU_IGNORE))) {
            // 切面织入方法
            MethodSignature signature = (MethodSignature) joinPoint.getSignature();
            // 声明方法主题
            String value = "";
            // 注解实例获取
            PreMenu menuOperation = (PreMenu) signature.getDeclaringType().getAnnotation(PreMenu.class);
            if (ObjectUtil.isNotNull(menuOperation)) {
                value = menuOperation.value();
            }
            log.info("pre menu check -> {}", value);
            if (StrUtil.isNotBlank(value)) {
                if (!permissionService.hasTarMenu(value)) {
                    throw new YcslAuthenticationException("用户菜单["+value+"]使用越权！", 500);
                }
            }
        }
    }
}
