package com.ztjo.aop;

import cn.hutool.core.util.ObjectUtil;
import com.ztjo.core.utils.SecurityUtils;
import com.ztjo.data.entity.model.BaseTenantModel;
import com.ztjo.data.exception.YcslAuthenticationException;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author 陈彬
 * @version 2021/12/13
 * description：用户租户检查
 */
@Slf4j
@Aspect
@Component
@Order(4)
public class PreTenantCheckAop {

    /**
     * 租户检查aop
     * todo 需要加单元测试
     *  1. 测试aop方法可以正确取得租户id
     *  2. 测试整体拦截是否正常
     * @param joinPoint
     */
    @Before("@annotation(com.ztjo.data.annotation.authorize.PreTenant)")
    public void preTenantCheck(JoinPoint joinPoint) {
        log.info("pre tenant check");
        BaseTenantModel tenantModel = null;
        Object[] args = joinPoint.getArgs();
        for(Object arg: args) {
            if(arg instanceof BaseTenantModel) {
                tenantModel = (BaseTenantModel)arg;
                break;
            }
        }
        if(ObjectUtil.isNull(tenantModel) || ObjectUtil.isNull(tenantModel.getTenantId())) {
            throw new YcslAuthenticationException("操作租户模型的实体数据时，必须给定租户id！", 500);
        }

        // 管理员校验
        if(!SecurityUtils.isAdmin()) {
            // 租管用户检查租户设置是否正确
            Long curTenantId = SecurityUtils.curTenantId();
            Long tarTenantId = tenantModel.getTenantId();
            if(!tarTenantId.equals(curTenantId)) {
                throw new YcslAuthenticationException("租户管理员操作租户模型对象时，必须保持租户与自身一致！", 500);
            }
        }
    }
}
