package com.ztjo.config;

import com.ztjo.common.annotation.YcslFrameworkScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author 陈彬
 * @version 2021/9/19
 * description：一窗受理主程序 - 依赖对象加载
 */
@Configuration
@YcslFrameworkScan
public class YcslMainLoadConfig {
}
