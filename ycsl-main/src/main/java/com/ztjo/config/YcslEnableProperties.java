package com.ztjo.config;

import com.ztjo.common.properties.ActBackLockProperties;
import com.ztjo.common.properties.AuthCustomProperties;
import com.ztjo.common.properties.GeneratorProperties;
import com.ztjo.common.properties.RedisCustomProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author 陈彬
 * @version 2021/9/4
 * description：properties生效配置
 */
@Configuration
@EnableConfigurationProperties(
        {
                AuthCustomProperties.class, GeneratorProperties.class,
                RedisCustomProperties.class, ActBackLockProperties.class})
public class YcslEnableProperties {
}
