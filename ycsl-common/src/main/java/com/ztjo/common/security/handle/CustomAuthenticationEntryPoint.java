package com.ztjo.common.security.handle;

import com.alibaba.fastjson.JSON;
import com.ztjo.common.utils.StringUtils;
import com.ztjo.data.exception.YcslUnAuthException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;

/**
 * 认证失败处理类 返回未授权
 * 
 * @author 陈彬
 */
@Slf4j
@Component
public class CustomAuthenticationEntryPoint implements AuthenticationEntryPoint, Serializable
{
    private static final long serialVersionUID = -8970718410437077606L;

    /**
     * prePostEnabled验证结果为false时的处理方法
     * @param request    --  请求request
     * @param response   --  响应response
     * @param e          --  授权异常，spring-security使用@PostAuthorize注解验证不通过时封装返回的异常
     */
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException e)
    {
        log.warn("[{}]越权访问接口资源 - [{}]", request.getRemoteHost(), request.getRequestURI());
        String msg = StringUtils.format("请求访问：{}，认证失败，无法访问系统资源", request.getRequestURI());
        throw new YcslUnAuthException(msg);
    }
}
