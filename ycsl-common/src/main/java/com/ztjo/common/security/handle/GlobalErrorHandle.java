package com.ztjo.common.security.handle;

import cn.hutool.core.collection.CollUtil;
import com.ztjo.data.exception.*;
import com.ztjo.data.pojo.api.ZtjoResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

import static com.ztjo.data.constants.HttpStatus.BAD_REQUEST;

/**
 * @author 陈彬
 * @version 2021/9/4
 * description：全局异常处理器
 */
@Slf4j
@Order(0)
@ControllerAdvice("com.ztjo")
@ResponseBody
public class GlobalErrorHandle {

    /**
     * 用户安全异常
     * @param e
     * @return
     */
    @ExceptionHandler(YcslAuthenticationException.class)
    public ZtjoResponse exceptionHandler(YcslAuthenticationException e) {
        log.error("用户安全异常！", e);
        return ZtjoResponse.failed(e.getStatus(), e.getMessage());
    }

    /**
     * 业务异常响应
     * @param e
     * @return
     */
    @ExceptionHandler(YcslBizException.class)
    public ZtjoResponse exceptionHandler(YcslBizException e) {
        log.error("业务执行异常！", e);
        return ZtjoResponse.failed(e.getMessage());
    }

    /**
     * 参数@Valid异常响应
     * @param exception 异常类型
     * @return 前台返回值
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ZtjoResponse exceptionHandler(MethodArgumentNotValidException exception) {
        BindingResult result = exception.getBindingResult();
        StringBuilder stringBuilder = new StringBuilder();
        if (result.hasErrors()) {
            List<ObjectError> errors = result.getAllErrors();
            if (CollUtil.isNotEmpty(errors)) {
                errors.forEach(p -> {
                    FieldError fieldError = (FieldError) p;
                    log.warn("Bad Request Parameters: dto entity [{}],field [{}],message [{}]", fieldError.getObjectName(), fieldError.getField(), fieldError.getDefaultMessage());
                    stringBuilder.append(fieldError.getDefaultMessage()).append(";");
                });
            }
        }
        return ZtjoResponse.failed(BAD_REQUEST, stringBuilder.toString());
    }

    /**
     * 全局500异常响应
     * @param ex
     * @return
     */
    @ExceptionHandler(Exception.class)
    public ZtjoResponse exceptionHandler(Exception ex) {
        log.error("全局运行时异常(未捕获处理)！", ex);
        return ZtjoResponse.failed("系统运行错误！", ex.getMessage());
    }
}
