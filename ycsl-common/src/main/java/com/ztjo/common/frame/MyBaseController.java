package com.ztjo.common.frame;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ztjo.data.pojo.api.ZtjoResponse;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.List;

/**
 * @author 陈彬
 * @version 2021/7/13
 * description：自定义基础Controller继承类
 */
public class MyBaseController<Service extends ServiceImpl,Entity> {
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(String.class, new StringEscapeEditor());
        binder.registerCustomEditor(String[].class, new StringEscapeEditor());
    }

    @Autowired
    protected HttpServletRequest request;
    @Autowired
    protected Service baseService;

    @ApiOperation(value = "记录新增", notes = "记录新增")
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    @ResponseBody
    public ZtjoResponse<Entity> add(@RequestBody Entity entity){
        baseService.save(entity);
        return ZtjoResponse.ok(entity);
    }

    @ApiOperation(value = "根据id查询", notes = "根据id查询")
    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    @ResponseBody
    public ZtjoResponse<Entity> get(@PathVariable Serializable id){
        Object o = baseService.getById(id);
        return ZtjoResponse.ok((Entity) o);
    }

    @ApiOperation(value = "根据id更新", notes = "根据id更新")
    @RequestMapping(value = "/updateById",method = RequestMethod.POST)
    @ResponseBody
    public ZtjoResponse<Entity> updateById(@RequestBody Entity entity){
        baseService.updateById(entity);
        return ZtjoResponse.ok(entity);
    }

    @ApiOperation(value = "根据id删除", notes = "根据id删除")
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    @ResponseBody
    public ZtjoResponse<Boolean> remove(@PathVariable Serializable id){
        return ZtjoResponse.ok(baseService.removeById(id));
    }

    @ApiOperation(value = "查询全部", notes = "查询全部")
    @RequestMapping(value = "/all",method = RequestMethod.GET)
    @ResponseBody
    public ZtjoResponse<List<Entity>> all(){
        return ZtjoResponse.ok(baseService.list());
    }

    @ApiOperation(value = "按条件查询", notes = "按传入条件查询")
    @RequestMapping(value = "/query",method = RequestMethod.POST)
    @ResponseBody
    public ZtjoResponse<List<Entity>> query(@RequestBody Entity entity){
        return ZtjoResponse.ok(baseService.list(Wrappers.lambdaQuery(entity)));
    }

    @ApiOperation(value = "根据条件分页查询", notes = "根据条件分页查询")
    @RequestMapping(value = "/page",method = RequestMethod.GET)
    @ResponseBody
    public ZtjoResponse list(Page page, Entity entity){
        return ZtjoResponse.ok(baseService.page(page, Wrappers.query(entity)));
    }

}
