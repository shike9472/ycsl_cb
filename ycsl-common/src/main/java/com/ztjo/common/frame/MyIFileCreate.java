package com.ztjo.common.frame;

import com.baomidou.mybatisplus.generator.config.rules.FileType;

import java.io.File;

/**
 * @author 陈彬
 * @version 2021/7/16
 * description：
 */
public interface MyIFileCreate {
    boolean isCreate(MyConfigBuilder configBuilder, FileType fileType, String filePath);

    default void checkDir(String filePath) {
        File file = new File(filePath);
        boolean exist = file.exists();
        if (!exist) {
            file.getParentFile().mkdir();
        }

    }
}
