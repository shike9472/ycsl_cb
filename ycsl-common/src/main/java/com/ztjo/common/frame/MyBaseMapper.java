package com.ztjo.common.frame;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author 陈彬
 * @version 2021/7/12
 * description：自定义Mapper框架
 */
public interface MyBaseMapper<T> extends BaseMapper<T> {
}
