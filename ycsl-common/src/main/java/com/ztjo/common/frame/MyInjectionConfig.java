package com.ztjo.common.frame;

import com.baomidou.mybatisplus.generator.InjectionConfig;

/**
 * @author 陈彬
 * @version 2021/7/16
 * description：
 */
public abstract class MyInjectionConfig extends InjectionConfig {
    protected MyConfigBuilder myConfig;
    private MyIFileCreate myFileCreate;

    public MyConfigBuilder getMyConfig(){
        return this.myConfig;
    }
    public MyInjectionConfig setMyConfig(final MyConfigBuilder myConfig){
        this.myConfig = myConfig;
        return this;
    }
    public MyIFileCreate getMyFileCreate() {
        return this.myFileCreate;
    }
    public InjectionConfig setMyFileCreate(final MyIFileCreate fileCreate) {
        this.myFileCreate = fileCreate;
        return this;
    }

    @Override
    protected boolean canEqual(final Object other) {
        return other instanceof MyInjectionConfig;
    }

    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof MyInjectionConfig)) {
            return false;
        } else {
            MyInjectionConfig other = (MyInjectionConfig)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label59: {
                    Object this$config = this.getMyConfig();
                    Object other$config = other.getMyConfig();
                    if (this$config == null) {
                        if (other$config == null) {
                            break label59;
                        }
                    } else if (this$config.equals(other$config)) {
                        break label59;
                    }

                    return false;
                }

                Object this$map = this.getMap();
                Object other$map = other.getMap();
                if (this$map == null) {
                    if (other$map != null) {
                        return false;
                    }
                } else if (!this$map.equals(other$map)) {
                    return false;
                }

                Object this$fileOutConfigList = this.getFileOutConfigList();
                Object other$fileOutConfigList = other.getFileOutConfigList();
                if (this$fileOutConfigList == null) {
                    if (other$fileOutConfigList != null) {
                        return false;
                    }
                } else if (!this$fileOutConfigList.equals(other$fileOutConfigList)) {
                    return false;
                }

                Object this$fileCreate = this.getMyFileCreate();
                Object other$fileCreate = other.getMyFileCreate();
                if (this$fileCreate == null) {
                    if (other$fileCreate != null) {
                        return false;
                    }
                } else if (!this$fileCreate.equals(other$fileCreate)) {
                    return false;
                }

                return true;
            }
        }
    }

    @Override
    public int hashCode() {
        int result = 1;
        Object $config = this.getMyConfig();
        result = result * 59 + ($config == null ? 43 : $config.hashCode());
        Object $map = this.getMap();
        result = result * 59 + ($map == null ? 43 : $map.hashCode());
        Object $fileOutConfigList = this.getFileOutConfigList();
        result = result * 59 + ($fileOutConfigList == null ? 43 : $fileOutConfigList.hashCode());
        Object $fileCreate = this.getMyFileCreate();
        result = result * 59 + ($fileCreate == null ? 43 : $fileCreate.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "InjectionConfig(config=" + this.getMyConfig() + ", map=" + this.getMap() + ", fileOutConfigList=" + this.getFileOutConfigList() + ", fileCreate=" + this.getFileCreate() + ")";
    }
}
