package com.ztjo.common.frame;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.engine.VelocityTemplateEngine;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

/**
 * @author 陈彬
 * @version 2021/7/16
 * description：自定义模板代码生成器
 */
@Slf4j
public class MyAutoGenerator extends AutoGenerator {
    protected MyConfigBuilder myConfig;

    @Override
    public void execute() {
        log.debug("==========================准备生成文件...==========================");
        if(ObjectUtil.isNotNull(this.myConfig)){
            log.info("正在重置当前数据库连接配置");
            Connection conn = this.myConfig.getConnection();
            try {
                log.info("正在关闭原有连接");
                myConfig.closeConn();
            } catch (SQLException e) {
                log.error("重置配置时，关闭jdbc连接引发异常！", e);
            }

            this.myConfig = null;
        }

        log.info("重连数据库并获取相应数据库表设置");
        if (null == this.myConfig) {
            this.myConfig = new MyConfigBuilder(super.getPackageInfo(),
                    super.getDataSource(), super.getStrategy(), super.getTemplate(), super.getGlobalConfig());
            if (null != super.injectionConfig) {
                ((MyInjectionConfig)super.injectionConfig).setMyConfig(this.myConfig);
            }
        }

        log.info("模板引擎检查");
        if (null == super.getTemplateEngine()) {
            super.setTemplateEngine(new VelocityTemplateEngine());
        }

        log.info("模板引擎启动并拷贝配置填充模板生成对应文件");
        ((MyAbstractTemplateEngine)super.getTemplateEngine()).init(this.pretreatmentConfigBuilder(this.myConfig)).mkdirs().batchOutput().open();

        log.debug("==========================文件生成完成！！！==========================");
    }

    public MyConfigBuilder getMyConfig(){
        return this.myConfig;
    }

    public MyConfigBuilder setMyConfig(MyConfigBuilder myConfig){
        this.myConfig = myConfig;
        return this.myConfig;
    }

    protected List<TableInfo> getAllTableInfoList(MyConfigBuilder config) {
        return config.getTableInfoList();
    }

    protected MyConfigBuilder pretreatmentConfigBuilder(MyConfigBuilder config) {
        if (null != this.injectionConfig) {
            this.injectionConfig.initMap();
            config.setInjectionConfig(this.injectionConfig);
        }

        List<TableInfo> tableList = this.getAllTableInfoList(config);
        Iterator var3 = tableList.iterator();

        while(var3.hasNext()) {
            TableInfo tableInfo = (TableInfo)var3.next();
            if (config.getGlobalConfig().isActiveRecord()) {
                tableInfo.setImportPackages(Model.class.getCanonicalName());
            }

            if (tableInfo.isConvert()) {
                tableInfo.setImportPackages(TableName.class.getCanonicalName());
            }

            if (config.getStrategyConfig().getLogicDeleteFieldName() != null && tableInfo.isLogicDelete(config.getStrategyConfig().getLogicDeleteFieldName())) {
                tableInfo.setImportPackages(TableLogic.class.getCanonicalName());
            }

            if (StringUtils.isNotBlank(config.getStrategyConfig().getVersionFieldName())) {
                tableInfo.setImportPackages(Version.class.getCanonicalName());
            }

            boolean importSerializable = true;
            if (StringUtils.isNotBlank(config.getSuperEntityClass())) {
                tableInfo.setImportPackages(config.getSuperEntityClass());
                importSerializable = false;
            }

            if (config.getGlobalConfig().isActiveRecord()) {
                importSerializable = true;
            }

            if (importSerializable) {
                tableInfo.setImportPackages(Serializable.class.getCanonicalName());
            }

            if (config.getStrategyConfig().isEntityBooleanColumnRemoveIsPrefix() && CollectionUtils.isNotEmpty(tableInfo.getFields())) {
                tableInfo.getFields().stream().filter((field) -> {
                    return "boolean".equalsIgnoreCase(field.getPropertyType());
                }).filter((field) -> {
                    return field.getPropertyName().startsWith("is");
                }).forEach((field) -> {
                    field.setConvert(true);
                    field.setPropertyName(StringUtils.removePrefixAfterPrefixToLower(field.getPropertyName(), 2));
                });
            }
        }

        return config.setTableInfoList(tableList);
    }
}
