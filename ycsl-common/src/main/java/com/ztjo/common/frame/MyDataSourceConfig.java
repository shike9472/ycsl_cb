package com.ztjo.common.frame;

import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author 陈彬
 * @version 2021/7/16
 * description：自定义数据源配置类
 */
@Slf4j
public class MyDataSourceConfig extends DataSourceConfig {
    public void creatDrive(){
        log.info("创建jdbc连接驱动对象！");
        try {
            Class.forName(super.getDriverName());
        } catch (ClassNotFoundException e) {
            log.error("JDBC连接驱动类未发现", e);
        }
    }

    @Override
    public Connection getConn() {
        log.info("【配置子类重新方法】- 取得jdbc连接对象！");
        try {
            Connection conn = DriverManager.getConnection(super.getUrl(), super.getUsername(), super.getPassword());
            return conn;
        } catch (SQLException var3) {
            throw new RuntimeException(var3);
        }
    }
}
