package com.ztjo.common.utils;

import cn.hutool.core.util.ObjectUtil;
import com.ztjo.data.exception.YcslBizException;
import com.ztjo.data.pojo.vo.SupportParamModelVo;
import io.swagger.annotations.ApiModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLDecoder;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

@Slf4j
public class PackageUtil {
    /**
     * 包内类加载方法
     * @param packageName
     * @param searchSign
     * @param classList
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static void getClasses(String packageName, String searchSign, List<SupportParamModelVo> classList) throws IOException, ClassNotFoundException {
        log.info("扫描的接口参数包名："+packageName);
        log.info("查询条件为："+searchSign);
        Enumeration<URL> iterator = Thread.currentThread().getContextClassLoader().getResources(new String(packageName.replace(".","/").getBytes(),"UTF-8"));
        URL url = null;
        File file = null;
        File[] files = null;
        Class<?> c = null;
        String className = null;
        while(iterator.hasMoreElements()){
            url = iterator.nextElement();
            log.info("当前的URL："+url.toString()+"Protocol为："+url.getProtocol());
            if("file".equals(url.getProtocol())){
                file = new File(URLDecoder.decode(url.getPath(),"UTF-8"));
                log.info("已取得fileFold:"+file.getName()+"该附件的isDirectory为："+file.isDirectory());
                if(file.isDirectory()){
                    files = file.listFiles();
                    log.info("取得附件列表为："+files.toString());
                    for(File f:files){
                        className = f.getName();
                        if(f.isDirectory()) {//递归调用
                            getClasses(packageName+"."+f.getName(),searchSign,classList);
                        }else {
                            className = className.substring(0,className.lastIndexOf("."));
                            if(searchSign!=null&&!className.toUpperCase().contains(searchSign.toUpperCase())) {
                                continue;
                            }
                            c = Thread.currentThread().getContextClassLoader().loadClass(packageName+"."+className);
                            classList.add(loadClassInfos(c, className));
                        }
                    }
                }
            } else if("jar".equals(url.getProtocol())){
                String parentPath = new File(ResourceUtils.getURL("classpath:").getPath())
                        .getParentFile().getParent();
                log.info("特殊字符处理前path: {}", parentPath);
                String jar_path = parentPath
                        .replaceAll("file:","")
                        .replaceAll("!","")
                        .replaceAll("\\\\","/")
                        .substring(1);
                log.info("初始jarPath:"+jar_path);
                String jarPath = URLDecoder.decode(new String(jar_path.getBytes(),"UTF-8"),"UTF-8");
                log.info("处理后jarPath:"+jarPath);
                List<Class> cs = PackageUtil.getClasssFromJarFile(jarPath,packageName);
                for(Class cc:cs) {
                    classList.add(loadClassInfos(cc, className));
                }
            }
        }
    }

    public static List<Class> getClasssFromJarFile(String jarPaht, String filePaht) {
        List<Class> clazzs = new ArrayList<Class>();

        JarFile jarFile = null;
        try {
            jarFile = new JarFile(jarPaht);
        } catch (IOException e1) {
            log.error("jar文件加载失败 -> ", e1);
        }
        // UNIX系统适配 - start
        if(ObjectUtil.isNull(jarFile)) {
            jarPaht = "/"+ jarPaht;
            try{
                jarFile = new JarFile(jarPaht);
            } catch (IOException e1) {
                log.error("jar文件加载失败<二次尝试> -> ", e1);
            }
        }
        // UNIX系统适配 - end
        if(ObjectUtil.isNull(jarFile)) {
            throw new YcslBizException("未发现jar包文件："+ jarPaht);
        }

        List<JarEntry> jarEntryList = new ArrayList<JarEntry>();

        Enumeration<JarEntry> ee = jarFile.entries();
        while (ee.hasMoreElements()) {
            JarEntry entry = (JarEntry) ee.nextElement();
//            log.debug(entry.getName());
            // 过滤我们出满足我们需求的东西
            if (entry.getName().replaceAll("/",".").contains(filePaht) && entry.getName().endsWith(".class")) {
                jarEntryList.add(entry);
            }
        }
        for (JarEntry entry : jarEntryList) {
            String className = entry.getName().replace('/', '.');
            className = className.substring(className.indexOf(filePaht));
            className = className.substring(0, className.length() - 6);
            try {
                clazzs.add(Thread.currentThread().getContextClassLoader().loadClass(className));
            } catch (ClassNotFoundException e) {
                log.error("类加载失败！ -> ", e);
            }
        }

        return clazzs;
    }

    /**
     * 获取属性名数组
     * @throws ClassNotFoundException
     * */
    public static List<Map<String,String>> getFiledName(String className) throws ClassNotFoundException{
        Field[] fields=Class.forName(className).getDeclaredFields();
        List<Map<String,String>> objs = new ArrayList<Map<String,String>>();
        for(int i=0;i<fields.length;i++){
            Map<String,String> obj = new HashMap<String,String>();
            String fieldName=fields[i].getName();
            String type = "String";
            if(fieldName.contains("Time")||fieldName.contains("time")||fieldName.contains("Date")||fieldName.contains("date")){
                type = "Date";
            }
            obj.put("fieldName",fieldName);
            obj.put("type",type);
            //type = fields[i].getType().getName();
            objs.add(obj);
        }
        return objs;
    }

    /**
     * 根据属性名获取对象中的属性值
     * */
    @SuppressWarnings("unused")
    public static Object getFieldValueByName(String fieldName, Object o) {
        try {
            String firstLetter = fieldName.substring(0, 1).toUpperCase();
            String getter = "get" + firstLetter + fieldName.substring(1);
            Method method = o.getClass().getMethod(getter, new Class[] {});
            Object value = method.invoke(o, new Object[] {});
            return value;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 加载类详情信息
     * @param c
     * @param className
     * @return
     */
    private static SupportParamModelVo loadClassInfos(Class<?> c, String className) {
        SupportParamModelVo vo = new SupportParamModelVo();
        vo.setSimpleName(className);
        vo.setName(c.getName());
        ApiModel apiModel = c.getAnnotation(ApiModel.class);
        if(ObjectUtil.isNotNull(apiModel)) {
            vo.setTitle(apiModel.value());
            vo.setNode(apiModel.description());
        } else {
            vo.setTitle(className);
            vo.setNode(c.getName());
        }
        return vo;
    }

}
