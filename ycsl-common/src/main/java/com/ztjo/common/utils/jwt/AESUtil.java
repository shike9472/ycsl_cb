package com.ztjo.common.utils.jwt;

import com.ztjo.common.properties.RedisCustomProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * @author 陈彬
 * @version 2021/7/22
 * description：AES加密处理工具
 */
@Slf4j
@Component
public class AESUtil {

    /**
     * 加密用的Key 可以用26个字母和数字组成 此处使用AES-128-CBC加密模式，key需要为16位。
     */
    @Autowired
    private RedisCustomProperties redisProperties;

    /**
     * 加密方法
     * @param sSrc
     * @return
     */
    public String encrypt(String sSrc){
        String result = "";
        try {
            log.info("加密使用密钥：{}，{}",
                    redisProperties.getAecKey(), redisProperties.getAecIv());
            Cipher cipher;
            cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            byte[] raw = redisProperties.getAecKey().getBytes();
            SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
            // 使用CBC模式，需要一个向量iv（加密偏移矢量），可增加加密算法的强度
            IvParameterSpec iv = new IvParameterSpec(redisProperties.getAecIv().getBytes());
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
            // CBC加密
            byte[] encrypted = cipher.doFinal(sSrc.getBytes("utf-8"));
            // Base64设定
            result = new BASE64Encoder().encode(encrypted);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 此处使用BASE64做转码。
        return result;

    }

    /**
     * 解密方法
     * @param sSrc
     * @return
     */
    public String decrypt(String sSrc){
        try {
            byte[] raw = redisProperties.getAecKey().getBytes("ASCII");
            SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            IvParameterSpec iv = new IvParameterSpec(redisProperties.getAecIv().getBytes());
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
            // 按加密顺序，从外到内解密，先用base64解密
            byte[] encrypted1 = new BASE64Decoder().decodeBuffer(sSrc);
            // CBC解密
            byte[] original = cipher.doFinal(encrypted1);
            String originalString = new String(original, "utf-8");
            return originalString;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
