package com.ztjo.common.utils.jwt;

import com.github.ag.core.util.jwt.IJWTInfo;
import com.github.ag.core.util.jwt.JWTHelper;
import com.ztjo.common.properties.AuthCustomProperties;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Map;

/**
 * @author 陈彬
 * @version 2021/7/22
 * description：JWT-TOKEN处理工具
 */
@Slf4j
@Component
public class JwtTokenUtil {
    @Autowired
    private AuthCustomProperties authProperties;

    @Autowired
    private JWTHelper jwtHelper;

    /**
     * 描述：String 类型 token 获取
     * 作者：陈彬
     * 日期：2021/7/24
     * 参数：[jwtInfo, otherInfo, expireTime]
     * 返回：java.lang.String
     * 更新记录：更新人：{}，更新日期：{}
     */
    public String generateToken(IJWTInfo jwtInfo, Map<String, String> otherInfo, Date expireTime) throws Exception {
        return jwtHelper.generateToken(jwtInfo, authProperties.getUserPriKey(), expireTime, otherInfo);
    }

    /**
     * 描述：token拿JWT信息
     * 作者：陈彬
     * 日期：2021/7/22
     * 参数：[token]
     * 返回：com.github.ag.core.util.jwt.IJWTInfo
     * 更新记录：更新人：{}，更新日期：{}
     */
    public IJWTInfo getInfoFromToken(String token) throws Exception {
        IJWTInfo infoFromToken = jwtHelper.getInfoFromToken(token, authProperties.getUserPubKey());
        return infoFromToken;
    }
}
