package com.ztjo.common.annotation;

import org.springframework.context.annotation.ComponentScan;

import java.lang.annotation.*;

/**
 * @author 陈彬
 * @version 2021/8/19
 * description：一窗受理框架级别组件扫描注解(框架来自老A开源代码)
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@ComponentScan({"com.github.ag.core","com.github.wxiaoqi.security.common.util"})
public @interface YcslFrameworkScan {
}
