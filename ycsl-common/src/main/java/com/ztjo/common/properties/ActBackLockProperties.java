package com.ztjo.common.properties;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author 陈彬
 * @version 2022/1/7
 * description：
 */
@Data
@Slf4j
@ConfigurationProperties(prefix = "act-back")
public class ActBackLockProperties {
    /** 工作流回退锁定redis-key前缀 */
    private String lockKeyPre;
    /** 锁定超时时长设置(提交) */
    private Long submitLockTimeout;
    /** 锁定超时时长设置(回退) */
    private Long backoffLockTimeout;
    /** 提交尝试时间间隔 */
    private Long lockSubmitTryTime;
    /** 回退尝试时间间隔 */
    private Long lockBackoffDoTime;
}
