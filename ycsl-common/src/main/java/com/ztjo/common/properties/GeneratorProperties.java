package com.ztjo.common.properties;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author 陈彬
 * @version 2021/7/16
 * description：模板自动生成文件配置
 */
@Data
@Slf4j
@ConfigurationProperties(prefix = "generator")
public class GeneratorProperties {
    /**
     * 工程位置
     */
    private String projectPath;
    /**
     * 文件输出位置
     */
    private String outputDir;
    /**
     * xml文件输出位置
     */
    private String xmlDir;
    /**
     * 基础包名
     */
    private String packageName;
}
