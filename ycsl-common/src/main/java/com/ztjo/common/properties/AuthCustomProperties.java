package com.ztjo.common.properties;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author 陈彬
 * @version 2021/7/22
 * description：用户信息配置项设置
 */
@Data
@Slf4j
@ConfigurationProperties(prefix = "auth")
public class AuthCustomProperties {
    /**
     * 检查忽略设置
     */
    private String urlIgnore;
    /**
     * 用户公钥
     */
    private byte[] userPubKey;
    /**
     * 用户私钥
     */
    private byte[] userPriKey;
    /**
     * jwt生成相关配置
     */
    private final AuthCustomProperties.Jwt jwt = new AuthCustomProperties.Jwt();
    /**
     * token使用相关配置
     */
    private final AuthCustomProperties.Token token = new AuthCustomProperties.Token();

    @Data
    public static class Jwt {
        private Integer expire = 0;
        /**
         * jwt初始化公/私钥生成时使用的 “密钥”，
         * 公/私钥生成使用方式： SecureRandom（随机密钥方式）
         */
        private String rsaSecret;

        public Jwt(){}
    }

    @Data
    public static class Token{
        private String tokenHeader;
        private Integer tokenExpire;

        public Token(){}
    }
}
