package com.ztjo.common.properties;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author 陈彬
 * @version 2021/7/22
 * description：redis使用过程中，其它自定义配置项
 */
@Data
@Slf4j
@ConfigurationProperties(prefix = "redis.custom")
public class RedisCustomProperties {
    /**
     * 【公/私钥】密文 redis存储时进行二次加密的密钥，加密使用AES-CBC模式（矢量-对称加密）
     */
    private String aecKey;
    /**
     * CBC模式下，iv向量值设定，可增加加密算法的强度
     */
    private String aecIv;
    /**
     * 系统启动时是否清空缓存
     */
    private boolean clearWhenInit;
    /**
     * 超时设置（包含各项超时设置）
     */
    private final RedisCustomProperties.Timeout timeout = new RedisCustomProperties.Timeout();

    @Data
    public static class Timeout {
        // 一般业务锁超时设置，单位为秒
        private Integer buzLock;
        // 表单提交锁超时设置，单位为秒
        private Integer commitLock;

        public Timeout() {

        }

    }


}
