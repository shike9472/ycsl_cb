package com.ztjo.common.component;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.dynamic.datasource.spring.boot.autoconfigure.DataSourceProperty;
import com.baomidou.dynamic.datasource.spring.boot.autoconfigure.DynamicDataSourceProperties;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.ztjo.common.frame.MyAutoGenerator;
import com.ztjo.common.frame.MyDataSourceConfig;
import com.ztjo.common.frame.MyInjectionConfig;
import com.ztjo.common.frame.MyVelocityTemplateEngine;
import com.ztjo.common.properties.GeneratorProperties;
import com.ztjo.data.enums.SFEnums;
import com.ztjo.data.pojo.dto.GeneratorDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.baomidou.mybatisplus.annotation.DbType.MYSQL;
import static com.baomidou.mybatisplus.annotation.DbType.ORACLE;
import static com.baomidou.mybatisplus.generator.config.rules.DateType.ONLY_DATE;
import static com.ztjo.data.constants.DataSourceKeysConstants.DATA_SOURCE_KEY_ZTK;

/**
 * @author 陈彬
 * @version 2021/7/16
 * description：系统构建工具组件
 */
@Slf4j
@Component
@ConditionalOnClass({DynamicDataSourceProperties.class, GeneratorProperties.class})
public class GeneratorComponent {
    /**
     * 动态数据源配置
     */
    private static DynamicDataSourceProperties dataSourceProperties;
    /**
     * 输入位置配置
     */
    private static GeneratorProperties generatorProperties;
    /**
     * 代码生成器顶部资源，全局唯一
     */
    private static AutoGenerator mpg;
    /**
     * 默认使用的数据库标识
     */
    private static final String DEFAULT_DATA_SOURCE_KEY = DATA_SOURCE_KEY_ZTK;

    /**
     * 装配系统配置文件配置
     * @param dataSourceProperties
     */
    @Autowired
    public void setDataSourceProperties(DynamicDataSourceProperties dataSourceProperties){
        GeneratorComponent.dataSourceProperties = dataSourceProperties;
        // 数据源配置
        mpg.setDataSource(initDataSourceConfig(DEFAULT_DATA_SOURCE_KEY, ""));
    }

    /**
     * 装配文件输出位置配置
     * @param generatorProperties
     */
    @Autowired
    public void setGenerator(GeneratorProperties generatorProperties){
        log.info("文件位置：{}", JSONObject.toJSONString(generatorProperties));
        GeneratorComponent.generatorProperties = generatorProperties;
        // 全局配置
        mpg.setGlobalConfig(initGlobalConfig());
        // 包配置
        mpg.setPackageInfo(initPackageConfig(null, null));
        // 自定义配置
        mpg.setCfg(initInjectionConfig());
    }

    /**
     * 初始化静态资源
     */
    static {
        // 代码生成器
        mpg = new MyAutoGenerator();
        //  模板引擎配置
        mpg.setTemplateEngine(new MyVelocityTemplateEngine());
        // 策略配置
        mpg.setStrategy(initStrategyConfig(null, null, null));
        // 基于引擎的具体模板配置
        mpg.setTemplate(initTemplateConfig());
    }

    /**
     * 描述：表的CRUD基础逻辑代码生成
     * 作者：陈彬
     * 日期：2021/7/16
     * 参数：[dataSourceKey, tableNames, ownner-表的所有者]
     * 返回：void
     * 更新记录：更新人：{}，更新日期：{}
     */
    public static synchronized void tableLogicCreate(GeneratorDto dto){
        // 重新选择数据源配置
        mpg.setDataSource(initDataSourceConfig(dto.getKey(), dto.getOwner()));
        // 重新赋值初始化表名
        String[] ts = null;
        if(CollUtil.isNotEmpty(dto.getTableNames())) {
            ts = dto.getTableNames().toArray(new String[0]);
        }
        mpg.setStrategy(initStrategyConfig(ts, dto.getMenuStyle(), dto.getEntityModel()));
        // 动态配置包名
        mpg.setPackageInfo(initPackageConfig(null, dto.getMvcComponentName()));

        // 执行生成
        mpg.execute();

    }

    /**
     * 描述：全局初始化配置
     * 作者：陈彬
     * 日期：2021/7/16
     * 参数：[]
     * 返回：com.baomidou.mybatisplus.generator.config.GlobalConfig
     * 更新记录：更新人：{}，更新日期：{}
     */
    public static GlobalConfig initGlobalConfig(){
        // 全局配置
        GlobalConfig gc = mpg.getGlobalConfig();
        if(ObjectUtil.isNull(gc)) {
            gc = new GlobalConfig();
        }
        // 绝对路径
        String projectPath = generatorProperties.getProjectPath();
        // 项目内路径
        gc.setOutputDir(generatorProperties.getOutputDir());
        // 作者
        gc.setAuthor("陈彬");
        // 是否打开输出目录, false - 完成后不要让资源管理器打开文件夹
        gc.setOpen(false);
        //支持AR操作,实体类继承的Model中,泛型设置为当前实体类,我们应该设置为true -- 已验证
        gc.setActiveRecord(true);
        // 二次生成时，是否覆盖
        gc.setFileOverride(true);
        // 生成 ResultMap，【xml】
        gc.setBaseResultMap(true);
        // 生成 columList，【xml】
        gc.setBaseColumnList(true);
        // 是否Swagger2注解，【entity】
        gc.setSwagger2(true);
        // 自定义文件命名，注意 %s 会自动填充表实体属性！
//        gc.setEntityName("%sEntity");
        gc.setMapperName("%sMapper");
        gc.setXmlName("%sMapper");
        gc.setServiceName("%sService");
        gc.setServiceImplName("%sServiceImpl");
        gc.setControllerName("%sController");
        //实体属性 Swagger2 注解
        gc.setSwagger2(true);
        // 日期格式设置 - java.util.Date 对应 ONLY_DATE常量,默认为LocalDate日期类
        gc.setDateType(ONLY_DATE);
        // 主键生成策略设置进入实体类,此配置可注销,交由yml中做统一配置即可,AUTO-数据库主键列自增长
//        gc.setIdType(ID_WORKER);

        return gc;
    }

    /**
     * 描述：数据源配置，直接引用项目的数据源配置
     *          这里因为使用苞米豆多数据源配置，此时需要告知使用哪个数据源
     * 作者：陈彬
     * 日期：2021/7/16
     * 参数：[dataSourceKey]
     * 返回：com.baomidou.mybatisplus.generator.config.DataSourceConfig
     * 更新记录：更新人：{}，更新日期：{}
     */
    public static DataSourceConfig initDataSourceConfig(String dataSourceKey, String ownner){
        if(StrUtil.isBlank(dataSourceKey)){
            throw new RuntimeException("数据源key值选择不得为空");
        }
        // 数据源配置
        MyDataSourceConfig dsc = (MyDataSourceConfig) mpg.getDataSource();
        boolean isFirst = ObjectUtil.isNull(dsc);
        if(isFirst) {
            dsc = new MyDataSourceConfig();
        }
        DataSourceProperty dataSourceProperty = dataSourceProperties.getDatasource().get(dataSourceKey);
        if("oracle.jdbc.OracleDriver".equals(dataSourceProperty.getDriverClassName())){
            dsc.setDbType(ORACLE);
            if(StrUtil.isBlank(ownner)) {
                ownner = dataSourceProperty.getUsername();
            }
        } else {
            dsc.setDbType(MYSQL);
            if(StrUtil.isBlank(ownner)) {
                ownner = "public";
            }
        }
        dsc.setSchemaName(ownner);

        // conn配置
        dsc.setUrl(dataSourceProperty.getUrl());
        dsc.setDriverName(dataSourceProperty.getDriverClassName());
        dsc.setUsername(dataSourceProperty.getUsername());
        dsc.setPassword(dataSourceProperty.getPassword());

        if(isFirst){
            // 第一次设置时，创建反射
            dsc.creatDrive();
        }
        return dsc;
    }

    /**
     * 描述：策略初始化配置，
     *          传入表名则只生成对应表，不传则全表生成
     * 作者：陈彬
     * 日期：2021/7/16
     * 参数：[tableNames]
     * 返回：com.baomidou.mybatisplus.generator.config.StrategyConfig
     * 更新记录：更新人：{}，更新日期：{}
     */
    public static StrategyConfig initStrategyConfig(String[] tableNames, String menuStyle, String entityModel){
        // 策略配置
        StrategyConfig strategy = mpg.getStrategy();
        if(ObjectUtil.isNull(strategy)) {
            strategy = new StrategyConfig();
        }
        // 需要生成的表, 传空全表重新生成
        if(ArrayUtil.isNotEmpty(tableNames)) {
            strategy.setInclude(tableNames);
        } else {
            strategy.setInclude(null);
        }
        // 全局大写命名 ORACLE 注意
        // strategy.setCapitalMode(true);
        // 此处可以修改为您的表前缀
//        strategy.setTablePrefix(new String[]{"t_"});
        // 是否使用Lombok简化代码
        strategy.setEntityLombokModel(true);
        // 实体类使用注解标明主键和列名等
        strategy.setEntityTableFieldAnnotationEnable(true);
        // 实体类继承Model
        strategy.setEntityBuilderModel(true);
        if(StrUtil.isBlank(entityModel)) {
            // 自定义实体父类
            strategy.setSuperEntityClass("com.baomidou.mybatisplus.extension.activerecord.Model");
        } else {
            strategy.setSuperEntityClass(entityModel);
        }

        // 是否跳过视图
        strategy.setSkipView(true);
        // 数据库表映射到实体的命名策略,下划线转大写
        strategy.setNaming(NamingStrategy.underline_to_camel);
        // 列名下划线转大写
        strategy.setColumnNaming(NamingStrategy.underline_to_camel);

        // 自定义 mapper 父类
        strategy.setSuperMapperClass("com.ztjo.common.frame.MyBaseMapper");
        // 自定义 service 父类
        strategy.setSuperServiceClass("com.baomidou.mybatisplus.extension.service.IService");
        // 自定义 service 实现类父类
        strategy.setSuperServiceImplClass("com.baomidou.mybatisplus.extension.service.impl.ServiceImpl");
        if(!SFEnums.SF_S.getCode().equals(menuStyle)) {
            // 自定义 controller 父类
            strategy.setSuperControllerClass("com.chenbin.demo.frame.MyBaseController");
        } else {
            strategy.setSuperControllerClass("");
        }
        // Restful接口风格
        strategy.setRestControllerStyle(true);

        return strategy;
    }

    /**
     * 描述：输出文件包设置，根据设置的模板输出文件所在包位置等
     * 作者：陈彬
     * 日期：2021/7/16
     * 参数：[packageName]
     * 返回：com.baomidou.mybatisplus.generator.config.PackageConfig
     * 更新记录：更新人：{}，更新日期：{}
     */
    public static PackageConfig initPackageConfig(String packageName, GeneratorDto.MVCComponentName componentNames){
        // 包配置
        PackageConfig pc = mpg.getPackageInfo();
        if(ObjectUtil.isNull(pc)) {
            pc = new PackageConfig();
        }
        if(StrUtil.isBlank(packageName)){
            packageName = generatorProperties.getPackageName();
        }
        pc.setParent(packageName);
        if(ObjectUtil.isNull(componentNames)) {
            pc.setEntity("entity");
            pc.setMapper("mapper");
            pc.setServiceImpl("service.impl");
            pc.setService("service");
            pc.setController("controller");
        } else {
            pc.setEntity(
                    Optional.ofNullable(componentNames.getEntityName())
                            .orElse("entity")
            );
            pc.setMapper(
                    Optional.ofNullable(componentNames.getMapperName())
                            .orElse("mapper")
            );
            pc.setServiceImpl(
                    Optional.ofNullable(componentNames.getServiceImplName())
                            .orElse("service.impl")
            );
            pc.setService(
                    Optional.ofNullable(componentNames.getServiceName())
                            .orElse("service")
            );
            pc.setController(
                    Optional.ofNullable(componentNames.getControllerName())
                            .orElse("controller")
            );
        }

        return pc;
    }

    /**
     * 描述：模板配置，此处用于指定各输出文件指定的模板存放位置
     *          什么都不设置时，会使用依赖的 mybatis-plus-generator jar包中的模板
     *          默认使用【velocity】的模板配置
     *              若需要使用自定义模板，可以在此处主动设置自定义模板存放路径，
     *              或者将自己生成好的自定义模板拷贝至项目的 resources/templates目录下即可
     * 作者：陈彬
     * 日期：2021/7/16
     * 参数：[]
     * 返回：com.baomidou.mybatisplus.generator.config.TemplateConfig
     * 更新记录：更新人：{}，更新日期：{}
     */
    public static TemplateConfig initTemplateConfig(){
        // 配置模板
        TemplateConfig templateConfig = mpg.getTemplate();
        if(ObjectUtil.isNull(templateConfig)) {
            templateConfig = new TemplateConfig();
        }
        //不生成如下类型模板
        templateConfig.setXml(null);
        return templateConfig;
    }

    /**
     * 描述：自定义配置
     * 作者：陈彬
     * 日期：2021/7/16
     * 参数：[]
     * 返回：com.baomidou.mybatisplus.generator.InjectionConfig
     * 更新记录：更新人：{}，更新日期：{}
     */
    public static InjectionConfig initInjectionConfig(){
        // 自定义配置
        InjectionConfig cfg = mpg.getCfg();
        if(ObjectUtil.isNull(cfg)) {
            cfg = new MyInjectionConfig() {
                @Override
                public void initMap() {
                }
            };
        }

//        // 如果模板引擎是 velocity
        String templatePath = "/templates/mapper.xml.vm";
        // 自定义输出配置
        List<FileOutConfig> focList = new ArrayList<>();
        // 自定义配置会被优先输出
        focList.add(new FileOutConfig(templatePath) {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输出文件名 ， 如果你 Entity 设置了前后缀、此处注意 xml 的名称会跟着发生变化！！
                return generatorProperties.getXmlDir() + tableInfo.getEntityName() + "Mapper" + StringPool.DOT_XML;
            }
        });
        cfg.setFileOutConfigList(focList);

        return cfg;
    }
}
