package com.ztjo.common.config;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * @author 陈彬
 * @version 2021/7/21
 * description：redis配置扫描
 */
@Slf4j
/**
 * 此注解的意义在于，spring有关@Cache类注解操作的缓存都将使用redisTemplate方式进行
 * 注解加在mangoDb的配置类上，则认为spring类似注解将数据都放mango内缓存
 */
@EnableCaching
@Configuration
/**
 * 此处是使RedisAutoConfiguration的部分设置失效的设置,
 *      因为自动装载redis配置(RedisAutoConfiguration生效)时,
 *      在IOC已经托管了RedisTemplate的情况下,上述自动配置类的@Bean将不再生效
 */
@AutoConfigureBefore(RedisAutoConfiguration.class)
public class RedisConfiguration {
    @Autowired
    private RedisProperties redisProperties;

    @Bean
    @ConditionalOnMissingBean(name = "redisTemplate")
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory factory) {
        log.info("正在生成并托管redis设置...");
        log.info("redis host-{},port-{}", redisProperties.getHost(), redisProperties.getPort());
        RedisTemplate redisTemplate = new RedisTemplate();
        redisTemplate.setConnectionFactory(factory);
        // 声明json转换策略(存值时,将对象转json做序列化存储)
        Jackson2JsonRedisSerializer jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class);
        // 数据取出时允许到具体类型拆箱(支持对象从redis中取用时做强转后直接使用)
        ObjectMapper om = new ObjectMapper();
        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        jackson2JsonRedisSerializer.setObjectMapper(om);

        // key序列化策略
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        // key hashkey序列化策略
        redisTemplate.setHashKeySerializer(new StringRedisSerializer());
        // value序列化策略
        redisTemplate.setValueSerializer(jackson2JsonRedisSerializer);
        // value hashmap序列化
        redisTemplate.setHashValueSerializer(jackson2JsonRedisSerializer);
        redisTemplate.afterPropertiesSet();
        return redisTemplate;
    }
}
