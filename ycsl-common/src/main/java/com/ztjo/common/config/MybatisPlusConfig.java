package com.ztjo.common.config;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.plugins.pagination.optimize.JsqlParserCountOptimize;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 陈彬
 * @version 2021/7/13
 * description：mybatis-plus的配置文件
 */
@Slf4j
@Configuration
public class MybatisPlusConfig {

    /**
     * mybatis-plus分页插件bean
     * @return
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        log.info("加载mybatis-plus分页插件...");
        // 分页插件使用
        PaginationInterceptor paginationInterceptor = new PaginationInterceptor();
        // 开启 count 的 join 优化,只针对部分 left join
        paginationInterceptor.setCountSqlParser(new JsqlParserCountOptimize(true));
        return paginationInterceptor;
    }

}
