package com.ztjo.data.entity.biz;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ztjo.data.entity.model.BizInfoModel;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * info表 - 税务收件录入信息表
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("sj_info_swsjlrxxb")
@ApiModel(value="SjInfoSwsjlrxxb对象", description="info表 - 税务收件录入信息表")
public class SjInfoSwsjlrxxb extends BizInfoModel<SjInfoSwsjlrxxb> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "主键")
        @TableId("id")
      private Long id;

      @ApiModelProperty(value = "原始接口数据 - （保留字段 - 新系统无用）")
      @TableField("data_json")
    private String dataJson;

      @ApiModelProperty(value = "数据获取来源 - （保留字段 - 新系统无用）")
      @TableField("data_come_from_mode")
    private String dataComeFromMode;

      @ApiModelProperty(value = "数据提供单位 - （保留字段 - 新系统无用）")
      @TableField("provide_unit")
    private String provideUnit;

      @ApiModelProperty(value = "[全局信息]-增存量房标志")
      @TableField("zlfclfbz")
    private String zlfclfbz;

      @ApiModelProperty(value = "[全局信息]-数据归属地区")
      @TableField("sjgsdq")
    private String sjgsdq;

      @ApiModelProperty(value = "[房源基本信息] - 主房产单元[sj_bdc_fw_info] - [house_id] - 房屋的唯一编号 - 对应存储主键")
      @TableField("fyjbxxuuid")
    private String fyjbxxuuid;

      @ApiModelProperty(value = "[房源基本信息] - 主房产单元[sj_bdc_fw_info] - [house_id] - 外部id - 与fyjbxxuuid相同")
      @TableField("wbid")
    private String wbid;

      @ApiModelProperty(value = "[房源基本信息] - 主房产单元[sj_bdc_fw_info] - [immovable_unit_number] - 不动产单元号")
      @TableField("bdcdyh")
    private String bdcdyh;

      @ApiModelProperty(value = "[房源基本信息] - 主房产单元[sj_bdc_fw_info] - [house_location] - 土地房屋地址 - 坐落")
      @TableField("tdfwdz")
    private String tdfwdz;

      @ApiModelProperty(value = "[房源基本信息] - 主房产单元[sj_bdc_fw_info] - [] - 幢号")
      @TableField("fwzh")
    private String fwzh;

      @ApiModelProperty(value = "[房源基本信息] - 主房产单元[sj_bdc_fw_info] - [] - 单元号")
      @TableField("dyh")
    private String dyh;

      @ApiModelProperty(value = "[房源基本信息] - 主房产单元[sj_bdc_fw_info] - [] - 楼层")
      @TableField("lc2")
    private Integer lc2;

      @ApiModelProperty(value = "[房源基本信息] - 主房产单元[sj_bdc_fw_info] - [] - 房间号")
      @TableField("fjh")
    private String fjh;

      @ApiModelProperty(value = "[房源基本信息] - 主房产单元[sj_bdc_fw_info] - [] - 建筑结构类型代码")
      @TableField("jzjglxdm")
    private String jzjglxdm;

      @ApiModelProperty(value = "[房源基本信息] - 主房产单元[sj_bdc_fw_info] - [] - 建筑面积")
      @TableField("mj")
    private BigDecimal mj;

      @ApiModelProperty(value = "[房源基本信息] - 主房产单元[sj_bdc_fw_info] - [] - 套内面积")
      @TableField("tnmj")
    private BigDecimal tnmj;

      @ApiModelProperty(value = "[房源基本信息] - 主房产单元[sj_bdc_fw_info] - [] - 楼层总数")
      @TableField("lczs")
    private String lczs;

      @ApiModelProperty(value = "[房源基本信息] - 交易合同信息[sj_info_jyhtxx] - [contract_amount] - 房屋土地的总价格")
      @TableField("fwtdzjg")
    private BigDecimal fwtdzjg;

      @ApiModelProperty(value = "[房源基本信息] - 不动产权利信息[sj_info_bdcqlxgxx] - [attic_area] - 阁楼面积")
      @TableField("glmj")
    private BigDecimal glmj;

      @ApiModelProperty(value = "[房源基本信息] - 不动产权利信息[sj_info_bdcqlxgxx] - [storeroom_area] - 储藏室面积")
      @TableField("ccsmj")
    private BigDecimal ccsmj;

      @ApiModelProperty(value = "[房源基本信息] - 不动产权利信息[sj_info_bdcqlxgxx] - [garage_area] - 自行车车库面积 ")
      @TableField("zxcckmj")
    private BigDecimal zxcckmj;

      @ApiModelProperty(value = "[房源基本信息] - 不动产权利信息[sj_info_bdcqlxgxx] - [garage_area] - （汽车）车库面积")
      @TableField("ckmj")
    private BigDecimal ckmj;

      @ApiModelProperty(value = "[房源基本信息] - 扩展字段[] - [] - 行政区划所在代码（涉税房屋）")
      @TableField("xzqhszdm")
    private String xzqhszdm;

      @ApiModelProperty(value = "[房源基本信息] - 扩展字段[] - [] - 街道乡镇代码（涉税房屋）")
      @TableField("jdxzdm")
    private String jdxzdm;

      @ApiModelProperty(value = "[房源基本信息] - 扩展字段[] - [] - 朝向代码")
      @TableField("cxdm")
    private String cxdm;

      @ApiModelProperty(value = "[房源基本信息] - 扩展字段[] - [] - 房源信息来源 (1-住建/2-不动产/3-一窗前台采集/4-一窗纳税人输入)")
      @TableField("fyxxly")
    private Integer fyxxly;

      @ApiModelProperty(value = "[房源基本信息] - 扩展字段[] - [] - 数据归属地区（行政区代码）")
      @TableField("sjgsdq_fy")
    private String sjgsdqFy;

      @ApiModelProperty(value = "[房源基本信息] - 扩展字段[] - [] - 土地使用证编号")
      @TableField("tdsyzbh")
    private String tdsyzbh;

      @ApiModelProperty(value = "[房源基本信息] - 扩展字段[] - [] - 土地税源编号")
      @TableField("tdsybh")
    private String tdsybh;

      @ApiModelProperty(value = "[房源基本信息] - 扩展字段[] - [] - 建筑年份")
      @TableField("jznf")
    private String jznf;

      @ApiModelProperty(value = "[房源基本信息] - 扩展字段[] - [] - 道路名称(与小区名称一起必传其一)")
      @TableField("dlmc")
    private String dlmc;

      @ApiModelProperty(value = "[房源基本信息] - 主房产单元[sj_bdc_fw_info] - [project_name] - 小区名称(与道路名称一起必传其一)")
      @TableField("xqmc")
    private String xqmc;

      @ApiModelProperty(value = "[房产交易基本信息] - 交易合同信息[sj_info_jyhtxx] - [contract_sign_time] - 签订合同的信息（年月日格式）")
      @TableField("htqdrq")
    private String htqdrq;

      @ApiModelProperty(value = "[房产交易基本信息] - 交易合同信息[sj_info_jyhtxx] - [contract_record_number] - 合同的编号(备案号)")
      @TableField("htbh")
    private String htbh;

      @ApiModelProperty(value = "[房产交易基本信息] - 交易合同信息[sj_info_jyhtxx] - [info_id] - 房屋交易唯一编号")
      @TableField("fwuuid")
    private String fwuuid;

      @ApiModelProperty(value = "[房产交易基本信息] - 交易合同信息[sj_info_jyhtxx] - [contract_amount] - 签订的合同金额")
      @TableField("htje")
    private BigDecimal htje;

      @ApiModelProperty(value = "[房产交易基本信息] - 交易合同信息[sj_info_jyhtxx] - [contract_amount] - 交易价格")
      @TableField("jyjg")
    private BigDecimal jyjg;

      @ApiModelProperty(value = "[房产交易基本信息] - 交易合同信息[sj_info_jyhtxx - sj_qlr_gl] - [shared_mode] - 1 单独共有，2 共 同共有，3 按份 共有（受让方）")
      @TableField("csfgyfs_dm")
    private Integer csfgyfsDm;

      @ApiModelProperty(value = "[房产交易基本信息] - 交易合同信息[sj_info_jyhtxx - sj_qlr_gl] - [shared_mode] - 1 单独共有，2 共 同共有，3 按份 共有（转让方）")
      @TableField("zrfgyfs_dm")
    private Integer zrfgyfsDm;

      @ApiModelProperty(value = "[房产交易基本信息] - 不动产权利信息[sj_info_bdcqlxgxx] - [immovable_certificate_No] - 房屋产权证书号 如果是存量房 则必填")
      @TableField("fwcqzsh")
    private String fwcqzsh;

      @ApiModelProperty(value = "[房产交易基本信息] - 不动产权利信息[sj_info_bdcqlxgxx] - [registration_date] - 权属登记日期--必须小于当天")
      @TableField("qsdjrq")
    private String qsdjrq;

      @ApiModelProperty(value = "[房产交易基本信息] - 主房产单元[sj_bdc_fw_info] - [project_name] - 不动产项目名称--对应小区名称")
      @TableField("bdcxmmc")
    private String bdcxmmc;

      @ApiModelProperty(value = "[房产交易基本信息] - 主房产单元[sj_bdc_fw_info] - [house_type] - 房屋类型代码必传")
      @TableField("fwlx_dm")
    private String fwlxDm;

      @ApiModelProperty(value = "[房产交易基本信息] - 扩展字段[] - [] - 房产交易方式代码")
      @TableField("fcjyfsdm")
    private String fcjyfsdm;

      @ApiModelProperty(value = "[房产交易基本信息] - 扩展字段[] - [] - 契税权属转移类别代码")
      @TableField("qsqszylbdm")
    private String qsqszylbdm;

      @ApiModelProperty(value = "[房产交易基本信息] - 扩展字段[] - [] - 契税权属转移对象代码")
      @TableField("qsqszydxdm")
    private String qsqszydxdm;

      @ApiModelProperty(value = "[房产交易基本信息] - 扩展字段[] - [] - 契税权属转移用途代码")
      @TableField("qsqszyytdm")
    private String qsqszyytdm;

      @ApiModelProperty(value = "[房产交易基本信息] - 扩展字段[] - [] - 是否普通住房标志 不传入则默认赋值1 是")
      @TableField("sfptzfbs")
    private Integer sfptzfbs;

      @ApiModelProperty(value = "[房产交易基本信息] - 扩展字段[] - [] - 单价")
      @TableField("dj")
    private BigDecimal dj;

      @ApiModelProperty(value = "[房产交易基本信息] - 扩展字段[] - [] - 成交价是否含税的标志 1是 0否 不传入则默认为否")
      @TableField("cjjgsfhs")
    private Integer cjjgsfhs;

      @ApiModelProperty(value = "[房产交易基本信息] - 扩展字段[] - [] - 本次申报交易是 否属于转让方自 开票  有是 和否  如果不传入则默认是否")
      @TableField("bcsbjysfsyzrfzkp")
    private String bcsbjysfsyzrfzkp;

      @ApiModelProperty(value = "[房产交易基本信息] - 扩展字段[] - [] - 差额征收增值税 标志 1 是 0 否")
      @TableField("cezszzsbz")
    private String cezszzsbz;

      @ApiModelProperty(value = "[房产交易基本信息] - 扩展字段[] - [] - 当前应收税款所属月份")
      @TableField("dqysskssyf")
    private String dqysskssyf;

      @ApiModelProperty(value = "[房产交易基本信息] - 扩展字段[] - [] - 是否不征契税 Y 不征，N 征收 不传入 则默认为征收 N")
      @TableField("sfbzqs")
    private String sfbzqs;

      @ApiModelProperty(value = "[房产交易基本信息] - 扩展字段[] - [] - 土地增值税扣除 成本 限定以下情况必 须输入：涉及土 增税的部分")
      @TableField("tdzsskccb")
    private BigDecimal tdzsskccb;

      @ApiModelProperty(value = "[房产交易基本信息] - 扩展字段[] - [] - 当期应收税款金额")
      @TableField("dqyskje")
    private BigDecimal dqyskje;

      @ApiModelProperty(value = "[房产交易基本信息] - 扩展字段[] - [] - 个人所得税扣除 合理费用")
      @TableField("grsdskchlfy")
    private BigDecimal grsdskchlfy;

      @ApiModelProperty(value = "[房产交易基本信息] - 扩展字段[] - [] - 开发不动产项目 编号")
      @TableField("kfbdcxmbh")
    private String kfbdcxmbh;

      @ApiModelProperty(value = "[房产交易基本信息] - 扩展字段[] - [] - 外部交易价格")
      @TableField("wbjyjg")
    private String wbjyjg;

      @ApiModelProperty(value = "[房产交易基本信息] - 扩展字段[] - [] - 本次交易代付增 值税税率或征收 率")
      @TableField("bcjydfzzsslhzsl")
    private String bcjydfzzsslhzsl;

      @ApiModelProperty(value = "[房产交易基本信息] - 扩展字段[] - [] - 申报属性代码")
      @TableField("sbsxdm1")
    private String sbsxdm1;

      @ApiModelProperty(value = "[房产交易基本信息] - 扩展字段[] - [] - 备注")
      @TableField("bz")
    private String bz;

      @ApiModelProperty(value = "[房产交易基本信息] - 扩展字段[] - [] - 契税不征收项目")
      @TableField("qsbzsxm_dm")
    private String qsbzsxmDm;

      @ApiModelProperty(value = "[增值税发票信息] - 扩展字段[] - [] - 发票代码")
      @TableField("fpdm")
    private String fpdm;

      @ApiModelProperty(value = "[增值税发票信息] - 扩展字段[] - [] - 发票号码")
      @TableField("fphm")
    private String fphm;

      @ApiModelProperty(value = "[差额征收抵扣信息] - 扩展字段[] - [] - 抵扣信息序号")
      @TableField("dkxxxh")
    private Integer dkxxxh;

      @ApiModelProperty(value = "[差额征收抵扣信息] - 扩展字段[] - [] - 抵扣分类 1 购房发票 2 装 修发票 ...")
      @TableField("dkxxfl")
    private Integer dkxxfl;

      @ApiModelProperty(value = "[差额征收抵扣信息] - 扩展字段[] - [] - 票据类型 1 发票 2 税票")
      @TableField("dkpjlx")
    private Integer dkpjlx;

      @ApiModelProperty(value = "[差额征收抵扣信息] - 扩展字段[] - [] - 发票代码或税票 字轨")
      @TableField("dkxxdm")
    private String dkxxdm;

      @ApiModelProperty(value = "[差额征收抵扣信息] - 扩展字段[] - [] - 发票号码或税票 号码")
      @TableField("dkxxhm")
    private String dkxxhm;

      @ApiModelProperty(value = "[差额征收抵扣信息] - 扩展字段[] - [] - 开票日期 填发日期")
      @TableField("dkxxkprq")
    private String dkxxkprq;

      @ApiModelProperty(value = "[差额征收抵扣信息] - 扩展字段[] - [] - 票面金额")
      @TableField("dkxxpmje")
    private BigDecimal dkxxpmje;

      @ApiModelProperty(value = "[其他信息] - 扩展字段[] - [] - 是否共有人打票 文档中要求必传 但实际协税系统会保持 为否 所以为非必传")
      @TableField("sffgyrdp")
    private String sffgyrdp;

      @ApiModelProperty(value = "[系统字段] - 增量房存量房标识")
      @TableField("zlfclfbs")
    private String zlfclfbs;


    @Override
    protected Serializable pkVal() {
          return this.id;
      }

}
