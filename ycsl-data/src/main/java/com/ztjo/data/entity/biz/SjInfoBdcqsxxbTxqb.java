package com.ztjo.data.entity.biz;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 不动产权属信息的  他项权记录表
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("sj_info_bdcqsxxb_txqb")
@ApiModel(value="SjInfoBdcqsxxbTxqb对象", description="不动产权属信息的  他项权记录表")
public class SjInfoBdcqsxxbTxqb extends Model<SjInfoBdcqsxxbTxqb> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "id")
        @TableId("id")
      private Long id;

      @ApiModelProperty(value = "info id")
      @TableField("info_id")
    private Long infoId;

      @ApiModelProperty(value = "受理编号")
      @TableField("bdcywh")
    private String bdcywh;

      @ApiModelProperty(value = "他项权开始日期")
      @TableField("txqkssj")
    private String txqkssj;

      @ApiModelProperty(value = "他项权结束日期")
      @TableField("txqjssj")
    private String txqjssj;

      @ApiModelProperty(value = "他项权期限")
      @TableField("txqqx")
    private String txqqx;

      @ApiModelProperty(value = "他项权类型（抵押/查封/异议）")
      @TableField("txqlx")
    private String txqlx;

      @ApiModelProperty(value = "不动产单元号")
      @TableField("bdcdyh")
    private String bdcdyh;

      @ApiModelProperty(value = "状态")
      @TableField("status")
    private String status;

      @ApiModelProperty(value = "扩展字段1")
      @TableField("ext1")
    private String ext1;

      @ApiModelProperty(value = "扩展字段2")
      @TableField("ext2")
    private String ext2;

      @ApiModelProperty(value = "扩展字段3")
      @TableField("ext3")
    private String ext3;


    @Override
    protected Serializable pkVal() {
          return this.id;
      }

}
