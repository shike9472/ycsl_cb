package com.ztjo.data.entity.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author 陈彬
 * @version 2022/1/10
 * description：基础业务数据模型
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value="业务服务数据存储模型",
        description="定义各业务入口数据入库时必然涉及的相关字段")
public class BizBaseModel<T extends Model<?>> extends Model<T> {

    @TableField("cjsj")
    @ApiModelProperty(value = "创建时间")
    private Date cjsj;

    @TableField("save_user")
    @ApiModelProperty(value = "保存用户username")
    private String saveUser;

    @TableField("save_user_name")
    @ApiModelProperty(value = "保存用户name")
    private String saveUserName;

    @TableField("create_user")
    @ApiModelProperty(value = "创建用户username")
    private String createUser;

    @TableField("create_user_name")
    @ApiModelProperty(value = "创建用户name")
    private String createUserName;

    @TableField("tenant_id")
    @ApiModelProperty(value = "租户id")
    private Long tenantId;

    @TableField("depart_id")
    @ApiModelProperty(value = "部门id")
    private Long departId;

    @TableField("depart_name")
    @ApiModelProperty(value = "部门名称")
    private String departName;

    @TableField("area_code")
    @ApiModelProperty(value = "区域编码")
    private String areaCode;

    @TableField("area_name")
    @ApiModelProperty(value = "区域名称")
    private String areaName;

    @TableField("attr1")
    @ApiModelProperty(value = "扩展字段1")
    private String attr1;

    @TableField("attr2")
    @ApiModelProperty(value = "扩展字段2")
    private String attr2;

    @TableField("attr3")
    @ApiModelProperty(value = "扩展字段3")
    private String attr3;

    @TableField("attr4")
    @ApiModelProperty(value = "扩展字段4")
    private String attr4;

    @TableField("attr5")
    @ApiModelProperty(value = "扩展字段5")
    private String attr5;

    @TableField("attr6")
    @ApiModelProperty(value = "扩展字段6")
    private String attr6;

    @TableField("attr7")
    @ApiModelProperty(value = "扩展字段7")
    private String attr7;

    @TableField("attr8")
    @ApiModelProperty(value = "扩展字段8")
    private String attr8;
}
