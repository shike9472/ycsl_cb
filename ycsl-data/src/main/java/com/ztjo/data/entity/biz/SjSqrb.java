package com.ztjo.data.entity.biz;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.ztjo.data.entity.model.BizSqrModel;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-12
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("sj_sqrb")
@ApiModel(value="SjSqrb对象", description="")
public class SjSqrb extends BizSqrModel<SjSqrb> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "主键")
        @TableId("id")
      private Long id;

      @ApiModelProperty(value = "申请编号")
      @TableField("sqbh")
    private String sqbh;

      @ApiModelProperty(value = "info表主键")
      @TableField("info_id")
    private Long infoId;

      @ApiModelProperty(value = "数据表标识")
      @TableField("sjbbs")
    private String sjbbs;

      @ApiModelProperty(value = "申请人种类")
      @TableField("sqrzl")
    private String sqrzl;

      @ApiModelProperty(value = "申请人种类名称")
      @TableField("sqrzlmc")
    private String sqrzlmc;

      @ApiModelProperty(value = "权利人顺序")
      @TableField("qlrsx")
    private Integer qlrsx;

      @ApiModelProperty(value = "共有方式")
      @TableField("gyfs")
    private String gyfs;

      @ApiModelProperty(value = "共有方式名称")
      @TableField("gyfsmc")
    private String gyfsmc;

      @ApiModelProperty(value = "共有份额")
      @TableField("gyfe")
    private String gyfe;

      @ApiModelProperty(value = "是否持证")
      @TableField("sfcz")
    private String sfcz;

      @ApiModelProperty(value = "所持证号")
      @TableField("sczh")
    private String sczh;


    @Override
    protected Serializable pkVal() {
          return this.id;
      }

}
