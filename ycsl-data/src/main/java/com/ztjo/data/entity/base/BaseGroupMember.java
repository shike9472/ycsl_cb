package com.ztjo.data.entity.base;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ztjo.data.entity.model.BaseRecordModel1;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 用户组 - 成员表
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("base_group_member")
@ApiModel(value="BaseGroupMember对象", description="用户组 - 成员表")
public class BaseGroupMember extends BaseRecordModel1<BaseGroupMember> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "主键")
        @TableId("id")
      private Long id;

      @ApiModelProperty(value = "角色id")
      @TableField("group_id")
    private Long groupId;

      @ApiModelProperty(value = "成员id")
      @TableField("user_id")
    private Long userId;

      @ApiModelProperty(value = "说明 - 介绍")
      @TableField("description")
    private String description;

      @ApiModelProperty(value = "扩展字段1")
      @TableField("attr1")
    private String attr1;

      @ApiModelProperty(value = "扩展字段2")
      @TableField("attr2")
    private String attr2;

      @ApiModelProperty(value = "扩展字段3")
      @TableField("attr3")
    private String attr3;

      @ApiModelProperty(value = "扩展字段4")
      @TableField("attr4")
    private String attr4;

      @ApiModelProperty(value = "扩展字段5")
      @TableField("attr5")
    private String attr5;

      @ApiModelProperty(value = "扩展字段6")
      @TableField("attr6")
    private String attr6;

      @ApiModelProperty(value = "扩展字段7")
      @TableField("attr7")
    private String attr7;

      @ApiModelProperty(value = "扩展字段8")
      @TableField("attr8")
    private String attr8;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
