package com.ztjo.data.entity.biz;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ztjo.data.entity.model.BizInfoModel;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * info - 水电气广收件信息表
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("sj_info_sdqgsjxxb")
@ApiModel(value="SjInfoSdqgsjxxb对象", description="info - 水电气广收件信息表")
public class SjInfoSdqgsjxxb extends BizInfoModel<SjInfoSdqgsjxxb> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "主键")
        @TableId("id")
      private Long id;

      @ApiModelProperty(value = "旧电力户号")
      @TableField("jdlhh")
    private String jdlhh;

      @ApiModelProperty(value = "新电力户号")
      @TableField("xdlhh")
    private String xdlhh;

      @ApiModelProperty(value = "电力公司")
      @TableField("dlgs")
    private String dlgs;

      @ApiModelProperty(value = "旧自来水户号")
      @TableField("jzlshh")
    private String jzlshh;

      @ApiModelProperty(value = "新自来水户号")
      @TableField("xzlshh")
    private String xzlshh;

      @ApiModelProperty(value = "自来水公司")
      @TableField("zlsgs")
    private String zlsgs;

      @ApiModelProperty(value = "旧燃气户号")
      @TableField("jrqhh")
    private String jrqhh;

      @ApiModelProperty(value = "新燃气户号")
      @TableField("xrqhh")
    private String xrqhh;

      @ApiModelProperty(value = "燃气公司")
      @TableField("rqgs")
    private String rqgs;

      @ApiModelProperty(value = "旧电视广播户号")
      @TableField("jdsgbhh")
    private String jdsgbhh;

      @ApiModelProperty(value = "新电视广播户号")
      @TableField("xdsgbhh")
    private String xdsgbhh;

      @ApiModelProperty(value = "广播电视过户公司")
      @TableField("dsgbgs")
    private String dsgbgs;

      @ApiModelProperty(value = "原始查询数据 - （保留字段 - 新系统无用）")
      @TableField("data_json")
    private String dataJson;

      @ApiModelProperty(value = "数据提供单位 - （保留字段 - 新系统无用）")
      @TableField("provide_unit")
    private String provideUnit;

      @ApiModelProperty(value = "数据获取方式（手输/接口/excel） - （保留字段 - 新系统无用）")
      @TableField("data_come_from_mode")
    private String dataComeFromMode;


    @Override
    protected Serializable pkVal() {
          return this.id;
      }

}
