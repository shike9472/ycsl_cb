package com.ztjo.data.entity.base;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ztjo.data.entity.model.BaseRecordModel1;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 菜单资源表 - 保存系统菜单
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("base_menu")
@ApiModel(value="BaseMenu对象", description="菜单资源表 - 保存系统菜单")
public class BaseMenu extends BaseRecordModel1<BaseMenu> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "主键")
        @TableId("id")
      private Long id;

      @ApiModelProperty(value = "路径编码")
      @TableField("code")
    private String code;

      @ApiModelProperty(value = "标题")
      @TableField("title")
    private String title;

      @ApiModelProperty(value = "父级节点")
      @TableField("parent_id")
    private Long parentId;

      @ApiModelProperty(value = "路由信息")
      @TableField("path")
    private String path;

      @ApiModelProperty(value = "图标")
      @TableField("icon")
    private String icon;

      @ApiModelProperty(value = "类型(dirt/menu)")
      @TableField("type")
    private String type;

      @ApiModelProperty(value = "排序")
      @TableField("order_num")
    private Integer orderNum;

      @ApiModelProperty(value = "描述 - 介绍")
      @TableField("description")
    private String description;

      @ApiModelProperty(value = "组件标志")
      @TableField("component")
    private String component;

      @ApiModelProperty(value = "启用禁用")
      @TableField("enabled")
    private String enabled;

      @ApiModelProperty(value = "扩展字段1")
      @TableField("attr1")
    private String attr1;

      @ApiModelProperty(value = "扩展字段2")
      @TableField("attr2")
    private String attr2;

      @ApiModelProperty(value = "扩展字段3")
      @TableField("attr3")
    private String attr3;

      @ApiModelProperty(value = "扩展字段4")
      @TableField("attr4")
    private String attr4;

      @ApiModelProperty(value = "扩展字段5")
      @TableField("attr5")
    private String attr5;

      @ApiModelProperty(value = "扩展字段6")
      @TableField("attr6")
    private String attr6;

      @ApiModelProperty(value = "扩展字段7")
      @TableField("attr7")
    private String attr7;

      @ApiModelProperty(value = "扩展字段8")
      @TableField("attr8")
    private String attr8;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
