package com.ztjo.data.entity.biz;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.sql.Blob;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 系统附件表
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("sj_fjxx_file")
@ApiModel(value="SjFjxxFile对象", description="系统附件表")
public class SjFjxxFile extends Model<SjFjxxFile> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "主键")
        @TableId("id")
      private Long id;

      @ApiModelProperty(value = "文件名称")
      @TableField("wjmc")
    private String wjmc;

      @ApiModelProperty(value = "文件扩展名")
      @TableField("wjkzm")
    private String wjkzm;

      @ApiModelProperty(value = "实际文件类型")
      @TableField("sjwjlx")
    private String sjwjlx;

      @ApiModelProperty(value = "文件大小")
      @TableField("wjdx")
    private String wjdx;

      @ApiModelProperty(value = "ftp保存路径")
      @TableField("wjbclj")
    private String wjbclj;

      @ApiModelProperty(value = "文件内容（不适用ftp模式时使用）")
      @TableField("wjnr")
    private byte[] wjnr;

      @ApiModelProperty(value = "文件当前状态")
      @TableField("status")
    private String status;

      @ApiModelProperty(value = "文件提交时间")
      @TableField("cjsj")
    private Date cjsj;

      @ApiModelProperty(value = "保存方式0-本地/1-FTP")
      @TableField("bcfs")
    private String bcfs;

      @ApiModelProperty(value = "扩展字段1")
      @TableField("ext1")
    private String ext1;

      @ApiModelProperty(value = "扩展字段2")
      @TableField("ext2")
    private String ext2;

      @ApiModelProperty(value = "扩展字段3")
      @TableField("ext3")
    private String ext3;

      @ApiModelProperty(value = "扩展字段4")
      @TableField("ext4")
    private String ext4;


    @Override
    protected Serializable pkVal() {
          return this.id;
      }

}
