package com.ztjo.data.entity.biz;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ztjo.data.entity.model.BizInfoModel;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * info表 - 不动产权属信息表
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("sj_info_bdcqsxxb")
@ApiModel(value="SjInfoBdcqsxxb对象", description="info表 - 不动产权属信息表")
public class SjInfoBdcqsxxb extends BizInfoModel<SjInfoBdcqsxxb> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "主键")
        @TableId("id")
      private Long id;

      @ApiModelProperty(value = "不动产受理编号")
      @TableField("bdcywh")
    private String bdcywh;

      @ApiModelProperty(value = "登记类型")
      @TableField("djlx")
    private String djlx;

      @ApiModelProperty(value = "不动产坐落")
      @TableField("zl")
    private String zl;

      @ApiModelProperty(value = "不动产权证号")
      @TableField("bdccqzh")
    private String bdccqzh;

      @ApiModelProperty(value = "房产证号")
      @TableField("fczh")
    private String fczh;

      @ApiModelProperty(value = "土地证号")
      @TableField("tdzh")
    private String tdzh;

      @ApiModelProperty(value = "登记时间")
      @TableField("djsj")
    private Date djsj;

      @ApiModelProperty(value = "证书类型(与sjsq中证书类型保持一致）")
      @TableField("zslx")
    private String zslx;

      @ApiModelProperty(value = "建筑面积")
      @TableField("jzmj")
    private BigDecimal jzmj;

      @ApiModelProperty(value = "套内建筑面积")
      @TableField("tnjzmj")
    private BigDecimal tnjzmj;

      @ApiModelProperty(value = "分摊建筑面积")
      @TableField("ftjzmj")
    private BigDecimal ftjzmj;

      @ApiModelProperty(value = "阁楼面积")
      @TableField("glmj")
    private BigDecimal glmj;

      @ApiModelProperty(value = "车库面积")
      @TableField("ckmj")
    private BigDecimal ckmj;

      @ApiModelProperty(value = "储藏室面积")
      @TableField("ccsmj")
    private BigDecimal ccsmj;

      @ApiModelProperty(value = "附属设施")
      @TableField("fsss")
    private String fsss;

      @ApiModelProperty(value = "房屋取得方式")
      @TableField("fwqdfs")
    private String fwqdfs;

      @ApiModelProperty(value = "房屋取得价格")
      @TableField("fwqdjg")
    private BigDecimal fwqdjg;

      @ApiModelProperty(value = "房屋规划用途")
      @TableField("fwghyt")
    private String fwghyt;

      @ApiModelProperty(value = "房屋规划用途名称")
      @TableField("fwghytmc")
    private String fwghytmc;

      @ApiModelProperty(value = "房屋评估金额")
      @TableField("fwpgje")
    private BigDecimal fwpgje;

      @ApiModelProperty(value = "房屋类型")
      @TableField("fwlx")
    private String fwlx;

      @ApiModelProperty(value = "房屋性质")
      @TableField("fwxz")
    private String fwxz;

      @ApiModelProperty(value = "房屋权利类型")
      @TableField("fwqllx")
    private String fwqllx;

      @ApiModelProperty(value = "房屋权利性质")
      @TableField("fwqlxz")
    private String fwqlxz;

      @ApiModelProperty(value = "土地权利类型")
      @TableField("tdqllx")
    private String tdqllx;

      @ApiModelProperty(value = "土地权利性质")
      @TableField("tdqlxz")
    private String tdqlxz;

      @ApiModelProperty(value = "土地使用权起始日期")
      @TableField("tdsyqqsrq")
    private Date tdsyqqsrq;

      @ApiModelProperty(value = "土地使用终止日期")
      @TableField("tdsyqzzrq")
    private Date tdsyqzzrq;

      @ApiModelProperty(value = "土地使用权人")
      @TableField("tdsyqr")
    private String tdsyqr;

      @ApiModelProperty(value = "土地使用期限")
      @TableField("tdsyqx")
    private String tdsyqx;

      @ApiModelProperty(value = "土地用途")
      @TableField("tdyt")
    private String tdyt;

      @ApiModelProperty(value = "土地取得方式")
      @TableField("tdqdfs")
    private String tdqdfs;

      @ApiModelProperty(value = "共有土地面积")
      @TableField("gytdmj")
    private BigDecimal gytdmj;

      @ApiModelProperty(value = "独用土地面积")
      @TableField("dytdmj")
    private BigDecimal dytdmj;

      @ApiModelProperty(value = "分摊土地面积")
      @TableField("fttdmj")
    private BigDecimal fttdmj;

      @ApiModelProperty(value = "建筑宗地面积")
      @TableField("jzzdmj")
    private BigDecimal jzzdmj;

      @ApiModelProperty(value = "备注附记")
      @TableField("bz")
    private String bz;

      @ApiModelProperty(value = "权利其它状况")
      @TableField("qt")
    private String qt;

      @ApiModelProperty(value = "其它限制(其它限制,内部限制/其它限制/内部限制/无或返回空值)")
      @TableField("qtxz")
    private String qtxz;

      @ApiModelProperty(value = "原始数据json - （保留字段 - 新系统无用）")
      @TableField("data_json")
    private String dataJson;

      @ApiModelProperty(value = "数据类型（存量/新增） - （保留字段 - 新系统无用）")
      @TableField("data_type")
    private String dataType;

      @ApiModelProperty(value = "数据提供单位 - （保留字段 - 新系统无用）")
      @TableField("provide_unit")
    private String provideUnit;

      @ApiModelProperty(value = "数据获取方式（手输/接口/excel）")
      @TableField("data_come_from_mode")
    private String dataComeFromMode;

      @ApiModelProperty(value = "首次登记证明号(收预告信息时存入)")
      @TableField("scdjzmh")
    private String scdjzmh;

      @ApiModelProperty(value = "首次登记日期(收预告信息时存入)")
      @TableField("scdjrq")
    private String scdjrq;


    @Override
    protected Serializable pkVal() {
          return this.id;
      }

}
