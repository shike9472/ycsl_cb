package com.ztjo.data.entity.base;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ztjo.data.entity.model.BaseRecordModel2;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 字典值表 - 用于规范固定字典项
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("dict_value")
@ApiModel(value="DictValue对象", description="字典值表 - 用于规范固定字典项")
public class DictValue extends BaseRecordModel2<DictValue> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "主键")
        @TableId("id")
      private Long id;

      @ApiModelProperty(value = "编码")
      @TableField("code")
    private String code;

      @ApiModelProperty(value = "值")
      @TableField("value")
    private String value;

      @ApiModelProperty(value = "默认显示")
      @TableField("label_default")
    private String labelDefault;

      @ApiModelProperty(value = "英文显示")
      @TableField("label_en_us")
    private String labelEnUs;

      @ApiModelProperty(value = "中文显示")
      @TableField("label_zh_ch")
    private String labelZhCh;

      @ApiModelProperty(value = "dic_type表主键id")
      @TableField("type_id")
    private Long typeId;

      @ApiModelProperty(value = "值1")
      @TableField("label_attr1")
    private String labelAttr1;

      @ApiModelProperty(value = "值2")
      @TableField("label_attr2")
    private String labelAttr2;

      @ApiModelProperty(value = "值3")
      @TableField("label_attr3")
    private String labelAttr3;

      @ApiModelProperty(value = "父级id")
      @TableField("parent_id")
    private Long parentId;

      @ApiModelProperty(value = "属性值1")
      @TableField("attr1")
    private String attr1;

      @ApiModelProperty(value = "属性值2")
      @TableField("attr2")
    private String attr2;

      @ApiModelProperty(value = "属性值3")
      @TableField("attr3")
    private String attr3;

      @ApiModelProperty(value = "属性值4")
      @TableField("attr4")
    private String attr4;

      @ApiModelProperty(value = "排序")
      @TableField("order_num")
    private Integer orderNum;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
