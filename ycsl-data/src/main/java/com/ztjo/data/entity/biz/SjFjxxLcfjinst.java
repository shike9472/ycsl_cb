package com.ztjo.data.entity.biz;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 附件信息 - 流程附件实例信息表（树形）
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("sj_fjxx_lcfjinst")
@ApiModel(value="SjFjxxLcfjinst对象", description="附件信息 - 流程附件实例信息表（树形）")
public class SjFjxxLcfjinst extends Model<SjFjxxLcfjinst> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "主键")
        @TableId("id")
      private Long id;

      @ApiModelProperty(value = "父级目录id")
      @TableField("pid")
    private Long pid;

      @ApiModelProperty(value = "申请编号")
      @TableField("sqbh")
    private String sqbh;

      @ApiModelProperty(value = "流程实例id")
      @TableField("proc_inst_id")
    private String procInstId;

      @ApiModelProperty(value = "附件实例名称")
      @TableField("fjslmc")
    private String fjslmc;

      @ApiModelProperty(value = "附件实例是否必选")
      @TableField("fjslsfbx")
    private String fjslsfbx;

      @ApiModelProperty(value = "附件实例类型（文件夹-folder/文件-file）")
      @TableField("fjsllx")
    private String fjsllx;

      @ApiModelProperty(value = "附件信息表id主键 - 文件型附件实例此项不可为空")
      @TableField("file_id")
    private Long fileId;

      @ApiModelProperty(value = "隶属条目id")
      @TableField("lstm_id")
    private Long lstmId;

      @ApiModelProperty(value = "序号")
      @TableField("xh")
    private Integer xh;

      @ApiModelProperty(value = "创建时间")
      @TableField("cjsj")
    private Date cjsj;

      @ApiModelProperty(value = "创建用户")
      @TableField("create_user")
    private String createUser;

      @ApiModelProperty(value = "创建用户名称")
      @TableField("create_user_name")
    private String createUserName;

      @ApiModelProperty(value = "状态（保留字段）")
      @TableField("status")
    private String status;

      @ApiModelProperty(value = "2021-11-13 兴化启用 字段长度扩展 做附件说明使用")
      @TableField("ext1")
    private String ext1;

    @TableField("ext2")
    private String ext2;

    @TableField("ext3")
    private String ext3;

      @ApiModelProperty(value = "收件数量")
      @TableField("sl")
    private Integer sl;

      @ApiModelProperty(value = "适配系统标识（bdc-不动产/zjj-住建局/swj-税务局）")
      @TableField("spxtbs")
    private String spxtbs;

      @ApiModelProperty(value = "映射其它系统文件夹名称")
      @TableField("yswjjmc")
    private String yswjjmc;


    @Override
    protected Serializable pkVal() {
          return this.id;
      }

}
