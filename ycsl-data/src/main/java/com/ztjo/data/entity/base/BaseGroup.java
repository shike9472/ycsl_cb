package com.ztjo.data.entity.base;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ztjo.data.entity.model.BaseRecordModel2;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 角色组表 - 权限集合表征
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("base_group")
@ApiModel(value="BaseGroup对象", description="角色组表 - 权限集合表征")
public class BaseGroup extends BaseRecordModel2<BaseGroup> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "主键")
        @TableId("id")
      private Long id;

      @ApiModelProperty(value = "角色编码")
      @TableField("code")
    private String code;

      @ApiModelProperty(value = "角色名称")
      @TableField("name")
    private String name;

      @ApiModelProperty(value = "上级节点")
      @TableField("parent_id")
    private Long parentId;

      @ApiModelProperty(value = "树状关系")
      @TableField("path")
    private String path;

      @ApiModelProperty(value = "类型")
      @TableField("type")
    private String type;

      @ApiModelProperty(value = "角色组类型")
      @TableField("group_type")
    private String groupType;

      @ApiModelProperty(value = "描述-介绍")
      @TableField("description")
    private String description;

      @ApiModelProperty(value = "扩展字段1")
      @TableField("attr1")
    private String attr1;

      @ApiModelProperty(value = "扩展字段2")
      @TableField("attr2")
    private String attr2;

      @ApiModelProperty(value = "扩展字段3")
      @TableField("attr3")
    private String attr3;

      @ApiModelProperty(value = "扩展字段4")
      @TableField("attr4")
    private String attr4;

      @ApiModelProperty(value = "扩展字段5")
      @TableField("attr5")
    private String attr5;

      @ApiModelProperty(value = "扩展字段6")
      @TableField("attr6")
    private String attr6;

      @ApiModelProperty(value = "扩展字段7")
      @TableField("attr7")
    private String attr7;

      @ApiModelProperty(value = "扩展字段8")
      @TableField("attr8")
    private String attr8;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
