package com.ztjo.data.entity.biz;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ztjo.data.entity.model.BizInfoModel;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * info表 - 在建工程抵押幢信息表（设立/变更/转移/注销均可入该表）
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("sj_info_zjgcdyzxxb")
@ApiModel(value="SjInfoZjgcdyzxxb对象", description="info表 - 在建工程抵押幢信息表（设立/变更/转移/注销均可入该表）")
public class SjInfoZjgcdyzxxb extends BizInfoModel<SjInfoZjgcdyzxxb> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "主键")
        @TableId("id")
      private Long id;

      @ApiModelProperty(value = "幢房屋代码")
      @TableField("zfwdm")
    private String zfwdm;

      @ApiModelProperty(value = "幢不动产单元号")
      @TableField("zbdcdyh")
    private String zbdcdyh;

      @ApiModelProperty(value = "幢坐落")
      @TableField("zfwzl")
    private String zfwzl;

      @ApiModelProperty(value = "登记事项")
      @TableField("djsx")
    private String djsx;

      @ApiModelProperty(value = "登记类型")
      @TableField("djlx")
    private String djlx;

      @ApiModelProperty(value = "申请方式")
      @TableField("sqfs")
    private String sqfs;

      @ApiModelProperty(value = "抵押方式")
      @TableField("dyfs")
    private String dyfs;

      @ApiModelProperty(value = "担保范围")
      @TableField("dbfw")
    private String dbfw;

      @ApiModelProperty(value = "币种")
      @TableField("currency")
    private String currency;

      @ApiModelProperty(value = "抵押合同签订日期")
      @TableField("qdrq")
    private String qdrq;

      @ApiModelProperty(value = "最⾼额确定事实")
      @TableField("zgeqdss")
    private String zgeqdss;

      @ApiModelProperty(value = "债务开始时间")
      @TableField("zwkssj")
    private String zwkssj;

      @ApiModelProperty(value = "债务结束时间")
      @TableField("zwjssj")
    private String zwjssj;

      @ApiModelProperty(value = "建筑⾯积")
      @TableField("jzmj")
    private BigDecimal jzmj;

      @ApiModelProperty(value = "债务⼈姓名")
      @TableField("zwr")
    private String zwr;

      @ApiModelProperty(value = "债务人证件类型")
      @TableField("zwrzjlx")
    private String zwrzjlx;

      @ApiModelProperty(value = "债务人证件类型名称")
      @TableField("zwrzjlxmc")
    private String zwrzjlxmc;

      @ApiModelProperty(value = "债务人证件号码")
      @TableField("zwrzjhm")
    private String zwrzjhm;

      @ApiModelProperty(value = "债务人联系电话")
      @TableField("zwrlxdh")
    private String zwrlxdh;

      @ApiModelProperty(value = "原始查询数据  - （保留字段 - 新系统无用）")
      @TableField("data_json")
    private String dataJson;

      @ApiModelProperty(value = "数据类型（存量/新增）  - （保留字段 - 新系统无用）")
      @TableField("data_type")
    private String dataType;

      @ApiModelProperty(value = "数据提供单位  - （保留字段 - 新系统无用）")
      @TableField("provide_unit")
    private String provideUnit;

      @ApiModelProperty(value = "数据获取方式（手输/接口/excel）  - （保留字段 - 新系统无用）")
      @TableField("data_come_from_mode")
    private String dataComeFromMode;


    @Override
    protected Serializable pkVal() {
          return this.id;
      }

}
