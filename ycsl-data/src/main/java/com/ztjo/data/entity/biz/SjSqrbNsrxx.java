package com.ztjo.data.entity.biz;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 纳税人表
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("sj_sqrb_nsrxx")
@ApiModel(value="SjSqrbNsrxx对象", description="纳税人表")
public class SjSqrbNsrxx extends Model<SjSqrbNsrxx> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "[交易双方(纳税人)的信息] - 主键")
        @TableId("id")
      private String id;

      @ApiModelProperty(value = "swsjlrb关联字段")
      @TableField("info_id")
    private String infoId;

      @ApiModelProperty(value = "[交易双方(纳税人)的信息] - 转让方/受让方标志 0转让 1受让")
      @TableField("zrfcsfbz")
    private Integer zrfcsfbz;

      @ApiModelProperty(value = "[交易双方(纳税人)的信息] - 纳税人识别号 如果是个人 请传入个人的身份证号码")
      @TableField("nsrsbh")
    private String nsrsbh;

      @ApiModelProperty(value = "[交易双方(纳税人)的信息] - 纳税人名称")
      @TableField("nsrmc")
    private String nsrmc;

      @ApiModelProperty(value = "[交易双方(纳税人)的信息] - 纳税人地址 可传入人员通讯地址或身份证上的地址 如果实在获取不到填写‘未知’")
      @TableField("dz")
    private String dz;

      @ApiModelProperty(value = "[交易双方(纳税人)的信息] - 主产权人标志")
      @TableField("zqrbz")
    private String zqrbz;

      @ApiModelProperty(value = "[交易双方(纳税人)的信息] - 身份证件类型代码")
      @TableField("sfzjlxd")
    private String sfzjlxd;

      @ApiModelProperty(value = "[交易双方(纳税人)的信息] - 身份证件号码 如果是身份证和则纳税人号码一致")
      @TableField("sfzjhm")
    private String sfzjhm;

      @ApiModelProperty(value = "[交易双方(纳税人)的信息] - 国籍代码（当前只给156-中国）")
      @TableField("gjdm")
    private String gjdm;

      @ApiModelProperty(value = "[交易双方(纳税人)的信息] - 联系电话")
      @TableField("lxdh")
    private String lxdh;

      @ApiModelProperty(value = "[交易双方(纳税人)的信息] - 变动份额")
      @TableField("bdfe2")
    private BigDecimal bdfe2;

      @ApiModelProperty(value = "[交易双方(纳税人)的信息] - 所占份额")
      @TableField("szfe")
    private BigDecimal szfe;

      @ApiModelProperty(value = "[交易双方(纳税人)的信息] - 房屋套次代码")
      @TableField("fwtcdm")
    private String fwtcdm;

      @ApiModelProperty(value = "[交易双方(纳税人)的信息] - 购买方直系亲属(Y-是/N-否)")
      @TableField("gmfzxqsbz")
    private String gmfzxqsbz;

      @ApiModelProperty(value = "[交易双方(纳税人)的信息] - 当前人员的婚姻状况 0 未婚 1 已婚 2 离异")
      @TableField("yhbz")
    private Integer yhbz;

      @ApiModelProperty(value = "[交易双方(纳税人)的信息] - 自然人标志 1 企业 2 自然人")
      @TableField("zrrbz")
    private Integer zrrbz;

      @ApiModelProperty(value = "[交易双方(纳税人)的信息] - 登记注册类型代码")
      @TableField("djzclxdm")
    private String djzclxdm;

      @ApiModelProperty(value = "[交易双方(纳税人)的信息] - 增值税一般纳税人标志 0 否 1 是")
      @TableField("zzsybnsr")
    private String zzsybnsr;

      @ApiModelProperty(value = "[交易双方(纳税人)的信息] - 单位隶属关系代码  仅限单位")
      @TableField("dwlsgxdm")
    private String dwlsgxdm;

      @ApiModelProperty(value = "[交易双方(纳税人)的信息] - 上次取得房屋方式 -- 增量房不需要  存量房需要（默认买卖）")
      @TableField("scqdfwfs")
    private String scqdfwfs;

      @ApiModelProperty(value = "[交易双方(纳税人)的信息] - 上次取得房屋时间 Yyyy-mm-dd")
      @TableField("scqdfwsj")
    private String scqdfwsj;

      @ApiModelProperty(value = "[交易双方(纳税人)的信息] - 实际报文大多是0上次取得房屋成本")
      @TableField("scqdfwcb")
    private String scqdfwcb;

      @ApiModelProperty(value = "[交易双方(纳税人)的信息] - 备注")
      @TableField("bz")
    private String bz;

      @ApiModelProperty(value = "[交易双方(纳税人)的信息] - 配偶姓名")
      @TableField("poxm")
    private String poxm;

      @ApiModelProperty(value = "[交易双方(纳税人)的信息] - 配偶国籍代码（156-中国）")
      @TableField("pogjdm")
    private String pogjdm;

      @ApiModelProperty(value = "[交易双方(纳税人)的信息] - 配偶证件类型")
      @TableField("pozjlx_dm")
    private String pozjlxDm;

      @ApiModelProperty(value = "[交易双方(纳税人)的信息] - 配偶证件号码")
      @TableField("pozjhm")
    private String pozjhm;


    @Override
    protected Serializable pkVal() {
          return this.id;
      }

}
