package com.ztjo.data.entity.biz;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("sj_info_zjgccfzxxb_hxxb")
@ApiModel(value="SjInfoZjgccfzxxbHxxb对象", description="")
public class SjInfoZjgccfzxxbHxxb extends Model<SjInfoZjgccfzxxbHxxb> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "主键")
        @TableId("id")
      private Long id;

      @ApiModelProperty(value = "sj_info_zjgccfzxxb表关联主键")
      @TableField("info_id")
    private Long infoId;

      @ApiModelProperty(value = "产权证号")
      @TableField("cqzh")
    private String cqzh;

      @ApiModelProperty(value = "不动产单元号")
      @TableField("bdcdyh")
    private String bdcdyh;

      @ApiModelProperty(value = "房屋代码")
      @TableField("fwdm")
    private String fwdm;

      @ApiModelProperty(value = "附着物代码")
      @TableField("fzwdm")
    private String fzwdm;

      @ApiModelProperty(value = "坐落")
      @TableField("zl")
    private String zl;

      @ApiModelProperty(value = "规划用途")
      @TableField("ghyt")
    private String ghyt;

      @ApiModelProperty(value = "规划用途名称")
      @TableField("ghytmc")
    private String ghytmc;

      @ApiModelProperty(value = "房屋结构")
      @TableField("fwjg")
    private String fwjg;

      @ApiModelProperty(value = "房屋结构名称")
      @TableField("fwjgmc")
    private String fwjgmc;

      @ApiModelProperty(value = "权利性质")
      @TableField("qlxz")
    private String qlxz;

      @ApiModelProperty(value = "建筑面积")
      @TableField("jzmj")
    private BigDecimal jzmj;

      @ApiModelProperty(value = "套内建筑面积")
      @TableField("tnjzmj")
    private BigDecimal tnjzmj;

      @ApiModelProperty(value = "分摊建筑面积")
      @TableField("ftjzmj")
    private BigDecimal ftjzmj;

      @ApiModelProperty(value = "幢号")
      @TableField("zh")
    private String zh;

      @ApiModelProperty(value = "户号")
      @TableField("hh")
    private String hh;

      @ApiModelProperty(value = "单元号")
      @TableField("dyh")
    private String dyh;

      @ApiModelProperty(value = "门牌号")
      @TableField("mph")
    private String mph;

      @ApiModelProperty(value = "查封原因")
      @TableField("cfyy")
    private String cfyy;

      @ApiModelProperty(value = "查封范围")
      @TableField("cffw")
    private String cffw;

      @ApiModelProperty(value = "查封类型（查封/轮候查封）")
      @TableField("cflx")
    private String cflx;

      @ApiModelProperty(value = "查封类型名称")
      @TableField("cflxmc")
    private String cflxmc;

      @ApiModelProperty(value = "查封编号")
      @TableField("cfbh")
    private String cfbh;

      @ApiModelProperty(value = "查封文件")
      @TableField("cfwj")
    private String cfwj;

      @ApiModelProperty(value = "查封文号")
      @TableField("cfwh")
    private String cfwh;

      @ApiModelProperty(value = "查封机关")
      @TableField("cfjg")
    private String cfjg;

      @ApiModelProperty(value = "查封期限")
      @TableField("cfqx")
    private String cfqx;

      @ApiModelProperty(value = "查封起始日期")
      @TableField("cfqsrq")
    private Date cfqsrq;

      @ApiModelProperty(value = "查封终止日期")
      @TableField("cfzzrq")
    private Date cfzzrq;

      @ApiModelProperty(value = "查封顺序")
      @TableField("cfsx")
    private Integer cfsx;

      @ApiModelProperty(value = "查封业务号")
      @TableField("cfywh")
    private String cfywh;

      @ApiModelProperty(value = "登记时间")
      @TableField("djsj")
    private Date djsj;

      @ApiModelProperty(value = "查封登记机构")
      @TableField("djjg")
    private String djjg;

      @ApiModelProperty(value = "附记")
      @TableField("fj")
    private String fj;

      @ApiModelProperty(value = "申请执行人")
      @TableField("sqzxr")
    private String sqzxr;

      @ApiModelProperty(value = "被执行人")
      @TableField("bzxr")
    private String bzxr;

      @ApiModelProperty(value = "来文日期")
      @TableField("lwrq")
    private Date lwrq;

      @ApiModelProperty(value = "查封备注")
      @TableField("cfbz")
    private String cfbz;

      @ApiModelProperty(value = "解封原因")
      @TableField("jfyy")
    private String jfyy;

      @ApiModelProperty(value = "解封文件")
      @TableField("jfwj")
    private String jfwj;

      @ApiModelProperty(value = "解封文号")
      @TableField("jfwh")
    private String jfwh;

      @ApiModelProperty(value = "解封机关")
      @TableField("jfjg")
    private String jfjg;

      @ApiModelProperty(value = "解封备注")
      @TableField("jfbz")
    private String jfbz;


    @Override
    protected Serializable pkVal() {
          return this.id;
      }

}
