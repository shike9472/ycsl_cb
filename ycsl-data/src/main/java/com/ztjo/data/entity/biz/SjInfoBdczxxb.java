package com.ztjo.data.entity.biz;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ztjo.data.entity.model.BizInfoModel;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * info表 - 不动产幢信息表（不动产首次登记时可以使用该表收录首次登记的相关幢信息）
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("sj_info_bdczxxb")
@ApiModel(value="SjInfoBdczxxb对象", description="info表 - 不动产幢信息表（不动产首次登记时可以使用该表收录首次登记的相关幢信息）")
public class SjInfoBdczxxb extends BizInfoModel<SjInfoBdczxxb> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "主键")
        @TableId("id")
      private Long id;

      @ApiModelProperty(value = "幢房屋代码")
      @TableField("zfwdm")
    private String zfwdm;

      @ApiModelProperty(value = "幢附着物代码")
      @TableField("zfzwdm")
    private String zfzwdm;

      @ApiModelProperty(value = "幢不动产单元号")
      @TableField("zbdcdyh")
    private String zbdcdyh;

      @ApiModelProperty(value = "附着物坐落")
      @TableField("zfwzl")
    private String zfwzl;

      @ApiModelProperty(value = "登记事项")
      @TableField("djsx")
    private String djsx;

      @ApiModelProperty(value = "登记类型")
      @TableField("djlx")
    private String djlx;

      @ApiModelProperty(value = "申请方式")
      @TableField("sqfs")
    private String sqfs;

      @ApiModelProperty(value = "建筑⾯积")
      @TableField("jzmj")
    private BigDecimal jzmj;


    @Override
    protected Serializable pkVal() {
          return this.id;
      }

}
