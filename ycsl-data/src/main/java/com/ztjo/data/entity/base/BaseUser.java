package com.ztjo.data.entity.base;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ztjo.data.entity.model.BaseTenantModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 系统用户表
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("base_user")
@ApiModel(value="BaseUser对象", description="系统用户表")
public class BaseUser extends BaseTenantModel<BaseUser> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "主键")
        @TableId("id")
      private Long id;

      @ApiModelProperty(value = "登录用户名")
      @TableField("username")
    private String username;

      @ApiModelProperty(value = "密码")
      @TableField("password")
    private String password;

      @ApiModelProperty(value = "系统内名称")
      @TableField("name")
    private String name;

      @ApiModelProperty(value = "生日")
      @TableField("birthday")
    private String birthday;

      @ApiModelProperty(value = "地址")
      @TableField("address")
    private String address;

      @ApiModelProperty(value = "移动电话")
      @TableField("mobile_phone")
    private String mobilePhone;

      @ApiModelProperty(value = "联系电话")
      @TableField("tel_phone")
    private String telPhone;

      @ApiModelProperty(value = "电子邮箱")
      @TableField("email")
    private String email;

      @ApiModelProperty(value = "性别")
      @TableField("sex")
    private String sex;

      @ApiModelProperty(value = "类型")
      @TableField("type")
    private String type;

      @ApiModelProperty(value = "状态")
      @TableField("status")
    private String status;

      @ApiModelProperty(value = "描述 - 介绍")
      @TableField("description")
    private String description;

      @ApiModelProperty(value = "扩展字段1")
      @TableField("attr1")
    private String attr1;

      @ApiModelProperty(value = "扩展字段2")
      @TableField("attr2")
    private String attr2;

      @ApiModelProperty(value = "扩展字段3")
      @TableField("attr3")
    private String attr3;

      @ApiModelProperty(value = "扩展字段4")
      @TableField("attr4")
    private String attr4;

      @ApiModelProperty(value = "扩展字段5")
      @TableField("attr5")
    private String attr5;

      @ApiModelProperty(value = "扩展字段6")
      @TableField("attr6")
    private String attr6;

      @ApiModelProperty(value = "扩展字段7")
      @TableField("attr7")
    private String attr7;

      @ApiModelProperty(value = "扩展字段8")
      @TableField("attr8")
    private String attr8;

      @ApiModelProperty(value = "身份证号")
      @TableField("id_card")
    private String idCard;

      @ApiModelProperty(value = "用户来源")
      @TableField("source_org")
    private String sourceOrg;

      @ApiModelProperty(value = "签名文件id")
      @TableField("sign_file_id")
    private Long signFileId;

      @ApiModelProperty(value = "是否删除")
      @TableField("is_deleted")
    private String isDeleted;

      @ApiModelProperty(value = "是否作废")
      @TableField("is_disabled")
    private String isDisabled;

      @ApiModelProperty(value = "归属部门")
      @TableField("depart_id")
    private Long departId;

      @ApiModelProperty(value = "是否超级管理员")
      @TableField("is_super_admin")
    private String isSuperAdmin;

    @ApiModelProperty(value = "归属部门名称")
    @TableField(exist = false)
    private String departName;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
