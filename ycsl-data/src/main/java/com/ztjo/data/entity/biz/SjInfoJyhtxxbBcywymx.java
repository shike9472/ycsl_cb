package com.ztjo.data.entity.biz;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 交易合同的补充与违约明细类数据表
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("sj_info_jyhtxxb_bcywymx")
@ApiModel(value="SjInfoJyhtxxbBcywymx对象", description="交易合同的补充与违约明细类数据表")
public class SjInfoJyhtxxbBcywymx extends Model<SjInfoJyhtxxbBcywymx> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "主键")
        @TableId("id")
      private Long id;

      @ApiModelProperty(value = "关联外键")
      @TableField("info_id")
    private Long infoId;

      @ApiModelProperty(value = "序号")
      @TableField("xh")
    private Integer xh;

      @ApiModelProperty(value = "明细")
      @TableField("mx")
    private String mx;

      @ApiModelProperty(value = "明细类型")
      @TableField("mxlx")
    private String mxlx;

      @ApiModelProperty(value = "扩展字段1")
      @TableField("ext1")
    private String ext1;

      @ApiModelProperty(value = "扩展字段2")
      @TableField("ext2")
    private String ext2;

      @ApiModelProperty(value = "扩展字段3")
      @TableField("ext3")
    private String ext3;


    @Override
    protected Serializable pkVal() {
          return this.id;
      }

}
