package com.ztjo.data.entity.base;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ztjo.data.entity.model.BaseRecordModel2;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 字典类型 - 用于规范固定字典项类型
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("dict_type")
@ApiModel(value="DictType对象", description="字典类型 - 用于规范固定字典项类型")
public class DictType extends BaseRecordModel2<DictType> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "主键")
        @TableId("id")
      private Long id;

      @ApiModelProperty(value = "编码")
      @TableField("code")
    private String code;

      @ApiModelProperty(value = "类型")
      @TableField("type")
    private String type;

      @ApiModelProperty(value = "目录名")
      @TableField("name")
    private String name;

      @ApiModelProperty(value = "父级id")
      @TableField("parent_id")
    private Long parentId;

      @ApiModelProperty(value = "扩展字段1")
      @TableField("attr1")
    private String attr1;

      @ApiModelProperty(value = "扩展字段2")
      @TableField("attr2")
    private String attr2;

      @ApiModelProperty(value = "扩展字段3")
      @TableField("attr3")
    private String attr3;

      @ApiModelProperty(value = "扩展字段4")
      @TableField("attr4")
    private String attr4;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
