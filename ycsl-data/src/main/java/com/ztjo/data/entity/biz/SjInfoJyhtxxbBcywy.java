package com.ztjo.data.entity.biz;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 交易合同的补充与违约类数据表
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("sj_info_jyhtxxb_bcywy")
@ApiModel(value="SjInfoJyhtxxbBcywy对象", description="交易合同的补充与违约类数据表")
public class SjInfoJyhtxxbBcywy extends Model<SjInfoJyhtxxbBcywy> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "主键-和合同的(sj_info_jyhtxxb)主键一致")
        @TableId("info_id")
      private Long infoId;

      @ApiModelProperty(value = "约定条件，（约定的时间期限或其它条件）")
      @TableField("ydtj")
    private String ydtj;

      @ApiModelProperty(value = "应当履行条款")
      @TableField("ydlxtk")
    private String ydlxtk;

      @ApiModelProperty(value = "甲方违约支付期限")
      @TableField("jfwyzfqx")
    private String jfwyzfqx;

      @ApiModelProperty(value = "甲方违约支付定金倍数")
      @TableField("jfwyzfdjbs")
    private String jfwyzfdjbs;

      @ApiModelProperty(value = "双方违约滞纳金份额")
      @TableField("sfwyznjfe")
    private String sfwyznjfe;

      @ApiModelProperty(value = "甲方多卖惩罚退款期限")
      @TableField("jfdmcftkqx")
    private String jfdmcftkqx;

      @ApiModelProperty(value = "甲方多卖惩罚利息支付份额")
      @TableField("jfdmcflxzffe")
    private String jfdmcflxzffe;

      @ApiModelProperty(value = "甲方多卖惩罚违约金支付比例（同已付款金额比例）")
      @TableField("jfdmcfwyjzfbl")
    private String jfdmcfwyjzfbl;


    @Override
    protected Serializable pkVal() {
          return this.infoId;
      }

}
