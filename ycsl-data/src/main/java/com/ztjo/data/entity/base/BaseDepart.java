package com.ztjo.data.entity.base;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ztjo.data.entity.model.BaseTenantModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 部门表 - 用于管理系统内部门
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("base_depart")
@ApiModel(value="BaseDepart对象", description="部门表 - 用于管理系统内部门")
public class BaseDepart extends BaseTenantModel<BaseDepart> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "主键")
        @TableId("id")
      private Long id;

      @ApiModelProperty(value = "组织名称")
      @TableField("name")
    private String name;

      @ApiModelProperty(value = "上级节点")
      @TableField("parent_id")
    private Long parentId;

      @ApiModelProperty(value = "证件类型")
      @TableField("identification_type")
    private String identificationType;

      @ApiModelProperty(value = "证件号码")
      @TableField("identification_no")
    private String identificationNo;

      @ApiModelProperty(value = "区域编码")
      @TableField("area_code")
    private String areaCode;

      @ApiModelProperty(value = "保存路径")
      @TableField("save_path")
    private String savePath;

      @ApiModelProperty(value = "部门空间URL")
      @TableField("depart_control_url")
    private String departControlUrl;

      @ApiModelProperty(value = "编码")
      @TableField("code")
    private String code;

      @ApiModelProperty(value = "路劲")
      @TableField("path")
    private String path;

      @ApiModelProperty(value = "部门类型")
      @TableField("type")
    private String type;

      @ApiModelProperty(value = "机构类型")
      @TableField("depart_type")
    private String departType;

      @ApiModelProperty(value = "扩展字段1")
      @TableField("attr1")
    private String attr1;

      @ApiModelProperty(value = "扩展字段2")
      @TableField("attr2")
    private String attr2;

      @ApiModelProperty(value = "扩展字段3")
      @TableField("attr3")
    private String attr3;

      @ApiModelProperty(value = "扩展字段4")
      @TableField("attr4")
    private String attr4;

      @ApiModelProperty(value = "地址")
      @TableField("dz")
    private String dz;

      @ApiModelProperty(value = "电话")
      @TableField("dh")
    private String dh;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
