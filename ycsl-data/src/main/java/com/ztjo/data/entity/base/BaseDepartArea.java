package com.ztjo.data.entity.base;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 部门区域权限表
 * </p>
 *
 * @author 陈彬
 * @since 2021-10-26
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("base_depart_area")
@ApiModel(value="BaseDepartArea对象", description="部门区域权限表")
public class BaseDepartArea extends Model<BaseDepartArea> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "主键")
        @TableId("id")
      private Long id;

      @ApiModelProperty(value = "部门id")
      @TableField("depart_id")
    private Long departId;

      @ApiModelProperty(value = "区域id")
      @TableField("area_id")
    private Long areaId;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
