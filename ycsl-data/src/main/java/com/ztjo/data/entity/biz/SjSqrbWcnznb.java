package com.ztjo.data.entity.biz;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("sj_sqrb_wcnznb")
@ApiModel(value="SjSqrbWcnznb对象", description="")
public class SjSqrbWcnznb extends Model<SjSqrbWcnznb> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "主键")
        @TableId("id")
      private String id;

      @ApiModelProperty(value = "税务收件录入表关联键")
      @TableField("info_id")
    private String infoId;

      @ApiModelProperty(value = "纳税人表关联键")
      @TableField("nsrxx_id")
    private String nsrxxId;

      @ApiModelProperty(value = "[未成年子女表] - 未成年子女姓名")
      @TableField("wcnznxm")
    private String wcnznxm;

      @ApiModelProperty(value = "[未成年子女表] - 未成年子女证件类型(201 - 身份证/出生证明)")
      @TableField("wcnznzjlx_dm")
    private String wcnznzjlxDm;

      @ApiModelProperty(value = "[未成年子女表] - 未未成年子女证件号码")
      @TableField("wcnznzjhm")
    private String wcnznzjhm;


    @Override
    protected Serializable pkVal() {
          return this.id;
      }

}
