package com.ztjo.data.entity.base;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 岗位 - 用户设置表
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("base_position_user")
@ApiModel(value="BasePositionUser对象", description="岗位 - 用户设置表")
public class BasePositionUser extends Model<BasePositionUser> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "主键")
        @TableId("id")
      private Long id;

      @ApiModelProperty(value = "岗位id")
      @TableField("position_id")
    private Long positionId;

      @ApiModelProperty(value = "用户id")
      @TableField("user_id")
    private Long userId;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
