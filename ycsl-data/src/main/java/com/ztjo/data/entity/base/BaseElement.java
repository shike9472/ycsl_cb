package com.ztjo.data.entity.base;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ztjo.data.entity.model.BaseRecordModel1;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 组件资源表 - 保存uri或button资源的表
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("base_element")
@ApiModel(value="BaseElement对象", description="组件资源表 - 保存uri或button资源的表")
public class BaseElement extends BaseRecordModel1<BaseElement> {

    private static final long serialVersionUID=1L;

      @TableId("id")
      private Long id;

      @ApiModelProperty(value = "资源编码")
      @TableField("code")
    private String code;

      @ApiModelProperty(value = "资源类型(" +
              "button-按钮/" +
              "api-无按钮接口/" +
              "function-功能性资源（比如登录功能抽象为一个资源后，菜单加载接口是否可以使用就要受这个登录资源是否授权的控制）)")
      @TableField("type")
    private String type;

      @ApiModelProperty(value = "资源名称")
      @TableField("name")
    private String name;

      @ApiModelProperty(value = "资源路径")
      @TableField("uri")
    private String uri;

      @ApiModelProperty(value = "资源关联菜单")
      @TableField("menu_id")
    private Long menuId;

      @ApiModelProperty(value = "父组件id")
      @TableField("parent_id")
    private Long parentId;

      @ApiModelProperty(value = "资源树状检索路径")
      @TableField("path")
    private String path;

      @ApiModelProperty(value = "资源请求类型")
      @TableField("method")
    private String method;

      @ApiModelProperty(value = "描述")
      @TableField("description")
    private String description;

      @ApiModelProperty(value = "扩展字段1")
      @TableField("attr1")
    private String attr1;

      @ApiModelProperty(value = "扩展字段2")
      @TableField("attr2")
    private String attr2;

      @ApiModelProperty(value = "扩展字段3")
      @TableField("attr3")
    private String attr3;

      @ApiModelProperty(value = "扩展字段4")
      @TableField("attr4")
    private String attr4;

      @ApiModelProperty(value = "扩展字段5")
      @TableField("attr5")
    private String attr5;

      @ApiModelProperty(value = "扩展字段6")
      @TableField("attr6")
    private String attr6;

      @ApiModelProperty(value = "扩展字段7")
      @TableField("attr7")
    private String attr7;

      @ApiModelProperty(value = "扩展字段8")
      @TableField("attr8")
    private String attr8;

    @ApiModelProperty(value = "菜单名称")
    @TableField(exist = false)
    private String menuName;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
