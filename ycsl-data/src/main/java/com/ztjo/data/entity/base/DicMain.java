package com.ztjo.data.entity.base;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 字典-字典主项表（目录和字典实例）
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("dic_main")
@ApiModel(value="DicMain对象", description="字典-字典主项表（目录和字典实例）")
public class DicMain extends Model<DicMain> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "主键")
        @TableId("id")
      private Long id;

      @ApiModelProperty(value = "字典编码")
      @TableField("dic_code")
    private String dicCode;

      @ApiModelProperty(value = "字典名称")
      @TableField("dic_name")
    private String dicName;

      @ApiModelProperty(value = "字典类别")
      @TableField("dic_type")
    private String dicType;

      @ApiModelProperty(value = "字典类别名称")
      @TableField("dic_type_name")
    private String dicTypeName;

      @ApiModelProperty(value = "字典状态（现势-1/历史-0）")
      @TableField("dic_state")
    private String dicState;

      @ApiModelProperty(value = "描述")
      @TableField("dic_note")
    private String dicNote;

      @ApiModelProperty(value = "SID")
      @TableField("sid")
    private Long sid;

      @ApiModelProperty(value = "父级目录id")
      @TableField("parent_id")
    private Long parentId;

      @ApiModelProperty(value = "上一版本id")
      @TableField("last_version")
    private Long lastVersion;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
