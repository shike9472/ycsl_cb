package com.ztjo.data.entity.model;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 陈彬
 * @version 2021/12/13
 * description：
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value="租户实体模型", description="部门-用户管理相关实体类定义时使用")
public class BaseTenantModel<T extends BaseRecordModel2<?>> extends BaseRecordModel2<T> {
    @ApiModelProperty(value = "所属租户")
    @TableField("tenant_id")
    private Long tenantId;
}
