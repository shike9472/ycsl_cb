package com.ztjo.data.entity.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author 陈彬
 * @version 2021/12/16
 * description：基础模型 - 带操作记录字段模型2
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "通用型设置记录模型2", description =
        "模型定义表固有字段 - [crt_time, crt_user_id, crt_user_name, upd_time, upd_user_id, upd_user_name]")
public class BaseRecordModel2 <T extends Model<?>> extends Model<T>{

    @ApiModelProperty(value = "创建时间")
    @TableField("crt_time")
    private Date crtTime;

    @ApiModelProperty(value = "创建者id")
    @TableField("crt_user_id")
    private Long crtUserId;

    @ApiModelProperty(value = "创建用户名")
    @TableField("crt_user_name")
    private String crtUserName;

    @ApiModelProperty(value = "更新时间")
    @TableField("upd_time")
    private Date updTime;

    @ApiModelProperty(value = "更新用户id")
    @TableField("upd_user_id")
    private Long updUserId;

    @ApiModelProperty(value = "更新用户名")
    @TableField("upd_user_name")
    private String updUserName;
}
