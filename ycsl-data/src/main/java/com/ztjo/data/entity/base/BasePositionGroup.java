package com.ztjo.data.entity.base;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 岗位 - 角色组挂接表
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("base_position_group")
@ApiModel(value="BasePositionGroup对象", description="岗位 - 角色组挂接表")
public class BasePositionGroup extends Model<BasePositionGroup> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "主键")
        @TableId("id")
      private Long id;

      @ApiModelProperty(value = "岗位id")
      @TableField("position_id")
    private Long positionId;

      @ApiModelProperty(value = "岗位组id")
      @TableField("group_id")
    private Long groupId;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
