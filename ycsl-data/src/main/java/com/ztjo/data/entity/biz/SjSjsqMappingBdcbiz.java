package com.ztjo.data.entity.biz;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 收件 - 不动产业务关联表
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("sj_sjsq_mapping_bdcbiz")
@ApiModel(value="SjSjsqMappingBdcbiz对象", description="收件 - 不动产业务关联表")
public class SjSjsqMappingBdcbiz extends Model<SjSjsqMappingBdcbiz> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "主键")
        @TableId("id")
      private Long id;

      @ApiModelProperty(value = "不动产流程的sid")
      @TableField("sid")
    private String sid;

      @ApiModelProperty(value = "不动产流程的sname（流程名称）")
      @TableField("sname")
    private String sname;

      @ApiModelProperty(value = "申请编号（一窗业务主键）")
      @TableField("sqbh")
    private String sqbh;

      @ApiModelProperty(value = "不动产业务类型")
      @TableField("bdcywlx")
    private String bdcywlx;

      @ApiModelProperty(value = "不动产业务号")
      @TableField("bdcywh")
    private String bdcywh;

      @ApiModelProperty(value = "领证方式")
      @TableField("lzfs")
    private String lzfs;

      @ApiModelProperty(value = "领证人姓名")
      @TableField("lzrxm")
    private String lzrxm;

      @ApiModelProperty(value = "领证人电话")
      @TableField("lzrdh")
    private String lzrdh;

      @ApiModelProperty(value = "领证人通讯地址")
      @TableField("lzrtxdz")
    private String lzrtxdz;

      @ApiModelProperty(value = "通知领证地址")
      @TableField("tzlzdz")
    private String tzlzdz;

      @ApiModelProperty(value = "承诺办结日期")
      @TableField("cnbjrq")
    private String cnbjrq;

      @ApiModelProperty(value = "承诺办结期限（单位：工作日）")
      @TableField("cnbjqx")
    private String cnbjqx;

      @ApiModelProperty(value = "邮递方式")
      @TableField("ydfs")
    private String ydfs;

      @ApiModelProperty(value = "邮编")
      @TableField("yb")
    private String yb;

      @ApiModelProperty(value = "省市县设置")
      @TableField("ssx")
    private String ssx;

      @ApiModelProperty(value = "省")
      @TableField("sheng")
    private String sheng;

      @ApiModelProperty(value = "市")
      @TableField("shi")
    private String shi;

      @ApiModelProperty(value = "区县（县级市）")
      @TableField("qx")
    private String qx;

      @ApiModelProperty(value = "详细地址")
      @TableField("xxdz")
    private String xxdz;

      @ApiModelProperty(value = "完整地址")
      @TableField("wzdz")
    private String wzdz;

      @ApiModelProperty(value = "承诺邮递发件日期")
      @TableField("cnydfjrq")
    private String cnydfjrq;

      @ApiModelProperty(value = "承诺邮递发件期限（单位：工作日）")
      @TableField("cnydfjqx")
    private String cnydfjqx;

      @ApiModelProperty(value = "是否打证(为空或为0时打印,为1时不打印")
      @TableField("sfdz")
    private String sfdz;

      @ApiModelProperty(value = "扩展字段2(已启用：存储老网申数据中 - DJDL)")
      @TableField("ext2")
    private String ext2;

      @ApiModelProperty(value = "扩展字段3(已启用：用于标记数据来自老网申)")
      @TableField("ext3")
    private String ext3;

      @ApiModelProperty(value = "预留不动产业务号")
      @TableField("ylbdcywh")
    private String ylbdcywh;


    @Override
    protected Serializable pkVal() {
          return this.id;
      }

}
