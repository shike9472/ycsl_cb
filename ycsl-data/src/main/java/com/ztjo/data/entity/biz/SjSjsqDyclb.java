package com.ztjo.data.entity.biz;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 收件打印材料记录表
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("sj_sjsq_dyclb")
@ApiModel(value="SjSjsqDyclb对象", description="收件打印材料记录表")
public class SjSjsqDyclb extends Model<SjSjsqDyclb> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "主键")
        @TableId("id")
      private Long id;

      @ApiModelProperty(value = "申请编号")
      @TableField("sqbh")
    private String sqbh;

      @ApiModelProperty(value = "附件表主键")
      @TableField("file_id")
    private Long fileId;

      @ApiModelProperty(value = "打印文件类型（申请书/收件单/审批表/简版合同等）")
      @TableField("dywjlx")
    private String dywjlx;

      @ApiModelProperty(value = "自定义标识")
      @TableField("zdybs")
    private String zdybs;

      @ApiModelProperty(value = "自定义标识名称")
      @TableField("zdybsmc")
    private String zdybsmc;


    @Override
    protected Serializable pkVal() {
          return this.id;
      }

}
