package com.ztjo.data.entity.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 陈彬
 * @version 2022/1/11
 * description：申请人数据存储模型
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value="申请人数据存储模型",
        description="定义各类申请人人员数据入库时必然涉及的相关字段")
public class BizSqrModel<T extends Model<?>> extends Model<T> {

    @ApiModelProperty(value = "人员名称")
    @TableField("rymc")
    private String rymc;

    @ApiModelProperty(value = "人员证件类型")
    @TableField("ryzjlx")
    private String ryzjlx;

    @ApiModelProperty(value = "人员证件类型名称")
    @TableField("ryzjlxmc")
    private String ryzjlxmc;

    @ApiModelProperty(value = "人员证件号码")
    @TableField("ryzjhm")
    private String ryzjhm;

    @ApiModelProperty(value = "人员联系电话")
    @TableField("rylxdh")
    private String rylxdh;

    @ApiModelProperty(value = "人员通讯地址")
    @TableField("rytxdz")
    private String rytxdz;

    @ApiModelProperty(value = "状态")
    @TableField("status")
    private String status;

    @ApiModelProperty(value = "扩展字段")
    @TableField("ext1")
    private String ext1;

    @ApiModelProperty(value = "扩展字段")
    @TableField("ext2")
    private String ext2;

    @ApiModelProperty(value = "扩展字段")
    @TableField("ext3")
    private String ext3;
}
