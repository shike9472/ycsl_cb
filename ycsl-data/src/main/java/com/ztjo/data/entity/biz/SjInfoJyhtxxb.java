package com.ztjo.data.entity.biz;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ztjo.data.entity.model.BizInfoModel;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * info - 交易合同信息表
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("sj_info_jyhtxxb")
@ApiModel(value="SjInfoJyhtxxb对象", description="info - 交易合同信息表")
public class SjInfoJyhtxxb extends BizInfoModel<SjInfoJyhtxxb> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "主键")
        @TableId("id")
      private Long id;

      @ApiModelProperty(value = "登记小类(1.0保留字段)")
      @TableField("djxl")
    private String djxl;

      @ApiModelProperty(value = "登记原因(1.0保留字段)")
      @TableField("djyy")
    private String djyy;

      @ApiModelProperty(value = "合同id")
      @TableField("htid")
    private String htid;

      @ApiModelProperty(value = "合同编号")
      @TableField("htbh")
    private String htbh;

      @ApiModelProperty(value = "交易合同备案号")
      @TableField("htbah")
    private String htbah;

      @ApiModelProperty(value = "合同类型（1-商品房/2-存量房）")
      @TableField("htlx")
    private String htlx;

      @ApiModelProperty(value = "合同类型名称（1-商品房/2-存量房）")
      @TableField("htlxmc")
    private String htlxmc;

      @ApiModelProperty(value = "合同备案时间")
      @TableField("htbasj")
    private Date htbasj;

      @ApiModelProperty(value = "合同签订时间")
      @TableField("htqdsj")
    private Date htqdsj;

      @ApiModelProperty(value = "合同金额")
      @TableField("htje")
    private BigDecimal htje;

      @ApiModelProperty(value = "分别持证")
      @TableField("fbcz")
    private String fbcz;

      @ApiModelProperty(value = "资金托管")
      @TableField("zjtg")
    private String zjtg;

      @ApiModelProperty(value = "支付方式")
      @TableField("zffs")
    private String zffs;

      @ApiModelProperty(value = "税费承担方式")
      @TableField("sfcdfs")
    private String sfcdfs;

      @ApiModelProperty(value = "交付模式")
      @TableField("jfms")
    private String jfms;

      @ApiModelProperty(value = "交付天数")
      @TableField("jfts")
    private String jfts;

      @ApiModelProperty(value = "交付日期")
      @TableField("jfrq")
    private Date jfrq;

      @ApiModelProperty(value = "原房产户编码(1.0保留字段)")
      @TableField("yfchbm")
    private String yfchbm;

      @ApiModelProperty(value = "不动产单元号")
      @TableField("bdcdyh")
    private String bdcdyh;

      @ApiModelProperty(value = "房屋代码")
      @TableField("fwdm")
    private String fwdm;

      @ApiModelProperty(value = "合同是否有效")
      @TableField("htsfyx")
    private String htsfyx;

      @ApiModelProperty(value = "原始查询数据 - （保留字段 - 新系统无用）")
      @TableField("data_json")
    private String dataJson;

      @ApiModelProperty(value = "数据获取方式（手输/接口/excel） - （保留字段 - 新系统无用）")
      @TableField("data_come_from_mode")
    private String dataComeFromMode;

      @ApiModelProperty(value = "数据提供单位 - （保留字段 - 新系统无用）")
      @TableField("provide_unit")
    private String provideUnit;

      @ApiModelProperty(value = "契约合同号")
      @TableField("qyhth")
    private String qyhth;

      @ApiModelProperty(value = "审核状态")
      @TableField("shzt")
    private String shzt;


    @Override
    protected Serializable pkVal() {
          return this.id;
      }

}
