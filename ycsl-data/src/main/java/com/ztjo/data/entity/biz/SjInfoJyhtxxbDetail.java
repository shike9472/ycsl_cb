package com.ztjo.data.entity.biz;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 交易合同信息表 - 补充细节数据表
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("sj_info_jyhtxxb_detail")
@ApiModel(value="SjInfoJyhtxxbDetail对象", description="交易合同信息表 - 补充细节数据表")
public class SjInfoJyhtxxbDetail extends Model<SjInfoJyhtxxbDetail> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "对应交易的关联主键(sj_info_jyhtxxb一致)")
        @TableId("info_id")
      private Long infoId;

      @ApiModelProperty(value = "是否包含房产附属设施")
      @TableField("sfbhfcfsss")
    private String sfbhfcfsss;

      @ApiModelProperty(value = "房产附属设施")
      @TableField("fcfsss")
    private String fcfsss;

      @ApiModelProperty(value = "是否出租")
      @TableField("sfcz")
    private String sfcz;

      @ApiModelProperty(value = "出租说明")
      @TableField("czsm")
    private String czsm;

      @ApiModelProperty(value = "资金监管开户行")
      @TableField("zjjgkhh")
    private String zjjgkhh;

      @ApiModelProperty(value = "资金监管账户")
      @TableField("zjjgzh")
    private String zjjgzh;

      @ApiModelProperty(value = "资金监管买方支付内容")
      @TableField("zjjgmfzfnr")
    private String zjjgmfzfnr;

      @ApiModelProperty(value = "全款付款日期")
      @TableField("qkfkrq")
    private String qkfkrq;

      @ApiModelProperty(value = "分期付款日期1")
      @TableField("fqfkrq1")
    private String fqfkrq1;

      @ApiModelProperty(value = "分期付款金额1")
      @TableField("fqfkje1")
    private BigDecimal fqfkje1;

      @ApiModelProperty(value = "分期付款日期2")
      @TableField("fqfkrq2")
    private String fqfkrq2;

      @ApiModelProperty(value = "分期付款金额2")
      @TableField("fqfkje2")
    private BigDecimal fqfkje2;

      @ApiModelProperty(value = "分期付款日期3")
      @TableField("fqfkrq3")
    private String fqfkrq3;

      @ApiModelProperty(value = "分期付款金额3")
      @TableField("fqfkje3")
    private BigDecimal fqfkje3;

      @ApiModelProperty(value = "贷款方式")
      @TableField("dkfs")
    private String dkfs;

      @ApiModelProperty(value = "首付款日期")
      @TableField("sfkrq")
    private String sfkrq;

      @ApiModelProperty(value = "首付款金额")
      @TableField("sfkje")
    private BigDecimal sfkje;

      @ApiModelProperty(value = "贷款申请日期")
      @TableField("dksqrq")
    private String dksqrq;

      @ApiModelProperty(value = "付款内容（支付方式4）")
      @TableField("fknr")
    private String fknr;

      @ApiModelProperty(value = "买方支付内容（支付方式4）")
      @TableField("zfnr_buyer")
    private String zfnrBuyer;

      @ApiModelProperty(value = "定金")
      @TableField("dj")
    private BigDecimal dj;

      @ApiModelProperty(value = "卖方名称")
      @TableField("seller_name")
    private String sellerName;

      @ApiModelProperty(value = "卖方账户")
      @TableField("seller_account")
    private String sellerAccount;

      @ApiModelProperty(value = "买方名称")
      @TableField("buyer_name")
    private String buyerName;

      @ApiModelProperty(value = "买方账户")
      @TableField("buyer_account")
    private String buyerAccount;

      @ApiModelProperty(value = "是否监管定金（0-不监管/1-监管）")
      @TableField("sfjgdj")
    private String sfjgdj;

      @ApiModelProperty(value = "利息归属（0-卖方/1-买方）")
      @TableField("lxgs")
    private String lxgs;


    @Override
    protected Serializable pkVal() {
          return this.infoId;
      }

}
