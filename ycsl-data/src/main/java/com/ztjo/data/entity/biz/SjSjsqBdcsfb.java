package com.ztjo.data.entity.biz;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 不动产业务收费表
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("sj_sjsq_bdcsfb")
@ApiModel(value="SjSjsqBdcsfb对象", description="不动产业务收费表")
public class SjSjsqBdcsfb extends Model<SjSjsqBdcsfb> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "主键")
        @TableId("id")
      private Long id;

      @ApiModelProperty(value = "sj_sjsq_mapping_bdcbiz表主键")
      @TableField("bdc_mapping_id")
    private Long bdcMappingId;

      @ApiModelProperty(value = "申请编号")
      @TableField("sqbh")
    private String sqbh;

      @ApiModelProperty(value = "不动产业务号")
      @TableField("bdcywh")
    private String bdcywh;

      @ApiModelProperty(value = "不动产业务类型id")
      @TableField("sid")
    private String sid;

      @ApiModelProperty(value = "不动产业务类型名称")
      @TableField("sname")
    private String sname;

      @ApiModelProperty(value = "受理人员")
      @TableField("slry")
    private String slry;

      @ApiModelProperty(value = "受理人员名称")
      @TableField("slrymc")
    private String slrymc;

      @ApiModelProperty(value = "创建时间")
      @TableField("cjsj")
    private Date cjsj;

      @ApiModelProperty(value = "缴费编号")
      @TableField("jfbh")
    private String jfbh;

      @ApiModelProperty(value = "缴费科目")
      @TableField("jfkm")
    private String jfkm;

      @ApiModelProperty(value = "缴费说明")
      @TableField("jfsm")
    private String jfsm;

      @ApiModelProperty(value = "缴费状态（0-待缴费/1-待确认/2-已缴费）")
      @TableField("jfzt")
    private String jfzt;

      @ApiModelProperty(value = "缴费状态名称")
      @TableField("jfztmc")
    private String jfztmc;

      @ApiModelProperty(value = "缴费二维码的fileId")
      @TableField("jfewm")
    private String jfewm;

      @ApiModelProperty(value = "收款人")
      @TableField("skr")
    private String skr;

      @ApiModelProperty(value = "收款时间")
      @TableField("sksj")
    private Date sksj;

      @ApiModelProperty(value = "应收金额")
      @TableField("ysje")
    private BigDecimal ysje;

      @ApiModelProperty(value = "实收金额")
      @TableField("ssje")
    private BigDecimal ssje;

      @ApiModelProperty(value = "付费方名称")
      @TableField("fffmc")
    private String fffmc;

      @ApiModelProperty(value = "实际付费人")
      @TableField("sjffr")
    private String sjffr;

      @ApiModelProperty(value = "电话")
      @TableField("dh")
    private String dh;

      @ApiModelProperty(value = "通讯地址")
      @TableField("txdz")
    private String txdz;

      @ApiModelProperty(value = "序号")
      @TableField("xh")
    private Integer xh;

      @ApiModelProperty(value = "登记大类")
      @TableField("djdl")
    private String djdl;


    @Override
    protected Serializable pkVal() {
          return this.id;
      }

}
