package com.ztjo.data.entity.biz;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.ztjo.data.entity.model.BizBaseModel;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("sj_sjsq")
@ApiModel(value="SjSjsq对象", description="")
public class SjSjsq extends BizBaseModel<SjSjsq> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "主键")
        @TableId("id")
      private Long id;

      @ApiModelProperty(value = "申请编号")
      @TableField("sqbh")
    private String sqbh;

      @ApiModelProperty(value = "流程分类（抵押，转移等）")
      @TableField("lcfl")
    private String lcfl;

      @ApiModelProperty(value = "权利操作类型（小类：设立/注销等-有需要需要记录）")
      @TableField("qlczlx")
    private String qlczlx;

      @ApiModelProperty(value = "申请原因（转库时对应登记原因）")
      @TableField("sqyy")
    private String sqyy;

      @ApiModelProperty(value = "不动产坐落")
      @TableField("bdczl")
    private String bdczl;

      @ApiModelProperty(value = "房地/净地/湖泊/林地/海岛等")
      @TableField("bdclx")
    private String bdclx;

      @ApiModelProperty(value = "权利通知人姓名")
      @TableField("qltzrxm")
    private String qltzrxm;

      @ApiModelProperty(value = "权利通知人证件类型")
      @TableField("qltzrzjlx")
    private String qltzrzjlx;

      @ApiModelProperty(value = "权利通知人证件号码")
      @TableField("qltzrzjhm")
    private String qltzrzjhm;

      @ApiModelProperty(value = "权利通知人电话")
      @TableField("qltzrdh")
    private String qltzrdh;

      @ApiModelProperty(value = "权利通知人通讯地址")
      @TableField("qltzrtxdz")
    private String qltzrtxdz;

      @ApiModelProperty(value = "义务通知人名称")
      @TableField("ywtzrxm")
    private String ywtzrxm;

      @ApiModelProperty(value = "义务通知人证件类型")
      @TableField("ywtzrzjlx")
    private String ywtzrzjlx;

      @ApiModelProperty(value = "义务通知人证件号码")
      @TableField("ywtzrzjhm")
    private String ywtzrzjhm;

      @ApiModelProperty(value = "义务通知人电话")
      @TableField("ywtzrdh")
    private String ywtzrdh;

      @ApiModelProperty(value = "义务通知人地址")
      @TableField("ywtzrdz")
    private String ywtzrdz;

      @ApiModelProperty(value = "申请方式(无锡上一代网申ext1-apply.sqfs)")
      @TableField("sqfs")
    private String sqfs;

      @ApiModelProperty(value = "数据来源id")
      @TableField("sjly_id")
    private String sjlyId;

      @ApiModelProperty(value = "数据来源单位")
      @TableField("sjly_org")
    private String sjlyOrg;

      @ApiModelProperty(value = "无锡上一代网申ext2-apply.djsx")
      @TableField("djsx")
    private String djsx;


    @Override
    protected Serializable pkVal() {
          return this.id;
      }

}
