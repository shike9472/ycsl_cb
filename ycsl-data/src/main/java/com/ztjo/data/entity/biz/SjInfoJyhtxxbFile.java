package com.ztjo.data.entity.biz;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 交易合同信息扩展附件数据表
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("sj_info_jyhtxxb_file")
@ApiModel(value="SjInfoJyhtxxbFile对象", description="交易合同信息扩展附件数据表")
public class SjInfoJyhtxxbFile extends Model<SjInfoJyhtxxbFile> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "主键")
        @TableId("id")
      private Long id;

      @ApiModelProperty(value = "服务id")
      @TableField("info_id")
    private Long infoId;

      @ApiModelProperty(value = "附件id")
      @TableField("file_id")
    private Long fileId;

      @ApiModelProperty(value = "文件显示名称")
      @TableField("show_name")
    private String showName;

      @ApiModelProperty(value = "文件扩展名")
      @TableField("file_ext")
    private String fileExt;

      @ApiModelProperty(value = "入库时间")
      @TableField("insert_time")
    private Date insertTime;


    @Override
    protected Serializable pkVal() {
          return this.id;
      }

}
