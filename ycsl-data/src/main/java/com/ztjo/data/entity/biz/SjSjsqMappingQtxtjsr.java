package com.ztjo.data.entity.biz;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 收件 - 第三方系统办件人员关联表
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("sj_sjsq_mapping_qtxtjsr")
@ApiModel(value="SjSjsqMappingQtxtjsr对象", description="收件 - 第三方系统办件人员关联表")
public class SjSjsqMappingQtxtjsr extends Model<SjSjsqMappingQtxtjsr> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "主键")
        @TableId("id")
      private Long id;

      @ApiModelProperty(value = "收件编号")
      @TableField("sqbh")
    private String sqbh;

      @ApiModelProperty(value = "适配系统标识（bdc-不动产/zjj-住建局/swj-税务局）")
      @TableField("spxtbs")
    private String spxtbs;

      @ApiModelProperty(value = "用户标识")
      @TableField("yhbs")
    private String yhbs;

      @ApiModelProperty(value = "用户名")
      @TableField("yhmc")
    private String yhmc;

      @ApiModelProperty(value = "数据生成方式")
      @TableField("sjscfs")
    private String sjscfs;

      @ApiModelProperty(value = "对接系统的全部业务标识(对应不动产使用方式 - sid使用,拼接)")
      @TableField("qbywbs")
    private String qbywbs;

      @ApiModelProperty(value = "对接系统的全部业务名称(对应不动产使用方式 - sname使用,拼接)")
      @TableField("qbywmc")
    private String qbywmc;

      @ApiModelProperty(value = "其它系统收件人员锁定标识(0-否/1-是) - 该标识为1时，其它系统接收任务时将无视指定人员是否签退，指定锁定其为任务接收人")
      @TableField("qtxtsjrysdbs")
    private String qtxtsjrysdbs;


    @Override
    protected Serializable pkVal() {
          return this.id;
      }

}
