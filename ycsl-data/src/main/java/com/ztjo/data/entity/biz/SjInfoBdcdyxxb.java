package com.ztjo.data.entity.biz;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ztjo.data.entity.model.BizInfoModel;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * info表 - 不动产抵押信息表
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("sj_info_bdcdyxxb")
@ApiModel(value="SjInfoBdcdyxxb对象", description="info表 - 不动产抵押信息表")
public class SjInfoBdcdyxxb extends BizInfoModel<SjInfoBdcdyxxb> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "主键")
        @TableId("id")
      private Long id;

      @ApiModelProperty(value = "不动产受理编号")
      @TableField("bdcywh")
    private String bdcywh;

      @ApiModelProperty(value = "不动产坐落")
      @TableField("zl")
    private String zl;

      @ApiModelProperty(value = "抵押证明号")
      @TableField("dyzmh")
    private String dyzmh;

      @ApiModelProperty(value = "登记时间")
      @TableField("djsj")
    private Date djsj;

      @ApiModelProperty(value = "抵押方式")
      @TableField("dyfs")
    private String dyfs;

      @ApiModelProperty(value = "抵押面积")
      @TableField("dymj")
    private BigDecimal dymj;

      @ApiModelProperty(value = "债权数额")
      @TableField("zqje")
    private BigDecimal zqje;

      @ApiModelProperty(value = "抵押金额")
      @TableField("dyje")
    private BigDecimal dyje;

      @ApiModelProperty(value = "评估价值")
      @TableField("pgjz")
    private BigDecimal pgjz;

      @ApiModelProperty(value = "抵押期限")
      @TableField("dyqx")
    private String dyqx;

      @ApiModelProperty(value = "抵押期起")
      @TableField("dyqsrq")
    private Date dyqsrq;

      @ApiModelProperty(value = "抵押期止")
      @TableField("dyjsrq")
    private Date dyjsrq;

      @ApiModelProperty(value = "抵押原因")
      @TableField("dyyy")
    private String dyyy;

      @ApiModelProperty(value = "备注附记")
      @TableField("bz")
    private String bz;

      @ApiModelProperty(value = "其它")
      @TableField("qt")
    private String qt;

      @ApiModelProperty(value = "原始查询数据  - （保留字段 - 新系统无用）")
      @TableField("data_json")
    private String dataJson;

      @ApiModelProperty(value = "数据类型（存量/新增） - （保留字段 - 新系统无用）")
      @TableField("data_type")
    private String dataType;

      @ApiModelProperty(value = "数据提供单位  -（保留字段 - 新系统无用）")
      @TableField("provide_unit")
    private String provideUnit;

      @ApiModelProperty(value = "数据获取方式（手输/接口/excel）")
      @TableField("data_come_from_mode")
    private String dataComeFromMode;

      @ApiModelProperty(value = "权利类型")
      @TableField("qllx")
    private String qllx;

      @ApiModelProperty(value = "其它限制")
      @TableField("qtxz")
    private String qtxz;

      @ApiModelProperty(value = "是否限制转让")
      @TableField("sfxzzr")
    private String sfxzzr;

      @ApiModelProperty(value = "担保范围（抵押范围）")
      @TableField("dbfw")
    private String dbfw;

      @ApiModelProperty(value = "债务人")
      @TableField("zwr")
    private String zwr;

      @ApiModelProperty(value = "债务人证件类型")
      @TableField("zwrzjlx")
    private String zwrzjlx;

      @ApiModelProperty(value = "债务人证件类型名称")
      @TableField("zwrzjlxmc")
    private String zwrzjlxmc;

      @ApiModelProperty(value = "债务人证件号码")
      @TableField("zwrzjhm")
    private String zwrzjhm;

      @ApiModelProperty(value = "债务人联系电话")
      @TableField("zwrlxdh")
    private String zwrlxdh;


    @Override
    protected Serializable pkVal() {
          return this.id;
      }

}
