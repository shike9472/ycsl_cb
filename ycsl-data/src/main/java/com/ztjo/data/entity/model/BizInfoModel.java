package com.ztjo.data.entity.model;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 陈彬
 * @version 2021/12/23
 * description：
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value="业务服务数据存储模型",
        description="定义各服务数据入库时必然涉及的相关字段")
public class BizInfoModel<T extends BizBaseModel<?>> extends BizBaseModel<T> {

    @ApiModelProperty(value = "申请编号")
    @TableField("sqbh")
    private String sqbh;

    @ApiModelProperty(value = "服务标识")
    @TableField("fwbs - service_code")
    private String fwbs;

    @ApiModelProperty(value = "服务数据表标识")
    @TableField("sjbbs - service_data_to")
    private String sjbbs;
}
