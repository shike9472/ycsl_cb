package com.ztjo.data.entity.biz;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ztjo.data.entity.model.BizInfoModel;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * info - 不动产查封信息表（解封时查询使用）
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("sj_info_bdccfxxb")
@ApiModel(value="SjInfoBdccfxxb对象", description="info - 不动产查封信息表（解封时查询使用）")
public class SjInfoBdccfxxb extends BizInfoModel<SjInfoBdccfxxb> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "主键")
        @TableId("id")
      private Long id;

      @ApiModelProperty(value = "查封种类（1/2）")
      @TableField("cfzl")
    private String cfzl;

      @ApiModelProperty(value = "查封种类名称（预查封/查封）")
      @TableField("cfzlmc")
    private String cfzlmc;

      @ApiModelProperty(value = "查封原因")
      @TableField("cfyy")
    private String cfyy;

      @ApiModelProperty(value = "查封范围")
      @TableField("cffw")
    private String cffw;

      @ApiModelProperty(value = "查封类型（查封/轮候查封）")
      @TableField("cflx")
    private String cflx;

      @ApiModelProperty(value = "查封类型名称")
      @TableField("cflxmc")
    private String cflxmc;

      @ApiModelProperty(value = "查封编号")
      @TableField("cfbh")
    private String cfbh;

      @ApiModelProperty(value = "查封文件")
      @TableField("cfwj")
    private String cfwj;

      @ApiModelProperty(value = "查封文号")
      @TableField("cfwh")
    private String cfwh;

      @ApiModelProperty(value = "查封机关")
      @TableField("cfjg")
    private String cfjg;

      @ApiModelProperty(value = "查封期限")
      @TableField("cfqx")
    private String cfqx;

      @ApiModelProperty(value = "查封起始日期")
      @TableField("cfqsrq")
    private Date cfqsrq;

      @ApiModelProperty(value = "查封终止日期")
      @TableField("cfzzrq")
    private Date cfzzrq;

      @ApiModelProperty(value = "查封顺序")
      @TableField("cfsx")
    private Integer cfsx;

      @ApiModelProperty(value = "查封业务号")
      @TableField("cfywh")
    private String cfywh;

      @ApiModelProperty(value = "登记时间")
      @TableField("djsj")
    private Date djsj;

      @ApiModelProperty(value = "查封登记机构")
      @TableField("djjg")
    private String djjg;

      @ApiModelProperty(value = "附记")
      @TableField("fj")
    private String fj;

      @ApiModelProperty(value = "产权证号")
      @TableField("cqzh")
    private String cqzh;

      @ApiModelProperty(value = "申请执行人")
      @TableField("sqzxr")
    private String sqzxr;

      @ApiModelProperty(value = "被执行人")
      @TableField("bzxr")
    private String bzxr;

      @ApiModelProperty(value = "来文日期")
      @TableField("lwrq")
    private Date lwrq;

      @ApiModelProperty(value = "查封备注")
      @TableField("cfbz")
    private String cfbz;

      @ApiModelProperty(value = "解封原因")
      @TableField("jfyy")
    private String jfyy;

      @ApiModelProperty(value = "解封文件")
      @TableField("jfwj")
    private String jfwj;

      @ApiModelProperty(value = "解封文号")
      @TableField("jfwh")
    private String jfwh;

      @ApiModelProperty(value = "解封机关")
      @TableField("jfjg")
    private String jfjg;

      @ApiModelProperty(value = "解封备注")
      @TableField("jfbz")
    private String jfbz;


    @Override
    protected Serializable pkVal() {
          return this.id;
      }

}
