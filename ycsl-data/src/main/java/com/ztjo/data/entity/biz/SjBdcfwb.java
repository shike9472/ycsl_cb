package com.ztjo.data.entity.biz;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("sj_bdcfwb")
@ApiModel(value="SjBdcfwb对象", description="")
public class SjBdcfwb extends Model<SjBdcfwb> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "主键")
        @TableId("id")
      private Long id;

      @ApiModelProperty(value = "房屋统一标识码(老geo图属统一编码)")
      @TableField("tstybm")
    private String tstybm;

      @ApiModelProperty(value = "合同中的幢id")
      @TableField("htzid")
    private String htzid;

      @ApiModelProperty(value = "合同中的户id")
      @TableField("hthid")
    private String hthid;

      @ApiModelProperty(value = "合同中的楼盘编号")
      @TableField("htlpbh")
    private String htlpbh;

      @ApiModelProperty(value = "合同中的房屋编号")
      @TableField("htfwbh")
    private String htfwbh;

      @ApiModelProperty(value = "房屋代码")
      @TableField("fwdm")
    private String fwdm;

      @ApiModelProperty(value = "房屋规划用途")
      @TableField("fwghyt")
    private String fwghyt;

      @ApiModelProperty(value = "坐落信息")
      @TableField("zl")
    private String zl;

      @ApiModelProperty(value = "不动产单元号")
      @TableField("bdcdyh")
    private String bdcdyh;

      @ApiModelProperty(value = "户编号")
      @TableField("hbh")
    private String hbh;

      @ApiModelProperty(value = "幢编号")
      @TableField("zh")
    private String zh;

      @ApiModelProperty(value = "户号")
      @TableField("hh")
    private String hh;

      @ApiModelProperty(value = "房间号")
      @TableField("fjh")
    private String fjh;

      @ApiModelProperty(value = "单元号")
      @TableField("dyh")
    private String dyh;

      @ApiModelProperty(value = "总层数")
      @TableField("zcs")
    private String zcs;

      @ApiModelProperty(value = "所在层")
      @TableField("szc")
    private String szc;

      @ApiModelProperty(value = "项目名称")
      @TableField("xmmc")
    private String xmmc;

      @ApiModelProperty(value = "建筑面积")
      @TableField("jzmj")
    private BigDecimal jzmj;

      @ApiModelProperty(value = "套内建筑面积")
      @TableField("tnjzmj")
    private BigDecimal tnjzmj;

      @ApiModelProperty(value = "分摊建筑面积")
      @TableField("ftjzmj")
    private BigDecimal ftjzmj;

      @ApiModelProperty(value = "建筑名称")
      @TableField("jzmc")
    private String jzmc;

      @ApiModelProperty(value = "不动产当前抵押情况")
      @TableField("bdcdqdyqk")
    private String bdcdqdyqk;

      @ApiModelProperty(value = "不动产当前查封情况")
      @TableField("bdcdqcfqk")
    private String bdcdqcfqk;

      @ApiModelProperty(value = "不动产当前是否存在异议")
      @TableField("sfczyy")
    private String sfczyy;

      @ApiModelProperty(value = "备注信息")
      @TableField("bz")
    private String bz;

      @ApiModelProperty(value = "不动产状态")
      @TableField("status")
    private String status;

      @ApiModelProperty(value = "扩展字段1")
      @TableField("ext1")
    private String ext1;

      @ApiModelProperty(value = "扩展字段2")
      @TableField("ext2")
    private String ext2;

      @ApiModelProperty(value = "扩展字段3")
      @TableField("ext3")
    private String ext3;

      @ApiModelProperty(value = "房屋类型")
      @TableField("fwlx")
    private String fwlx;

      @ApiModelProperty(value = "房屋性质")
      @TableField("fwxz")
    private String fwxz;

      @ApiModelProperty(value = "房屋结构")
      @TableField("fwjg")
    private String fwjg;

      @ApiModelProperty(value = "附着物代码")
      @TableField("fzwdm")
    private String fzwdm;

      @ApiModelProperty(value = "是否预测楼盘数据（0-实测/1-预测）")
      @TableField("sfyc")
    private String sfyc;

      @ApiModelProperty(value = "原不动产单元号")
      @TableField("hisbdcdyh")
    private String hisbdcdyh;


    @Override
    protected Serializable pkVal() {
          return this.id;
      }

}
