package com.ztjo.data.entity.biz;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 业务 - 不动产关联表（关联info表和bdc表的记录）
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("sj_bdcglb")
@ApiModel(value="SjBdcglb对象", description="业务 - 不动产关联表（关联info表和bdc表的记录）")
public class SjBdcglb extends Model<SjBdcglb> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "主键")
        @TableId("id")
      private Long id;

      @ApiModelProperty(value = "申请编号")
      @TableField("sqbh")
    private String sqbh;

      @ApiModelProperty(value = "info表主键")
      @TableField("info_id")
    private Long infoId;

      @ApiModelProperty(value = "不动产表主键（宗地/房地）")
      @TableField("bdc_id")
    private Long bdcId;

      @ApiModelProperty(value = "数据表标识")
      @TableField("sjbbs")
    private String sjbbs;

      @ApiModelProperty(value = "不动产类型")
      @TableField("bdclx")
    private String bdclx;

      @ApiModelProperty(value = "房产标识（Z-房屋主体/Z1-其它户或幢/F-附属设施）")
      @TableField("sslb")
    private String sslb;

      @ApiModelProperty(value = "状态")
      @TableField("status")
    private String status;


    @Override
    protected Serializable pkVal() {
          return this.id;
      }

}
