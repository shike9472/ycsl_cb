package com.ztjo.data.entity.base;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ztjo.data.entity.model.BaseTenantModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 物理岗位表 - 用户人员岗位分类使用
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("base_position")
@ApiModel(value="BasePosition对象", description="物理岗位表 - 用户人员岗位分类使用")
public class BasePosition extends BaseTenantModel<BasePosition> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "主键")
        @TableId("id")
      private Long id;

      @ApiModelProperty(value = "职位")
      @TableField("name")
    private String name;

      @ApiModelProperty(value = "编码")
      @TableField("code")
    private String code;

      @ApiModelProperty(value = "部门ID")
      @TableField("depart_id")
    private Long departId;

      @ApiModelProperty(value = "类型")
      @TableField("type")
    private String type;

      @ApiModelProperty(value = "扩展字段1")
      @TableField("attr1")
    private String attr1;

      @ApiModelProperty(value = "扩展字段2")
      @TableField("attr2")
    private String attr2;

      @ApiModelProperty(value = "扩展字段3")
      @TableField("attr3")
    private String attr3;

      @ApiModelProperty(value = "扩展字段4")
      @TableField("attr4")
    private String attr4;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
