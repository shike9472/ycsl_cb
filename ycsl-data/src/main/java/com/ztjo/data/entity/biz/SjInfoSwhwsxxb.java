package com.ztjo.data.entity.biz;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ztjo.data.entity.model.BizInfoModel;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * info表 - 税务核/完税信息表
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("sj_info_swhwsxxb")
@ApiModel(value="SjInfoSwhwsxxb对象", description="info表 - 税务核/完税信息表")
public class SjInfoSwhwsxxb extends BizInfoModel<SjInfoSwhwsxxb> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "主键")
        @TableId("id")
      private Long id;

      @ApiModelProperty(value = "原始查询数据 - （保留字段 - 新系统无用）")
      @TableField("data_json")
    private String dataJson;

      @ApiModelProperty(value = "数据获取方式（手输/接口/excel） - （保留字段 - 新系统无用）")
      @TableField("data_come_from_mode")
    private String dataComeFromMode;

      @ApiModelProperty(value = "数据提供单位 - （保留字段 - 新系统无用）")
      @TableField("provide_unit")
    private String provideUnit;

      @ApiModelProperty(value = "系统税票号码")
      @TableField("xtsphm")
    private String xtsphm;

    @TableField("zrfcsfbz")
    private String zrfcsfbz;

      @ApiModelProperty(value = "合同编号")
      @TableField("htbh")
    private String htbh;

      @ApiModelProperty(value = "纳税人编号")
      @TableField("nsrsbh")
    private String nsrsbh;

      @ApiModelProperty(value = "纳税人名称")
      @TableField("nsrmc")
    private String nsrmc;

      @ApiModelProperty(value = "电子税票号码")
      @TableField("dzsphm")
    private String dzsphm;

      @ApiModelProperty(value = "凭证种类代码")
      @TableField("pzzl_dm")
    private String pzzlDm;

      @ApiModelProperty(value = "票证子规代码")
      @TableField("pzzg_dm")
    private String pzzgDm;

      @ApiModelProperty(value = "票证号码")
      @TableField("pzhm")
    private String pzhm;

      @ApiModelProperty(value = "税款所属期起")
      @TableField("skssqq")
    private String skssqq;

      @ApiModelProperty(value = "税款所属期止")
      @TableField("skssqz")
    private String skssqz;

      @ApiModelProperty(value = "征收项目")
      @TableField("zsxm_dm")
    private String zsxmDm;

      @ApiModelProperty(value = "征收品目")
      @TableField("zspm_dm")
    private String zspmDm;

      @ApiModelProperty(value = "征收子目")
      @TableField("zszm_dm")
    private String zszmDm;

      @ApiModelProperty(value = "征收项目名称")
      @TableField("zsxm_mc")
    private String zsxmMc;

      @ApiModelProperty(value = "征收品目名称")
      @TableField("zspm_mc")
    private String zspmMc;

      @ApiModelProperty(value = "征收子目名称")
      @TableField("zszm_mc")
    private String zszmMc;

      @ApiModelProperty(value = "计税依据")
      @TableField("jsyj")
    private String jsyj;

      @ApiModelProperty(value = "税率")
      @TableField("sl")
    private String sl;

      @ApiModelProperty(value = "实际缴纳金额")
      @TableField("sjje")
    private BigDecimal sjje;

      @ApiModelProperty(value = "主管税务机关科分局")
      @TableField("zgswskfj_dm")
    private String zgswskfjDm;

      @ApiModelProperty(value = "征收税务机关")
      @TableField("zsswjg_dm")
    private String zsswjgDm;

      @ApiModelProperty(value = "税款所属机关")
      @TableField("skssswjg_dm")
    private String skssswjgDm;

    @TableField("zgswskfj_mc")
    private String zgswskfjMc;

    @TableField("zsswjg_mc")
    private String zsswjgMc;

    @TableField("skssswjg_mc")
    private String skssswjgMc;

      @ApiModelProperty(value = "扣缴日期")
      @TableField("kjrq")
    private String kjrq;

      @ApiModelProperty(value = "备注")
      @TableField("bz")
    private String bz;

      @ApiModelProperty(value = "不动产单元号")
      @TableField("bdcdyh")
    private String bdcdyh;

      @ApiModelProperty(value = "登记注册类型")
      @TableField("djzclx")
    private String djzclx;

      @ApiModelProperty(value = "合同金额")
      @TableField("hjnum")
    private String hjnum;

      @ApiModelProperty(value = "合同金额大写")
      @TableField("hjchn")
    private String hjchn;

      @ApiModelProperty(value = "地址")
      @TableField("dz")
    private String dz;

      @ApiModelProperty(value = "填票人")
      @TableField("tpr")
    private String tpr;

      @ApiModelProperty(value = "base64（电子税票）")
      @TableField("dzsp")
    private String dzsp;


    @Override
    protected Serializable pkVal() {
          return this.id;
      }

}
