package com.ztjo.data.entity.base;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ztjo.data.entity.model.BaseRecordModel2;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 区域表 - 用区域隔离时使用
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("base_area")
@ApiModel(value="BaseArea对象", description="区域表 - 用区域隔离时使用")
public class BaseArea extends BaseRecordModel2<BaseArea> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "主键")
        @TableId("id")
      private Long id;

      @ApiModelProperty(value = "区域码")
      @TableField("code")
    private String code;

      @ApiModelProperty(value = "区域名称")
      @TableField("name")
    private String name;

    @ApiModelProperty(value = "请求方式(http/https)")
    @TableField("middle_req_type")
    private String middleReqType;

    @ApiModelProperty(value = "请求ip")
    @TableField("middle_host")
    private String middleHost;

    @ApiModelProperty(value = "请求port")
    @TableField("middle_port")
    private String middlePort;

    @ApiModelProperty(value = "请求转发(代理)后缀")
    @TableField("middle_suffix")
    private String middleSuffix;

      @ApiModelProperty(value = "扩展字段1")
      @TableField("attr1")
    private String attr1;

      @ApiModelProperty(value = "扩展字段2")
      @TableField("attr2")
    private String attr2;

      @ApiModelProperty(value = "扩展字段3")
      @TableField("attr3")
    private String attr3;

      @ApiModelProperty(value = "扩展字段4")
      @TableField("attr4")
    private String attr4;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
