package com.ztjo.data.entity.biz;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ztjo.data.entity.model.BizInfoModel;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * info表 - 在建工程查封幢信息表（查封/解封均入该表）
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("sj_info_zjgccfzxxb")
@ApiModel(value="SjInfoZjgccfzxxb对象", description="info表 - 在建工程查封幢信息表（查封/解封均入该表）")
public class SjInfoZjgccfzxxb extends BizInfoModel<SjInfoZjgccfzxxb> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "主键")
        @TableId("id")
      private Long id;

      @ApiModelProperty(value = "幢房屋代码")
      @TableField("zfwdm")
    private String zfwdm;

      @ApiModelProperty(value = "幢附着物代码")
      @TableField("zfzwdm")
    private String zfzwdm;

      @ApiModelProperty(value = "幢不动产单元号")
      @TableField("zbdcdyh")
    private String zbdcdyh;

      @ApiModelProperty(value = "登记事项")
      @TableField("djsx")
    private String djsx;

      @ApiModelProperty(value = "幢坐落")
      @TableField("zzl")
    private String zzl;

      @ApiModelProperty(value = "建筑物名称")
      @TableField("jzwmc")
    private String jzwmc;

      @ApiModelProperty(value = "小区名称")
      @TableField("xqmc")
    private String xqmc;

      @ApiModelProperty(value = "项目名称")
      @TableField("xmmc")
    private String xmmc;

      @ApiModelProperty(value = "建筑物结构")
      @TableField("jzwjg")
    private String jzwjg;

      @ApiModelProperty(value = "查封原因")
      @TableField("cfyy")
    private String cfyy;

      @ApiModelProperty(value = "查封编号")
      @TableField("cfbh")
    private String cfbh;

      @ApiModelProperty(value = "查封文件")
      @TableField("cfwj")
    private String cfwj;

      @ApiModelProperty(value = "查封文号")
      @TableField("cfwh")
    private String cfwh;

      @ApiModelProperty(value = "查封机关")
      @TableField("cfjg")
    private String cfjg;

      @ApiModelProperty(value = "查封期限")
      @TableField("cfqx")
    private String cfqx;

      @ApiModelProperty(value = "查封起始日期")
      @TableField("cfqsrq")
    private Date cfqsrq;

      @ApiModelProperty(value = "查封终止日期")
      @TableField("cfzzrq")
    private Date cfzzrq;

      @ApiModelProperty(value = "申请执行人")
      @TableField("sqzxr")
    private String sqzxr;

      @ApiModelProperty(value = "被执行人")
      @TableField("bzxr")
    private String bzxr;

      @ApiModelProperty(value = "来文日期")
      @TableField("lwrq")
    private Date lwrq;

      @ApiModelProperty(value = "解封原因")
      @TableField("jfyy")
    private String jfyy;

      @ApiModelProperty(value = "解封文件")
      @TableField("jfwj")
    private String jfwj;

      @ApiModelProperty(value = "解封文号")
      @TableField("jfwh")
    private String jfwh;

      @ApiModelProperty(value = "解封机关")
      @TableField("jfjg")
    private String jfjg;


    @Override
    protected Serializable pkVal() {
          return this.id;
      }

}
