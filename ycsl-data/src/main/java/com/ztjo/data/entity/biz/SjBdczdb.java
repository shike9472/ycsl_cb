package com.ztjo.data.entity.biz;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 不动产 - 宗地信息表
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("sj_bdczdb")
@ApiModel(value="SjBdczdb对象", description="不动产 - 宗地信息表")
public class SjBdczdb extends Model<SjBdczdb> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "主键")
        @TableId("id")
      private Long id;

      @ApiModelProperty(value = "宗地类型")
      @TableField("zdlx")
    private String zdlx;

      @ApiModelProperty(value = "宗地统一编码(老geo图属统一编码)")
      @TableField("zdtybm")
    private String zdtybm;

      @ApiModelProperty(value = "不动产单元号")
      @TableField("bdcdyh")
    private String bdcdyh;

      @ApiModelProperty(value = "地籍号")
      @TableField("djh")
    private String djh;

      @ApiModelProperty(value = "宗地坐落")
      @TableField("zl")
    private String zl;

      @ApiModelProperty(value = "土地用途")
      @TableField("tdyt")
    private String tdyt;

      @ApiModelProperty(value = "共有土地面积")
      @TableField("gytdmj")
    private BigDecimal gytdmj;

      @ApiModelProperty(value = "独用土地面积")
      @TableField("dytdmj")
    private BigDecimal dytdmj;

      @ApiModelProperty(value = "分摊土地面积")
      @TableField("fttdmj")
    private BigDecimal fttdmj;

      @ApiModelProperty(value = "不动产当前抵押情况")
      @TableField("dqdyqk")
    private String dqdyqk;

      @ApiModelProperty(value = "不动产当前查封情况")
      @TableField("dqcfqk")
    private String dqcfqk;

      @ApiModelProperty(value = "不动产当前是否存在异议")
      @TableField("sfczyy")
    private String sfczyy;

      @ApiModelProperty(value = "备注信息")
      @TableField("bz")
    private String bz;

      @ApiModelProperty(value = "不动产状态")
      @TableField("status")
    private String status;

      @ApiModelProperty(value = "扩展字段1")
      @TableField("ext1")
    private String ext1;

      @ApiModelProperty(value = "扩展字段2")
      @TableField("ext2")
    private String ext2;

      @ApiModelProperty(value = "扩展字段3")
      @TableField("ext3")
    private String ext3;

      @ApiModelProperty(value = "原不动产单元号")
      @TableField("hisbdcdyh")
    private String hisbdcdyh;


    @Override
    protected Serializable pkVal() {
          return this.id;
      }

}
