package com.ztjo.data.entity.biz;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ztjo.data.entity.model.BizInfoModel;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * info - 抵押合同信息表
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("sj_info_dyhtxxb")
@ApiModel(value="SjInfoDyhtxxb对象", description="info - 抵押合同信息表")
public class SjInfoDyhtxxb extends BizInfoModel<SjInfoDyhtxxb> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "主键")
        @TableId("id")
      private Long id;

      @ApiModelProperty(value = "登记小类(1.0保留字段)")
      @TableField("djxl")
    private String djxl;

      @ApiModelProperty(value = "登记原因(1.0保留字段)")
      @TableField("djyy")
    private String djyy;

      @ApiModelProperty(value = "抵押合同编号")
      @TableField("dyhtbh")
    private String dyhtbh;

      @ApiModelProperty(value = "抵押方式")
      @TableField("dyfs")
    private String dyfs;

      @ApiModelProperty(value = "抵押面积")
      @TableField("dymj")
    private BigDecimal dymj;

      @ApiModelProperty(value = "被担保主债权数额")
      @TableField("bdbzzqse")
    private BigDecimal bdbzzqse;

      @ApiModelProperty(value = "抵押金额")
      @TableField("dyje")
    private BigDecimal dyje;

      @ApiModelProperty(value = "评估价值")
      @TableField("pgje")
    private BigDecimal pgje;

      @ApiModelProperty(value = "抵押期限")
      @TableField("dyqx")
    private String dyqx;

      @ApiModelProperty(value = "抵押期起")
      @TableField("dyqq")
    private Date dyqq;

      @ApiModelProperty(value = "抵押期止")
      @TableField("dyqz")
    private Date dyqz;

      @ApiModelProperty(value = "抵押原因")
      @TableField("dyyy")
    private String dyyy;

      @ApiModelProperty(value = "最高债权确实事实")
      @TableField("zgzqqdss")
    private String zgzqqdss;

      @ApiModelProperty(value = "最高债权数额")
      @TableField("zgzqse")
    private BigDecimal zgzqse;

      @ApiModelProperty(value = "原始查询数据 - （保留字段 - 新系统无用）")
      @TableField("data_json")
    private String dataJson;

      @ApiModelProperty(value = "数据获取方式（手输/接口/excel） - （保留字段 - 新系统无用）")
      @TableField("data_come_from_mode")
    private String dataComeFromMode;

      @ApiModelProperty(value = "业务申请时间")
      @TableField("apply_time")
    private Date applyTime;

      @ApiModelProperty(value = "数据提供单位 - （保留字段 - 新系统无用）")
      @TableField("provide_unit")
    private String provideUnit;

      @ApiModelProperty(value = "备注附记")
      @TableField("bz")
    private String bz;

      @ApiModelProperty(value = "合同签订时间")
      @TableField("htqdsj")
    private Date htqdsj;

      @ApiModelProperty(value = "是否限制转让")
      @TableField("sfxzzr")
    private String sfxzzr;

      @ApiModelProperty(value = "担保范围,空值传/")
      @TableField("dbfw")
    private String dbfw;

      @ApiModelProperty(value = "债务人")
      @TableField("zwr")
    private String zwr;

      @ApiModelProperty(value = "债务人证件类型")
      @TableField("zwrzjlx")
    private String zwrzjlx;

      @ApiModelProperty(value = "债务人证件类型名称")
      @TableField("zwrzjlxmc")
    private String zwrzjlxmc;

      @ApiModelProperty(value = "债务人证件号码")
      @TableField("zwrzjhm")
    private String zwrzjhm;

      @ApiModelProperty(value = "债务人联系电话")
      @TableField("zwrlxdh")
    private String zwrlxdh;


    @Override
    protected Serializable pkVal() {
          return this.id;
      }

}
