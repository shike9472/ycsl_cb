package com.ztjo.data.entity.base;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 字典-字典字项表
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("dic_item")
@ApiModel(value="DicItem对象", description="字典-字典字项表")
public class DicItem extends Model<DicItem> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "id")
        @TableId("id")
      private Long id;

      @ApiModelProperty(value = "字典编码")
      @TableField("dic_code")
    private String dicCode;

      @ApiModelProperty(value = "显示名称")
      @TableField("item_name")
    private String itemName;

      @ApiModelProperty(value = "实际值")
      @TableField("item_value")
    private String itemValue;

      @ApiModelProperty(value = "子项描述")
      @TableField("item_note")
    private String itemNote;

      @ApiModelProperty(value = "序号")
      @TableField("item_order")
    private Integer itemOrder;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
