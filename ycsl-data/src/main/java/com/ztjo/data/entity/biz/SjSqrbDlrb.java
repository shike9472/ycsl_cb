package com.ztjo.data.entity.biz;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.ztjo.data.entity.model.BizSqrModel;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 申请人-代理人表
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-12
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("sj_sqrb_dlrb")
@ApiModel(value="SjSqrbDlrb对象", description="申请人-代理人表")
public class SjSqrbDlrb extends BizSqrModel<SjSqrbDlrb> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "主键")
        @TableId("id")
      private Long id;

      @ApiModelProperty(value = "申请人表id")
      @TableField("sqrb_id")
    private Long sqrbId;


    @Override
    protected Serializable pkVal() {
          return this.id;
      }

}
