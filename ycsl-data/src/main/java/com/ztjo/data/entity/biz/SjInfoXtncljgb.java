package com.ztjo.data.entity.biz;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ztjo.data.entity.model.BizInfoModel;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * info表 - 系统内处理结果表
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("sj_info_xtncljgb")
@ApiModel(value="SjInfoXtncljgb对象", description="info表 - 系统内处理结果表")
public class SjInfoXtncljgb extends BizInfoModel<SjInfoXtncljgb> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "主键")
        @TableId("id")
      private Long id;

      @ApiModelProperty(value = "不动产受理编号（新产生的）")
      @TableField("bdcywh")
    private String bdcywh;

      @ApiModelProperty(value = "合同备案号（备案成功回推时产生）")
      @TableField("htbah")
    private String htbah;

      @ApiModelProperty(value = "合同编号（备案成功回推时产生）")
      @TableField("htbh")
    private String htbh;

      @ApiModelProperty(value = "旧编号 - （保留字段）")
      @TableField("old_number")
    private String oldNumber;

      @ApiModelProperty(value = "处理结果（登记类型+处理结果） - （保留字段）")
      @TableField("handle_result")
    private String handleResult;

      @ApiModelProperty(value = "处理结果说明 - （保留字段）")
      @TableField("handle_text")
    private String handleText;

      @ApiModelProperty(value = "备注附记 - （保留字段）")
      @TableField("remarks")
    private String remarks;

      @ApiModelProperty(value = "原始查询数据 - （保留字段）")
      @TableField("data_json")
    private String dataJson;

      @ApiModelProperty(value = "数据提供单位 - （保留字段）")
      @TableField("provide_unit")
    private String provideUnit;

      @ApiModelProperty(value = "数据获取方式（手输/接口/excel） - （保留字段）")
      @TableField("data_come_from_mode")
    private String dataComeFromMode;


    @Override
    protected Serializable pkVal() {
          return this.id;
      }

}
