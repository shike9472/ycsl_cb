package com.ztjo.data.entity.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author 陈彬
 * @version 2021/12/16
 * description：基础模型 - 带操作记录字段模型1
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "通用型设置记录模型1", description =
        "模型定义表固有字段 - [crt_time, crt_user, crt_name, crt_host, upd_time, upd_user, upd_name, upd_host]")
public class BaseRecordModel1<T extends Model<?>> extends Model<T> {

    @ApiModelProperty(value = "创建时间")
    @TableField("crt_time")
    private Date crtTime;

    @ApiModelProperty(value = "创建者id")
    @TableField("crt_user")
    private Long crtUser;

    @ApiModelProperty(value = "创建用户名")
    @TableField("crt_name")
    private String crtName;

    @ApiModelProperty(value = "创建者ip")
    @TableField("crt_host")
    private String crtHost;

    @ApiModelProperty(value = "更新时间")
    @TableField("upd_time")
    private Date updTime;

    @ApiModelProperty(value = "更新人id（最后）")
    @TableField("upd_user")
    private Long updUser;

    @ApiModelProperty(value = "更新人用户名")
    @TableField("upd_name")
    private String updName;

    @ApiModelProperty(value = "更新来自ip")
    @TableField("upd_host")
    private String updHost;
}
