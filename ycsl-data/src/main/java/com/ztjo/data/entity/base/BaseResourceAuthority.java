package com.ztjo.data.entity.base;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ztjo.data.entity.model.BaseRecordModel1;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 菜单，uri，按钮等资源 - group授权表
 * </p>
 *
 * @author 陈彬
 * @since 2021-09-04
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("base_resource_authority")
@ApiModel(value="BaseResourceAuthority对象", description="菜单，uri，按钮等资源 - group授权表")
public class BaseResourceAuthority extends BaseRecordModel1<BaseResourceAuthority> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "主键")
        @TableId("id")
      private Long id;

      @ApiModelProperty(value = "角色ID（base_group表主键）")
      @TableField("authority_id")
    private Long authorityId;

      @ApiModelProperty(value = "角色类型（role or org）")
      @TableField("authority_type")
    private String authorityType;

      @ApiModelProperty(value = "资源ID（base_element或base_menu表主键）")
      @TableField("resource_id")
    private Long resourceId;

      @ApiModelProperty(value = "资源类型（button or uri or menu）")
      @TableField("resource_type")
    private String resourceType;

      @ApiModelProperty(value = "父级id")
      @TableField("parent_id")
    private Long parentId;

      @ApiModelProperty(value = "path路径")
      @TableField("path")
    private String path;

      @ApiModelProperty(value = "介绍 - 描述")
      @TableField("description")
    private String description;

      @ApiModelProperty(value = "扩展字段1")
      @TableField("attr1")
    private String attr1;

      @ApiModelProperty(value = "扩展字段2")
      @TableField("attr2")
    private String attr2;

      @ApiModelProperty(value = "扩展字段3")
      @TableField("attr3")
    private String attr3;

      @ApiModelProperty(value = "扩展字段4")
      @TableField("attr4")
    private String attr4;

      @ApiModelProperty(value = "扩展字段5")
      @TableField("attr5")
    private String attr5;

      @ApiModelProperty(value = "扩展字段6")
      @TableField("attr6")
    private String attr6;

      @ApiModelProperty(value = "扩展字段7")
      @TableField("attr7")
    private String attr7;

      @ApiModelProperty(value = "扩展字段8")
      @TableField("attr8")
    private String attr8;

      @ApiModelProperty(value = "类型")
      @TableField("type")
    private String type;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
