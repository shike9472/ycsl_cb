package com.ztjo.data.entity.biz;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 陈彬
 * @since 2022-01-13
 */
@Data
  @EqualsAndHashCode(callSuper = true)
  @Accessors(chain = true)
@TableName("sj_info_zjgcdyzxxb_hxxb")
@ApiModel(value="SjInfoZjgcdyzxxbHxxb对象", description="")
public class SjInfoZjgcdyzxxbHxxb extends Model<SjInfoZjgcdyzxxbHxxb> {

    private static final long serialVersionUID=1L;

      @ApiModelProperty(value = "主键")
        @TableId("id")
      private Long id;

      @ApiModelProperty(value = "sj_info_zjgcdyzxxb表主键")
      @TableField("info_id")
    private Long infoId;

      @ApiModelProperty(value = "不动产业务号")
      @TableField("bdcywh")
    private String bdcywh;

      @ApiModelProperty(value = "不动产在建工程抵押证明号")
      @TableField("bdczmh")
    private String bdczmh;

      @ApiModelProperty(value = "抵押物⾯积")
      @TableField("txmj")
    private BigDecimal txmj;

      @ApiModelProperty(value = "评估⾦额")
      @TableField("pgje")
    private BigDecimal pgje;

      @ApiModelProperty(value = "抵押⾦额")
      @TableField("dyje")
    private BigDecimal dyje;

      @ApiModelProperty(value = "担保范围")
      @TableField("dbfw")
    private String dbfw;

      @ApiModelProperty(value = "签订⽇期")
      @TableField("qdrq")
    private String qdrq;

      @ApiModelProperty(value = "币种")
      @TableField("currency")
    private String currency;

      @ApiModelProperty(value = "最⾼额确定事实")
      @TableField("zgeqdss")
    private String zgeqdss;

      @ApiModelProperty(value = "债务开始时间")
      @TableField("zwkssj")
    private String zwkssj;

      @ApiModelProperty(value = "债务结束时间")
      @TableField("zwjssj")
    private String zwjssj;

      @ApiModelProperty(value = "不动产登记证明号")
      @TableField("bdcdjzmh")
    private String bdcdjzmh;

      @ApiModelProperty(value = "抵押物价值")
      @TableField("dywjz")
    private BigDecimal dywjz;

      @ApiModelProperty(value = "债权金额")
      @TableField("zqje")
    private BigDecimal zqje;

      @ApiModelProperty(value = "申请信息")
      @TableField("sqxx")
    private String sqxx;

      @ApiModelProperty(value = "房屋代码")
      @TableField("fwdm")
    private String fwdm;

      @ApiModelProperty(value = "附着物代码")
      @TableField("fzwdm")
    private String fzwdm;

      @ApiModelProperty(value = "不动产单元号")
      @TableField("bdcdyh")
    private String bdcdyh;

      @ApiModelProperty(value = "权力性质")
      @TableField("qlxz")
    private String qlxz;

      @ApiModelProperty(value = "规划用途")
      @TableField("ghyt")
    private String ghyt;

      @ApiModelProperty(value = "建筑面积")
      @TableField("jzmj")
    private BigDecimal jzmj;

      @ApiModelProperty(value = "房屋结构")
      @TableField("fwjg")
    private String fwjg;

      @ApiModelProperty(value = "门牌号")
      @TableField("mph")
    private String mph;

      @ApiModelProperty(value = "坐落")
      @TableField("zl")
    private String zl;

      @ApiModelProperty(value = "户室名")
      @TableField("hh")
    private String hh;

      @ApiModelProperty(value = "备注")
      @TableField("remark")
    private String remark;

      @ApiModelProperty(value = "历史不动产单元号")
      @TableField("hisbdcdyh")
    private String hisbdcdyh;


    @Override
    protected Serializable pkVal() {
          return this.id;
      }

}
