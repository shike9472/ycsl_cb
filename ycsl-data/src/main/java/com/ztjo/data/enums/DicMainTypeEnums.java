package com.ztjo.data.enums;

/**
 * @author 陈彬
 * @version 2021/12/17
 * description：字典类型字典
 */
public enum DicMainTypeEnums {
    /**
     * 字典类型 - 是
     */
    SF_S("目录", "list"),
    /**
     * 字典类型 - 否
     */
    SF_F("字典", "item");

    private String msg;
    private String code;

    DicMainTypeEnums(String msg, String code){
        this.msg = msg;
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public String getCode() {
        return code;
    }
}
