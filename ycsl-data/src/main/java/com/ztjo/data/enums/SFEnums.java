package com.ztjo.data.enums;

/**
 * @author 陈彬
 * @version 2021/9/4
 * description：是，否 - 字典
 */
public enum SFEnums {
    /**
     * 是否 - 是
     */
    SF_S("是", "1"),
    /**
     * 是否 - 否
     */
    SF_F("否", "0");

    private String msg;
    private String code;

    SFEnums(String msg, String code){
        this.msg = msg;
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public String getCode() {
        return code;
    }
}
