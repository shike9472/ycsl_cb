package com.ztjo.data.enums;

import static com.ztjo.data.constants.HttpStatus.FORBIDDEN;
import static com.ztjo.data.constants.HttpStatus.UNAUTHORIZED;

/**
 * @author 陈彬
 * @version 2021/9/3
 * description：登陆错误 - 字典
 */
public enum LoginErrorEnums {

    /**
     * token未携带
     */
    NON_LOGIN("未携带通行令牌，禁止访问！", UNAUTHORIZED),
    /**
     * url鉴权未通过
     */
    URL_NON_AUTH("无效的请求，未取得访问资源的权限！", UNAUTHORIZED),
    /**
     * token未在缓存中发现
     */
    TOKEN_NOT_FINDED("页面超时，请重新登录后重试！", FORBIDDEN),
    /**
     * 用户已登出
     */
    TOKEN_USER_ALREADY_LOGOUT("用户已登出！请重新登录后继续", FORBIDDEN),
    /**
     * token无法解析
     */
    TOKEN_CANNOT_ANALYSIS("无效的令牌信息，请重新登录！", FORBIDDEN),
    /**
     * token过期
     */
    TOKEN_EXPIRE("令牌过期，请重新登录！", FORBIDDEN),
    /**
     * 用户不存在
     */
    TOKEN_USER_NOT_EXIST("用户不存在，请检查登录", FORBIDDEN);

    private String msg;
    private int code;

    LoginErrorEnums(String msg, int code){
        this.msg = msg;
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public int getCode() {
        return code;
    }
}
