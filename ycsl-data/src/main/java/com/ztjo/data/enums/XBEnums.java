package com.ztjo.data.enums;

/**
 * @author 陈彬
 * @version 2021/9/4
 * description：性别 - 字典
 */
public enum XBEnums {
    /**
     * 性别 - 男
     */
    XB_MAN("男", "1"),
    /**
     * 性别 - 女
     */
    XB_WOMAN("女", "2"),
    /**
     * 性别 - 未知
     */
    XB_UNKNOWN("未知", "0");

    private String msg;
    private String code;

    XBEnums(String msg, String code){
        this.msg = msg;
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public String getCode() {
        return code;
    }
}
