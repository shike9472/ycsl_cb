package com.ztjo.data.annotation.authorize;

import java.lang.annotation.*;

/**
 * @author 陈彬
 * @version 2021/10/29
 * description：菜单权限检查注解
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface PreMenu {
    String value() default "";
}
