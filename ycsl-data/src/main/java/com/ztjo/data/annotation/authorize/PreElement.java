package com.ztjo.data.annotation.authorize;

import java.lang.annotation.*;

/**
 * @author 陈彬
 * @version 2021/10/29
 * description：请求资源权限检查注解
 */
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface PreElement {
    String value() default "";
}
