package com.ztjo.data.annotation.authorize;

import java.lang.annotation.*;

/**
 * @author 陈彬
 * @version 2021/12/13
 * description：菜单检查忽略注解
 */
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface PreMenuIgnore {
    String value() default "";
}
