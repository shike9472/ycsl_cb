package com.ztjo.data.annotation.authorize;

import java.lang.annotation.*;

/**
 * @author 陈彬
 * @version 2021/12/13
 * description：租户检查注解
 */
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface PreTenant {
}
