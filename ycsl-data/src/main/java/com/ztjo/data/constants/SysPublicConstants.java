package com.ztjo.data.constants;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @author 陈彬
 * @version 2021/9/4
 * description：系统共用常量
 */
public interface SysPublicConstants {
    /**
     * 用户密码加密对象
     */
    BCryptPasswordEncoder ENCODER = new BCryptPasswordEncoder(12);
    /**
     * 登出标志
     */
    String LOGOUT_MARK_VALUE = "user already logout!";
}
