package com.ztjo.data.constants;

/**
 * @author 陈彬
 * @version 2021/12/13
 * description：
 */
public interface PreCheckConstants {
    String PRE_CHECK_MENU_IGNORE = "isIgnoreMenu";
    String PRE_AREA_SAVE_IN_HEADER = "tarAreaCode";
}
