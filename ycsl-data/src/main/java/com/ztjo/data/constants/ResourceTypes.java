package com.ztjo.data.constants;

import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import com.ztjo.data.entity.base.BaseElement;
import com.ztjo.data.entity.base.BaseMenu;
import com.ztjo.data.exception.YcslBizException;

/**
 * @author 陈彬
 * @version 2021/10/26
 * description：资源类型常量
 */
public interface ResourceTypes {
    /** 菜单-页 */
    String RESOURCE_TYPE_MENU = "menu";
    /** 菜单-目录 */
    String RESOURCE_TYPE_DIRT = "dirt";
    /** 资源-按钮 */
    String RESOURCE_TYPE_BUTTON = "button";
    /** 资源-路径(菜单内) */
    String RESOURCE_TYPE_MENU_URI = "api";
    /** 资源-路径(系统内) - [服务查询，签收，保存，提交，回退，挂起] 等功能性接口可以抽象为独立URI资源 */
    String RESOURCE_TYPE_FUNCTION = "function";

    /** 菜单类型集合 */
    String[] menuTypes = {RESOURCE_TYPE_MENU, RESOURCE_TYPE_DIRT};

    /** 菜单内资源类型集合 */
    String[] elementInMenuTypes = {RESOURCE_TYPE_BUTTON, RESOURCE_TYPE_MENU_URI};

    /** 全体资源类型集合 */
    String[] elementTypes = {RESOURCE_TYPE_BUTTON, RESOURCE_TYPE_MENU_URI, RESOURCE_TYPE_FUNCTION};
}
