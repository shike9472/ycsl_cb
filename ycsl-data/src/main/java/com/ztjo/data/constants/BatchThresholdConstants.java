package com.ztjo.data.constants;

/**
 * @author 陈彬
 * @version 2021/7/29
 * description：批量操作的阈值设置
 */
public class BatchThresholdConstants {
    /**
     * 批量启动阈值，
     *      1. 超过该设定值时，查询使用in
     *      2. 低于该设定值时，遍历条件，取每次的查询结果add进入结果集中
     */
    public static final int BATCH_START_THRESHOLD = 5;
    /**
     * 并行流单个流长度切换阈值（I级），在这个类场景下：
     *      1. 查询传入条件集合长度 超过该设定值，低于II级，并行流截取长度直接使用该值
     *      2. 查询传入条件集合长度 低于该设定值，并行流截取长度使用100
     */
    public static final int SINGLE_LENGTH_CHANGE_THRESHOLD_I = 200;
    /**
     * 并行流单个流长度切换阈值（II级），在这个类场景下：
     *      1. 查询传入条件集合长度 超过该设定值，并行流截取长度使用1000
     */
    public static final int SINGLE_LENGTH_CHANGE_THRESHOLD_II = 2000;

    public static int defaultPresetLength(int listLength){
        //将预设阈值I与阈值II传入presetLength方法获取期待的预设长度值
        return presetLength(
                listLength,SINGLE_LENGTH_CHANGE_THRESHOLD_I,SINGLE_LENGTH_CHANGE_THRESHOLD_II);
    }

    /**
     * 描述：计算异步分流预设长度
     * 作者：陈彬
     * 日期：2021/1/29
     * 参数：[listLength -- 集合长度, thresholdI -- 一级阈值设置, thresholdII -- 二级阈值设置]
     * 返回：int
     * 更新记录：更新人：{}，更新日期：{}
     */
    public static int presetLength(int listLength,int thresholdI,Integer thresholdII){
        //声明默认预设长度为一级阈值
        int presetLength = thresholdI;
        //未给出二级阈值的，计算一级阈值的5倍充作二级阈值
        if(thresholdII==null){
            thresholdII = 5 * thresholdI;
        }
        //集合长度值大于二级阈值，直接启动最大分流长度（SQL中IN条件的最大传入数据量）
        if(listLength>thresholdII){
            presetLength = 1000;
        }
        //集合长度值小于一级阈值，启动最小分流长度（相对优化解）
        if(listLength<thresholdI){
            presetLength = 100;
        }
        //返回处理得到的预设分流长度值
        return presetLength;
    }
}
