package com.ztjo.data.constants;

/**
 * @author 陈彬
 * @version 2021/10/27
 * description：特殊权限角色组常量
 */
public interface SpecialGroupConstants {
    /** 超级管理员 - 系统给定all-hold角色 */
    String GROUP_ALL_HOLD = "GROUP_ALL_HOLD";
}
