package com.ztjo.data.constants;

/**
 * @author 陈彬
 * @version 2021/12/23
 * description：
 */
public interface PackageConstants {
    String SYS_ENTITY_MODEL_PACKAGE = "com.ztjo.data.entity.model";
}
