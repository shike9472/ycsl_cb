package com.ztjo.data.constants;

/**
 * @author 陈彬
 * @version 2021/9/4
 * description：角色组类型常量
 */
public interface GroupTypes {
    /** 角色组 */
    String GROUP_ROLE = "role";
    /** 岗位组 */
    String GROUP_ORG = "org";
}
