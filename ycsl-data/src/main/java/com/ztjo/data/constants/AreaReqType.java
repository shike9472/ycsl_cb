package com.ztjo.data.constants;

/**
 * @author 陈彬
 * @version 2021/12/13
 * description：
 */
public interface AreaReqType {
    String AREAS_MIDDLE_REQUEST_TYPE_HTTP = "http";
    String AREAS_MIDDLE_REQUEST_TYPE_HTTPS = "https";
}
