package com.ztjo.data.constants;

/**
 * @author 陈彬
 * @version 2021/9/4
 * description：redis存储数据头前缀
 */
public interface RedisKeyHeaders {
    /**
     * 登录用户token，redis存储key
     */
    String LOGIN_TOKEN_HEADER = "user_login_token";
    /**
     * 登录用户细节数据，redis存储key，FIXME 已选择不用
     */
    String LOGIN_USER_DETAIL_HEADER = "user_login_detail";
    /**
     * 用户角色组redis存储key
     */
    String CACHE_VALUE_USER_GROUP_PERMISSION = "permission:group:user";
    /**
     * 部门区域接入授权redis存储key
     */
    String CACHE_VALUE_DEPART_AREA_GRANT = "grant:depart:area";
    /**
     * 全体菜单的redis存储key
     */
    String CACHE_VALUE_MENU_ALL = "menu:all";
    /**
     * 用户menu资源key
     */
    String CACHE_VALUE_MENU_USER = "menu:user";
    /**
     * 全体资源的redis存储key
     */
    String CACHE_VALUE_ELEMENT_ALL = "element:all";
    /**
     * 用户element资源key
     */
    String CACHE_VALUE_ELEMENT_USER = "element:user";
    /**
     * 全体字典目录
     */
    String CACHE_VALUE_DIC_MAIN_ALL = "dic:main:all";
    /**
     * 按key区分字典项
     */
    String CACHE_VALUE_DIC_ITEM_DICCODE = "dic:item:code";
}
