package com.ztjo.data.constants;

/**
 * @author 陈彬
 * @version 2021/7/16
 * description：数据源key-常量类
 */
public interface DataSourceKeysConstants {
    /**
     * 主体库
     */
    String DATA_SOURCE_KEY_ZTK = "ztk";
    /**
     * 从属库
     */
    String DATA_SOURCE_KEY_CSK = "csk";
}
