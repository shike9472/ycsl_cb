package com.ztjo.data.constants;

/**
 * @author 陈彬
 * @version 2021/9/4
 * description：返回code常量
 */
public interface ZtjoRespCodes {
    /**
     * 成功码
     */
    int SUCCESS = 200;
    /**
     * 失败码
     */
    int FAILED = 500;
}
