package com.ztjo.data.pojo.vo.resource;

import lombok.Data;

import java.io.Serializable;

/**
 * @author 陈彬
 * @version 2021/10/26
 * description：资源基础Vo
 */
@Data
public class ResourceBaseVo implements Serializable {
    /** 资源标识 */
    private String code;
    /** 资源类型（menu, dirt, button, uri） */
    private String type;
    /** 资源名称 */
    private String name;
}
