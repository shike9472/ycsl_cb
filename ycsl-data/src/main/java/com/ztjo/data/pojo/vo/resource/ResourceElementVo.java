package com.ztjo.data.pojo.vo.resource;

import com.ztjo.data.entity.base.BaseElement;
import lombok.Data;

/**
 * @author 陈彬
 * @version 2021/10/26
 * description：按钮资源Vo
 */
@Data
public class ResourceElementVo extends ResourceBaseVo {

    /** uri路径 */
    private String uri;
    /** 请求方式 */
    private String method;
    /** 所属菜单 */
    private String menuId;

    private BaseElement element;
}
