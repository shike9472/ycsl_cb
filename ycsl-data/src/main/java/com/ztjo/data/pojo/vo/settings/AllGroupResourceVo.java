package com.ztjo.data.pojo.vo.settings;

import com.ztjo.data.entity.base.BaseElement;
import com.ztjo.data.pojo.bo.MenuBo;
import com.ztjo.data.pojo.vo.model.TreeEntityVo;
import lombok.Data;

import java.util.List;

/**
 * @author 陈彬
 * @version 2021/12/21
 * description：角色资源pool加载返回vo
 */
@Data
public class AllGroupResourceVo {
    /**
     * 菜单资源树
     */
    private List<TreeEntityVo<MenuBo>> menuResourceTree;
    /**
     * 功能资源集合
     */
    private List<BaseElement> elementResources;
}
