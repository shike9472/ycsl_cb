package com.ztjo.data.pojo.vo.model;

import lombok.Data;

import java.util.List;

/**
 * @author 陈彬
 * @version 2021/12/20
 * description：关系型修改-条件加载返回值
 */
@Data
public class RelationModifyConditionVo<T> {
    /**
     * modify目标id（被授权目标id）
     */
    private Long tarId;
    /**
     * 已经生成挂接关系的授权实体id集合
     */
    private List<Long> alreadys;
    /**
     * 授权数据池
     */
    private T pool;
}
