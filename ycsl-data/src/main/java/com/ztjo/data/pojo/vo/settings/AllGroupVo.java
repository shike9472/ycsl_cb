package com.ztjo.data.pojo.vo.settings;

import com.ztjo.data.pojo.bo.GroupBo;
import com.ztjo.data.pojo.vo.model.TreeEntityVo;
import lombok.Data;

import java.util.List;

/**
 * @author 陈彬
 * @version 2021/12/20
 * description：所有角色vo对象
 */
@Data
public class AllGroupVo {
    /**
     * 用户角色集合
     */
    private List<TreeEntityVo<GroupBo>> userGroups;
    /**
     * 岗位角色集合
     */
    private List<TreeEntityVo<GroupBo>> positionGroups;
}
