package com.ztjo.data.pojo.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author 陈彬
 * @version 2021/12/9
 * description：关系型表更新入参dto
 */
@Data
public class RelationModifyDto<T> {
    /** 目标id */
    @NotNull(message = "更新主id不可为空")
    private Long tarId;
    /** 当前有效实体集合 */
    private List<T> curEntitys;
}
