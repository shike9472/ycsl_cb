package com.ztjo.data.pojo.vo.resource;

import com.ztjo.data.entity.base.BaseMenu;
import lombok.Data;

/**
 * @author 陈彬
 * @version 2021/10/26
 * description：菜单资源Vo
 */
@Data
public class ResourceMenuVo extends ResourceBaseVo{
    /** 组件 */
    private String component;
    /** 路由 */
    private String path;
    /** 父级菜单id */
    private String parentId;
    /** ID */
    private Long id;

    private BaseMenu menu;
}
