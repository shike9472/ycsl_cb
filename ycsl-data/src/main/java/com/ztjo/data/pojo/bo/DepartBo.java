package com.ztjo.data.pojo.bo;

import com.ztjo.data.pojo.api.TreeElement;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author 陈彬
 * @version 2021/12/15
 * description：基础部门Bo类
 */
@Data
public class DepartBo extends TreeElement {
    @ApiModelProperty(value = "组织名称")
    private String name;

    @ApiModelProperty(value = "证件类型")
    private String identificationType;

    @ApiModelProperty(value = "证件号码")
    private String identificationNo;

    @ApiModelProperty(value = "区域编码")
    private String areaCode;

    @ApiModelProperty(value = "保存路径")
    private String savePath;

    @ApiModelProperty(value = "部门空间URL")
    private String departControlUrl;

    @ApiModelProperty(value = "编码")
    private String code;

    @ApiModelProperty(value = "路劲")
    private String path;

    @ApiModelProperty(value = "部门类型")
    private String type;

    @ApiModelProperty(value = "机构类型")
    private String departType;

    @ApiModelProperty(value = "创建人")
    private String crtUserName;

    @ApiModelProperty(value = "创建人ID")
    private Long crtUserId;

    @ApiModelProperty(value = "创建时间")
    private Date crtTime;

    @ApiModelProperty(value = "最后更新人")
    private String updUserName;

    @ApiModelProperty(value = "最后更新人ID")
    private Long updUserId;

    @ApiModelProperty(value = "最后更新时间")
    private Date updTime;

    @ApiModelProperty(value = "扩展字段1")
    private String attr1;

    @ApiModelProperty(value = "扩展字段2")
    private String attr2;

    @ApiModelProperty(value = "扩展字段3")
    private String attr3;

    @ApiModelProperty(value = "扩展字段4")
    private String attr4;

    @ApiModelProperty(value = "地址")
    private String dz;

    @ApiModelProperty(value = "电话")
    private String dh;

    @ApiModelProperty(value = "所属租户")
    private Long tenantId;
}
