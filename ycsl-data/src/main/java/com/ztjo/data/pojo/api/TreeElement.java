package com.ztjo.data.pojo.api;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 陈彬
 * @version 2021/12/15
 * description：基础树形实体模型
 */
@Data
public class TreeElement {
    @ApiModelProperty(value = "id")
    private Long id;

    @ApiModelProperty(value = "上级节点")
    private Long parentId;
}
