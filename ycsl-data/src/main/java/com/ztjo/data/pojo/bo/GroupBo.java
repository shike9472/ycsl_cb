package com.ztjo.data.pojo.bo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.ztjo.data.pojo.api.TreeElement;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 陈彬
 * @version 2021/12/20
 * description：基础角色BO类
 */
@Data
public class GroupBo extends TreeElement {
    @ApiModelProperty(value = "角色编码")
    private String code;

    @ApiModelProperty(value = "角色名称")
    private String name;

    @ApiModelProperty(value = "树状关系（已取消）")
    private String path;

    @ApiModelProperty(value = "类型")
    private String type;

    @ApiModelProperty(value = "角色组类型")
    private String groupType;

    @ApiModelProperty(value = "描述-介绍")
    private String description;

    @ApiModelProperty(value = "扩展字段1")
    private String attr1;

    @ApiModelProperty(value = "扩展字段2")
    private String attr2;

    @ApiModelProperty(value = "扩展字段3")
    private String attr3;

    @ApiModelProperty(value = "扩展字段4")
    private String attr4;

    @ApiModelProperty(value = "扩展字段5")
    private String attr5;

    @ApiModelProperty(value = "扩展字段6")
    private String attr6;

    @ApiModelProperty(value = "扩展字段7")
    private String attr7;

    @ApiModelProperty(value = "扩展字段8")
    private String attr8;
}
