package com.ztjo.data.pojo.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * @author 陈彬
 * @version 2021/12/14
 * description：
 */
@Data
public class UserChangePasswordDto {
    /** 新密码 */
    @NotEmpty(message = "输入新密码不可为空")
    private String newPassword1;
    /** 确认密码 */
    @NotEmpty(message = "输入确认密码不可为空")
    private String newPassword2;
}
