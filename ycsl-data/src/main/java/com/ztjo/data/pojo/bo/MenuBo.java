package com.ztjo.data.pojo.bo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.ztjo.data.entity.base.BaseElement;
import com.ztjo.data.pojo.api.TreeElement;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @author 陈彬
 * @version 2021/12/15
 * description：基础菜单Bo类
 */
@Data
public class MenuBo extends TreeElement {
    @ApiModelProperty(value = "路径编码")
    private String code;

    @ApiModelProperty(value = "标题")
    private String title;

    @ApiModelProperty(value = "路由信息")
    private String path;

    @ApiModelProperty(value = "图标")
    private String icon;

    @ApiModelProperty(value = "类型")
    private String type;

    @ApiModelProperty(value = "排序")
    private Integer orderNum;

    @ApiModelProperty(value = "描述 - 介绍")
    private String description;

    @ApiModelProperty(value = "组件标志")
    private String component;

    @ApiModelProperty(value = "启用禁用")
    private String enabled;

    @ApiModelProperty(value = "创建时间")
    private Date crtTime;

    @ApiModelProperty(value = "创建者id")
    private Long crtUser;

    @ApiModelProperty(value = "创建用户名")
    private String crtName;

    @ApiModelProperty(value = "创建者ip")
    private String crtHost;

    @ApiModelProperty(value = "更新时间")
    private Date updTime;

    @ApiModelProperty(value = "更新人id（最后）")
    private Long updUser;

    @ApiModelProperty(value = "更新人用户名")
    private String updName;

    @ApiModelProperty(value = "更新来自ip")
    private String updHost;

    @ApiModelProperty(value = "扩展字段1")
    private String attr1;

    @ApiModelProperty(value = "扩展字段2")
    private String attr2;

    @ApiModelProperty(value = "扩展字段3")
    private String attr3;

    @ApiModelProperty(value = "扩展字段4")
    private String attr4;

    @ApiModelProperty(value = "扩展字段5")
    private String attr5;

    @ApiModelProperty(value = "扩展字段6")
    private String attr6;

    @ApiModelProperty(value = "扩展字段7")
    private String attr7;

    @ApiModelProperty(value = "扩展字段8")
    private String attr8;

    @ApiModelProperty(value = "菜单内资源")
    private List<BaseElement> elements;
}
