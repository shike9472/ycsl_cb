package com.ztjo.data.pojo.dto;

import com.ztjo.data.pojo.group.LoginWithPwd;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.groups.Default;

/**
 * @author 陈彬
 * @version 2021/7/24
 * description：用户登录使用的实体DTO
 */
@Data
@ApiModel(value = "登录接口入参", description = "登录接口入参")
public class UserLoginDto {
    @NotBlank(groups = {Default.class},message = "输入的用户名为空")
    private String username;
    @NotBlank(groups = {LoginWithPwd.class},message = "输入的用户密码为空")
    private String password;
}
