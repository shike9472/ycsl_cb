package com.ztjo.data.pojo.api;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.io.Serializable;

import static com.ztjo.data.constants.ZtjoRespCodes.FAILED;
import static com.ztjo.data.constants.ZtjoRespCodes.SUCCESS;

/**
 * @author 陈彬
 * @version 2021/9/4
 * description：自定义返回类
 */
@NoArgsConstructor
@AllArgsConstructor
public class ZtjoResponse<T> implements Serializable {
    private static final long serialVersionUID = 1L;
    private int code;
    private T data;
    private String msg;
    private String cause;

    public static <T> ZtjoResponse<T> ok(T data) {
        return ok(data, "success");
    }

    public static <T> ZtjoResponse<T> ok(T data, String msg) {
        return restResult(data, SUCCESS, msg, null);
    }

    public static <T> ZtjoResponse<T> failed(String msg) {
        return failed(FAILED, msg);
    }

    public static <T> ZtjoResponse<T> failed(String msg, String cause) {
        return failed(FAILED, msg, cause);
    }

    public static <T> ZtjoResponse<T> failed(int code, String msg) {
        return restResult(null, code, msg, null);
    }

    public static <T> ZtjoResponse<T> failed(int code, String msg, String cause) {
        return restResult(null, code, msg, cause);
    }

    private static <T> ZtjoResponse<T> restResult(T data, int code, String msg, String cause) {
        ZtjoResponse<T> apiResult = new ZtjoResponse();
        apiResult.setCode(code);
        apiResult.setData(data);
        apiResult.setMsg(msg);
        apiResult.setCause(cause);
        return apiResult;
    }

    public boolean ok() {
        return SUCCESS == this.code;
    }

    public int getCode() {
        return this.code;
    }

    public T getData() {
        return this.data;
    }

    public String getMsg() {
        return this.msg;
    }

    public String getCause() {
        return this.cause;
    }

    public ZtjoResponse<T> setCode(final int code) {
        this.code = code;
        return this;
    }

    public ZtjoResponse<T> setData(final T data) {
        this.data = data;
        return this;
    }

    public ZtjoResponse<T> setMsg(final String msg) {
        this.msg = msg;
        return this;
    }

    public ZtjoResponse<T> setCause(final String cause) {
        this.cause = cause;
        return this;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof ZtjoResponse;
    }

    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof ZtjoResponse)) {
            return false;
        } else {
            ZtjoResponse<?> other = (ZtjoResponse)o;
            if (!other.canEqual(this)) {
                return false;
            } else if (this.getCode() != other.getCode()) {
                return false;
            } else {
                Object this$data = this.getData();
                Object other$data = other.getData();
                if (this$data == null) {
                    if (other$data != null) {
                        return false;
                    }
                } else if (!this$data.equals(other$data)) {
                    return false;
                }

                Object this$msg = this.getMsg();
                Object other$msg = other.getMsg();
                if (this$msg == null) {
                    if (other$msg != null) {
                        return false;
                    }
                } else if (!this$msg.equals(other$msg)) {
                    return false;
                }

                return true;
            }
        }
    }

    @Override
    public int hashCode() {
        boolean PRIME = true;
        int result = 1;
        long $code = this.getCode();
        result = result * 59 + (int)($code >>> 32 ^ $code);
        Object $data = this.getData();
        result = result * 59 + ($data == null ? 43 : $data.hashCode());
        Object $msg = this.getMsg();
        result = result * 59 + ($msg == null ? 43 : $msg.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "ZtjoResponse(code=" + this.getCode() + ", data=" + this.getData() + ", msg=" + this.getMsg() + ")";
    }
}
