package com.ztjo.data.pojo.dto.act;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * @author 陈彬
 * @version 2022/1/7
 * description：任务回退入参
 */
@Data
public class TaskBackOffDto {
    /**
     * 任务id
     */
    @NotEmpty(message = "任务id不可为空！")
    private String taskId;

    /**
     * 目标节点
     */
    @NotEmpty(message = "回退目标节点不可为空！")
    private String tarActivityDefKey;

    /** 提交意见 */
    private String comment;
}
