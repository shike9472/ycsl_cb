package com.ztjo.data.pojo.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 陈彬
 * @version 2021/12/23
 * description：代码动态生成器使用-加载系统支持模型的返回vo
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SupportParamModelVo {
    /** 类名 */
    private String name;
    /** 简单的类名 */
    private String simpleName;
    /** 类标题 */
    private String title;
    /** 类介绍 */
    private String node;
}
