package com.ztjo.data.pojo.bo;

import lombok.Data;

/**
 * @author 陈彬
 * @version 2022/1/12
 * description：
 */
@Data
public class MysqlTablesBo {
    private String name;
}
