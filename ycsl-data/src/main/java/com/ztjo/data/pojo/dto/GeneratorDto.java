package com.ztjo.data.pojo.dto;

import lombok.Data;

import java.util.List;

/**
 * @author 陈彬
 * @version 2021/12/22
 * description：代码生成器入参
 */
@Data
public class GeneratorDto {
    /** 数据库key（多数据源时用于指定选择的数据连接） */
    private String key;
    /** 数据源拥有者标识 - 给空可以默认处置 ORACLE 和 MYSQL两种 */
    private String owner;
    /** 菜单风格（是否集成父controller类<0-否/1-是>） */
    private String menuStyle;
    /** 继承的实体类（给空时默认继承com.baomidou.mybatisplus.extension.activerecord.Model，可以给其它该Model的自定义派生类） */
    private String entityModel;
    /** 本次操作的表名集合 */
    private List<String> tableNames;
    /** 包名赋值 */
    private MVCComponentName mvcComponentName;

    @Data
    public static class MVCComponentName {
        private String entityName;
        private String mapperName;
        private String serviceImplName;
        private String serviceName;
        private String controllerName;
    }
}
