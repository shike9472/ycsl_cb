package com.ztjo.data.pojo.vo.model;

import com.ztjo.data.pojo.api.TreeElement;
import lombok.Data;

import java.util.List;

/**
 * @author 陈彬
 * @version 2021/12/15
 * description：树形构造实体
 */
@Data
public class TreeEntityVo<T extends TreeElement> {
    /** 自身 */
    private T entity;
    /** 节点信息 */
    private List<TreeEntityVo<T>> nodes;
}
