package com.ztjo.data.pojo.dto.act;

import lombok.Data;

/**
 * @author 陈彬
 * @version 2022/1/4
 * description：工作流流程模板创建入参类
 */
@Data
public class ActModelCreateDto {
    /** 模板名称 */
    private String name;
    /** 模板标识 */
    private String key;
    /** 模板介绍 */
    private String description;
}
