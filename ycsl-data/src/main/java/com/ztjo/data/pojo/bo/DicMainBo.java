package com.ztjo.data.pojo.bo;

import com.ztjo.data.pojo.api.TreeElement;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 陈彬
 * @version 2021/12/23
 * description：字典主体过程变量
 */
@Data
public class DicMainBo extends TreeElement {
    @ApiModelProperty(value = "字典编码")
    private String dicCode;

    @ApiModelProperty(value = "字典名称")
    private String dicName;

    @ApiModelProperty(value = "字典类别")
    private String dicType;

    @ApiModelProperty(value = "字典类别名称")
    private String dicTypeName;

    @ApiModelProperty(value = "字典状态（现势-1/历史-0）")
    private String dicState;

    @ApiModelProperty(value = "描述")
    private String dicNote;

    @ApiModelProperty(value = "SID")
    private Long sid;

    @ApiModelProperty(value = "上一版本id")
    private Long lastVersion;
}
