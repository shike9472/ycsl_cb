package com.ztjo.data.pojo.dto.act;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;
import java.util.Map;

/**
 * @author 陈彬
 * @version 2022/1/7
 * description：任务提交入参
 */
@Data
public class TaskSubmitDto {

    /**
     * 任务id
     */
    @NotEmpty(message = "任务id不可为空！")
    private String taskId;

    /** 提交意见 */
    private String comment;

    /** 提交参数 */
    private Map<String, Object> variables;

    /** 下一节点人员设置 */
    private List<nextTaskUser> nextTaskUsers;

    /**
     * 下一节点人员预设类
     */
    @Data
    public static class nextTaskUser {

        /** 下一节点任务标识 */
        private String nextTaskDefKey;

        /** 下一任务审批人 */
        private String nextTaskAssign;

        /** 下一任务候选人集合 */
        private List<String> nextTaskCandidates;

    }
}
