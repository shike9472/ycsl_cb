package com.ztjo.data.exception;

import com.github.ag.core.exception.BaseException;

import static com.ztjo.data.constants.HttpStatus.ERROR;

/**
 * @author 陈彬
 * @version 2021/9/3
 * description：一窗受理 - 业务异常
 */
public class YcslBizException extends BaseException {
    public YcslBizException(String message) {
        super(message, ERROR);
    }
}
