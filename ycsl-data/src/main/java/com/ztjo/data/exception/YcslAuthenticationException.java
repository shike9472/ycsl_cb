package com.ztjo.data.exception;

import org.springframework.security.core.AuthenticationException;

/**
 * @author 陈彬
 * @version 2021/9/25
 * description：一窗受理框架级用户信息异常
 */
public class YcslAuthenticationException extends AuthenticationException {

    private int status = 500;

    public int getStatus() {
        return this.status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public YcslAuthenticationException() {
        super("登录异常！");
    }

    public YcslAuthenticationException(String message, int status) {
        super(message);
        this.status = status;
    }
}
