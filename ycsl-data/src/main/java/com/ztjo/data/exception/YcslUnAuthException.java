package com.ztjo.data.exception;

import java.util.Optional;

import static com.ztjo.data.enums.LoginErrorEnums.URL_NON_AUTH;

/**
 * @author 陈彬
 * @version 2021/9/3
 * description：一窗受理 - 访问未授权资源
 */
public class YcslUnAuthException extends YcslAuthenticationException {
    public YcslUnAuthException(String message) {
        super(Optional.ofNullable(message).orElse(URL_NON_AUTH.getMsg()), URL_NON_AUTH.getCode());
    }
}
