package com.ztjo.data.exception;

import com.github.ag.core.exception.BaseException;

import static com.ztjo.data.constants.HttpStatus.ACT_CLASH_ERROR;

/**
 * @author 陈彬
 * @version 2022/1/10
 * description：
 */
public class YcslActClashException extends BaseException {
    public YcslActClashException(String message) {
        super(message, ACT_CLASH_ERROR);
    }
}
