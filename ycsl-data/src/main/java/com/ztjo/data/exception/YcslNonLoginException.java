package com.ztjo.data.exception;

import static com.ztjo.data.enums.LoginErrorEnums.NON_LOGIN;

/**
 * @author 陈彬
 * @version 2021/9/3
 * description：一窗受理 - 用户未登录
 */
public class YcslNonLoginException extends YcslAuthenticationException {
    public YcslNonLoginException() {
        super(NON_LOGIN.getMsg(), NON_LOGIN.getCode());
    }
}
