package com.ztjo.data.exception;

import com.ztjo.data.enums.LoginErrorEnums;

/**
 * @author 陈彬
 * @version 2021/9/3
 * description：一窗受理 - token超期异常
 */
public class YcslTokenErrorException extends YcslAuthenticationException {
    public YcslTokenErrorException(LoginErrorEnums tokenError) {
        super(tokenError.getMsg(), tokenError.getCode());
    }
}
