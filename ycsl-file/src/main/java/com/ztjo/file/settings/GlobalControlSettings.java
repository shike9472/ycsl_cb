package com.ztjo.file.settings;

import java.util.List;

/**
 * @author 陈彬
 * @version 2021/6/18
 * description：全局控制静态变量类
 */
public class GlobalControlSettings {
    /**
     * ftp使用开闭状态
     */
    public static boolean ftpEnable;
    /**
     * ftp池化使用开闭状态
     */
    public static boolean ftpPoolEnable;
    /**
     * 系统支持的文件类型
     */
    public static List<String> allowTypes;
}
