package com.ztjo.file.settings;

import lombok.Data;

/**
 * @author 陈彬
 * @version 2021/6/18
 * description：ftp连接池设置对象
 */
@Data
public class FtpPoolSettings {
    /**
     * 连接池允许最大连接数
     */
    private Integer maxTotal;
    /**
     * 最大保持空闲连接数，超过时当有新连接被归还时销毁
     */
    private Integer maxIdle;
    /**
     * 最小空闲连接数，低于该空闲连接数，新建连接并加入连接池
     */
    private Integer minIdle;
    /**
     * 连接池连接获取最大等待时长(单位：毫秒)
     */
    private Long maxWaitMillis;
    /**
     * 空闲连接的最小保持时间，空闲检测发现空闲连接超过该设定时，强制从池子中移除(单位：毫秒)
     */
    private Long minEvictableIdleTimeMillis;
    /**
     * 空闲连接的最小保持时间，但执行移除时会检查是否低于最小设定空闲数(单位：毫秒)
     */
    private Long softMinEvictableIdleTimeMillis;
    /**
     * 空闲检测的执行周期(单位：毫秒)
     */
    private Long timeBetweenEvictionRunsMillis;
    /**
     * 一个对象在被borrow之后多少秒未归还则认为是abandon状态
     */
    private Integer removeAbandonedTimeout;
    /**
     * 连接对象被借出时检查是否处于Abandoned状态并移除
     */
    private boolean removeAbandonedOnBorrow;
    /**
     * 连接空闲检测时是否检查有对象是否处于Abandoned状态，有则移除对象
     */
    private boolean removeAbandonedOnMaintenance;
    /**
     * 池化对象创建时是否检查被创建对象有效性
     */
    private boolean testOnCreate;
    /**
     * 池化对象获取时是否检查被获取对象有效性
     */
    private boolean testOnBorrow;
    /**
     * 池化对象归还时是否检查被获取对象有效性
     */
    private boolean testOnReturn;
    /**
     * 在检测空闲对象线程检测到对象空闲且不需要移除时，是否检测对象的有效性
     */
    private boolean testWhileIdle;
}
