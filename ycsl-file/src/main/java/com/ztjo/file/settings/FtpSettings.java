package com.ztjo.file.settings;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author 陈彬
 * @version 2021/6/18
 * description：FTP相关设置
 */
@Data
@Slf4j
@Component
@ConfigurationProperties(prefix = "ftp")
public class FtpSettings {
    private boolean enable = true;
    private boolean usePool;
    private String host;
    private Integer port;
    private String username;
    private String password;
    /**
     * passive-被动/active-主动
     */
    private String transMode;
    private String env;
    /** FTPClient 控制编码设置将在init时直接给定逻辑，这里不再做动态控制 */
    /** private String encoding; */
    private Integer connectTimeout;
    private Integer soTimeOut;
    private Integer dataTimeOut;
    private Integer controlKeepAliveTimeout;
    private Integer controlKeepAliveReplyTimeout;
    private FtpPoolSettings poolSetting;
}
