package com.ztjo.file.factory;

import cn.hutool.core.util.ObjectUtil;
import com.ztjo.file.component.FtpHandleComponent;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @author 陈彬
 * @version 2021/6/19
 * description：ftp客户端资源工厂
 */
@Slf4j
@Component
@ConditionalOnProperty(value = "ftp.usePool", havingValue = "true")
public class FtpClientFactory extends BasePooledObjectFactory<FTPClient> {

    @Autowired
    private FtpHandleComponent ftpHandleComponent;

    public FtpClientFactory(){
        super();
        log.info("扫描装载FTPClient连接工厂！");
    }

    /**
     * 新建对象
     */
    @Override
    public FTPClient create() throws Exception {
        log.info("FTPClient工厂开始 create ftp客户端连接资源!");
        // 调用统一的初始化方法 new 并 return
        FTPClient ftp = ftpHandleComponent.initFtpClient();
        log.info("FTPClient工厂生产完毕ftp -> {}", ftp.hashCode());
        return ftp;
    }

    /**
     * 连接池补充被工厂create出的对象，
     * 缺省方法是makeObject，该方法会关联使用create和wrap方法
     * @param ftpClient
     * @return
     */
    @Override
    public PooledObject<FTPClient> wrap(FTPClient ftpClient) {
        log.info("连接池连接对象工厂（FTPClient Factory）被告知将工厂生产对象补充进入连接池");
        DefaultPooledObject pooledObject = new DefaultPooledObject<FTPClient>(ftpClient);
        log.info("初始化完成连接池对象 -> {}，并装配成功内核连接对象 - FTPClient", pooledObject.hashCode());
        return pooledObject;
    }

    /**
     * 销毁对象
     */
    @Override
    public void destroyObject(PooledObject<FTPClient> p) throws Exception {
        log.info("开始使用工厂销毁池子中 1.多余的空闲对象；" +
                "2. 空闲超时的连接对象；3. 因borrow之后过长时间没归还的连接对象");
        FTPClient ftpClient = p.getObject();
        if (ftpClient != null && ftpClient.isConnected()) {
            ftpClient.changeWorkingDirectory("/");
            ftpClient.logout();
            ftpClient.disconnect();
            super.destroyObject(p);
        }
    }

    /**
     * 验证对象
     */
    @Override
    public boolean validateObject(PooledObject<FTPClient> p) {
        FTPClient ftpClient = p.getObject();
        if(ObjectUtil.isNull(ftpClient)){
            return false;
        }
        boolean connect = false;
        try {
            connect = ftpClient.sendNoOp();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return connect;
    }

    /**
     * No-op.
     *
     * @param p ignored
     */
    @Override
    public void activateObject(PooledObject<FTPClient> p) throws Exception {
        // The default implementation is a no-op.
    }

    /**
     * No-op.
     *
     * @param p ignored
     */
    @Override
    public void passivateObject(PooledObject<FTPClient> p) throws Exception {
        // The default implementation is a no-op.
    }

}
