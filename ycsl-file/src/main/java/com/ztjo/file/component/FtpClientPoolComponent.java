package com.ztjo.file.component;

import com.ztjo.file.factory.FtpClientFactory;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

/**
 * @author 陈彬
 * @version 2021/6/20
 * description：todo 补全行动日志
 */
@Slf4j
@Component
@ConditionalOnProperty(
        // 条件采样位置
        value = "ftp.usePool",
        // 成立条件
        havingValue = "true"
        // 未配置时的默认处理方式(因为本身就是默认的false，这里注释掉即可)
//        ,matchIfMissing = false
)
public class FtpClientPoolComponent {

    @Autowired
    private FtpClientFactory factory;

    @Autowired
    private GenericObjectPool<FTPClient> ftpClientPool;

    public synchronized FTPClient borrowFtpClient() throws Exception {
        log.info("[borrow]here! total client: {}, active: {}, idle: {}",
                ftpClientPool.listAllObjects().size(), ftpClientPool.getNumActive(), ftpClientPool.getNumIdle());
        log.info("开始借出FTPClient连接资源");
        FTPClient ftp = ftpClientPool.borrowObject();
        if (!ftp.sendNoOp()) {
            log.info("被借出对象异常，重新加载ftp连接至连接池");
            // 使池中的对象无效
            ftp.logout();
            ftp.disconnect();
            ftpClientPool.invalidateObject(ftp);
            ftp = factory.create();
            ftpClientPool.addObject();
        }
        log.info("[borrow]here! when we complete borrowing, total client: {}, active: {}, idle: {}",
                ftpClientPool.listAllObjects().size(), ftpClientPool.getNumActive(), ftpClientPool.getNumIdle());
        return ftp;
    }

    public void releaseFtpClient(FTPClient ftp){
        log.info("[release]here! total client: {}, active: {}, idle: {}",
                ftpClientPool.listAllObjects().size(), ftpClientPool.getNumActive(), ftpClientPool.getNumIdle());
        if (ftp != null) {
            ftpClientPool.returnObject(ftp);
        }
        log.info("[release]here! when we complete release action, total client: {}, active: {}, idle: {}",
                ftpClientPool.listAllObjects().size(), ftpClientPool.getNumActive(), ftpClientPool.getNumIdle());
    }
}
