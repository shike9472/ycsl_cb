package com.ztjo.file.component;

import cn.hutool.core.codec.Base64Decoder;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.ztjo.data.exception.YcslBizException;
import com.ztjo.file.pojo.bo.FtpFileDownResultBo;
import com.ztjo.file.settings.FtpSettings;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPClientConfig;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import static com.ztjo.file.constants.SysPlatformConstants.FTP_ENV_UNIX;
import static com.ztjo.file.constants.SysPlatformConstants.FTP_USE_MODE_ACTIVE;
import static com.ztjo.file.settings.GlobalControlSettings.ftpEnable;
import static com.ztjo.file.settings.GlobalControlSettings.ftpPoolEnable;

/**
 * @author 陈彬
 * @version 2021/6/18
 * description：ftp操作组件
 */
@Slf4j
@Component
public class FtpHandleComponent {
    @Autowired
    private FtpSettings ftpSettings;
    @Autowired(required = false)
    private FtpClientPoolComponent ftpClientPoolComponent;

    /** FTP服务端控制字符编码，服务端解析ISO-8859-1字符串即将输出的目标编码 */
    private static String SERVER_CHARSET = "GBK";

    /** FTP协议里面，规定路径与文件名传输编码为iso-8859-1，通道内传输编码 */
    private static String CHANNEL_CHARSET = "ISO-8859-1";

    /**
     * 描述：根据传入的文件base64文本上传附件
     * 作者：陈彬
     * 日期：2021/10/14
     * 参数：[path, fileName, file]
     * 返回：boolean
     * 更新记录：更新人：{}，更新日期：{}
     */
    public boolean uploadFile(String path, String fileName, String file) throws IOException{
        if(StrUtil.isBlank(file)) {
            throw new YcslBizException("要求上传文件为空文件！");
        }
        return uploadFile(path, fileName, Base64Decoder.decode(file));
    }

    /**
     * 描述：根据传入的文件byte数组上传附件
     * 作者：陈彬
     * 日期：2021/10/14
     * 参数：[path, fileName, file]
     * 返回：boolean
     * 更新记录：更新人：{}，更新日期：{}
     */
    public boolean uploadFile(String path, String fileName, byte[] file) throws IOException {
        if(ArrayUtil.isEmpty(file)) {
            throw new YcslBizException("要求上传文件为空文件！");
        }
        return uploadFile(path, fileName, new ByteArrayInputStream(file));
    }

    /**
     * 描述：根据传入的文件字节流上传附件
     * 作者：陈彬
     * 日期：2021/10/14
     * 参数：[path, fileName, file]
     * 返回：boolean
     * 更新记录：更新人：{}，更新日期：{}
     */
    public boolean uploadFile(String remotePath, String fileName, InputStream file) throws IOException {
        // FTP设置是否成功获取
        if(!ftpEnable){
            throw new YcslBizException("当前程序暂未成功接入可用的FTP服务");
        }
        // 返回结果定义
        boolean uploadResult;
        // 获取ftp client对象
        FTPClient ftp = getFtpClient();
        if(ObjectUtil.isNull(ftp)){
            throw new YcslBizException("未成功取得FTP连接客户端资源");
        }
        try {
            log.info("附件上传路径包括：{}，文件名：{}", remotePath, fileName);
            // 预处理路径标准
            if(remotePath.contains("\\")){
                remotePath = remotePath.replaceAll("\\\\","/");
            }
            log.info("处理后path:{},name:{}", remotePath, fileName );

            // 创建并切换文件夹路径
            log.info("创建并切换工作目录！");
            mkDirs(ftp, remotePath);
            log.info("ftp当前工作区：[{}]", ftp.printWorkingDirectory());

            // 检查文件是否存在
            log.info("检查文件名是否重复");
            FTPFile[] fs = ftp.listFiles(new String(fileName.getBytes(SERVER_CHARSET), CHANNEL_CHARSET));
            if (ArrayUtil.isEmpty(fs)) {
                log.info("经查无同名附件存在目录中！");
            } else if (fs.length == 1) {
                log.info("经查存在同名附件存在目录中！采取覆盖上传！");
                ftp.deleteFile(new String(fs[0].getName().getBytes(SERVER_CHARSET), CHANNEL_CHARSET));
            }
            // 上传文件
            log.info("执行文件上传！");
            uploadResult = ftp.storeFile(new String(fileName.getBytes(SERVER_CHARSET), CHANNEL_CHARSET), file);
            log.info("文件上传结束！执行结果 -> {}", uploadResult);
        } catch (Exception e) {
            log.error("附件上传时出现异常-[{}]", e.getMessage(), e);
            uploadResult = false;
        } finally {
            // 关闭输入流
            file.close();
            // 归还ftp
            returnFtpClient(ftp);
        }
        return uploadResult;
    }

    /**
     * 描述：FTP附件下载
     * 作者：chenb
     * 日期：2020/8/13
     * 参数：[key, remotePath, fileName]
     * 返回：FtpFileDownResult
     * 更新记录：更新人：{}，更新日期：{}
     */
    public FtpFileDownResultBo downFile(String remotePath, String fileName) throws IOException {
        // FTP设置是否成功获取
        if(!ftpEnable){
            throw new YcslBizException("当前程序暂未成功接入可用的FTP服务");
        }
        // 声明返回对象
        FtpFileDownResultBo result = new FtpFileDownResultBo();
        // 获取ftp client对象
        FTPClient ftp = getFtpClient();
        if(ObjectUtil.isNull(ftp)){
            throw new YcslBizException("未成功取得FTP连接客户端资源");
        }
        try {
            log.info("附件下载路径包括：{}文件名：{}", remotePath, fileName);
            // 预处理路径标准
            if(remotePath.contains("\\")){
                remotePath = remotePath.replaceAll("\\\\","/");
            }
            log.info("处理后path:{},name:{}", remotePath, fileName );
            // 切换工作目录
            boolean changeDirSuccess = ftp.changeWorkingDirectory(new String(remotePath.getBytes(SERVER_CHARSET), CHANNEL_CHARSET));
            log.info("workDir change successfully or not? -> {}, curent:{}", changeDirSuccess, ftp.printWorkingDirectory());
            // 下载指定文件,写流信息入字节数组
            InputStream is = ftp.retrieveFileStream(new String(fileName.getBytes(SERVER_CHARSET), CHANNEL_CHARSET));
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            IOUtils.copy(is, out);
            byte[] bs = out.toByteArray();
            int length = bs.length;
            log.info("附件字节长度：" + length);
            out.close();
            is.close();
            log.info("流操作完毕");
            // 通知ftpclient流已操作完毕
            ftp.completePendingCommand();
            // 返回成功数据
            return result.success(Integer.toString(length),bs);
        } catch (IOException e) {
            log.error("附件下载时出现异常", e);
            return result.fail("从【"+ftpSettings.getHost()+"】下载文件失败，错误信息："+e.getMessage());
        } finally {
            returnFtpClient(ftp);
        }
    }

    /**
     * 描述：FTP客户端资源初始化方法
     * 作者：陈彬
     * 日期：2021/6/18
     * 参数：[]
     * 返回：org.apache.commons.net.ftp.FTPClient
     * 更新记录：更新人：{}，更新日期：{}
     */
    public FTPClient initFtpClient() throws IOException{
        try {
            FTPClient ftp = new FTPClient();
            // 连接超时设置
            ftp.setConnectTimeout(ftpSettings.getConnectTimeout());
            // 连接ftp
            ftp.connect(ftpSettings.getHost(), ftpSettings.getPort());
            // 登录ftp用户
            ftp.login(ftpSettings.getUsername(), ftpSettings.getPassword());
            // 预连接标识获取
            int reply = ftp.getReplyCode();
            // 预连接标识判断连接结果
            if (!FTPReply.isPositiveCompletion(reply)) {
                ftp.disconnect();
                log.error("FTP客户端资源初始化过程中，验证连接失败，host:" + ftpSettings.getHost() + ", port:" + ftpSettings.getPort());
                throw new RuntimeException("FTP客户端资源初始化过程中，验证连接失败!");
            }
            //设置操作系统 windows or unix 环境相关设置
            if (FTP_ENV_UNIX.equals(ftpSettings.getEnv())) {
                log.info("【注意】FTP使用UNIX环境");
                FTPClientConfig conf = new FTPClientConfig("UNIX");
                ftp.configure(conf);
            }
            log.info("ftp连接验证成功！");
            // 控制字符集设置
            if(FTPReply.isPositiveCompletion(ftp.sendCommand("OPTS UTF8","ON"))){
                log.info("ftp服务端支持\"UTF-8\"的编码格式，即将完成编码设置 -> UTF-8");
                SERVER_CHARSET = "UTF-8";
            } else {
                log.info("ftp服务端不支持\"UTF-8\"的编码格式，即将完成编码设置 -> GBK<本地编码>");
            }
            ftp.setControlEncoding(SERVER_CHARSET);
            // 通道保持超时设置
            ftp.setSoTimeout(ftpSettings.getSoTimeOut());
            // 数据传输超时设置
            ftp.setDataTimeout(ftpSettings.getDataTimeOut());
            // 数据传输过程中控制信道保持发包间隔时间，单位为秒(大文件操作时设置)
            ftp.setControlKeepAliveTimeout(ftpSettings.getControlKeepAliveTimeout());
            // 发包间隔期间控制信道保持的临时性 通道保持超时设置
            ftp.setControlKeepAliveReplyTimeout(ftpSettings.getControlKeepAliveReplyTimeout());
            // 缓冲区大小设置（10M）
            ftp.setBufferSize(1024 * 1024 * 10);
            // 模式选择
            if (FTP_USE_MODE_ACTIVE.equals(ftpSettings.getTransMode())) {
                ftp.enterLocalActiveMode();      //主动模式
            } else {
                ftp.enterLocalPassiveMode();     //被动模式
            }
            // 文件字节传输
            ftp.setFileType(FTPClient.BINARY_FILE_TYPE);
            ftp.setFileTransferMode(FTPClient.BINARY_FILE_TYPE);

            // 切换工作目录至根目录
            ftp.changeWorkingDirectory("/");
            return ftp;
        } catch (IOException e){
            log.error("INIT FTP客户端连接出现IO异常", e);
            throw e;
        }
    }

    /**
     * ------------------------------以下为私有方法------------------------------
     */

    /**
     * 描述：获取FTP客户端资源方法，区分池化和非池化两种方式
     * 作者：陈彬
     * 日期：2021/6/18
     * 参数：[]
     * 返回：org.apache.commons.net.ftp.FTPClient
     * 更新记录：更新人：{}，更新日期：{}
     */
    private FTPClient getFtpClient() throws IOException{
        log.info("借出或新建ftp客户端资源");
        if(ftpPoolEnable){
            // 从ftp客户端连接池中取资源
            try {
                return ftpClientPoolComponent.borrowFtpClient();
            } catch (Exception e) {
                log.error("连接池借FTP客户端资源时异常，", e);
                throw new YcslBizException("连接池中获取FTP客户端资源失败！");
            }
        } else {
            return initFtpClient();
        }
    }

    /**
     * 描述：归还FTP客户端资源方法，区分池化和非池化两种方式
     * 作者：陈彬
     * 日期：2021/6/18
     * 参数：[]
     * 返回：org.apache.commons.net.ftp.FTPClient
     * 更新记录：更新人：{}，更新日期：{}
     */
    private void returnFtpClient(FTPClient ftp){
        log.info("归还或释放ftp客户端资源");
        if(ftpPoolEnable){
            try {
                ftp.changeWorkingDirectory("/");
                // 客户端对象归还方法
                ftpClientPoolComponent.releaseFtpClient(ftp);
            } catch (IOException ioe) {
                log.error("ftp客户端资源归还连接池异常！", ioe);
            }
        } else {
            try {
                if (ftp.isConnected()) {
                    ftp.changeWorkingDirectory("/");
                    ftp.logout();
                    ftp.disconnect();
                }
            } catch (IOException ioe) {
                log.error("ftp客户端资源非池化释放异常！", ioe);
            }
        }
    }

    /**
     * 描述：创建并切换工作区
     * 作者：chenb
     * 日期：2020/8/13
     * 参数：[client, p]
     * 返回：void
     * 更新记录：更新人：{}，更新日期：{}
     */
    private void mkDirs(FTPClient client, String p) throws Exception {
        // 路径为空时做根目录处理
        if (StrUtil.isBlank(p)) {
            return;
        }
        // 路径非空且不为“/”
        if (StringUtils.isNotBlank(p) && !"/".equals(p)) {
            // 规范化处理路径
            if(p.startsWith("/")) {
                p = p.substring(1);
            }
            // 声明即将切换的路径
            String ps = "/";
            String []jps = p.split("/");
            for (int i = 0; i < jps.length; i++) {
                ps += jps[i] + "/";
                log.info("检查创建【" + ps + "】目录，存在则切换目录，不存在则创建然后切换目录");
                if (!isDirExist(client, ps)) {
                    // 创建目录
                    log.info("创建【" + ps + "】目录");
                    boolean creatSucc = client.makeDirectory(new String(ps.getBytes(SERVER_CHARSET), CHANNEL_CHARSET));
                    if(creatSucc) {
                        log.info("[{}]目录创建成功！", ps);
                    } else {
                        log.info("[{}]目录创建失败！", ps);
                    }
                    // 进入创建的目录
                    log.info("切换【" + ps + "】目录");
                    boolean changeSucc = client.changeWorkingDirectory(new String(ps.getBytes(SERVER_CHARSET), CHANNEL_CHARSET));
                }
            }
        }
    }

    /**
     * 描述：判断工作区文件夹是否存在 - （添加了对中文的依赖）
     * 作者：chenb
     * 日期：2020/8/13
     * 参数：[client, dir]
     * 返回：boolean
     * 更新记录：更新人：{}，更新日期：{}
     */
    private static boolean isDirExist(FTPClient client, String dir) {
        try {
            return client.changeWorkingDirectory(new String(dir.getBytes(SERVER_CHARSET), CHANNEL_CHARSET));
        } catch (Exception e) {
            return false;
        }
    }
}
