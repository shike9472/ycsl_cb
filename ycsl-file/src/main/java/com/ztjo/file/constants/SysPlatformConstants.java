package com.ztjo.file.constants;

/**
 * @author 陈彬
 * @version 2021/6/17
 * description：工程基于平台常量
 */
public interface SysPlatformConstants {
    String PLAT_GEO = "geo";
    String PLAT_ZTJO = "ztjo";
    //ftp模式
    String FTP_USE_MODE_ACTIVE = "active";
    String FTP_USE_MODE_PASSIVE = "passive";
    // ftp服务器环境
    String FTP_ENV_WIN = "Win";
    String FTP_ENV_UNIX = "Unix";
}
