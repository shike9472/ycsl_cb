package com.ztjo.file.config;

import cn.hutool.core.util.ObjectUtil;
import com.ztjo.file.factory.FtpClientFactory;
import com.ztjo.file.settings.FtpPoolSettings;
import com.ztjo.file.settings.FtpSettings;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.pool2.impl.AbandonedConfig;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 陈彬
 * @version 2021/6/18
 * description：FTP客户端资源连接池配置类
 */
@Slf4j
@Configuration
//@ConditionalOnClass({FtpSettings.class, FTPClientFactory.class})
@ConditionalOnProperty(value = "ftp.usePool", havingValue = "true")
public class FtpClientPoolConfiguration {
    @Autowired
    private FtpSettings ftpSettings;
    @Autowired
    private FtpClientFactory ftpClientFactory;

    @Bean
    public GenericObjectPool<FTPClient> ftpClientPool(){
        log.info("实例化连接池并交IOC托管，使用相关配置：ftpSettings load success -> {}; factory load success -> {}",
                ObjectUtil.isNotEmpty(ftpSettings), ObjectUtil.isNotNull(ftpClientFactory));
        FtpPoolSettings poolSetting = ftpSettings.getPoolSetting();
        /**
         * 连接池配置
         */
        GenericObjectPoolConfig poolConfig = new GenericObjectPoolConfig();
        poolConfig.setJmxEnabled(false);
        // 最大连接数
        if(ObjectUtil.isNotNull(poolSetting.getMaxTotal())){
            poolConfig.setMaxTotal(poolSetting.getMaxTotal());
        }
        // 最大空闲连接保持数
        if(ObjectUtil.isNotNull(poolSetting.getMaxIdle())){
            poolConfig.setMaxIdle(poolSetting.getMaxIdle());
        }
        // 最小空闲连接保持数
        if(ObjectUtil.isNotNull(poolSetting.getMinIdle())){
            poolConfig.setMinIdle(poolSetting.getMinIdle());
        }
        // 最大空闲连接获取时长
        poolConfig.setMaxWaitMillis(ObjectUtil.isNotNull(poolSetting.getMaxWaitMillis())?poolSetting.getMaxWaitMillis():30*1000);
        // 下述检查不通过时会调用distroy方法销毁并将poolObject从pool中移除
        // 创建时检查（foctory的validateObject()方法调用时机之一，这里设置为false，因为创建失败会把异常抛出来从而中断向pool中push的过程）
        poolConfig.setTestOnCreate(poolSetting.isTestOnCreate());
        // 空闲对象借出时检查
        poolConfig.setTestOnBorrow(poolSetting.isTestOnBorrow());
        // 对象归还时检查
        poolConfig.setTestOnReturn(poolSetting.isTestOnReturn());
        // 对象状态变更为空闲时检查
        poolConfig.setTestWhileIdle(poolSetting.isTestWhileIdle());
        // 池中空闲连接的最小保持时间
        if(ObjectUtil.isNotNull(poolSetting.getMinEvictableIdleTimeMillis())) {
            poolConfig.setMinEvictableIdleTimeMillis(poolSetting.getMinEvictableIdleTimeMillis());
        }
        // 空闲连接数大于最小空闲连接设置时，池中空闲连接的最小保持时间
        if(ObjectUtil.isNotNull(poolSetting.getSoftMinEvictableIdleTimeMillis())) {
            poolConfig.setSoftMinEvictableIdleTimeMillis(poolSetting.getSoftMinEvictableIdleTimeMillis());
        }
        // 空闲连接的扫描周期
        if(ObjectUtil.isNotNull(poolSetting.getTimeBetweenEvictionRunsMillis())) {
            poolConfig.setTimeBetweenEvictionRunsMillis(poolSetting.getTimeBetweenEvictionRunsMillis());
        }

        /**
         * 连接对象放弃配置
         * 当对象被借出超过设置的时间不归还将把借出的poolObject置为放弃对象，并会在设置的触发时机销毁对象并从pool中清理掉
         */
        AbandonedConfig abandonedConfig = new AbandonedConfig();
        // 借出放弃超时设置
        if(ObjectUtil.isNotNull(poolSetting.getRemoveAbandonedTimeout())) {
            abandonedConfig.setRemoveAbandonedTimeout(poolSetting.getRemoveAbandonedTimeout());
        }
        // 借出时检查放弃状态，是则移除
        abandonedConfig.setRemoveAbandonedOnBorrow(poolSetting.isRemoveAbandonedOnBorrow());
        // 连接空闲检测时检查放弃状态，是则移除
        abandonedConfig.setRemoveAbandonedOnMaintenance(poolSetting.isRemoveAbandonedOnMaintenance());
        // 实例化托管连接池对象
        GenericObjectPool<FTPClient> ftpClientPool = new GenericObjectPool<FTPClient>(ftpClientFactory, poolConfig, abandonedConfig);
        log.info("实例化连接池并交IOC托管成功！");
        return ftpClientPool;
    }
}
