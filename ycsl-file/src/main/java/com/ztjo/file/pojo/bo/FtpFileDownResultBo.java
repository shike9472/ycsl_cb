package com.ztjo.file.pojo.bo;

import lombok.Data;

/**
 * @author 陈彬
 * @version 2021/6/18
 * description：FTP附件下载结果BO
 */
@Data
public class FtpFileDownResultBo {
    private boolean isSuccess;
    private String error;
    private String fjdx;
    private byte[] fjnr;

    public FtpFileDownResultBo success(String fjdx,byte[] fjnr){
        this.isSuccess = true;
        this.fjdx = fjdx;
        this.fjnr = fjnr;
        return this;
    }
    public FtpFileDownResultBo fail(String error){
        this.isSuccess = false;
        this.error = error;
        return this;
    }
}
